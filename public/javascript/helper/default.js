		// <![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('#status').fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow':'visible'});
            })
        //]]>
        
        $(function() {
            $('a.back').click(function(){
                parent.history.back();
                return false;
            });
        });
        
        function doClear(theText) {
            if (theText.value == theText.defaultValue) {
                theText.value = "" ;
            }
        }
        
        function autoSubmit() {
            var formObject = document.forms['theForm'];
            formObject.submit();
        }

        var popUpWin=0;
        function popUpWindow(URLStr, left, top, width, height)
        {
          if(popUpWin)
          {
            if(!popUpWin.closed) popUpWin.close();
          }
          popUpWin = open(URLStr, 'popUpWin', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=yes,width='+800+',height='+600+',left='+left+', top='+top+',screenX='+left+',screenY='+top+'');
        }

        function downloadDoc(filename) {
            location.href = filename ;
        }

(function($){
    $(document).ready(function(){
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault(); 
            event.stopPropagation(); 
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);