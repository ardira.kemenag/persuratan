$(function() {
    $('#datepicker').datepicker({ altField: '#datepicker', altFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
    
    for (i = 0; i < 100; i++) { 
    	$('#datepicker' + i).datepicker({ altField: '#datepicker' + i, altFormat: 'yy-mm-dd', changeMonth: true, changeYear: true });
	}
}) ;