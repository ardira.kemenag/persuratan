$(function() {
	$('a[rel*=facebox]').facebox({
    	loadingImage : js_url + 'facebox/loading.gif',
        closeImage   : js_url + 'facebox/closelabel.png'
    });
});