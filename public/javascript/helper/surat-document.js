$("#button-add-softcopy").click(function() {
    var doc_id   = document.getElementById("doc_id").value ;
    var input = '<div class="span-add-softcopy" style="display:none"><div class="row">' ;
    input   += '<div class="col-sm-10"><input type="file" name="document_name'+ doc_id + '" class="form-control" /></div><div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-sm btn-danger button-delete-softcopy"><i class="glyphicon glyphicon-trash"></i></div>';
    input   += '</div></div>' ;

    document.getElementById("doc_id").value = (doc_id - 1) + 2 ; 

    $("#add-softcopy").append(input);
    $(".span-add-softcopy").slideDown("fast");

    //bind element with event function
    $(".button-delete-softcopy").click(function() {
        $(this).parent().slideUp("fast");
        $(this).parent().parent().empty();
    });
});