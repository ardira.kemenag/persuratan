    function showAddress(type) {
        var level_id  = document.getElementById("addressID").value;

        if (level_id == 4) {
            $('#provinsi_show').show();
            $('.kabupaten_show').hide();
            $('.konsultan').hide();
        }
        else if (level_id == 5 || level_id == 6) {
            $('.kabupaten_show').show();
            $('#provinsi_show').hide();

            if (level_id == 6) {
                $('.konsultan').show();
            }
        }
        else {
            $('#provinsi_show').hide();
            $('.kabupaten_show').hide();
             $('.konsultan').hide();
        }
    }