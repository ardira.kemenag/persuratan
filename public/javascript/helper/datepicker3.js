$(function() {
    $('#datepicker').datepicker({ altField: '#datepicker', altFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
    
    for (i = 0; i < 100; i++) { 
    	$('#datepicker' + i).datepicker({ altField: '#datepicker' + i, altFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
	}
}) ;