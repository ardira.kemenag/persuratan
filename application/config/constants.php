<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* ------------------------------------------------------------------------
 | URL AND PATH CONFIGURATION
 | ------------------------------------------------------------------------
 */
$path   = NULL ;
if(isset($_SERVER['HTTP_HOST'])) {
    $base_url   = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
    $base_url   .= '://'. $_SERVER['HTTP_HOST'];
    
    $path       = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']) ;
    $base_url   .= $path ;
}
else {
    $base_url = 'http://localhost/';
}

define('_BASE_URL_'  , $base_url ) ;
define('_BASE_PATH_' , $_SERVER['DOCUMENT_ROOT'] . $path ) ;

/* ------------------------------------------------------------------------
 * TEMPLATING
 * ------------------------------------------------------------------------
 */

define('_TEMPLATE_LOGIN_'   , 'login/')     ;
define('_TEMPLATE_USER_'    , 'user/')      ;
define('_TEMPLATE_GENERAL_' , 'general/')   ;
define('_TEMPLATE_ADMIN_'   , 'admin/')     ;
define('_TEMPLATE_EXPORT_'  , 'export/')    ;
define('_TEMPLATE_PORTAL_'  , 'portal/')    ;
define('_TEMPLATE_MOBILE_'  , 'mobile/')    ;
define('_TEMPLATE_EMAIL_'   , 'email/')    ;
 
/* ------------------------------------------------------------------------
 * ASSETS FOLDER
 * ------------------------------------------------------------------------
 */

define('_ASSETS_' , 'public/') ;
define('_UPLOAD_' , 'upload/') ;

define('_URL_ASSETS_'   , _BASE_URL_  . _ASSETS_ ) ;
define('_PATH_ASSETS_'  , _BASE_PATH_ . _ASSETS_ ) ;
define('_URL_UPLOAD_'   , _BASE_URL_  . _UPLOAD_ ) ;
define('_PATH_UPLOAD_'  , _BASE_PATH_ . _UPLOAD_ ) ;

define('_BOOTSTRAP_'    , _URL_ASSETS_ . 'bootstrap/' ) ;

/* ------------------------------------------------------------------------
 * STYES CSS & ICON URL
 * ------------------------------------------------------------------------
 */

define('_STYLES_'           , _URL_ASSETS_ . 'styles/') ;
define('_STYLES_GENERAL_'   , _STYLES_ . 'general/') ;
define('_STYLES_ICON_'      , _STYLES_ . 'icon/') ;

define('_STYLES_LOGIN_'     , _STYLES_ . 'login/') ;
define('_STYLES_ADMIN_'     , _STYLES_ . 'admin/') ;
define('_STYLES_PORTAL_'    , _STYLES_ . 'portal/') ;
define('_STYLES_MOBILE_'    , _STYLES_ . 'mobile/') ;

/* -------------------------------------------------------------------------
 | JAVASCRIPT URL
 | -------------------------------------------------------------------------
 */

define('_JAVASCRIPT_'   , _URL_ASSETS_ . 'javascript/') ;


/* -------------------------------------------------------------------------
 | FLASH URL
 | -------------------------------------------------------------------------
 */

define('_FLASH_'   , _URL_ASSETS_ . 'flash/') ;

/* -------------------------------------------------------------------------
 | IMAGE URL
 | -------------------------------------------------------------------------
 */

define('_IMAGES_'   , _URL_ASSETS_ . 'images/') ;

/* ------------------------------------------------------------------------
 * URL AND UPLOAD FILE ACCESS
 * ------------------------------------------------------------------------
 */

    define('_URL_UPLOAD_TEMP_'  , _URL_UPLOAD_  . 'temp/') ;
    define('_URL_UPLOAD_FILES_' , _URL_UPLOAD_  . 'files/') ;

    define('_URL_UPLOAD_VIDEO_' , _URL_UPLOAD_  . 'videos/') ;
    
    define('_URL_UPLOAD_IMG_'               , _URL_UPLOAD_      . 'images/') ;
        define('_URL_UPLOAD_IMG_MEDIUM_'    , _URL_UPLOAD_IMG_  . 'medium/') ;
        define('_URL_UPLOAD_IMG_THUMBS_'    , _URL_UPLOAD_IMG_  . 'thumbs/') ;
        define('_URL_UPLOAD_IMG_HEADLINE_'  , _URL_UPLOAD_IMG_  . 'headline/') ;
        define('_URL_UPLOAD_IMG_MOBILE_'    , _URL_UPLOAD_IMG_  . 'mobile/') ;

    define('_PATH_UPLOAD_TEMP_'  , _PATH_UPLOAD_  . 'temp/') ;
    define('_PATH_UPLOAD_FILES_' , _PATH_UPLOAD_  . 'files/') ;

    define('_PATH_UPLOAD_VIDEO_' , _PATH_UPLOAD_  . 'videos/') ;

    define('_PATH_UPLOAD_IMG_'               , _PATH_UPLOAD_      . 'images/') ;
        define('_PATH_UPLOAD_IMG_MEDIUM_'    , _PATH_UPLOAD_IMG_  . 'medium/') ;
        define('_PATH_UPLOAD_IMG_THUMBS_'    , _PATH_UPLOAD_IMG_  . 'thumbs/') ;
        define('_PATH_UPLOAD_IMG_HEADLINE_'  , _PATH_UPLOAD_IMG_  . 'headline/') ;
        define('_PATH_UPLOAD_IMG_MOBILE_'    , _PATH_UPLOAD_IMG_  . 'mobile/') ;

/*
|--------------------------------------------------------------------------
| WIDE SIZE IMAGE
|--------------------------------------------------------------------------
*/
define('_MEDIUM_WIDTH_' , 262) ;
define('_MEDIUM_HEIGHT_', 172) ;
define('_THUMB_WIDTH_'  , 95) ;
define('_THUMB_HEIGHT_' , 73) ;

define('_PHOTO_MEDIUM_WIDTH_'   , 300) ;
define('_PHOTO_MEDIUM_HEIGHT_'  , 225) ;
define('_PHOTO_THUMB_WIDTH_'    , 100) ;
define('_PHOTO_THUMB_HEIGHT_'   , 75) ;

/* -------------------------------------------------------------------------
 | EMAIL WEB MASTER
 | -------------------------------------------------------------------------
 */
define('_EMAIL_WEBMASTER_'  , 'dolpin@argociptapersada.com') ;
define('_EMAIL_ADMIN_'      , 'dolpin@argociptapersada.com') ;
