<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('time_to_words')) {
    function time_to_words ($w_date,$days = FALSE, $time = FALSE) {
        $date   = '' ;
        if (empty($w_date)) return NULL ;

        $w = explode(' ',$w_date) ;
        if (count($w) > 1) {
            $w_date = $w[0] ;
        }

        $c_date	= explode('-', $w_date);

        if(!empty($c_date[2])) {
            $day	= $c_date[2];
            if ($day[0] == 0) $day = substr($day, 1);
            $date   .= $day . ' ' ;
        }

        if (!empty($c_date[1])) {
            $month	= data_month($c_date[1]);
            $date   .= $month . ' ' ;
        }

        if (!empty($c_date[0])) {
            $year	= $c_date[0];
            $date   .= $year ;
        }

        $time   = $time ? ' ' . substr($w[1],0,5)   : '' ;
        $day    = $days  ? data_day(date("w",strtotime($w_date))).', ' : '' ;

        return $day . $date . $time ;
    }
}

if ( ! function_exists('data_month')) {
    function data_month($m = NULL,$short = FALSE) {
        $CI =& get_instance() ;

        $CI->lang->load('calendar') ;

        $months = array(
                    ''      => ' -- '    ,
                    '01'    => $short ? $CI->lang->line('cal_jan') : $CI->lang->line('cal_january')    ,
                    '02'    => $short ? $CI->lang->line('cal_feb') : $CI->lang->line('cal_february')   ,
                    '03'    => $short ? $CI->lang->line('cal_mar') : $CI->lang->line('cal_march')      ,
                    '04'    => $short ? $CI->lang->line('cal_apr') : $CI->lang->line('cal_april')      ,
                    '05'    => $short ? $CI->lang->line('cal_may') : $CI->lang->line('cal_mayl')       ,
                    '06'    => $short ? $CI->lang->line('cal_jun') : $CI->lang->line('cal_june')       ,
                    '07'    => $short ? $CI->lang->line('cal_jul') : $CI->lang->line('cal_july')       ,
                    '08'    => $short ? $CI->lang->line('cal_aug') : $CI->lang->line('cal_august')     ,
                    '09'    => $short ? $CI->lang->line('cal_sep') : $CI->lang->line('cal_september')  ,
                    '10'    => $short ? $CI->lang->line('cal_oct') : $CI->lang->line('cal_october')    ,
                    '11'    => $short ? $CI->lang->line('cal_nov') : $CI->lang->line('cal_november')   ,
                    '12'    => $short ? $CI->lang->line('cal_dec') : $CI->lang->line('cal_december')
            ) ;

        return $m !== NULL ? $months[$m] : $months ;
    }
}

if ( ! function_exists('data_day')) {
    function data_day($w = NULL , $short = FALSE) {
        $CI =& get_instance() ;

        $CI->lang->load('calendar') ;

        $days   = array(
                    ''     => ' -- '    ,
                    '0'    => $short ? $CI->lang->line('cal_su') : $CI->lang->line('cal_sunday')   ,
                    '1'    => $short ? $CI->lang->line('cal_mo') : $CI->lang->line('cal_monday')   ,
                    '2'    => $short ? $CI->lang->line('cal_tu') : $CI->lang->line('cal_tuesday')  ,
                    '3'    => $short ? $CI->lang->line('cal_we') : $CI->lang->line('cal_wednesday'),
                    '4'    => $short ? $CI->lang->line('cal_th') : $CI->lang->line('cal_thursday') ,
                    '5'    => $short ? $CI->lang->line('cal_fr') : $CI->lang->line('cal_friday')   ,
                    '6'    => $short ? $CI->lang->line('cal_sa') : $CI->lang->line('cal_saturday') ,
                ) ;

        return $w !== NULL ? $days[$w] : $days ;
    }
}

if ( ! function_exists('data_year')) {
    function data_year($start = NULL) {
        $year[''] = ' -- ' ;
        $now    = date('Y') + 1;
        if ($start == NULL) $start = '1950' ;
        foreach (range($start,$now) as $y) {
            $year[$y]   = $y ;
        }

        return $year ;
    }
}

if ( ! function_exists('set_date')) {
    function set_date($date,$separator = '/',$to_separator = '-') {
        
        $dates  = explode($separator,$date) ;
        
        if (count($dates) == 3) {
            return $dates[2].$to_separator .$dates[1].$to_separator .$dates[0] ;
        }
        
        return $date ;
    }
}

if ( ! function_exists('seconds_to_words')) {
    function seconds_to_words ($seconds) {
        /*** return value ***/
        $ret = "";

        /*** get the hours ***/
        $hours = intval(intval($seconds) / 3600);
        if($hours > 0) {
            $ret .= $hours > 9 ? "$hours:" : "0$hours:";
        }
        else {

            $ret    .= "00:" ;
        }

        /*** get the minutes ***/
        $minutes = bcmod((intval($seconds) / 60),60);
        if($hours > 0 || $minutes > 0) {
            $ret .= $minutes > 9 ? "$minutes:" : "0$minutes:" ;
        }
        else {
            $ret .= "00:" ;
        }

        /*** get the seconds ***/
        $seconds = bcmod(intval($seconds),60);
        $ret .= $seconds > 9 ? "$seconds" : "0$seconds" ;

        return $ret;
    }
}

if ( ! function_exists('seconds_duration')) {
    function seconds_duration ($from,$to) {
        $to_time = strtotime($to);
        $from_time = strtotime($from);

        return round(abs($to_time - $from_time) / 60,2)*60 ;
    }
}

/* End of file BI_date_helper.php */
/* Location: ./application/helpers/BI_date_helper.php */