<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('apc_clean')) {
    function apc_clean() {
        $CI =& get_instance() ;
        $CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'dolpin_'));

    	return $CI->cache->clean() ;
    }
}

if (! function_exists('apc_get')) {
    function apc_get($sql) {
        $CI =& get_instance() ;
        $CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'dolpin_'));

        $file_cache = md5($sql) ;
        if ( ! $query = $CI->cache->get($file_cache)) {
            $query  = $CI->db->query($sql) ;
                
            $CR = new CI_DB_result($CI->db);
            $CR->result_object  = $query->result_object();
            $CR->result_array   = $query->result_array();
            $CR->num_rows       = $query->num_rows();

            $CR->conn_id        = NULL;
            $CR->result_id      = NULL;

            // Save into the cache for 90 minutes
            $CI->cache->save($file_cache, serialize($CR) , 90*60) ;
        }
        else {
            $query = unserialize($query) ;
        }

        return $query ;
    }
}

