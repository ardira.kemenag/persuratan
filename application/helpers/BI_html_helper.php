<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('prep_css')) {
	function prep_css($params , $default = '') {
		if (!empty($params)) {
			foreach ($params as $param) {
				$results[]	= '<link href="'.$default.$param.'" rel="stylesheet" />' ;
			}

			return implode("\n",$results) ;
		}

		return NULL ;
	}
}

if ( ! function_exists('prep_js')) {
	function prep_js($params , $default = '') {
		if (!empty($params)) {
			foreach ($params as $param) {
				$results[]	= '<script src="'.$default.$param.'"></script>' ;
			}

			return implode("\n",$results) ;
		}

		return NULL ;
	}
}

/* End of file BI_html_helper.php */
/* Location: ./application/helpers/BI_html_helper.php */