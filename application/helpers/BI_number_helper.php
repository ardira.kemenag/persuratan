<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('number_clean')) {
    function number_clean($num,$replace = NULL) {
        if (!empty($num)) {
            if ($replace == NULL) {
                $replace    = array ( '-'=> '' , '.' => '' , ',' => '','*'=>'' ) ;
            }

            $strtr  = strtr($num, $replace) ;
            
            return is_numeric($strtr) ? round($strtr,2) : $num ;
        }

        return 0 ;
    }
}

if (! function_exists('separator')) {
    function separator($num, $sign = '.',$suffix = '',$empty = '-') {

        if (empty($num))        return $empty ;
        if (!is_numeric($num))  return $num ;

        $num_suffix = '' ;
        $delimiter  = $sign == '.' ? ',' : ($sign == ',' ? '.' : NULL) ;
        if ($delimiter) {
            $num_exp    = explode($delimiter, $num) ;
            if (count($num_exp) > 1) {
                $num_suffix = $delimiter . $num_exp[1] ;
                $num        = $num_exp[0] ;
            }
        }

        $ina_format_number = number_format($num, 3, ',',$sign);
        $result = (substr($ina_format_number, -4) == ',000')
                    ? substr($ina_format_number, 0, -4)
                        : str_replace(',000',$suffix,$ina_format_number) ;

        return number_format($num) ; //$result . $num_suffix ;
    }
}

if (! function_exists('coalesce')) {
    function coalesce($number,$empty = '-') {
        return !empty($number) ? $number : $empty ;
    }
}

if (! function_exists('romanic_number')) {
    function romanic_number($integer, $upcase = true) { 
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
        $return = ''; 
        while($integer > 0) 
        { 
            foreach($table as $rom=>$arb) 
            { 
                if($integer >= $arb) 
                { 
                    $integer -= $arb; 
                    $return .= $rom; 
                    break; 
                } 
            } 
        } 

        return $return; 
    } 
}

/* End of file BI_number_helper.php */
/* Location: ./application/helpers/BI_number_helper.php */