<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Base Path
 *
 * Returns the "base_path" item from your config file
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('base_path') ) {
    function base_path() {
        $CI =& get_instance();
        return $CI->config->slash_item('base_path');
    }
}

if ( ! function_exists('url_category') ) {
    function url_category($data,$url = NULL,$separator_start = '~',$separataor_end = ';',$method = 'GET') {
        if (!empty($data)) {
            $split  = explode($separator_start, $data) ;
            if (count($split) > 0) {
                $result = array() ;
                foreach($split as $string) {
                    list($category_name,$category_id)   = explode($separataor_end, $string) ;

                    $result[]   = $url 
                                        ? ($method == 'GET' 
                                                ? '<a href="'.site_url($url).'?category_id='.$category_id.'">'.$category_name.'</a>'
                                                    : anchor($url .'/'. url_title($category_name),$category_name))
                                        : $category_name ;
                }

                return implode(', ', $result) ;
            }
        }

        return '' ;
    }
}

if ( ! function_exists('url_tags') ) {
    function url_tags($data,$url = NULL,$separator = ', ',$method = 'GET') {
        if (!empty($data)) {
            $tags   = explode($separator, $data) ;
            $result = array() ;
            foreach ($tags as $tag) {
                $result[]   = $url
                                ? ($method == 'GET'
                                        ? '<a href="'.site_url($url).'?tag='.urlencode($tag).'>'.$tag.'</a>'
                                            : anchor($url .'/'. url_title($tag),$tag))
                                : $tag ;
            }

            return implode(', ',$result) ;
        }

        return '' ;
    }
}

if ( !function_exists( 'get_category_id' ) ) {
    function get_category_id($data) {
        if (!empty($data)) {
            $category   = explode('~', $data) ;
            $explode    = explode(';', $category[0]) ;

            if (!empty($explode[1])) return $explode[1] ;
        }

        return '' ;
    }
}

if ( ! function_exists('url_exists')) {
    function url_exists($url) {
        $hdrs = @get_headers($url);
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
    }
}

/* End of file BI_url_helper.php */
/* Location: ./application/helpers/BI_url_helper.php */
