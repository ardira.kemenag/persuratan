<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Delete Files
 *
 * Deletes all files contained in the supplied directory path.
 * Files must be writable or owned by the system in order to be deleted.
 * If the second parameter is set to TRUE, any directories contained
 * within the supplied base directory will be nuked as well.
 *
 * @access	public
 * @param	string	path to file
 * @param	bool	whether to delete any directories found in the path
 * @return	bool
 */
if ( ! function_exists('delete_files'))
{
    function delete_files($path, $del_dir = FALSE, $level = 0, $just_this_extension = NULL, $not_this_extension = 'tpl')
    {
        // Trim the trailing slash
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        if ( ! $current_dir = @opendir($path))
        {
            return FALSE;
        }

        while(FALSE !== ($filename = @readdir($current_dir)))
        {
            if ($filename != "." and $filename != "..")
            {
                if (is_dir($path.DIRECTORY_SEPARATOR.$filename))
                {
                    // Ignore empty folders
                    if (substr($filename, 0, 1) != '.')
                    {
                        delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
                    }
                }
                else
                {
                    if ($just_this_extension) {
                        $extension = strtolower(substr(strrchr($filename, '.'), 1)) ;
                        if ($extension == strtolower($just_this_extension)) {
                            unlink($path.DIRECTORY_SEPARATOR.$filename) ;
                        }
                    }
                    elseif (!in_array($filename,array('index.html','.htaccess'))) {
                        if ($not_this_extension) {
                            $extension = strtolower(substr(strrchr($filename, '.'), 1)) ;
                            if ($extension != strtolower($not_this_extension)) {
                                unlink($path.DIRECTORY_SEPARATOR.$filename) ;
                            }
                        }
                        else {
                            unlink($path.DIRECTORY_SEPARATOR.$filename) ;
                        }
                    }
                }
            }
        }
        @closedir($current_dir);

        if ($del_dir == TRUE AND $level > 0)
        {
                return @rmdir($path);
        }

        return TRUE;
    }
}

if ( ! function_exists('generate_filename')) {
    function generate_filename($config=array()) {
        $defaults = array(  'entitas_id'     => NULL ,
                            'tahun'          => NULL ,
                            'view_data'      => 1    );

        $file   = '' ;
	foreach ($defaults as $key => $val) {
            if ( !empty($config[$key])) {
                $file .= '{'.$key.'=>'.$config[$key].'}' ;
            }
	}

        return md5($file) ;
    }
}

/* End of file BI_file_helper.php */
/* Location: ./application/helpers/BI_file_helper.php */