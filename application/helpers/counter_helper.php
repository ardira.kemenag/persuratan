<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('hits')) {
    function hits($jangka_waktu = 3600) {
        //jangka waktu berlakukan cookie (dalam detik)
        $jangka_waktu = $jangka_waktu;

        //buka file
        $file   = _PATH_UPLOAD_FILES_ . 'counter.txt' ;
        if (!file_exists($file)) {
            $CI = & get_instance() ;
            $CI->load->helper('file') ;
            $data   = '1' ;
            write_file($file,$data) ;
        }
        $f = fopen($file, 'r');

        //ambil jumlah pengunjung
        $baca = fscanf($f, "%d");
        $jumlah_pengunjung = $baca[0];

        //tutup file
        fclose($f);

        //tambahkan jumlah pengunjung jika belum tercatat
        if (!empty($_COOKIE["djpthomepage"])) {
        }
        else {
            $jumlah_pengunjung++;

            //buka file kembali, tulis jumlah pengunjung, dan tutup file
            $f = fopen($file, 'w');
            fwrite($f, $jumlah_pengunjung);
            fclose($f);

            //beri penanda ke dalam cookie
            setcookie("djpthomepage", "pengunjung", time()+$jangka_waktu);
        }
        
        // buka lagi
        $file   = _PATH_UPLOAD_FILES_ . 'counter.txt' ;
        $f = fopen($file, 'r');

        //ambil jumlah pengunjung
        $baca = fscanf($f, "%d");
        $jumlah_pengunjung = !empty($baca[0]) ? 1234 + $baca[0] : 1234 ;
        
        return $jumlah_pengunjung ;
    }
}
/* End of file counter_helper.php */
/* Location: ./application/helpers/counter_helper.php */