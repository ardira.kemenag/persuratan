<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('array_exeptions') ) {
    function array_exeptions($array,$exception) {
        if (is_array($array) && !empty($exception)) {
            $results = array() ;
            foreach ($array as $key => $value) {
                if (is_array($exception)) {
                    if (!in_array($key, $exception)) {
                        $results[$key] = $value ;
                    }
                }
                else {
                    if ($key != $exception) {
                        $results[$key] = $value ;
                    }
                }
            }

            return $results ;
        }

        return $array ;
    }
}

if ( ! function_exists('array_get') ) {
    function array_get($string,$arr = 0) {
        if (!empty($string)) {
            $exp1   = explode('~', $string) ;

            if (count($exp1) > 0) {
                $i = 0 ;
                foreach ($exp1 as $ex) {
                    $exp2   = explode(';', $ex) ;
                    $rs[0][$i]  = $exp2[0] ;
                    $rs[1][$i]  = $exp2[1] ;

                    if (!empty($exp2[2]))   $rs[2][$i]  = $exp2[2] ;
                    if (!empty($exp2[3]))   $rs[3][$i]  = $exp2[3] ;

                    $i++ ;
                }            

                return $rs[$arr] ;
            }                
        }

       return array() ;
    }   
} 

if ( ! function_exists('array_to_list') ) {
    function array_to_list($params,$data,$glue = '<br />') {
        $pieces = array() ;
        $datas  = is_array($data) ? $data : explode('|',$data) ;
        if (!empty($datas) && !empty($data) && !empty($params)) {
            foreach ($datas as $d) {
                if (!empty($params[$d])) {
                    $pieces[]  = $params[$d] ;                    
                }
            }

            return implode($glue, $pieces) ;
        }

        return NULL ;
    }
}  

/* End of file BI_array_helper.php */
/* Location: ./application/helpers/BI_array_helper.php */