<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('_TEMPLATE_HIGHCHARTS_','highcharts/') ;

if ( !function_exists( 'single_chart' ) ) {
    function single_chart($config=array()) {
       $defaults = array(   'from_db'   => FALSE ,
                            'data'      => array()  ,
                            'type'      => 'line' ,
                            'title'     => NULL ,
                            'show_title'=> TRUE ,
                            'subtitle'  => NULL ,
                            'label_x'   => NULL ,
                            'label_y'   => NULL,
                            'xAxisName' => NULL ,
                            'yAxisName' => NULL ,
                            'series_name'=> NULL ,
                            'width'     => '100%',
                            'height'    => '350px' ,
                            'style'     => NULL );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $data   = $from_db  == TRUE ? ($data->num_rows() > 0 ? $data->result_array() : FALSE) : (count($data) > 1 ? $data : FALSE) ;
        
        if ($data) {
            $categories = array() ;
            $series_data= array() ;
            foreach ($data as $r) {
                $categories[]   = "'".$r[$label_x]."'" ;
                $series_data[]  = $r[$label_y] ;
            }

            $charts['type']     = $type ;
            $charts['title']    = $title ;
            $charts['id']       = url_title($title) ;
            $charts['subtitle'] = $subtitle ;
            $charts['xAxisName']= $xAxisName ;
            $charts['yAxisName']= $yAxisName ;
            $charts['series_name']= $series_name ;
            $charts['width']    = $width ;
            $charts['height']   = $height ;
            $charts['style']    = $style ;
            $charts['show_title']= $show_title ;
        
            $charts['categories']   = implode(',', $categories) ;
            $charts['series_data']  = implode(',', $series_data) ;
            
            $CI = &get_instance() ;
            return $CI->load->view( _TEMPLATE_HIGHCHARTS_ . 'single_chart' , $charts , TRUE) ;
        }
        
        return NULL ;
    }
}

if ( ! function_exists('multi_chart')) {
    function multi_chart($config = array()) {
        $defaults = array(  'from_db'   => FALSE ,
                            'data'      => array()  ,
                            'type'      => 'column' ,
                            'id'        => NULL ,
                            'title'     => NULL ,
                            'subtitle'  => NULL ,
                            'show_title'=> TRUE ,
                            'label_x'   => NULL ,
                            'label_y'   => array(),
                            'legend'    => array(),
                            'xAxisName' => NULL ,
                            'yAxisName' => NULL ,
                            'valueSuffix' => NULL ,
                            'width'     => '100%',
                            'height'    => '350px' ,
                            'style'     => NULL  );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $data   = $from_db  == TRUE ? ($data->num_rows() > 0 ? $data->result_array() : FALSE) : (count($data) > 1 ? $data : FALSE) ;
        
        if ($data && count($label_y) == count($legend)) {
            $categories = array() ;
            $series_data= array() ;
            foreach ($data as $r) {
                $categories[]   = "'".$r[$label_x]."'" ;
                
                $i = 0 ;
                foreach ($label_y as $ly) {
                    $series_data[$legend[$i++]][] = $r[$ly] ;
                }
            }

            foreach ($legend as $lg) {
                $sd[]   = "{name : '".$lg."' , data : [".implode(',', $series_data[$lg])."]}" ;
            }
            
            $charts['type']     = $type ;
            $charts['title']    = $title ;
            $charts['id']       = !empty($id) ? $id : url_title($title) ;
            $charts['subtitle'] = $subtitle ;
            $charts['xAxisName']= $xAxisName ;
            $charts['yAxisName']= $yAxisName ;
            $charts['valueSuffix'] = $valueSuffix ;
            $charts['legend']   = $legend ;
            $charts['width']    = $width ;
            $charts['height']   = $height ;
            $charts['style']    = $style ;
            $charts['categories']   = implode(',', $categories) ;
            $charts['series_data']  = implode(',', $sd) ;
            $charts['show_title']   = $show_title ;
            
            $CI = &get_instance() ;
            return $CI->load->view( _TEMPLATE_HIGHCHARTS_ . 'multi_chart' , $charts , TRUE) ;
        }
        
        return NULL ;
    }
}

if ( !function_exists( 'pie_chart' ) ) {
    function pie_chart($config=array()) {
       $defaults = array(   'from_db'   => FALSE ,
                            'data'      => array()  ,
                            'title'     => NULL ,
                            'subtitle'  => NULL ,
                            'show_title'=> TRUE ,
                            'label_x'   => NULL ,
                            'label_y'   => NULL,
                            'series_name'=> NULL ,
                            'width'     => '100%',
                            'height'    => '350px' ,
                            'style'     => NULL );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $data   = $from_db  == TRUE ? ($data->num_rows() > 0 ? $data->result_array() : FALSE) : (count($data) > 1 ? $data : FALSE) ;
        
        if ($data) {
            $series_data= array() ;
            foreach ($data as $r) {
                $series_data[]  = "['".$r[$label_x]."', ".$r[$label_y]."]" ;
            }
            
            $charts['title']    = $title ;
            $charts['id']       = url_title($title) ;
            $charts['subtitle'] = $subtitle ;
            $charts['series_name']= $series_name ;
            $charts['width']    = $width ;
            $charts['height']   = $height ;
            $charts['style']    = $style ;
            $charts['show_title']= $show_title ;
        
            $charts['series_data']  = implode(',', $series_data) ;
            
            $CI = &get_instance() ;
            return $CI->load->view( _TEMPLATE_HIGHCHARTS_ . 'pie_chart' , $charts , TRUE) ;
        }
        
        return NULL ;
    }
}

/* End of file hightcharts_helper.php */
/* Location: ./application/helpers/hightcharts_helper.php */