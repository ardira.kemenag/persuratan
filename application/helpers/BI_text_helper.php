<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * * * Character Limiter
 *
 * Limits the string based on the character count.  Preserves complete words
 * so the character count may not be exactly as specified.
 *
 * @access	public
 * @param	string
 * @param	integer
 * @param	string	the end character. Usually an ellipsis
 * @return	string
 */
if ( ! function_exists('character_limiter')) {
    function character_limiter($str, $n = 500, $end_char = '&#8230;') {
        if (strlen($str) < $n)	{
            return $str;
        }

        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

        if (strlen($str) <= $n)	{
            return $str;
        }

        return substr($str, 0, $n) . $end_char ;
    }
}

if ( ! function_exists('text_clean')) {
    function text_clean($text,$path = FALSE)
    {
        $dir_assets         = str_replace(array(base_url()), "", _ASSETS_) ;
        $dir_asstes_suffix  = substr($dir_assets, -1, 1) ;
        $assets             = $dir_asstes_suffix == "/" ? substr ($dir_assets, 0, -1) : $dir_assets ;

        $trash  = array(    '../../../../../../../../' . $assets ,
                            '../../../../../../../' . $assets ,
                            '../../../../../../' . $assets ,
                            '../../../../../' . $assets ,
                            '../../../../' . $assets ,
                            '../../../' . $assets ,
                            '../../' . $assets ,
                            '../' . $assets ) ;
        return str_replace($trash, ($path ? base_path() : base_url()) . $assets , $text) ;
    }
}

if ( ! function_exists('text_printf')) {
    function text_printf($text,$data = array()) {
        $i = 1 ;
        foreach($data as $d) {
            $text   = str_replace('<'.$i.'>', $d , $text) ;

            $i++ ;
        }

        return $text ;
    }
}

if ( ! function_exists('text_split')) {
    function text_split($text,$tag = '<br />') {
        $strings    = explode(' ', $text) ;

        $results = '' ;
        foreach ($strings as $string) {
            $results .= $string . $tag ;
        }

        return $results ;
    }
}

if ( ! function_exists('nl2br2')) {
    function nl2br2($string) {
        $string = str_replace(array("\r\n", "\r", "\n"), "\n", $string); 
        return preg_replace("/(\r\n)+|(\n|\r)+/", "<br />", $string); 
    }
}

/* End of file BI_text_helper.php */
/* Location: ./application/helpers/BI_text_helper.php */