<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('symbol_clean')) {
    function symbol_clean($subject) {
        $search = array('#',
                        '&',
                        ';',
                        ' ',
                        '!',
                        '"',
                        '$',
                        '%',
                        "'",
                        '(',
                        ')',
                        '*',
                        '+',
                        ',',
                        '-',
                        '.',
                        '/',
                        ':',
                        '<',
                        '=',
                        '>',
                        '?',
                        '[',
                        '\\',
                        ']',
                        '^',
                        '_',
                        '`',
                        '{',
                        '|',
                        '}',
                        '~') ;
        
        $replace    = '' ;
        
        return str_replace($search, $replace, $subject) ;
    }
}    

/* End of file BI_security_helper.php */
/* Location: ./application/helpers/BI_security_helper.php */
