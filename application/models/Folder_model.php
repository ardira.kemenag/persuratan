<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Folder_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Folder_model extends CI_Model {

    public $table           = 'dokumen__folder' ;
    public $table_file      = 'dokumen__folder_file' ;
    
    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'folder_id'         => NULL ,
                            'esatu_id'          => NULL ,
                            'folder_parent'     => NULL ,
                            'parent_not_null'   => FALSE ,
                            'make_array'        => FALSE,
                            'make_id'           => FALSE ,
                            'where_in'          => NULL ,
                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'order'             => NULL ,
                            'parent'            => FALSE ,
                            'file_num'          => FALSE );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "C.*" ;
        $select[$i++]   = "DATE_FORMAT(C.folder_lastupdate,'%d/%m/%Y') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(C.folder_lastupdate,'%H:%i') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(C.folder_lastupdate,'%Y') as Y, DATE_FORMAT(C.folder_lastupdate,'%m') as m, DATE_FORMAT(C.folder_lastupdate,'%d') as d"  ;

        if ($parent)        $select[$i++]   = "P.folder_id AS parent_id,P.folder_name AS parent_name" ;
        if ($file_num)      $select[$i++]   = "(SELECT COUNT(*) FROM ".$this->table_file." F
                                                    WHERE 1
                                                          AND F.folder_id = C.folder_id)
                                               AS file_num" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($folder_id)                 $this->db->where('C.folder_id'    , $folder_id) ;
        if ($folder_parent !== NULL)    $this->db->where('C.folder_parent', $folder_parent) ;
        if ($parent_not_null)           $this->db->where('C.folder_parent <>',0) ;
        if ($where_in)                  $this->db->where_in('C.folder_id',$where_in) ;
        if ($esatu_id)                  $this->db->where('C.esatu_id',$esatu_id) ;

        if ($parent)        $this->db->join($this->table.' P','P.folder_id = C.folder_parent','left') ;

        if ($make_array)    $this->db->order_by('folder_name ASC') ;
        elseif ($order)     $this->db->order_by($order) ;
        else                $this->db->order_by('folder_order ASC, folder_name ASC') ;

        if ($limit)         $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table.' C') ;

        if ($folder_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            return FALSE ;
        }
        elseif ($make_array OR $make_id) {
            $rs = array() ;
            if ($make_array) $rs[''] = '- folder/sub/kegiatan -' ;
            if ($query->num_rows() >0) {
                foreach($query->result() as $r) {
                    if ($make_array) $rs[$r->folder_id] = $r->folder_name ;
                    else {
                        if ($parent_not_null)   $rs[] = $r->folder_parent ;
                        else                    $rs[] = $r->folder_id ;
                    }
                }
            }
            return $rs ;
        }

        return $query ;
    }

    public function show_tree($config = array()) {
        $defaults = array(  'make_array'        => FALSE ,
                            'folder_id'         => NULL ,
                            'esatu_id'          => NULL ,
                            'where_in'          => NULL ,
                            'distinct_id'       => NULL ,
                            'folder_parent'   => NULL ,
                            'space'             => 0 ,
                            'data'              => array() );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $where  = "" ;
        if ($folder_id)               $where .= " AND folder_id = ".$this->db->escape($folder_id)." " ;
        if ($where_in)                $where .= " AND folder_id IN (".implode(',',$where_in).")" ;
        if ($distinct_id)             $where .= " AND folder_id <> ".$this->db->escape($distinct_id)." " ;
        if ($esatu_id)                $where .= " AND esatu_id = ".$this->db->escape($esatu_id)." " ;
        if ($folder_parent !== NULL ) $where .= " AND folder_parent = ".$this->db->escape($folder_parent)." " ;

        $sql    = "SELECT  folder_id, level , folder_name AS folname , RIGHT(level,1) AS lvl ,
                           folder_parent , folder_order , 
                           CONCAT( REPEAT('&#8230; ', RIGHT(level,1)), folder_name) AS folder_name 
                    FROM (
                            SELECT  E.folder_id      , E.folder_name  ,
                                    E.folder_parent  , E.folder_order ,
                               (
                                SELECT CONCAT(LPAD(E1.folder_id, 5, '0'),'~',0)
                                    FROM ".$this->table." E1
                                        WHERE 1
                                              AND E1.folder_id = E.folder_id
                                              AND E1.folder_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.folder_id, 5, '0'), '.',
                                              LPAD(E2.folder_id, 5, '0'), '~',1)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.folder_id = E2.folder_parent)
                                        WHERE 1
                                              AND E2.folder_id = E.folder_id
                                              AND E1.folder_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.folder_id, 5, '0'), '.',
                                              LPAD(E2.folder_id, 5, '0'), '.',
                                              LPAD(E3.folder_id, 5, '0'), '~',2)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.folder_id = E2.folder_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.folder_id = E3.folder_parent)
                                        WHERE 1
                                              AND E3.folder_id = E.folder_id
                                              AND E1.folder_parent = 0
                                              
                                UNION

                                SELECT CONCAT(LPAD(E1.folder_id, 5, '0'), '.',
                                              LPAD(E2.folder_id, 5, '0'), '.',
                                              LPAD(E3.folder_id, 5, '0'), '.',
                                              LPAD(E4.folder_id, 5, '0'), '~',3)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.folder_id = E2.folder_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.folder_id = E3.folder_parent)
                                        INNER JOIN ".$this->table." E4
                                            ON (E3.folder_id = E4.folder_parent)
                                        WHERE 1
                                              AND E4.folder_id = E.folder_id
                                              AND E1.folder_parent = 0
                                              
                                UNION

                                SELECT CONCAT(LPAD(E1.folder_id, 5, '0'), '.',
                                              LPAD(E2.folder_id, 5, '0'), '.',
                                              LPAD(E3.folder_id, 5, '0'), '.',
                                              LPAD(E4.folder_id, 5, '0'), '.',
                                              LPAD(E5.folder_id, 5, '0'), '~',4)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.folder_id = E2.folder_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.folder_id = E3.folder_parent)
                                        INNER JOIN ".$this->table." E4
                                            ON (E3.folder_id = E4.folder_parent)
                                        INNER JOIN ".$this->table." E5
                                            ON (E4.folder_id = E5.folder_parent)
                                        WHERE 1
                                              AND E5.folder_id = E.folder_id
                                              AND E1.folder_parent = 0 

                           ) AS level
                        FROM ".$this->table." E
                            WHERE 1 ".$where."
                            ORDER BY SUBSTRING_INDEX(level,'~',1), folder_order
                    ) T WHERE 1
                              AND level <> '' ;" ;

        $query  = $this->db->query($sql) ;
        
        if ($make_array) {
            if ($make_array)    $data['']   = '-' ;
            foreach ($query->result() as $q) {
                $data[$q->folder_id]  = $q->folder_name ;
            }

            return $data ;            
        }

        return $query ;
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('folder_name'           , $this->lang->line('dokumen_model_cat_val_title')         , 'required|trim|xss_clean');
        $this->form_validation->set_rules('folder_description'    , $this->lang->line('dokumen_model_cat_val_description')   , 'trim|xss_clean');
        $this->form_validation->set_rules('folder_order'          , $this->lang->line('dokumen_model_cat_val_order')         , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('folder_parent'         , $this->lang->line('dokumen_model_cat_val_parent')        , 'trim|xss_clean|is_numeric');

        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }

    // INSERT DAN EDIT folder
    public function save() {
        $folder_id    = $this->input->dokumen('folder_id') ;

        $data['folder_name']          = set_value('folder_name')  ;
        $data['folder_order']         = set_value('folder_order')  ;
        $data['folder_description']   = set_value('folder_description')  ;
        $data['folder_parent']        = set_value('folder_parent')  ;

        if (!empty($data['folder_name'])) {
            if (!empty($folder_id)) {
                $this->db->where('folder_id',$folder_id) ;
                if ($this->db->update($this->table,$data)) {

                    return TRUE ;
                }
            }
            else {
                if ($this->db->insert($this->table,$data)) {
                    return TRUE ;
                }
            }
        }

        return FALSE ;
    }

    // UPDATE GROUP
    public function save_group() {
        $page   = $this->input->post('page',TRUE) ;
        $limit  = $this->input->post('limit',TRUE) ;

        $data   = $this->get(NULL,FALSE ,$page,$limit) ;

        // UPDATE DATA SEBELUMNYA
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $d) {
                $ins['folder_name']           = $folder_name        = $this->input->post('folder_name_edit'.$d->folder_id  , TRUE) ;
                $ins['folder_order']          = $folder_order       = $this->input->post('folder_order_edit'.$d->folder_id  , TRUE) ;
                $ins['folder_description']    = $folder_description = $this->input->post('folder_description_edit'.$d->folder_id  , TRUE) ;
                $ins['folder_parent']         = $folder_parent      = $this->input->post('folder_parent_edit'.$d->folder_id  , TRUE) ;
                $ins['esatu_id']              = $esatu_id           = $this->input->post('esatu_id_edit'.$d->folder_id  , TRUE) ;
                
                if (!empty($folder_name)) {
                    $this->db->where('folder_id',$d->folder_id) ;
                    if (!$this->db->update($this->table,$ins)) { return FALSE ; } ;
                }
            }
        }

        // INSERT DATA BARU
        foreach (range(1,5) as $i) {
            $ins['folder_name']           = $folder_name        = $this->input->post('folder_name_new'.$i,TRUE) ;
            $ins['folder_description']    = $folder_description = $this->input->post('folder_description_new'.$i,TRUE) ;
            $ins['folder_order']          = $folder_order       = $this->input->post('folder_order_new'.$i,TRUE) ;
            $ins['folder_parent']         = $folder_parent      = $this->input->post('folder_parent_new'.$i,TRUE) ;
            $ins['esatu_id']              = $esatu_id           = $this->input->post('esatu_id_new'.$i  , TRUE) ;
            
            if (!empty($folder_name)) {
                if (!$this->db->insert($this->table,$ins)) { return FALSE ; } ;
            }
        }

        return TRUE ;
    }

    // DELETE
    public function delete($folder_id) {
        if (!empty($folder_id)) {
            $this->db->where('folder_parent',$folder_id) ;
            if ($this->db->delete($this->table)) {
                $this->db->where('folder_id',$folder_id) ;
                return $this->db->delete($this->table) ;
            }
        }

        return FALSE ;
    }

    public function num($config=array()) {
         $defaults = array( 'folder_parent'     => NULL  ,
                            'parent_not_null'   => FALSE ,
                            'esatu_id'          => NULL ,
                            'where_in'          => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        if ($folder_parent !== NULL)    $this->db->where('C.folder_parent', $folder_parent) ;
        if ($parent_not_null)           $this->db->where('C.folder_parent <>',0) ;
        if ($where_in)                  $this->db->where_in('C.folder_id',$where_in) ;
        if ($esatu_id)                  $this->db->where('C.esatu_id',$esatu_id) ;
        
        return $this->db->count_all_results($this->table.' C') ;
    }
    
    public function sitemap($parent_id = 0,$ul  = TRUE ,$tahun = NULL , $file_url = 'admin/dokumen/read/') {
        $CI =&get_instance() ;
        $CI->load->model(array('folder_file_model')) ;
        
        $folder = $this->get(array('folder_parent'=>$parent_id,'order'=>'folder_order')) ;
        
        $html   = '' ;
        $id     =  $parent_id == 0 ? ' id="tree"' : '' ;
        if ($folder->num_rows() > 0) {
            if ($ul) $html   .=  '<ul'.$id.'>' ;
            
            foreach ($folder->result() as $f) {
                $html   .= '<li>' ;
                
                $html   .= $f->folder_name ;
                
                $files      = $CI->folder_file_model->get(array('folder_id'=>$f->folder_id,'tahun'=>$tahun)) ;
                if ($files->num_rows() > 0) {
                    $html   .= '<ul>' ;
                    
                    if ($files->num_rows() > 0) {
                        foreach ($files->result() as $f) {
                            $html   .= '<li><a href="'.site_url($file_url.$f->file_id) .'" class="a-'.$f->file_ext.'">'.$f->file_title.' ('.  byte_format($f->file_size).')</a></li>' ;
                        }
                    }
                    
                    $html   .= $this->sitemap($f->folder_id,FALSE,$tahun,$file_url) ;
                    
                    $html   .= '</ul>' ;
                }
                else {
                    $html   .= $this->sitemap($f->folder_id,TRUE,$tahun,$file_url) ;                    
                }
                
                $html   .= '</li>' ;
            }
            
            if ($ul) $html   .= '</ul>' ;
        }
        
        return $html ;
    }
}

/* End of file folder_model.php */
/* Location: ./application/models/folder_model.php */