<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_into_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Surat_into_model extends CI_Model {

    public $table           = 'surat__into' ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'into_id'           => NULL ,
                            'surat_id'          => NULL ,
                            'subdis_id'         => FALSE ,
                            
                            'array'             => FALSE ,
                            'array_id'          => NULL ,
                            'array_name'        => NULL ,

                            'json'              => FALSE ,

                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'order'             => 'A.subdis_id'  );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }
        
        $i = 0 ;
        $select[$i++]   = "A.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($into_id)       $this->db->where('A.into_id'   , $into_id) ;
        if ($surat_id)      $this->db->where('A.surat_id'  , $surat_id) ;
        if ($subdis_id)     $this->db->where('A.subdis_id' , $aksi_id) ;
        
        if ($order)         $this->db->order_by($order) ;
        
        if ($limit)         $this->db->limit($limit,$page) ;

        $sql = $this->db->get_compiled_select($this->table.' A') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($into_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            return FALSE ;
        }
        elseif ($array OR $json) {
            $rs = $array ? array(''=>'- - -') : NULl ;
            if ($query->num_rows() > 0) {
                foreach($query->result() as $r) {
                    if ($array) $rs[$r->$array_id] = $r->$array_name ;
                    else        $rs[]              = $r->$array_id ;
                }
            }
            return $rs ;
        }

        return $query ;
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('into_id'     , 'ID'              , 'is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('surat_id'    , 'Surat'           , 'required|is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('subdis_id'   , 'Eselon Pengirim' , 'required|is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('into_msg'    , 'Pesan Disposisi' , 'trim|xss_clean');
        
        $this->form_validation->set_error_delimiters('<span class="error_label">', '</span>');

        return $this->form_validation->run() ;
    }

    public function save() {
        $into_id    = set_value('into_id') ;

        $data['surat_id']   = set_value('surat_id') ;
        $data['subdis_id']  = set_value('subdis_id') ;
        $data['into_msg']   = set_value('into_msg') ;

        if (!empty($into_id)) {
            $this->db->where('into_id',$into_id) ;
            $save = $this->db->update($this->table,$data) ;
        }
        else {
            if ($this->db->insert($this->table,$data)) {
                $into_id    = $this->db->insert_id() ;
                $save       = TRUE ;
            }
        }

        if (!empty($into_id) && $save) {
            $CI =& get_instance();
            $CI->load->model(array('surat_indisposisi_model')) ;
            $CI->surat_indisposisi_model->save( $data['surat_id']
                                                ,   $this->input->post('surat_disposisi')
                                                ,   $this->input->post('surat_respon')
                                                ,   $into_id ) ;      



            apc_clean() ;

            return TRUE ;
        }

        return FALSE ;
    }

    public function save_id($surat_id) {
        $subdis_id = set_value('fsubdis_id') ;

        if (!empty($surat_id) && !empty($subdis_id)) {

            $into_msg = set_value('into_msg') ;

            $cek =  $this->db->where('surat_id',$surat_id)
                             ->where('subdis_id',$subdis_id)
                             ->select('into_id')
                             ->get($this->table) ;

            if ($cek->num_rows() > 0 ) {
                $into_id = $cek->row()->into_id ;

                $this->db->where('into_id',$into_id) ;
                $this->db->update($this->table,array('into_msg'=>$into_msg)) ;

                apc_clean() ;

                return $into_id ;
            }                             
            else {
                if ($this->db->insert($this->table,array('surat_id'  => $surat_id
                                                            ,'subdis_id'=> $subdis_id
                                                            ,'into_msg' => $into_msg) ))  {

                    apc_clean() ;

                    return $this->db->insert_id() ;
                }
            }
        }

        return FALSE ;
    }

    // DELETE
    public function delete($surat_id,$into_id = NULL) {
        if (!empty($surat_id)) {
            $this->db->where('surat_id',$surat_id) ;

            if ($into_id) $this->db->where('into_id',$into_id) ;

            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function num($config = array()) {
        $defaults = array(  'surat_id'  => NULL ,
                            'subdis_id' => FALSE );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }
        
        if ($surat_id)      $this->db->where('A.surat_id'   , $surat_id) ;
        if ($subdis_id)     $this->db->where('A.subdis_id'  , $subdis_id) ;
        
        return $this->db->count_all_results($this->table.' A') ;
    }
}

/* End of file Surat_into_model.php */
/* Location: ./application/models/Surat_into_model.php */