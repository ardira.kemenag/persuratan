<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Eselon_satu_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Eselon_satu_model extends CI_Model {
    
    public $table   = 'eselon__satu' ;
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('image_model') ;
    }
    
    public function get($config = array()) {
        $defaults = array(  'esatu_id'      => NULL ,
                            'distinct_id'   => NULL ,
                            'having'        => NULL ,
                            'array'         => FALSE ,
                            'array_note'    => '- unit -' ,
                            'json'          => FALSE ,
                            'page'          => 0 ,
                            'limit'         => NULL ,
                            'group'         => NULL ,
                            'order'         => 'esatu_order ASC, P.esatu_name ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "P.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($esatu_id)      $this->db->where('P.esatu_id', $esatu_id) ;
        if ($distinct_id)   $this->db->where('P.esatu_id <>', $distinct_id) ;
        
        if ($having)    $this->db->having($having) ;
        if ($group)     $this->db->group_by($group) ;
        if ($limit)     $this->db->limit($limit,$page) ;
        if ($order)     $this->db->order_by($order) ;

        /*$this->db->from($this->table.' P') ;
        $sql = $this->db->_compile_select() ;
        $this->db->_reset_select();

        echo $sql ; exit (0) ; */

        $query  = $this->db->get($this->table.' P') ;

        if ($esatu_id OR $array OR $json) {
            $result = ($esatu_id OR $json) ? FALSE : array(0=>$array_note) ;
            if ($query->num_rows() > 0) {
                if ($esatu_id && $array == FALSE) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->esatu_id]   = $p->esatu_akronim ;
                        else        $result[]               = $p->esatu_akronim ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }

    public function num($config = array()) {
        $defaults = array(  'distinct_id'   => NULL ,
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($distinct_id)   $this->db->where('P.esatu_id <>', $distinct_id) ;
        
        return $this->db->count_all_results($this->table.' P') ;
    }

    public function delete($esatu_id) {
        if (!empty($esatu_id) && is_numeric($esatu_id)) {
            $this->db->where('esatu_id',$esatu_id) ;
            return $this->db->delete($this->table) ;                    
        }

        return FALSE ;
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('esatu_id'     , 'ID esatu'         , 'trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('esatu_name'   , 'Nama Eselon I'    , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('esatu_akronim', 'Kode Eselon I'    , 'trim|xss_clean') ;
        $this->form_validation->set_rules('esatu_order'  , 'Urutan'             , 'trim|xss_clean|is_numeric') ;
        
        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }

    public function save() {
        $esatu_id    = set_value('esatu_id') ;
        
        $data['esatu_name']      = set_value('esatu_name') ;
        $data['esatu_order']     = set_value('esatu_order') ;
        $data['esatu_akronim']   = set_value('esatu_akronim') ;
        
        if (!empty($esatu_id)) {
            $this->db->where('esatu_id',$esatu_id) ;
            return $this->db->update($this->table,$data) ;
        }
        else {
            return $this->db->insert($this->table,$data) ;
        }

        return FALSE ;
    }

}
/* End of file eselon_satu_model.php */
/* Location: ./application/models/eselon_satu_model.php */