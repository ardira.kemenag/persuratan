<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Comment Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Comment_model extends CI_Model {

    public $table_email   = 'comment__email' ;

    public function  __construct() {
        parent::__construct();
    }

    public function set_email($email,$status = NULL) {
        $this->db->where('email_name',$email) ;

        $num = $this->db->count_all_results($this->table_email) ;

        if ($status) {
            if ($status == 'Visible' && $num == 0) {
                $data['email']  = $email ;
                $this->db->insert($this->table_email,$data) ;
            }

            return $status ;
        }
        else {
            return $num > 0 ? 'Visible' : 'Hidden' ;
        }
    }
}

/* End of file comment_model.php */
/* Location: ./application/models/comment_model.php */

