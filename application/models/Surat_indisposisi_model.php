<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_indisposisi_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Surat_indisposisi_model extends CI_Model {

    public $table           = 'surat__indisposisi' ;
    public $table_subdis    = 'disposisi__sublist' ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'indisposisi_id'    => NULL ,
                            'surat_id'          => NULL ,
                            'subdis_id'         => FALSE ,
                            'subdis'            => FALSE ,
                            'into_id'           => NULL ,
                            'respon_disposisi'  => NULL ,
                            'indisposisi_status'=> NULL ,
                            'read_status'       => NULL ,

                            'array'             => FALSE ,
                            'array_id'          => NULL ,
                            'array_name'        => NULL ,

                            'json'              => FALSE ,

                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'order'             => 'A.subdis_id'  );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }
        
        $i = 0 ;
        $select[$i++]   = "A.*" ;
        
        if ($subdis)    $select[$i++]   = "S.subdis_name,S.subdis_parent,S.subdis_kode,S.subdis_eselon,S.subdis_man" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($subdis)    $this->db->join($this->table_subdis.' S','S.subdis_id = A.subdis_id') ;
        
        if ($indisposisi_id)    $this->db->where('A.indisposisi_id'     , $indisposisi_id) ;
        if ($surat_id)          $this->db->where('A.surat_id'           , $surat_id) ;
        if ($subdis_id)         $this->db->where('A.subdis_id'          , $subdis_id) ;
        if ($respon_disposisi)  $this->db->where('A.respon_disposisi'   , $respon_disposisi) ;
        if ($into_id)           $this->db->where('A.into_id'    , $into_id) ;
        if ($read_status)       $this->db->where('A.read_status', $read_status) ;
        if ($indisposisi_status)$this->db->where('A.indisposisi_status' , $indisposisi_status) ;
        
        if ($order)         $this->db->order_by($order) ;
        
        if ($limit)         $this->db->limit($limit,$page) ;

        $sql = $this->db->get_compiled_select($this->table.' A') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($indisposisi_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            return FALSE ;
        }
        elseif ($array OR $json) {
            $rs = $array ? array(''=>'- - -') : NULl ;
            if ($query->num_rows() > 0) {
                foreach($query->result() as $r) {
                    if ($array) $rs[$r->$array_id] = $r->$array_name ;
                    else        $rs[]              = $r->$array_id ;
                }
            }
            return $rs ;
        }

        return $query ;
    }

    public function save($surat_id,$disposisi = array(),$respon = array(), $into_id = NULL, $to = array() , $tembusan = array()) {
        $surat_id   = !empty($surat_id)  ? $surat_id    : $this->input->post('surat_id'         , TRUE) ;
        $into_id    = !empty($into_id)   ? $into_id     : $this->input->post('into_id'          , TRUE) ;
        $disposisi  = !empty($disposisi) ? $disposisi   : $this->input->post('surat_disposisi'  , TRUE) ;
        $respon     = !empty($respon)    ? $respon      : $this->input->post('surat_respon'     , TRUE) ;

        if (!empty($surat_id) && !empty($into_id)) {
            // ERASE CURRENTto
            $this->db->where('surat_id' , $surat_id)
                     ->where('into_id'  , $into_id)
                     ->delete($this->table) ;
            /**=============== **/

            if (!empty($disposisi) && count($disposisi) > 0) {
                foreach ($disposisi as $subdis_id) {
                    if (!empty($subdis_id)) {
                        $data = array() ;
                        $data['surat_id']   = $surat_id ;
                        $data['into_id']    = $into_id ;
                        $data['subdis_id']  = $subdis_id ;
                        $data['respon_disposisi']   = is_array($respon) && in_array($subdis_id,$respon) ? 2 : 1 ;
                        $data['indisposisi_status'] = 1 ;

                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                        else {
                            $sender_id[]    = $subdis_id ;
                        }
                    }
                }                
            }

            if (!empty($to)) {
                foreach ($to as $subdis_id) {
                    if (!empty($subdis_id)) {
                        $data['surat_id']   = $surat_id ;
                        $data['into_id']    = $into_id ;
                        $data['subdis_id']  = $subdis_id ;
                        $data['respon_disposisi']   = 1 ;
                        $data['indisposisi_status'] = 2 ;

                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                        else {
                            $sender_id[]    = $subdis_id ;
                        }
                    }
                }
            }

            if (!empty($tembusan)) {
                foreach ($tembusan as $subdis_id) {
                    if (!empty($subdis_id)) {
                        $data['surat_id']   = $surat_id ;
                        $data['into_id']    = $into_id ;
                        $data['subdis_id']  = $subdis_id ;
                        $data['respon_disposisi']   = 1 ;
                        $data['indisposisi_status'] = 3 ;

                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                        else {
                            $sender_id[]    = $subdis_id ;
                        }
                    }
                }
            }            

            $CI =& get_instance();
            $CI->load->model(array('surat_in_model','email_log_model')) ;
            $surat = $this->surat_in_model->get(array('surat_id'=>$surat_id
                                                    ,'jenis'=>TRUE
                                                    ,'select_self'=>'surat_id,jenis_name,surat_respon,surat_disposisi')) ;
            if (!empty($surat['surat_respon']) OR !empty($surat['surat_disposisi'])) {
                $surat_respon   = !empty($surat['surat_respon'])    ? json_decode($surat['surat_respon'],TRUE) : array() ;
                $surat_disposisi= !empty($surat['surat_disposisi']) ? json_decode($surat['surat_disposisi'],TRUE) : array() ;

                $this->db->where('surat_id',$surat_id)
                         ->update($CI->surat_in_model->table,array( 'surat_respon'      => json_encode(array_unique(array_merge($surat_respon,$respon))) 
                                                                ,   'surat_disposisi'   => json_encode(array_unique(array_merge($surat_disposisi,$disposisi))) )) ;
            
                if (!empty($sender_id)) {
                    $CI->email_log_model->save(array(
                                                        'message' => array('surat_id'=>$surat['surat_id'],'jenis_name'=>$surat['jenis_name'])
                                                    ,   'template'=> 'surat_in'
                                                    ,   'subject' => 'Anda mendapat Disposisi Surat.'
                                                    ),
                                                array('subdis_id'=>$sender_id) ) ;

                    $CI->email_log_model->send() ;


                }
            }

            apc_clean() ;

            return TRUE ;
        }

        return FALSE ;
    }

    // DELETE
    public function delete($surat_id,$indisposisi_id = NULL) {
        if (!empty($surat_id)) {
            $this->db->where('surat_id',$surat_id) ;

            if ($indisposisi_id) $this->db->where('indisposisi_id',$indisposisi_id) ;

            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function num($config = array()) {
        $defaults = array(  'surat_id'          => NULL ,
                            'subdis_id'         => FALSE ,
                            'into_id'           => NULL ,
                            'respon_disposisi'  => NULL ,
                            'indisposisi_status'=> NULL ,
                            'read_status'       => NULL );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }
        
        if ($surat_id)          $this->db->where('A.surat_id'           , $surat_id) ;
        if ($subdis_id)         $this->db->where('A.subdis_id'          , $subdis_id) ;
        if ($respon_disposisi)  $this->db->where('A.respon_disposisi'   , $respon_disposisi) ;
        if ($into_id)           $this->db->where('A.into_id'            , $into_id) ;
        if ($read_status)       $this->db->where('A.read_status'        , $read_status) ;
        if ($indisposisi_status)$this->db->where('A.indisposisi_status' , $indisposisi_status) ;
        
        return $this->db->count_all_results($this->table.' A') ;
    }

    public function set_read($surat_id,$subdis_id) {
        if (!empty($surat_id) && !empty($subdis_id) ) {
            
            apc_clean() ;

            return $this->db->where('surat_id'  , $surat_id)
                            ->where('subdis_id' , $subdis_id)
                            ->where('read_status', 1)
                            ->update($this->table,array('read_status'   => 2
                                                    ,   'read_date'     => date('Y-m-d H:i:s') )) ;                
        }

        return FALSE ;
    }
}

/* End of file Surat_indisposisi_model.php */
/* Location: ./application/models/Surat_indisposisi_model.php */