<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of image_model
 *
 * @author Freddy Yuswanto
 */
class Image_model extends CI_Model {

    public function  __construct() {
        parent::__construct() ;
    }

    public function upload($filename,$destination, $encrypt = FALSE ) {
        $config['upload_path']      = $destination ;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = '15000';
        $config['max_width']        = '10000';
        $config['max_height']       = '10000';
        $config['encrypt_name']     = $encrypt ;

        $this->load->library('upload', $config);

        if ( $this->upload->do_upload($filename)) {
            $data   = $this->upload->data();

            chmod($destination . $data['file_name'], 0777) ;

            return $data ;
        }

        return FALSE ;
    }

    public function delete($dir, $filename) {
        //chmod($dir . $filename, 0777) ;
        if (!empty($dir) && !empty($filename)) {
            if (file_exists($dir . $filename)) {
                return unlink($dir . $filename) ;
            }
        }
        
        return TRUE ;
    }

    public function resize($limit_width, $limit_height, $image,$source,$destination) {
        $this->load->library('image_moo') ;
        
        $limit_width    = $image['image_width']  > $limit_width  ? $limit_width  : $image['image_width']  ;
        $limit_height   = $image['image_height'] > $limit_height ? $limit_height : $image['image_height'] ;

        $this->image_moo
            ->load($source)
            ->resize($limit_width,$limit_height)
            ->save($destination . $image['file_name'])  ;

        chmod($destination . $image['file_name'], 0777) ;
    }

    public function resize_crop($limit_width, $limit_height, $image,$source,$destination) {
        $this->load->library('image_moo') ;

        $limit_width    = $image['image_width']  > $limit_width  ? $limit_width  : $image['image_width']  ;
        $limit_height   = $image['image_height'] > $limit_height ? $limit_height : $image['image_height'] ;

        $this->image_moo
            ->load($source)
            ->resize_crop($limit_width,$limit_height)
            ->save($destination . $image['file_name'])  ;

        chmod($destination . $image['file_name'], 0777) ;
    }
}

/* End of file image_model.php */
/* Location: ./application/models/image_model.php */