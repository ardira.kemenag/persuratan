<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Config_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Config_model extends CI_Model {

    public $table   = 'set__config' ;

    public function  __construct() {
        parent::__construct();
    }

    public function get($config=array()) {
        $defaults = array(  'config_id'      => NULL ,
                            'page'           => 0    ,
                            'limit'          => NULL ,
                            'config_name'    => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($config_id)     $this->db->where('config_id'    , $config_id) ;
        if ($config_name)   $this->db->where('config_name'  , $config_name) ;

        if ($limit) $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table) ;

        if ($config_id OR $config_name) {
            if ($query->num_rows() > 0)   return $query->row_array() ;
            else                        return FALSE ;
        }

        return $query ;
    }


    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('config_id'   , $this->input->post('config_id_lang')    , 'is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('config_name' , $this->input->post('config_name_lang')  , 'trim|xss_clean');
        $this->form_validation->set_rules('config_style', $this->input->post('config_style_lang') , 'required');

        return $this->form_validation->run() ;
    }

    public function save() {
        $config_id      = set_value('config_id') ;
        $config_name    = set_value('config_name') ;
        $data['config_style']   = trim($this->input->post('config_style',TRUE)) ;

        if ($config_id OR $config_name) {
            if ($config_id) $this->db->where('config_id'    , $config_id) ;
            else            $this->db->where('config_name'  , $config_name) ;

            return $this->db->update($this->table,$data) ;
        }

        return FALSE ;
    }
}

/* End of file language_model.php */
/* Location: ./application/models/language_model.php */