<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Email_log_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Email_log_model extends CI_Model {
    
    public $table   = 'email__log' ;

    public function __construct() {
        parent::__construct();
    }
    
    public function get($config=array()) {
        $defaults = array(  'id'        => NULL ,
                            'email'     => NULL ,
                            'is_sent'   => NULL ,
                            'page'      => 0    ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'order'     => 'E.created ASC'
                         );

    	foreach ($defaults as $key => $val) {
                $$key = ( ! isset($config[$key])) ? $val : $config[$key];
    	}
        
        $i = 0 ;
        $select[$i++]   = "E.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($id)        $this->db->where('E.id',$id) ;
        if ($email)     $this->db->where('E.email',$email) ;
        if ($is_sent)   $this->db->where('E.is_sent',$is_sent) ;

        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;
        
        $query  = $this->db->get($this->table.' E') ;

        if ($id) {
            $result = FALSE ;
            if ($query->num_rows() > 0) {
                return $query->row_array() ;
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'email'     => NULL ,
                            'is_sent'   => NULL ,
                            'group'     => NULL ,
                        );

    	foreach ($defaults as $key => $val) {
                $$key = ( ! isset($config[$key])) ? $val : $config[$key];
    	}

        if ($email)     $this->db->where('E.email'  , $email) ;
        if ($is_sent)   $this->db->where('E.is_sent', $is_sent) ;
        
        if ($group) {
            $this->db->select('E.id') ;
            
            $this->db->group_by($group) ;
            
            $query  = $this->db->get($this->table.' E') ;
            
            return $query->num_rows() ;
        }
        
        return $this->db->count_all_results($this->table.' E') ;
    }
    
    public function save($params, $sender_id = NULL) {
        if (!empty($params['message']) && !empty($params['template']) && !empty($params['subject'])) {
            $params['message']  = is_array($params['message']) ? json_encode($params['message']) : $params['message'] ;
            if (!empty($params['email'])) {
                $to_email[] = $params['email'] ;
            }
            elseif (!empty($sender_id)) {
                $where_in = array() ;
                if (!empty($sender_id['user_id']))      $where_in[] = "user_id IN (".implode(',',$sender_id['user_id']).")" ;
                if (!empty($sender_id['subdis_id']))    $where_in[] = "subdis_id IN (".implode(',',$sender_id['subdis_id']).")" ;

                $users =    $this   ->db->select('email')
                                    ->where("(".implode(' OR ',$where_in).")",NULL,FALSE)
                                    ->get('users__list') ;

                if ($users->num_rows() > 0) {
                    foreach ($users->result() as $u) {
                        $to_email[] = $u->email ;
                    }
                }                                    

                if (!empty($to_email) && count($to_email) > 0) {
                    foreach ($to_email as $email) {
                        $params['email']    = $email ;

                        if ( ! $this->db->insert( $this->table , $params ) ) return FALSE ;
                    }
                }
            }
        }
        
        return FALSE ;
        
    }
    
    public function delete($id) {
        if (!empty($id) && is_numeric($id)) {
            $this->db->where('id',$id) ;
            return $this->db->delete($this->table) ;
        }
        
        return FALSE ;
    }

    public function send() {
        $emails     = $this->get(array('is_sent'=>1)) ;
        if ($emails->num_rows() > 0) {
            $this->load->library('email');

            foreach ($emails->result() as $e) {

                $this->email->from( _EMAIL_ADMIN_ , 'Persuratan DJPSDKP -- no reply');
                $this->email->to($e->email);

                $html   = $this->load->view( _TEMPLATE_EMAIL_ . $e->template , json_decode($e->message,TRUE) , TRUE) ;

                $this->email->subject($e->subject);
                $this->email->message($html) ;

                if ($this->email->send()) {
                    $this->db->where('id',$e->id) ;
                    $this->db->update($this->table,array('is_sent'=>2)) ;
                }
            }

            return TRUE ;
        }

        return FALSE ;
    }
}
/* End of file Entitas_model.php */
/* Location: ./application/models/Entitas_model.php */