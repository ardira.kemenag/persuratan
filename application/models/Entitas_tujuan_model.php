<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_tujuan_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_tujuan_model extends CI_Model {
    
    public $table       = 'entitas__tujuan' ;
    public $table_surat = 'surat__out' ;

    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'tujuan_id'   => NULL ,
                            'tujuan_kode' => NULL ,

                            'tujuan_parent' => NULL ,
                            'parent'        => NULL ,
                            'surat_num' => FALSE ,
                            'array'     => FALSE ,
                            'array_id'  => 'tujuan_id' ,
                            'array_name'=> 'tujuan_name' ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'order'     => 'tujuan_order ASC'
                         );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        if ($parent)    $select[$i++]   = "P.tujuan_name AS parent_name, P.tujuan_kode AS parent_kode" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($parent)    $this->db->join($this->table.' P','P.tujuan_id = T.tujuan_parent') ;

        if ($tujuan_id)     $this->db->where('T.tujuan_id'      , $tujuan_id) ;
        if ($tujuan_kode)   $this->db->where('T.tujuan_kode'    , $tujuan_kode) ;
        if ($tujuan_parent) $this->db->where('T.tujuan_parent'  , $tujuan_parent) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($tujuan_id OR $array OR $json) {
            $result = ($tujuan_id OR $json) ? FALSE : array(''=>'- tujuan -') ;
            if ($query->num_rows() > 0) {
                if ($tujuan_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->$array_id]  = $p->$array_name ;
                        else        $result[]               = $p->$array_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'tujuan_kode'  => NULL ,
                            'tujuan_parent'=> NULL ,
                            'group'     => NULL ,
                            'order'     => 'tujuan_order ASC'
                         );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }

        if ($tujuan_kode)      $this->db->where('T.tujuan_kode'   , $tujuan_kode) ;
        if ($tujuan_parent)    $this->db->where('T.tujuan_parent' , $tujuan_parent) ;

        if ($group) {
            $this->db->select('T.tujuan_id') ;
            $this->db->group_by($group) ;

            $query = $this->db->get($this->table.' T') ;

            return $query->num_rows() ;
        }

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($tujuan_id) {
        if (!empty($tujuan_id) && is_numeric($tujuan_id)) {
            $this->db->where('tujuan_id',$tujuan_id) ;
            return $this->db->delete($this->table)  ;
        }

        return FALSE ;
    }
    
    public function save() {
        $tujuan_id     = $this->input->post('tujuan_id') ;
        $tujuan_name   = $this->input->post('tujuan_name') ;
        $tujuan_kode   = $this->input->post('tujuan_kode') ;
        $tujuan_parent = $this->input->post('tujuan_parent') ;
        $tujuan_order  = $this->input->post('tujuan_order') ;

        if (count($tujuan_name) > 0) {
            $i = 0 ;
            foreach ($tujuan_name as $tm) {
                if (!empty($tm)) {
                    $data['tujuan_name']   = $tm ;
                    $data['tujuan_kode']   = !empty($tujuan_kode[$i])   ? $tujuan_kode[$i]  : '' ;
                    $data['tujuan_parent'] = !empty($tujuan_parent[$i]) ? $tujuan_parent[$i] : '' ;
                    $data['tujuan_order']  = !empty($tujuan_order[$i])  ? $tujuan_order[$i] : '' ;
                    
                    if (!empty($tujuan_id[$i])) {
                        $this->db->where('tujuan_id',$tujuan_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;
            return TRUE ;
        }

        return FALSE ;
    }

    public function show_tree($config = array()) {
        $defaults = array(  'make_array'    => FALSE ,
                            'array_single'  => FALSE ,
                            'array_id'      => 'tujuan_id' ,
                            'array_name'    => 'tujuan_name' ,
                            'tujuan_id'     => NULL ,
                            'where_in'      => NULL ,
                            'distinct_id'   => NULL ,
                            'tujuan_parent' => NULL ,
                            'n1'    => TRUE ,
                            'n2'    => TRUE ,
                            'n3'    => TRUE ,
                            'space'         => 0 ,
                            'data'          => array() );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }

        $where  = "" ;
        if ($tujuan_id)                 $where .= " AND tujuan_id = ".$this->db->escape($tujuan_id)." " ;
        if ($where_in)                  $where .= " AND tujuan_id IN (".implode(',',$where_in).")" ;
        if ($distinct_id)               $where .= " AND tujuan_id <> ".$this->db->escape($distinct_id)." " ;
        if ($tujuan_parent !== NULL )   $where .= " AND tujuan_parent = ".$this->db->escape($tujuan_parent)." " ;

        $sql    = "SELECT  tujuan_id, level ,
                           CONCAT( REPEAT('.. ', RIGHT(level,1)), tujuan_name) AS tujuan_name , tujuan_name AS sdname
                    FROM (
                            SELECT  E.tujuan_id      , E.tujuan_name  ,
                                    E.tujuan_parent  , E.tujuan_order ,
                               (
                                SELECT CONCAT(LPAD(E1.tujuan_order, 5, '0'),'~',0)
                                    FROM ".$this->table." E1
                                        WHERE 1
                                              AND E1.tujuan_id = E.tujuan_id
                                              AND E1.tujuan_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.tujuan_order, 5, '0'), '.',
                                              LPAD(E2.tujuan_order, 5, '0'), '~',1)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.tujuan_id = E2.tujuan_parent)
                                        WHERE 1
                                              AND E2.tujuan_id = E.tujuan_id
                                              AND E1.tujuan_parent = 0 " ;

        if ($n1)
        $sql    .=  "           UNION

                                SELECT CONCAT(LPAD(E1.tujuan_order, 5, '0'), '.',
                                              LPAD(E2.tujuan_order, 5, '0'), '.',
                                              LPAD(E3.tujuan_order, 5, '0'), '~',2)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.tujuan_id = E2.tujuan_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.tujuan_id = E3.tujuan_parent)
                                        WHERE 1
                                              AND E3.tujuan_id = E.tujuan_id
                                              AND E1.tujuan_parent = 0 " ;

        if ($n2)
        $sql    .=  "           UNION              

                                SELECT CONCAT(LPAD(E1.tujuan_order, 5, '0'), '.',
                                              LPAD(E2.tujuan_order, 5, '0'), '.',
                                              LPAD(E3.tujuan_order, 5, '0'), '~',
                                              LPAD(E4.tujuan_order, 5, '0'), '~',3)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.tujuan_id = E2.tujuan_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.tujuan_id = E3.tujuan_parent)
                                        INNER JOIN ".$this->table." E4
                                                ON (E3.tujuan_id = E4.tujuan_parent)
                                        WHERE 1
                                              AND E4.tujuan_id = E.tujuan_id
                                              AND E1.tujuan_parent = 0   " ;  

        
        if ($n3)
        $sql    .=  "           UNION              
                                              
                                SELECT CONCAT(LPAD(E1.tujuan_order, 5, '0'), '.',
                                              LPAD(E2.tujuan_order, 5, '0'), '.',
                                              LPAD(E3.tujuan_order, 5, '0'), '~',
                                              LPAD(E4.tujuan_order, 5, '0'), '~',
                                              LPAD(E5.tujuan_order, 5, '0'), '~',4)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.tujuan_id = E2.tujuan_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.tujuan_id = E3.tujuan_parent)
                                        INNER JOIN ".$this->table." E4
                                                ON (E3.tujuan_id = E4.tujuan_parent)
                                        INNER JOIN ".$this->table." E5
                                                ON (E4.tujuan_id = E5.tujuan_parent)        
                                        WHERE 1
                                              AND E5.tujuan_id = E.tujuan_id
                                              AND E1.tujuan_parent = 0 " ;                            

        $sql    .= " ) AS level
                        FROM ".$this->table." E
                            WHERE 1 ".$where."
                            ORDER BY SUBSTRING_INDEX(level,'~',1), tujuan_order
                    ) T WHERE 1
                              AND level <> '' ;" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;                      

        if ($array_single)    $data['']   = '' ;

        foreach ($query->result() as $q) {
            $data[$q->$array_id]  = $q->$array_name ;
        }

        return $data ;
    }
}
/* End of file Entitas_tujuan_model.php */
/* Location: ./application/models/Entitas_tujuan_model.php */