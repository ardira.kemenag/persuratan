<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Statistics_Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Statistics_model extends CI_Model {

    public $file_statistics  ;

    public function  __construct() {
        parent::__construct();

        $this->file_statistics  = APPPATH . 'cache/' . md5('statistik-'.date('Y-m-d')).'.tpl' ;
    }

    public function get() {

        if (file_exists($this->file_statistics)) {
            $get_file       = file_get_contents($this->file_statistics) ;
            $decode_file    = $this->encrypt->decode($get_file) ;
            return json_decode($decode_file,true) ;
        }
        else {
            $config['email']    = _GA_EMAIL_ ;
            $config['password'] = _GA_PASSWORD_ ;

            $this->load->library('gapi',$config) ;

            $start_date = '2011-01-01' ;
            $to_date    = date('Y-m-d') ;
        
            $this->gapi->requestReportData(_GA_PROFILE_ID_,array('date'),array('pageviews','visits','visitors','newVisits','exits'),array('date') ,null,$start_date,$to_date,1,3);
            $data['totalVisits']        = separator($this->gapi->getVisits()) ;
            $data['totalPageViews']     = separator($this->gapi->getPageviews()) ;
            $data['totalVisitors']      = separator($this->gapi->getVisitors()) ;
            $data['totalNewVisits']     = str_replace('.', ',', round(100 * ($this->gapi->getNewVisits()/$this->gapi->getVisits()),2)).'%' ;
            $data['totalPageVisits']    = str_replace('.', ',', round($this->gapi->getPageviews()/$this->gapi->getVisits(),2)) ;

            $this->gapi->requestReportData(_GA_PROFILE_ID_,array('country'),array('visits'),'-visits',null,$start_date,$to_date,1,3);
            $data['totalCountry']= $this->gapi->getTotalResults() ;
            
            $this->gapi->requestReportData(_GA_PROFILE_ID_,array('city'),array('visits'),'-visits',null,$start_date,$to_date,1,3);
            $data['totalCity']   = $this->gapi->getTotalResults() ;

            $json_data      = json_encode($data) ;
            $encode_data    = $this->encrypt->encode($json_data) ;

            $this->load->helper('file') ;
            write_file($this->file_statistics, $encode_data) ;

            return $data ;
        }
    }

    public function images($count,$min_digits = 8) {
        $count  = str_replace(array(',','.'), '', $count) ;
        if ($min_digits > 0) {
            $count = sprintf('%0'.$min_digits.'s',$count);
        }
        $len = strlen($count);

        $str    = '' ;
        for ($i=0;$i<$len;$i++) {
            $str .= '<img src="'. _IMAGES_ . 'statistik/' . substr($count,$i,1) . '.gif" border="0">' ;
        }

        return $str ;
    }
}
/* End of file statistics_model.php */
/* Location: ./application/models/statistics_model.php */

