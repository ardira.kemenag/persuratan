<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_users_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Surat_users_model extends CI_Model {
    
    public $table       = 'hub__surat_to_user' ;
    public $table_users = 'users__list' ;
    public $table_level = 'users__level' ;
    public $table_surat = 'surat__list' ;
    
    public $table_sifat = 'surat__sifat' ;
    public $table_jenis = 'surat__jenis' ;
    public $table_format= 'surat__format' ;
    
    public $table_disposisi     = 'surat__disposisi_sublist' ;
    public $table_disposisi_hub = 'hub__surat_disposisi' ;
    
    public $table_aksi          = 'surat__aksi' ;
    public $table_aksi_hub      = 'hub__surat_aksi' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'surat_id'          => NULL ,
                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'distinct_id'       => NULL ,
                            'have_read'         => FALSE ,
                            'have_unread'       => FALSE ,
                            'surat_date_month'  => NULL ,
                            'start_date'        => NULL ,
                            'to_date'           => NULL ,
                            'keyword'           => NULL ,
                            'sifat'             => FALSE ,
                            'sifat_id'          => NULL ,
                            'format'            => FALSE ,
                            'format_id'         => NULL ,
                            'have_view'         => NULL ,
                            'have_hidden'       => NULL ,
                            'jenis'             => FALSE ,
                            'jenis_id'          => NULL ,
                            'disposisi'         => FALSE ,
                            'aksi'              => FALSE ,
                            'order'             => 'S.surat_date_taken DESC' ,
                            'user'              => NULL ,
                            'user_id'           => NULL ,
                            'from_user'         => FALSE ,
                            'from_user_id'      => NULL , 
                            'reply'             => FALSE ,
                            'reply_id'          => NULL ,
                            'group'             => 'S.surat_id');
 
	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "S.*" ;
        $select[$i++]   = "DATE_FORMAT(S.surat_date,'%Y/%m/%d') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(S.surat_date,'%H:%i') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(S.surat_date,'%Y') as Y, DATE_FORMAT(S.surat_date,'%m') as m, DATE_FORMAT(S.surat_date,'%d') as d,DATE_FORMAT(S.surat_date,'%H') as H, DATE_FORMAT(S.surat_date,'%i') as i, DATE_FORMAT(S.surat_date,'%s') as s"  ;
    
        $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%Y/%m/%d') AS taken_date_diff" ;
        $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%H:%i') AS taken_time_diff" ;
        $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%Y') as taken_Y, DATE_FORMAT(S.surat_date_taken,'%m') as taken_m, DATE_FORMAT(S.surat_date_taken,'%d') as taken_d,DATE_FORMAT(S.surat_date_taken,'%H') as taken_H, DATE_FORMAT(S.surat_date_taken,'%i') as taken_i, DATE_FORMAT(S.surat_date_taken,'%s') as taken_s"  ;
    
        if ($sifat)     $select[$i++]   = "Sf.sifat_name" ;
        if ($jenis)     $select[$i++]   = "Jn.jenis_name" ;
        if ($format)    $select[$i++]   = "Fm.format_name" ;
        if ($reply)     $select[$i++]   = "Ry.surat_number AS reply_surat_number,Ry.surat_from AS reply_surat_from,Ry.surat_perihal AS reply_surat_perihal,Ry.surat_date AS reply_surat_date,Ry.surat_date_taken AS reply_surat_date_taken" ;
        
        if ($user_id)   $select[$i++]   = "H.surat_read,H.surat_read_date" ;
        if ($from_user) $select[$i++]   = "U.username,U.fullname,U.position,U.user_img" ;
        
        if ($keyword)   $select[$i++]   = "MATCH (surat_number,surat_from,surat_perihal) AGAINST (".$this->db->escape($keyword).") AS score" ;
        
        if ($disposisi) $select[$i++]   = "(SELECT GROUP_CONCAT(DISTINCT CONCAT(D.subdis_name, ';', D.subdis_id) ORDER BY D.subdis_id DESC SEPARATOR '~') 
                                            FROM ".$this->table_disposisi." D
                                                INNER JOIN ".$this->table_disposisi_hub." DH
                                                    ON DH.subdis_id = D.subdis_id
                                                WHERE DH.surat_id = S.surat_id) AS disposisi" ;
        
        if ($aksi)      $select[$i++]   = "(SELECT GROUP_CONCAT(DISTINCT CONCAT(A.aksi_name, ';', A.aksi_id) ORDER BY A.aksi_name DESC SEPARATOR '~') 
                                            FROM ".$this->table_aksi." A
                                                INNER JOIN ".$this->table_aksi_hub." AH
                                                    ON AH.subdis_id = A.subdis_id
                                                WHERE AH.surat_id = S.surat_id) AS aksi" ;
                
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($sifat)  $this->db->join($this->table_sifat.' Sf','Sf.sifat_id = S.sifat_id') ;
        if ($jenis)  $this->db->join($this->table_jenis.' Jn','Jn.jenis_id = S.jenis_id') ;
        if ($format) $this->db->join($this->table_format.' Fm','Fm.format_id = S.format_id') ;
        
        if ($reply)  $this->db->join($this->table_surat.' Ry','Ry.surat_id = S.surat_reply_id','left') ;
        
        if ($from_user) $this->db->join($this->table_users.' U','U.user_id = S.user_id') ;
        
        if ($user_id)   {
            $this->db->join($this->table.' H','H.surat_id = S.surat_id') ;
            $this->db->where('H.user_id',$user_id) ;
            
            if ($have_read)     $this->db->where('H.surat_read','read') ;
            if ($have_unread)   $this->db->where('H.surat_read','unread') ;
        }    
        
        if ($surat_id)      $this->db->where('S.surat_id'   , $surat_id) ;
        if ($distinct_id)   $this->db->where('S.surat_id <>', $distinct_id) ;
        if ($sifat_id)      $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($format_id)     $this->db->where('S.format_id'  , $format_id) ;
        if ($jenis_id)      $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($reply_id)      $this->db->where('S.surat_reply_id' , $reply_id) ;
        if ($from_user_id)  $this->db->where('S.user_id'        , $user_id) ;
        if ($have_view)     $this->db->where('S.surat_view' , 'Visible') ;
        if ($have_hidden)   $this->db->where('S.surat_view' , 'Hidden') ;
        
        if ($surat_date_month)  $this->db->where("DATE_FORMAT(S.surat_date,'%m/%Y') = '".$surat_date_month."'", NULL , FALSE) ;
        if ($start_date)        $this->db->where("DATE_FORMAT(S.surat_date,'%Y/%m/%d') >= '".$start_date."'"  , NULL , FALSE) ;
        if ($to_date)           $this->db->where("DATE_FORMAT(S.surat_date,'%Y/%m/%d') <= '".$to_date."'"     , NULL , FALSE) ;
        
        if ($keyword) {
            $i = 0 ;
            $having[$i++]   = 'score > 0' ;
            $this->db->having(implode(',',$having),FALSE) ;
            $this->db->order_by('score','DESC') ;
        }
        else {
            $this->db->order_by($order) ;
        }
        
        $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        
        /* $this->db->from($this->table_surat.' S') ;
        $sql = $this->db->_compile_select() ;
        $this->db->_reset_select();

        echo $sql ; exit (0) ; */
        
        $query  = $this->db->get($this->table_surat.' S') ;
        
        if ($surat_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'distinct_id'       => NULL ,
                            'have_read'         => FALSE ,
                            'have_unread'       => FALSE ,
                            'surat_date_month'  => NULL ,
                            'start_date'        => NULL ,
                            'to_date'           => NULL ,
                            'keyword'           => NULL ,
                            'sifat_id'          => NULL ,
                            'format_id'         => NULL ,
                            'have_view'         => NULL ,
                            'have_hidden'       => NULL ,
                            'jenis_id'          => NULL ,
                            'user_id'           => NULL ,
                            'from_user_id'      => NULL , 
                            'reply_id'          => NULL );
 
	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        if ($user_id)   {
            $this->db->join($this->table.' H','H.surat_id = S.surat_id') ;
            $this->db->where('H.user_id',$user_id) ;
            
            if ($have_read)     $this->db->where('H.surat_read','read') ;
            if ($have_unread)   $this->db->where('H.surat_read','unread') ;
        }    
        
        if ($distinct_id)   $this->db->where('S.surat_id <>', $distinct_id) ;
        if ($sifat_id)      $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($format_id)     $this->db->where('S.format_id'  , $format_id) ;
        if ($jenis_id)      $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($reply_id)      $this->db->where('S.surat_reply_id' , $reply_id) ;
        if ($from_user_id)  $this->db->where('S.user_id'        , $user_id) ;
        
        if ($have_view)     $this->db->where('S.surat_view' , 'Visible') ;
        if ($have_hidden)   $this->db->where('S.surat_view' , 'Hidden') ;
        
        if ($surat_date_month)  $this->db->where("DATE_FORMAT(S.surat_date,'%m/%Y') = '".$surat_date_month."'", NULL , FALSE) ;
        if ($start_date)        $this->db->where("DATE_FORMAT(S.surat_date,'%Y/%m/%d') >= '".$start_date."'"  , NULL , FALSE) ;
        if ($to_date)           $this->db->where("DATE_FORMAT(S.surat_date,'%Y/%m/%d') <= '".$to_date."'"     , NULL , FALSE) ;
        
        if ($keyword)           $this->db->where("MATCH (surat_number,surat_from,surat_perihal) AGAINST (".$this->db->escape($keyword).")",NULL,FALSE) ;
        
        $this->db->group_by('S.surat_id') ;
        
        $this->db->select('S.surat_id') ;
        
        /* $this->db->from($this->table_surat.' S') ;
        $sql = $this->db->_compile_select() ;
        $this->db->_reset_select();

        echo $sql ; exit (0) ; */
        
        $query  = $this->db->get($this->table_surat.' S') ;
        
        return $query->num_rows() ;
     }
    
    public function delete($surat_id,$user_id) {
        $this->db->where('surat_id', $surat_id) ;
        $this->db->where('user_id' , $user_id) ;
        
        return $this->db->delete($this->table) ;
    }
    
    public function delete_group($user_id,$surat_id = array()) {
        if (!empty($surat_id) && is_array($surat_id) && count($surat_id) > 0) {
            foreach ($surat_id as $sid) {
                $this->db->where('surat_id', $sid) ;
                $this->db->where('user_id' , $user_id) ;

                $this->db->delete($this->table) ;
            }
            
            return TRUE ;
        }
        
        return FALSE ;
    } 
    
    public function num_hub($config=array()) {
        $defaults = array(  'surat_id'  => NULL ,
                            'level_id'  => NULL ,
                            'user_id'   => FALSE ,
                            'have_read' => FALSE ,
                            'have_unread'=> FALSE );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        if ($surat_id)  $this->db->where('surat_id', $surat_id) ;
        if ($level_id)  $this->db->where('level_id', $level_id) ;
        if ($user_id)   $this->db->where('user_id' , $user_id) ;
        if ($have_read)   $this->db->where('surat_read' , 'read') ;
        if ($have_unread) $this->db->where('surat_read' , 'unread') ;
        
        return $this->db->count_all_results($this->table) ;
    }
    
    public function set_read($surat_id,$user_id,$status = 'read') {
        $data['surat_read']      = $status ;
        $data['surat_read_date'] = date('Y-m-d H:i:s') ;
        
        if ($surat_id)  $this->db->where('surat_id', $surat_id) ;
        if ($user_id)   $this->db->where('user_id' , $user_id) ;
        
        return $this->db->update($this->table,$data) ;
    }
    
    public function get_from_users($surat_id = NULL, $make_array = FALSE) {
        if ($surat_id) {
            $sql    = "SELECT C.*
                            FROM ". $this->table_users ." C ,
                                 ". $this->table ." CP
                      WHERE 1
                            AND C.user_id   = CP.user_id
                            AND CP.surat_id = ".$this->db->escape($surat_id)."
                      ORDER BY C.fullname DESC " ;


            $query  = $this->db->query($sql) ;

            if ($make_array) {
                $d  = array() ;

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $c) {
                        $d[]    = $c->user_id ;
                    }
                }

                return $d ;
            }

            return $query ;
        }

        return NULL ;
    }
    
    public function get_from_level($surat_id = NULL, $make_array = FALSE) {
        if ($surat_id) {
            $sql    = "SELECT C.*
                            FROM ". $this->table_level ." C ,
                                 ". $this->table ." CP
                      WHERE 1
                            AND C.level_id   = CP.level_id
                            AND CP.surat_id = ".$this->db->escape($surat_id)."
                      GROUP BY CP.level_id
                        ORDER BY C.level_name DESC " ;


            $query  = $this->db->query($sql) ;

            if ($make_array) {
                $d  = array() ;

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $c) {
                        $d[]    = $c->level_id ;
                    }
                }

                return $d ;
            }

            return $query ;
        }

        return NULL ;
    }
}
/* End of file surat_model.php */
/* Location: ./application/models/surat_model.php */
