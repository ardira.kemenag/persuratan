<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_jenis_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_jenis_model extends CI_Model {
    
    public $table           = 'entitas__jenis' ;
    public $table_surat     = 'surat__in' ;
    public $table_hub_user  = 'surat_user' ;

    public $table_hub_dis   = 'surat__indisposisi' ;

    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'jenis_id'  => NULL ,
                            'jid'       => NULL ,
                            'jnot'      => NULL ,
                            'surat'     => FALSE ,
                            'hubdis'    => FALSE ,
                            'disposisi' => NULL ,
                            'tujuan_out'=> FALSE ,
                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'start_date'=> NULL ,
                            'compiled'  => FALSE ,
                            'to_date'   => NULL ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'user_id'   => NULL ,
                            'to_user_id'=> NULL ,
                            'order'     => 'jenis_name ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        
        if (!empty($hubdis) && $disposisi->num_rows() > 0) {
            $dis = array(
                        1 => '(0)'
                    ,   2 => '(1)'
                    ,   3 => '( SELECT subdis_id FROM disposisi__sublist WHERE subdis_parent = 1 )'
                    ,   4 => '( SELECT subdis_id FROM disposisi__sublist WHERE subdis_parent IN ( SELECT subdis_id FROM disposisi__sublist WHERE subdis_parent = 1 )  )'    
                    ) ;

            foreach ($dis as $key => $val) {
                $sql = "(SELECT COUNT(DISTINCT HS.surat_id)
                              FROM surat__indisposisi HS
                                INNER JOIN surat__in S ON HS.surat_id = S.surat_id
                                   WHERE 1
                                       AND S.jenis_id = T.jenis_id
                                       AND HS.subdis_id IN ".$val ;

                if ($start_date)    $sql    .= " AND DATE_FORMAT(S.surat_date,'%Y/%m/%d') >= ".$this->db->escape($start_date) ;
                if ($to_date)       $sql    .= " AND DATE_FORMAT(S.surat_date,'%Y/%m/%d') <= ".$this->db->escape($to_date) ;                                       

                $sql .= " ) AS disposisi".$key ;

                $select[$i++]   = $sql ;
            }
        }

        if ($tujuan_out) {
            $sql = "SELECT COUNT(surat_id) FROM surat__out S WHERE 1 AND S.jenis_id = T.jenis_id " ;
            if ($start_date)    $sql    .= " AND DATE_FORMAT(S.surat_date,'%Y/%m/%d') >= ".$this->db->escape($start_date) ;
            if ($to_date)       $sql    .= " AND DATE_FORMAT(S.surat_date,'%Y/%m/%d') <= ".$this->db->escape($to_date) ;

            $select[$i++]   = "(".$sql." AND ( S.tujuan_id > 0 OR S.tujuan_id IS NOT NULL OR S.tujuan_disposisi > 0 OR S.tujuan_disposisi IS NOT NULL )  ) AS tujuan_out1" ;
            $select[$i++]   = "(".$sql." AND (S.surat_tujuan <> '' AND S.surat_tujuan IS NOT NULL) ) AS tujuan_out2" ;
        }
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($jenis_id)  $this->db->where('T.jenis_id' , $jenis_id) ;
        if ($jid)       $this->db->where_in('T.jenis_id' , $jid) ;
        if ($jnot)      $this->db->where_not_in('T.jenis_id' , $jnot) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        if ($compiled) e($this->db->get_compiled_select($this->table.' T'));

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($jenis_id OR $array OR $json) {
            $result = ($jenis_id OR $json) ? FALSE : array(''=>'- jenis surat -') ;
            if ($query->num_rows() > 0) {
                if ($jenis_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->jenis_id]   = $p->jenis_name ;
                        else        $result[]               = $p->jenis_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'jenis_id'  => NULL
                        ,   'jid'       => NULL
                        ,   'jnot'      => NULL
                         );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        if ($jenis_id)  $this->db->where('T.jenis_id' , $jenis_id) ;
        if ($jid)       $this->db->where('T.jenis_id' , $jid) ;
        if ($jnot)      $this->db->where('T.jenis_id <>' , $jnot) ;

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($jenis_id) {
        if (is_numeric($jenis_id)) {
            $this->db->where('jenis_id',$jenis_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function save() {
        $jenis_id        = $this->input->post('jenis_id') ;
        $jenis_name      = $this->input->post('jenis_name') ;

        if (count($jenis_name) > 0) {
            $i = 0 ;
            foreach ($jenis_name as $tm) {
                if (!empty($tm)) {
                    $data['jenis_name']      = $tm ;
                    if (!empty($jenis_id[$i])) {
                        $this->db->where('jenis_id',$jenis_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;

            return TRUE ;
        }

        return FALSE ;
    }
}
/* End of file Entitas_jenis_model.php */
/* Location: ./application/models/Entitas_jenis_model.php */