<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Agenda Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Agenda_model extends CI_Model {
    
    public $table           = 'agenda__kegiatan' ;
    public $table_status    = 'agenda__status' ;
    public $table_users     = 'users__list' ;
    
    public $table_surat     = 'users__list' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config=array()) {
        $defaults = array(  'kegiatan_id'     => NULL ,
                            'user_id'         => NULL ,
                            'surat_id'        => NULL ,
                            'surat'           => FALSE ,
                            'user'            => FALSE ,
                            'status_id'       => NULL ,
                            'status'          => FALSE ,
                            'kegiatan_date'   => NULL ,
                            'kdate'           => NULL ,
                            'sdate'           => NULL ,
                            'ndate'           => NULL ,
                            'today_and_next'  => FALSE ,
                            'month'           => NULL ,
                            'year'            => NULL ,
                            'keyword'         => NULL ,
                            'order'           => 'K.kegiatan_date DESC' ,
                            'having'          => NULL ,
                            'group'           => NULL ,
                            'page'            => 0 ,
                            'limit'           => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $i = 0 ;
        $select[$i++]                   = "K.*" ;
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date,'%d/%m/%Y') AS date_diff" ;
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date,'%H:%i') AS time_diff" ;
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date,'%Y') as Y, DATE_FORMAT(K.kegiatan_date,'%m') as m, DATE_FORMAT(K.kegiatan_date,'%d') as d"  ;
        
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date_start,'%d/%m/%Y') AS start_date_diff" ;
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date_start,'%Y') as start_Y, DATE_FORMAT(K.kegiatan_date_start,'%m') as start_m, DATE_FORMAT(K.kegiatan_date_start,'%d') as start_d"  ;
        
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date_end,'%d/%m/%Y') AS end_date_diff" ;
        $select[$i++]                   = "DATE_FORMAT(K.kegiatan_date_end,'%Y') as end_Y, DATE_FORMAT(K.kegiatan_date_end,'%m') as end_m, DATE_FORMAT(K.kegiatan_date_end,'%d') as end_d"  ;
        
        if ($user)      $select[$i++]   = "U.username,U.fullname,U.level_id" ;
        if ($status)    $select[$i++]   = "S.status_name" ;
        if ($surat)     $select[$i++]   = "ST.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($user)      $this->db->join($this->table_users.' U' , 'U.user_id  = K.user_id'    , 'left') ;
        if ($status)    $this->db->join($this->table_status.' S', 'S.status_id = K.status_id') ;
        if ($surat)     $this->db->join($this->table_surat.' ST', 'ST.surat_id = K.surat_id'  , 'left') ;
        
        if ($kegiatan_id)   $this->db->where('K.kegiatan_id', $kegiatan_id) ;
        if ($user_id)       $this->db->where('K.user_id'    , $user_id) ;
        if ($status_id)     $this->db->where('K.status_id'  , $status_id) ;
        if ($surat_id)      $this->db->where('K.surat_id'   , $surat_id) ;
        
        if ($month)         $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%m') = '".$month."'",NULL,FALSE) ;
        if ($year)          $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%Y') = '".$year."'",NULL,FALSE) ;
        if ($kegiatan_date) $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%d/%m/%Y') = '".$kegiatan_date."'",NULL,FALSE) ;
        if ($today_and_next)$this->db->where("(( kegiatan_date_start >= DATE(NOW()) AND kegiatan_date_end <= DATE(NOW()) ) OR ( kegiatan_date_start >= DATE(NOW()) ))",NULL,FALSE) ;
        if ($sdate)         $this->db->where("(K.kegiatan_date_start = '".$sdate."' OR K.kegiatan_date_end = '".$sdate."')",NULL,FALSE) ;
        if ($kdate)         $this->db->where("( DATE_FORMAT(kegiatan_date_start,'%d/%m/%Y') = ".$this->db->escape($kdate)." OR DATE_FORMAT(kegiatan_date_end,'%d/%m/%Y') = ".$this->db->escape($kdate)." )",NULL,FALSE) ;
        if ($ndate)         $this->db->where("(( kegiatan_date_start >= '".$ndate."' AND kegiatan_date_end <= '".$ndate."' ) OR (kegiatan_date_start <= '".$ndate."' AND kegiatan_date_end >= '".$ndate."'))",NULL,FALSE) ;
        
        if ($keyword) {
            $search     = array() ;
            $search[]   = " kegiatan_name LIKE    ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_tempat LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_penyelenggara LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_peserta LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_keterangan LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            
            $this->db->where('('.implode(' OR ', $search).')',NULL,FALSE) ;
        }
        
        if ($order)     $this->db->order_by($order) ;
        if ($having)    $this->db->having($having)  ;
        if ($group)     $this->db->group_by($group) ;
        if ($limit)     $this->db->limit($limit,$page) ;
        
        /* $this->db->from($this->table.' K') ;
        $sql = $this->db->_compile_select() ;
        $this->db->_reset_select();

        echo $sql ; exit (0) ; */
        
        $query  = $this->db->get($this->table.' K') ;

        if ($kegiatan_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'user_id'         => NULL ,
                            'status_id'       => NULL ,
                            'surat_id'        => NULL ,
                            'kegiatan_date'   => NULL ,
                            'sdate'           => NULL ,
                            'kdate'           => NULL ,
                            'ndate'           => NULL ,
                            'today_and_next'  => FALSE ,
                            'month'           => NULL ,
                            'year'            => NULL ,
                            'keyword'         => NULL ,
                            'group'           => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        if ($user_id)       $this->db->where('K.user_id'    , $user_id) ;
        if ($status_id)     $this->db->where('K.status_id'  , $status_id) ;
        if ($surat_id)      $this->db->where('K.surat_id'   , $surat_id) ;
        
        if ($month)         $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%m') = '".$month."'",NULL,FALSE) ;
        if ($year)          $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%Y') = '".$year."'",NULL,FALSE) ;
        if ($kegiatan_date) $this->db->where("DATE_FORMAT(K.kegiatan_date_start,'%d/%m/%Y') = '".$kegiatan_date."'",NULL,FALSE) ;
        if ($today_and_next)$this->db->where("(( kegiatan_date_start >= DATE(NOW()) AND kegiatan_date_end <= DATE(NOW()) ) OR ( kegiatan_date_start >= DATE(NOW()) ))",NULL,FALSE) ;
        if ($sdate)         $this->db->where("(K.kegiatan_date_start = '".$sdate."' OR K.kegiatan_date_end = '".$sdate."')",NULL,FALSE) ;
        
        if ($kdate)         $this->db->where("( DATE_FORMAT(kegiatan_date_start,'%d/%m/%Y') = ".$this->db->escape($kdate)." OR DATE_FORMAT(kegiatan_date_end,'%d/%m/%Y') = ".$this->db->escape($kdate)." )",NULL,FALSE) ;
        if ($ndate)         $this->db->where("(( kegiatan_date_start >= '".$ndate."' AND kegiatan_date_end <= '".$ndate."' ) OR (kegiatan_date_start <= '".$ndate."' AND kegiatan_date_end >= '".$ndate."'))",NULL,FALSE) ;
        
        if ($keyword) {
            $search     = array() ;
            $search[]   = " kegiatan_name LIKE    ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_tempat LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_penyelenggara LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_peserta LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            $search[]   = " kegiatan_keterangan LIKE  ".$this->db->escape('%'.$keyword.'%')." " ;
            
            $this->db->where('('.implode(' OR ', $search).')',NULL,FALSE) ;
        }
        
        if ($group) {
            $this->db->group_by($group) ;
            
            $this->db->select('K.kegiatan_id') ;
            $query  = $this->db->get($this->table.' K') ;
            
            return $query->num_rows() ;
        }
        
        return $this->db->count_all_results($this->table.' K') ;
    }
    
    public function delete($kegiatan_id) {
        if (!empty($kegiatan_id) && is_numeric($kegiatan_id)) {
            $CI =& get_instance() ;
            $CI->load->model('agenda_google_model') ;
            $CI->agenda_google_model->delete($kegiatan_id) ;
            
            $this->db->where('kegiatan_id',$kegiatan_id) ;
            return $this->db->delete($this->table) ;
        }
        
        return FALSE ;
    }
    
    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('kegiatan_id'         , 'ID Kegiatan'             , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('status_id'           , 'Status Kegiatan'         , 'required|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('surat_id'            , 'Surat'                   , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('user_id'             , 'Pengunggah'              , 'required|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('kegiatan_name'       , 'Agenda Rapat'          , 'required|trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_tempat'     , 'Tempat Kegiatan'         , 'trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_date_start' , 'Tanggal Mulai Kegiatan'  , 'required|trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_date_end'   , 'Tanggal Akhir Kegiatan'  , 'trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_time_start' , 'Waktu Mulai Kegiatan'    , 'trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_time_end'   , 'Waktu Akhir Kegiatan'    , 'trim|xss_clean');
        $this->form_validation->set_rules('timezone'            , 'Time Zone'               , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('kegiatan_penyelenggara'  , 'Asal Undangan'       , 'trim|xss_clean');
        $this->form_validation->set_rules('kegiatan_peserta'        , 'Disposisi'           , 'trim|xss_clean');
          
        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }
    public function save_draft() {
		
	}
	
    public function save() {
        $kegiatan_id    = set_value('kegiatan_id') ;
        
        $data['status_id']      = set_value('status_id') ;
        $data['user_id']        = set_value('user_id') ;
        $data['surat_id']       = set_value('surat_id') ;
        
        $data['kegiatan_name']          = set_value('kegiatan_name') ;
        $data['kegiatan_tempat']        = set_value('kegiatan_tempat') ;
        $data['kegiatan_date_start']    = set_date(set_value('kegiatan_date_start')) ;
        $data['kegiatan_date_end']      = set_date(set_value('kegiatan_date_end')) ;
        $data['kegiatan_time_start']    = set_value('kegiatan_time_start') ;
        $data['kegiatan_time_end']      = set_value('kegiatan_time_end') ;
        $data['kegiatan_penyelenggara'] = set_value('kegiatan_penyelenggara') ;
        $data['kegiatan_peserta']       = set_value('kegiatan_peserta') ;
        $data['kegiatan_keterangan']    = $this->input->post('kegiatan_keterangan') ;
        $data['timezone']               = set_value('timezone') ;
        
        $result = FALSE ;
        if (!empty($kegiatan_id)) {
            $this->db->where('kegiatan_id',$kegiatan_id) ;
            if ($this->db->update($this->table,$data)) {
                $result         = TRUE ;
                $this->hint($kegiatan_id, 'update') ;
            }
        }
        else {
            $data['kegiatan_date']  = date('Y-m-d H:i:s') ;
            if ($this->db->insert($this->table,$data)) {
                $result         = TRUE ;
                $kegiatan_id   = $this->db->insert_id();
            }
        }
        
        if (!empty($kegiatan_id)) {
            /*$CI =& get_instance() ;
            $CI->load->model('agenda_google_model') ;
            $CI->agenda_google_model->save($kegiatan_id)*/ ;
        }
        
        return $result ;
    }
    
     public function hint($kegiatan_id,$field = 'download') {
        $sql    = "UPDATE ".$this->table."
                        SET kegiatan_hint_".$field." = kegiatan_hint_".$field." + 1
                            WHERE   1
                                    AND kegiatan_id = ".$this->db->escape($kegiatan_id)."" ;

        return $this->db->query($sql) ;
    }
    
}
/* End of file agenda_model.php */
/* Location: ./application/models/agenda_model.php */