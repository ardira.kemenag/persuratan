<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Date_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Date_model extends CI_Model {

    public $table   = 'set__date' ;

    public function  __construct() {
        parent::__construct();
    }

    public function get($config=array()) {
        $defaults = array(  'page'          => 0    ,
                            'limit'         => NULL ,
                            'date_month'    => NULL ,
                            'start_date'    => NULL ,
                            'to_date'       => NULL ,
                            'order'         => 'date_name ASC' ,
                            'select_self'   => NULL ,
                            'group'         => NULL ,
                            'join'          => array() ,
                            'field_join'    => array() );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]       = "D.*" ;
        if ($select_self)   $select[$i++]   = $select_self ;

        $cjoin = count($join) ; $cfield_join = count($field_join) ;
        if ($cjoin > 0 && $cfield_join > 0 && $cjoin == $cfield_join) {
            $alphabet = array('A','B','C','E','F','G','H') ;

            for ($j = 0; $j < $cjoin ; $j++) {
                $select[$i++]   = "COALESCE(num".$alphabet[$j].",0) AS num".$alphabet[$j] ;
                $table_join = "(SELECT DATE(".$field_join[$j].") AS ".$alphabet[$j]."date,
                                        COUNT(*) AS num".$alphabet[$j]."
                                    FROM ".$join[$j]."
                                    WHERE 1 " ;

                if ($date_month)    $table_join .= " AND DATE_FORMAT(".$field_join[$j].",'%m/%Y')    = '".$date_month."' " ;
                if ($start_date)    $table_join .= " AND DATE_FORMAT(".$field_join[$j].",'%Y/%m/%d') >= '".$start_date."' " ;
                if ($to_date)       $table_join .= " AND DATE_FORMAT(".$field_join[$j].",'%Y/%m/%d') <= '".$to_date."' " ;

                if ($group)         $table_join .= " GROUP BY ".$group[$j]." " ;
                $table_join .= ") ".$alphabet[$j]."" ;

                $this->db->join($table_join, $alphabet[$j] . 'date = date_name ','left') ;
            }
        }

        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($date_month)    $this->db->where("DATE_FORMAT(date_name,'%m/%Y')    = '".$date_month."'"  , NULL, FALSE) ;
        if ($start_date)    $this->db->where("DATE_FORMAT(date_name,'%Y/%m/%d') >= '".$start_date."'" , NULL, FALSE) ;
        if ($to_date)       $this->db->where("DATE_FORMAT(date_name,'%Y/%m/%d') <= '".$to_date."'"    , NULL, FALSE) ;

        if ($group && !is_array($group))
                    $this->db->group_by($group) ;
        
        if ($order) $this->db->order_by($order) ;
        if ($limit) $this->db->limit($limit,$page) ;

        return $this->db->get($this->table.' D') ;
    }
}
/* End of file date_model.php */
/* Location: ./application/models/date_model.php */