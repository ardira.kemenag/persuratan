<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_sifat_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_sifat_model extends CI_Model {
    
    public $table       = 'entitas__sifat' ;
    public $table_surat = 'surat__list' ;
    public $table_hub_user  = 'hub__surat_to_user' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'sifat_id'  => NULL ,
                            'surat'     => FALSE ,
                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'user_id'   => NULL ,
                            'to_user_id'=> NULL ,
                            'order'     => 'sifat_id ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($sifat_id)      $this->db->where('T.sifat_id' , $sifat_id) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($sifat_id OR $array OR $json) {
            $result = ($sifat_id OR $json) ? FALSE : array(''=>'- sifat surat -') ;
            if ($query->num_rows() > 0) {
                if ($sifat_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->sifat_id]   = $p->sifat_name ;
                        else        $result[]               = $p->sifat_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
	
	public function get_color($config = array()) {
        $defaults = array(  'sifat_id'  => NULL ,
                            'surat'     => FALSE ,
                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'user_id'   => NULL ,
                            'to_user_id'=> NULL ,
                            'order'     => 'sifat_id ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($sifat_id)      $this->db->where('T.sifat_id' , $sifat_id) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($sifat_id OR $array OR $json) {
            $result = ($sifat_id OR $json) ? FALSE : array(''=>'- sifat surat -') ;
            if ($query->num_rows() > 0) {
                if ($sifat_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->sifat_id]   = $p->sifat_color ;
                        else        $result[]               = $p->sifat_color ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'sifat_id'   => NULL
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($sifat_id)   $this->db->where('T.sifat_id' , $sifat_id) ;

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($sifat_id) {
        if (is_numeric($sifat_id)) {
            $this->db->where('sifat_id',$sifat_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }
    
    public function save() {
        $sifat_id        = $this->input->post('sifat_id') ;
        $sifat_name      = $this->input->post('sifat_name') ;

        if (count($sifat_name) > 0) {
            $i = 0 ;
            foreach ($sifat_name as $tm) {
                if (!empty($tm)) {
                    $data['sifat_name']      = $tm ;
                    if (!empty($sifat_id[$i])) {
                        $this->db->where('sifat_id',$sifat_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;
            return TRUE ;
        }

        return FALSE ;
    }
}
/* End of file Entitas_sifat_model.php */
/* Location: ./application/models/Entitas_sifat_model.php */