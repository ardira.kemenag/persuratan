<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Language_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Language_model extends CI_Model {

    public $table           = 'set__language' ;
    public $table_config    = 'set__config' ;

    public function  __construct() {
        parent::__construct();
        $this->lang->load('setting') ;
    }

    public function get($config = array()) {
        $defaults = array(  'language_id'        => NULL ,
                            'page'           => 0    ,
                            'limit'          => NULL ,
                            'distinct_id'    => NULL ,
                            'idiom'          => NULL ,
                            'langfile'       => NULL ,
                            'keyword'        => NULL ,
                            'order'          => NULL ,
                            'group'          => NULL ,
                            'array'          => FALSE );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        if ($group) $select[$i++]   = $group ;
        else        $select[$i++]   = "*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($language_id)   $this->db->where('language_id'  ,$language_id) ;
        if ($distinct_id)   $this->db->where('language_id <>',$distinct_id) ;
        if ($idiom)         $this->db->where('idiom'    , $idiom) ;
        if ($langfile)      $this->db->where('langfile' , $langfile) ;
        if ($keyword)       $this->db->where('language_line LIKE '.$this->db->escape('%'.$keyword.'%').' OR language_key LIKE '.$this->db->escape('%'.$keyword.'%').'',NULL,FALSE) ;
        
        if ($group) $this->db->group_by($group) ;

        if ($order) {
            $this->db->order_by($order) ;
        }
        else {
            $this->db->order_by('idiom ASC, langfile ASC, language_id ASC') ;
        }

        if ($limit) $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table) ;

        if ($language_id OR ($array && $group)) {
            if ($array) $results['']    = sprintf ($this->lang->line('set_language_model_group'),$group) ;
            if ($query->num_rows() > 0) {
                if ($array) {
                    foreach ($query->result_array() as $q) {
                        $results[$q[$group]]    = $q[$group] ;
                    }
                }
                else
                    return $query->row_array();
            }

            return $array ? $results : FALSE ;
        }

        return $query ;
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('idiom'           , $this->lang->line('set_language_model_val_idiom')     , 'required|trim|xss_clean');
        $this->form_validation->set_rules('langfile'        , $this->lang->line('set_language_model_val_langfile')  , 'required|trim|xss_clean');
        $this->form_validation->set_rules('language_key'    , $this->lang->line('set_language_model_val_key')       , 'required|trim|xss_clean');
        $this->form_validation->set_rules('language_line'   , $this->lang->line('set_language_model_val_line')      , 'required|trim|xss_clean');
        
        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }

    public function save() {
        $language_id    = $this->input->post('language_id') ;

        $data['idiom']          = set_value('idiom')  ;
        $data['langfile']       = set_value('langfile')  ;
        $data['language_key']   = set_value('language_key')  ;
        $data['language_line']  = set_value('language_line')  ;

        if (!empty($language_id)) {
            $this->db->where('language_id',$language_id) ;
            if ($this->db->update($this->table,$data)) {

                return TRUE ;
            }
        }
        else {
            if ($this->db->insert($this->table,$data)) {
                return TRUE ;
            }
        }

        return FALSE ;
    }

    // UPDATE GROUP
    public function save_group() {
        $postdata   = $this->input->post('postdata',TRUE) ;

        $data   = $this->get($postdata) ;

        // UPDATE DATA SEBELUMNYA
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $d) {
                $ins['idiom']           = $idiom            = $this->input->post('idiom_edit'.$d->language_id   , TRUE) ;
                $ins['langfile']        = $langfile         = $this->input->post('langfile_edit'.$d->language_id    , TRUE) ;
                $ins['language_key']    = $language_key     = $this->input->post('language_key_edit'.$d->language_id    , TRUE) ;
                $ins['language_line']   = $language_line    = $this->input->post('language_line_edit'.$d->language_id   , TRUE) ;

                if (!empty($idiom) && !empty($langfile) && !empty($language_key) && !empty($language_line)) {
                    $this->db->where('language_id',$d->language_id) ;
                    if (!$this->db->update($this->table,$ins)) { return FALSE ; } ;
                }
            }
        }

        // INSERT DATA BARU
        foreach (range(1,5) as $i) {
                $ins['idiom']           = $idiom            = $this->input->post('idiom_new'.$i , TRUE) ;
                $ins['langfile']        = $langfile         = $this->input->post('langfile_new'.$i  , TRUE) ;
                $ins['language_key']    = $language_key     = $this->input->post('language_key_new'.$i  , TRUE) ;
                $ins['language_line']   = $language_line    = $this->input->post('language_line_new'.$i , TRUE) ;

            if (!empty($idiom) && !empty($langfile) && !empty($language_key) && !empty($language_line)) {
                if (!$this->db->insert($this->table,$ins)) { return FALSE ; } ;
            }
        }

        return TRUE ;
    }

    public function num($config = array()) {
        $defaults = array(  'distinct_id'    => NULL ,
                            'idiom'          => NULL ,
                            'langfile'       => NULL ,
                            'keyword'        => NULL ,
                            'group'          => NULL ) ;

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($distinct_id)   $this->db->where('language_id <>',$distinct_id) ;
        if ($idiom)         $this->db->where('idiom'    , $idiom) ;
        if ($langfile)      $this->db->where('langfile' , $langfile) ;
        if ($keyword)       $this->db->where('language_line LIKE '.$this->db->escape('%'.$keyword.'%').' OR language_key LIKE '.$this->db->escape('%'.$keyword.'%').'',NULL,FALSE) ;

        if ($group) {
            $this->db->group_by($group) ;

            $query  = $this->db->get($this->table) ;

            return $query->num_rows() ;
        }
        else {
            return $this->db->count_all_results($this->table) ;
        }
    }

    public function delete($language_id) {
        if (!empty($language_id)) {
            $this->db->where('language_id',$language_id) ;

            return $this->db->delete($this->table) ;
        }

        return FALSE ;
    }

}

/* End of file language_model.php */
/* Location: ./application/models/language_model.php */