<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_out_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Surat_out_model extends CI_Model {
    
    public $table               = 'surat__out' ;

    public $table_klasifikasi   = 'entitas__klasifikasi' ;
    public $table_tujuan        = 'entitas__tujuan' ;
    public $table_jenis         = 'entitas__jenis' ;
    public $table_user          = 'users__list' ;
    public $table_sifat         = 'entitas__sifat' ;

    public $table_dispoisi      = 'disposisi__sublist' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'surat_id'      => NULL ,
                            'surat_n'       => NULL ,

                            'select_self'   => NULL ,

                            'surat_date'    => NULL ,
                            'date_start'    => NULL ,
                            'date_end'      => NULL ,

                            'ttd_id'        => NULL ,
                            'surat_ttd'     => NULL ,
                            'olah_id'       => NULL ,
                            'klasifikasi_id'=> NULL ,
                            'klasifikasi_pr'=> NULL ,
                            'surat_hal'     => NULL ,
                            'tujuan_id'     => NULL ,
                            'jenis_id'      => NULL ,
                            'user_id'       => NULL ,
                            'sifat_id'      => NULL ,
                            'type_warning'  => NULL ,
                            'berita_acara'  => NULL ,
                            'tujuan_disposisi'  => NULL ,

                            'ttd'           => FALSE ,
                            'olah'          => FALSE ,
                            'klasifikasi'   => FALSE ,
                            'jenis'         => FALSE ,
                            'user'          => FALSE ,
                            'sifat'         => FALSE ,
                            'tujuan_dispo'  => FALSE ,

                            'month_start'   => NULL ,
                            'month_end'     => NULL ,
                            'tahun_start'   => NULL ,
                            'tahun_end'     => NULL ,
                            'tahun'         => NULL ,
                            'month'         => NULL ,

                            'where'     => NULL ,

                            'keyword'   => NULL ,

                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'order'     => 'S.surat_date DESC'
                         );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $i = 0 ;
        if ($select_self)   $select[$i++]   = $select_self ;
        else {
            $select[$i++]   = "S.*" ;
            if ($ttd)           $select[$i++]   = "T.subdis_man AS ttd_name,T.subdis_kode AS ttd_kode" ;
            if ($olah)          $select[$i++]   = "O.subdis_name AS olah_name,O.subdis_kode AS olah_kode" ;
            
            if ($klasifikasi)   $select[$i++]   = "K.klasifikasi_name,K.klasifikasi_kode,K.klasifikasi_parent" ;
            if ($jenis)         $select[$i++]   = "J.jenis_name" ;
            if ($user)          $select[$i++]   = "U.username,U.fullname,U.level_id" ;
            if ($sifat)         $select[$i++]   = "SF.sifat_name" ;

            $select[$i++] = "DATE_FORMAT(S.surat_date,'%d/%m/%Y') AS s_date
                            ,IF(S.surat_start IS NOT NULL AND S.surat_start             <> '0000-00-00',DATE_FORMAT(S.surat_start,'%d/%m/%Y'),NULL) AS s_start
                            ,IF(S.surat_end   IS NOT NULL AND S.surat_end               <> '0000-00-00',DATE_FORMAT(S.surat_end,'%d/%m/%Y'),NULL) AS s_end
                            ,IF(S.surat_inv_date IS NOT NULL AND S.surat_inv_date       <> '0000-00-00',DATE_FORMAT(S.surat_inv_date,'%d/%m/%Y'),NULL) AS s_inv_date
                            ,IF(S.surat_kapal_date IS NOT NULL AND S.surat_kapal_date   <> '0000-00-00',DATE_FORMAT(S.surat_kapal_date,'%d/%m/%Y'),NULL) AS s_kapal_date" ;
        }

        $this->db->select(implode(',', $select),FALSE) ;

        if ($ttd)           $this->db->join($this->table_dispoisi.' T'      , 'T.subdis_id = S.ttd_id'          , 'left') ;
        if ($olah)          $this->db->join($this->table_dispoisi.' O'      , 'O.subdis_id = S.olah_id'        , 'left') ;
        if ($klasifikasi)   $this->db->join($this->table_klasifikasi.' K'   , 'K.klasifikasi_id = S.klasifikasi_id','left') ;
        if ($jenis)         $this->db->join($this->table_jenis.' J'         , 'J.jenis_id = S.jenis_id'      , 'left') ;
        if ($user)          $this->db->join($this->table_user.' U'          , 'U.user_id = S.user_id'        , 'left') ;
        if ($sifat)         $this->db->join($this->table_sifat.' SF'        , 'SF.sifat_id = S.sifat_id'     , 'left') ;

        if ($surat_id)          $this->db->where('S.surat_id'   , $surat_id) ;
        if ($surat_n)           $this->db->where('S.surat_n'    , $surat_n) ;
        if ($tujuan_disposisi)  $this->db->where('S.tujuan_disposisi LIKE '.$this->db->escape('%"'.$tujuan_disposisi.'"%'),NULL,FALSE) ;
        if ($surat_date)        $this->db->where('S.surat_date' , $surat_date) ;
        if ($ttd_id)            $this->db->where('S.ttd_id'     , $ttd_id) ;
        if ($surat_ttd)         $this->db->where('S.surat_ttd'  , $surat_ttd) ;
        if ($olah_id)           $this->db->where('S.olah_id'    , $olah_id) ;
        if ($klasifikasi_id)    {
            if (is_numeric($klasifikasi_id))
                $this->db->where('S.klasifikasi_id',$klasifikasi_id) ;
            else
                $this->db->where('S.klasifikasi_pr',$klasifikasi_id) ;
        }
        if ($klasifikasi_pr)    $this->db->where('S.klasifikasi_pr',$klasifikasi_pr) ;
        if ($tujuan_id)         $this->db->where('S.tujuan_id LIKE '.$this->db->escape('%"'.$tujuan_id.'"%'),NULL,FALSE) ;
        if ($jenis_id)          $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($user_id)           $this->db->where('S.user_id'    , $user_id) ;
        if ($sifat_id)          $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($type_warning)      $this->db->where('S.type_warning'   , $type_warning) ;
        if ($berita_acara)      $this->db->where('S.berita_acara'   , $berita_acara) ;

        if ($date_start)    $this->db->where('S.surat_date >=',$date_start) ;
        if ($date_end)      $this->db->where('S.surat_date <=',$date_end) ;
        if ($month_start)   $this->db->where('MONTH(S.surat_date) >=', $month_start) ;
        if ($month_end)     $this->db->where('MONTH(S.surat_date) <=', $month_end) ;
        if ($tahun_start)   $this->db->where('YEAR(S.surat_date) >=' , $tahun_start) ;
        if ($tahun_end)     $this->db->where('YEAR(S.surat_date) <=' , $tahun_end) ;
        if ($month)         $this->db->where('MONTH(S.surat_date) <=', $month) ;
        if ($tahun)         $this->db->where('YEAR(S.surat_date) <=' , $tahun) ;

        if ($where)     $this->db->where($where,NULL,FALSE) ;

        if ($keyword) {
            $keys[] = "S.surat_date     LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_plh      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_hal      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_ket      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_no       LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_timbang  LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_dasar    LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_pelaksana LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_tujuan   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_lokasi   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_inv_hour LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kegiatan LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_plh_as   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_owner   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_address LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_date    LIKE ".$this->db->escape('%'.$keyword.'%') ;

            $this->db->where('('.implode(' OR ', $keys).')',NULL,FALSE) ;
        }

        if ($group)     $this->db->group_by($group) ;
        if ($limit)     $this->db->limit($limit,$page) ;
        if ($order)     $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' S') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($surat_id OR $array OR $json) {
            $result = ($surat_id OR $json) ? FALSE : array(''=>'- surat keluar -') ;
            if ($query->num_rows() > 0) {
                if ($surat_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->surat_id]   = $p->surat_hal ;
                        else        $result[]               = $p->surat_hal ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  
                            'surat_date'    => NULL ,
                            'date_start'    => NULL ,
                            'date_end'      => NULL ,

                            'ttd_id'        => NULL ,
                            'surat_ttd'     => NULL ,
                            'olah_id'       => NULL ,
                            'klasifikasi_id'=> NULL ,
                            'klasifikasi_pr'=> NULL ,
                            'surat_hal'     => NULL ,
                            'tujuan_id'     => NULL ,
                            'jenis_id'      => NULL ,
                            'user_id'       => NULL ,
                            'sifat_id'      => NULL ,
                            'type_warning'  => NULL ,
                            'berita_acara'  => NULL ,
                            'tujuan_disposisi'  => NULL ,

                            'month_start'   => NULL ,
                            'month_end'     => NULL ,
                            'tahun_start'   => NULL ,
                            'tahun_end'     => NULL ,
                            'tahun'         => NULL ,
                            'month'         => NULL ,

                            'keyword'   => NULL ,

                            'where'     => NULL ,

                            'group'     => NULL ,
                        );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }

        if ($surat_date)        $this->db->where('S.surat_date' , $surat_date) ;
        if ($ttd_id)            $this->db->where('S.ttd_id'     , $ttd_id) ;
        if ($surat_ttd)         $this->db->where('S.surat_ttd'  , $surat_ttd) ;
        if ($olah_id)           $this->db->where('S.olah_id'    , $olah_id) ;
        if ($klasifikasi_id)    {
            if (is_numeric($klasifikasi_id))
                $this->db->where('S.klasifikasi_id',$klasifikasi_id) ;
            else
                $this->db->where('S.klasifikasi_pr',$klasifikasi_id) ;
        }
        if ($klasifikasi_pr)    $this->db->where('S.klasifikasi_pr',$klasifikasi_pr) ;
        if ($tujuan_id)         $this->db->where('S.tujuan_id LIKE '.$this->db->escape('%"'.$tujuan_id.'"%'),NULL,FALSE) ;
        if ($jenis_id)          $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($user_id)           $this->db->where('S.user_id'    , $user_id) ;
        if ($sifat_id)          $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($tujuan_disposisi)  $this->db->where('S.tujuan_disposisi LIKE '.$this->db->escape('%"'.$tujuan_disposisi.'"%'),NULL,FALSE) ;
        if ($type_warning)      $this->db->where('S.type_warning'   , $type_warning) ;
        if ($berita_acara)      $this->db->where('S.berita_acara'   , $berita_acara) ;

        if ($date_start)    $this->db->where('S.surat_date >=',$date_start) ;
        if ($date_end)      $this->db->where('S.surat_date <=',$date_end) ;
        if ($month_start)   $this->db->where('MONTH(S.surat_date) >=', $month_start) ;
        if ($month_end)     $this->db->where('MONTH(S.surat_date) <=', $month_end) ;
        if ($tahun_start)   $this->db->where('YEAR(S.surat_date) >=' , $tahun_start) ;
        if ($tahun_end)     $this->db->where('YEAR(S.surat_date) <=' , $tahun_end) ;
        if ($month)         $this->db->where('MONTH(S.surat_date) <=', $month) ;
        if ($tahun)         $this->db->where('YEAR(S.surat_date) <=' , $tahun) ;

        if ($where)         $this->db->where($where,NULL,FALSE) ;

        if ($keyword) {
            $keys[] = "S.surat_date     LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_plh      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_hal      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_ket      LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_no       LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_timbang  LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_dasar    LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_pelaksana LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_tujuan   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_lokasi   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_inv_hour LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kegiatan LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_plh_as   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_owner   LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_address LIKE ".$this->db->escape('%'.$keyword.'%') ;
            $keys[] = "S.surat_kapal_date    LIKE ".$this->db->escape('%'.$keyword.'%') ;

            $this->db->where('('.implode(' OR ', $keys).')',NULL,FALSE) ;
        }
        
        if ($group) {
            $this->db->select('S.surat_id') ;
            $this->db->group_by($group) ;

            $sql = $this->db->get_compiled_select($this->table.' S') ;
        
            //$query  = $this->db->query($sql) ;
            $query  = apc_get($sql) ;

            return $query->num_rows() ;
        }

        return $this->db->count_all_results($this->table.' S') ;
    }

    public function delete($surat_id) {
        if (!empty($surat_id)) {
            if (is_array($surat_id))    $this->db->where_in('surat_id',$surat_id) ;
            else                        $this->db->where('surat_id',$surat_id) ;
            
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }
    
    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('surat_id'        , 'ID'                      , 'is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('surat_date'      , 'Tanggal'                 , 'required|trim|xss_clean');
        $this->form_validation->set_rules('ttd_id'          , 'Tanda Tangan'            , 'required|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('surat_ttd'       , 'Langsung/PLH'            , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('surat_plh'       , 'Atas Nama'               , 'trim|xss_clean');
        $this->form_validation->set_rules('olah_id'         , 'Pengolah / Asal Surat'   , 'required|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('klasifikasi_id'  , 'Klasifikasi Surat'       , 'required|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('klasifikasi_pr'  , 'Klasifikasi'             , 'trim|xss_clean');
        $this->form_validation->set_rules('surat_hal'       , 'Perihal'                 , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_ket'       , 'Keterangan'              , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_no'        , 'Nomor Surat'             , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('jenis_id'        , 'Pesan Disposisi'         , 'required|trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('surat_timbang'   , 'Pertimbangan'            , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_dasar'     , 'Dasar'                   , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_tujuan'    , 'Tujuan'                  , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_lokasi'    , 'Lokasi'                  , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_start'     , 'Tanggal Mulai'           , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_end'       , 'Tanggal Selesai'         , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_biaya'     , 'Pembiayaan'              , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_inv_date'  , 'Tanggal Pelaksanaan'     , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_inv_hour'  , 'Jam Pelaksanaan'         , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_kegiatan'  , 'Kegiatan'                , 'trim|xss_clean') ;
        $this->form_validation->set_rules('user_id'         , 'Pengunggah'              , 'required|trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('sifat_id'        , 'Sifat'                   , 'required|trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('surat_plh_as'    , 'PLH Sebagai'             , 'trim|xss_clean') ;

        $this->form_validation->set_rules('surat_kapal'         , 'Nama Kapal'          , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_kapal_owner'   , 'Perusahaan / Pemilik', 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_kapal_address' , 'Alamat'              , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_kapal_date'    , 'Berlaku Surat'       , 'trim|xss_clean') ;
        $this->form_validation->set_rules('type_warning'        , 'Jenis Surat'         , 'trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('berita_acara'        , 'Jenis Berita Acara'  , 'trim|xss_clean') ;
        
        $this->form_validation->set_error_delimiters('<span class="error_label">', '</span>');

        return $this->form_validation->run() ;
    }

    public function save() {
        $surat_id   = set_value('surat_id') ;

        $data['surat_date']     = set_value('surat_date') ;
        $data['ttd_id']         = set_value('ttd_id') ;
        $data['surat_ttd']      = set_value('surat_ttd') ;
        $data['surat_plh']      = set_value('surat_plh') ;
        $data['olah_id']        = set_value('olah_id') ;
        $data['klasifikasi_id'] = set_value('klasifikasi_id') ;
        $data['klasifikasi_pr'] = set_value('klasifikasi_pr') ;
        $data['surat_hal']      = set_value('surat_hal') ;
        $data['tujuan_id']      = set_value('tujuan_id') ;
        $data['tujuan_disposisi']= set_value('tujuan_disposisi') ;
        $data['surat_ket']      = set_value('surat_ket') ;
        $data['surat_no']       = set_value('surat_no') ;
        $data['jenis_id']       = set_value('jenis_id') ;
        $data['surat_timbang']  = set_value('surat_timbang') ;
        $data['surat_dasar']    = set_value('surat_dasar') ;
        $data['surat_pelaksana']= !empty($_POST['surat_pelaksana']) ? json_encode($this->input->post('surat_pelaksana')) : '' ;
        $data['surat_tujuan']   = set_value('surat_tujuan') ;
        $data['surat_lokasi']   = set_value('surat_lokasi') ;
        $data['surat_start']    = set_value('surat_start') ;
        $data['surat_end']      = set_value('surat_end') ;
        $data['surat_biaya']    = set_value('surat_biaya') ;
        $data['surat_inv_date'] = set_value('surat_inv_date') ;
        $data['surat_inv_hour'] = set_value('surat_inv_hour') ;
        $data['surat_kegiatan'] = set_value('surat_kegiatan') ;
        $data['user_id']        = set_value('user_id') ;
        $data['sifat_id']       = set_value('sifat_id') ;
        $data['surat_plh_as']   = set_value('surat_plh_as') ;

        $data['surat_kapal']        = set_value('surat_kapal') ;
        $data['surat_kapal_owner']  = set_value('surat_kapal_owner') ;
        $data['surat_kapal_address']= set_value('surat_kapal_address') ;
        $data['surat_kapal_date']   = set_value('surat_kapal_date') ;
        $data['type_warning']       = set_value('type_warning') ;
        $data['berita_acara']       = set_value('berita_acara') ;

				
        $json = array('tujuan_id','tujuan_disposisi') ;
        foreach ($json as $j) {
            $p = $this->input->post($j) ;
            $data[$j] = !empty($p) ? json_encode($p) : NULL ;
        }
    
        $save = FALSE ;

        if (!empty($surat_id)) {
            $this->db->where('surat_id',$surat_id) ;
            $save = $this->db->update($this->table,$data) ;
        }
        else {
            if ( $this->db->insert($this->table,$data) ) {
				
                $surat_id = $this->db->insert_id() ;
				
                $save = TRUE ;
            }
        }

        if ($save && !empty($surat_id)) {
			$CI =& get_instance() ;
			$CI->load->model( array('surat_outdocument_model') ) ;
			$CI->surat_outdocument_model->save($surat_id) ;
			apc_clean() ;  

			$documen=$this->db->get_where('surat__outdocument',array('surat_id' => $surat_id));
		
			foreach($documen->result() as $mn){
				$namafile[]=$mn->document_filename;
				$size[]=$mn->document_filesize;
				$type[]=$mn->document_filetype;
				$ext[]=$mn->document_fileext;
				$date[]=$mn->document_lastupdate;	
			}
			
			$doc = TRUE;
        }		
		
		if($save && set_value('jenis_id')==3){
			$query=$this->db->get_where('users__list',array('subdis_id' => set_value('olah_id')),1);
			foreach($query->result() as $n){
				$from=$n->fullname;
			}
		
			$data_memo = array(
			   'surat_number' => set_value('surat_no'),
			   'surat_from' => $from,
			   'from_status' => 3,
			   'jenis_id' => set_value('jenis_id'),
			   'user_id' => set_value('user_id'),
			   'surat_to' => json_encode(set_value('tujuan_disposisi')),
			   'sifat_id' =>set_value('sifat_id'),
			   'surat_date' => set_value('surat_date'),
			   'surat_date_taken' => set_value('surat_date'),
			   'date_entry' =>  date("Y-m-d h:i:s"),
			   'surat_perihal' => set_value('surat_hal'),
			   'subdis_id'=> $this->session->userdata('subdis_id'),
			   'surat_view' => 'Visible'
			);	
			$this->db->insert('surat__in',$data_memo);
			$id_surat= $this->db->insert_id() ;
			$data_into= array(
				'surat_id' => $id_surat,
				'subdis_id' => $this->session->userdata('subdis_id'),
			);
			$this->db->insert('surat__into',$data_into);
			$id_into=$this->db->insert_id();
			$query1=$this->db->get_where('surat__into',array('into_id' => $id_into));
			foreach($query1->result() as $m){
				$id_surat1=$m->surat_id;
			}
			$data_to=set_value('tujuan_disposisi');
				$panjang=count($data_to);
				for($x = 0; $x < $panjang; $x++) {
					
					$data_dispos= array(
						'subdis_id' =>$data_to[$x],
						'into_id' =>$id_into,
						'surat_id' =>$id_surat1,
						'indisposisi_status' =>2,
						'respon_disposisi' =>1,
						'read_status' =>1,
					);
					$this->db->insert('surat__indisposisi',$data_dispos);
				}
				
				if($doc){
					$akhir=count($namafile);
					for($y = 0; $y < $akhir; $y++) {
						$data_indoc= array(
							'document_filename' =>$namafile[$y],
							'document_filesize' =>$size[$y],
							'document_filetype' =>$type[$y],
							'document_fileext' =>$ext[$y],
							'document_lastupdate' =>$date[$y],
							'surat_id' =>$id_surat1,
						);
						$this->db->insert('surat__indocument',$data_indoc);
					}
				}
		}		
        return $save ;
    }
}
/* End of file Surat_out_model.php */
/* Location: ./application/models/Surat_out_model.php */