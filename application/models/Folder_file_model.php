<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Folder_file_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Folder_file_model extends CI_Model {
    
    public $table           = 'dokumen__folder_file' ;
    public $table_folder    = 'dokumen__folder' ;
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('file_model') ;
    }
    
    public function get($config=array()) {
        $defaults = array(  'file_id'       => NULL ,
                            'folder_id'     => NULL ,
                            'folder'        => FALSE ,
                            'order'         => 'F.file_order, F.file_title' ,
                            'group'         => NULL ,
                            'tahun'         => NULL ,
                            'page'          => 0 ,
                            'limit'         => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $i = 0 ;
        $select[$i++]   = "F.*" ;
        $select[$i++]   = "DATE_FORMAT(F.file_lastupdate,'%d/%m/%Y') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(F.file_lastupdate,'%H:%i') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(F.file_lastupdate,'%Y') as Y, DATE_FORMAT(F.file_lastupdate,'%m') as m, DATE_FORMAT(F.file_lastupdate,'%d') as d"  ;

        if ($folder) $select[$i++]   = "N.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($folder) $this->db->join($this->table_folder.' N','N.folder_id = F.folder_id') ;
        
        if ($file_id)        $this->db->where('F.file_id'   , $file_id)  ;
        if ($folder_id)      $this->db->where('F.folder_id' , $folder_id) ;
        if ($tahun)          $this->db->where('F.tahun'     , $tahun) ;
        
        if ($order)     $this->db->order_by($order) ;
        if ($group)     $this->db->group_by($group) ;
        if ($limit)     $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table.' F') ;

        if ($file_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'folder_id'     => NULL ,
                            'tahun'         => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        if ($folder_id)     $this->db->where('F.folder_id'  , $folder_id) ;
        if ($tahun)         $this->db->where('F.tahun'      , $tahun) ;
        
        return $this->db->count_all_results($this->table.' F') ;
    }
    
    public function save($folder_id) {
        if (!empty($folder_id) && is_numeric($folder_id)) {
            $file_id            = $this->input->post('file_id') ;
            $file_title_edit    = $this->input->post('file_title_edit') ;
            $file_order_edit    = $this->input->post('file_order_edit') ;
            
            if (!empty($file_id)) {
                $i = 0 ;
                foreach ($file_id as $fid) {
                    $data   = array() ;
                    $data['folder_id']      = $folder_id ;
                    $data['file_title']     = $file_title_edit[$i] ;
                    $data['file_order']     = $file_order_edit[$i] ;
                    
                    $this->db->where('file_id',$fid) ;
                    if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    
                    $i++ ;
                }
            }
            
            foreach (range(0,9) as $i) {
                // Upload File
                $file_id    = array() ;
                $temp_file  = $this->input->post('temp_file_name'.$i,TRUE) ;
                $file       = $this->file_model->upload('file_name'.$i,_PATH_UPLOAD_FILES_) ;
                $data   = array() ;
                
                $data['tahun']  = $this->input->post('tahun',TRUE) ;
                
                if ($file) {
                    $data['folder_id']   = $folder_id ;
                    $data['file_title']  = $this->input->post('file_title'.$i,TRUE) ;
                    $data['file_order']  = $this->input->post('file_order'.$i,TRUE) ;
                    $data['file_name']   = $file['file_name']  ;
                    $data['file_size']   = $file['file_size']  ;
                    $data['file_type']   = $file['file_type']  ;
                    $data['file_ext']    = $file['file_ext']   ;

                    if (!empty($temp_file)) {
                        $this->file_model->delete(_PATH_UPLOAD_FILES_   , $temp_file) ;
                    }
                }
                else {
                    if (!empty($temp_file)) $data['file_name']      = $temp_file ;
                }

                if (!empty($data['file_name'])) {

                    if (!empty($file_id[$i])) {
                        $this->db->where('file_id',$file_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }
            }

            return TRUE ;
        }
        
        return FALSE ;
    }
    
    public function delete($file_id) {
        if (!empty($file_id) && is_numeric($file_id)) {
            $this->db->where('file_id',$file_id) ;
            $file   = $this->db->get($this->table) ;
            
            if ($file->num_rows() > 0) {
                $file   = $file->row() ;
                $this->file_model->delete(_PATH_UPLOAD_FILES_ , $file->file_name) ;
            }
            
            $this->db->where('file_id',$file_id) ;
            return $this->db->delete($this->table) ;
        }
        
        return FALSE ;
    }
    
    public function hint($file_id,$field = 'download') {
        $sql    = "UPDATE ".$this->table."
                        SET file_hint_".$field." = file_hint_".$field." + 1
                            WHERE   1
                                    AND file_id = ".$this->db->escape($file_id)."" ;

        return $this->db->query($sql) ;
    }
}
/* End of file folder_file_model.php */
/* Location: ./application/models/folder_file_model.php */