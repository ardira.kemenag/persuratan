<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_in_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Surat_in_model extends CI_Model {
    
    public $table       = 'surat__in' ;
    
    public $table_sifat     = 'entitas__sifat' ;
    public $table_jenis     = 'entitas__jenis' ;
    public $table_document  = 'surat__indocument' ;
    
    public $table_aksi      = 'entitas__aksi' ;
    public $table_disposisi = 'disposisi__sublist' ;
    
    public $table_users = 'users__list' ;
    public $table_level = 'users__level' ;
    
    public $table_subdis= 'disposisi__sublist' ;
    
    public $table_inaksi        = 'surat__inaksi' ;
    public $table_indisposisi   = 'surat__indisposisi' ;
    public $table_into          = 'surat__into' ;
    
    public $table_agenda        = 'agenda__kegiatan' ;
    
    public function __construct() {
        parent::__construct();
        $this->load->model(array('file_model')) ;
    }
    
    public function get($config=array()) {
        $defaults = array(  'surat_id'          => NULL ,
                            'select_self'       => NULL ,
                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'distinct_id'       => NULL ,
                            
                            'date_start'    => NULL ,
                            'date_end'      => NULL ,
                            'month_start'   => NULL ,
                            'month_end'     => NULL ,
                            'tahun_start'   => NULL ,
                            'tahun_end'     => NULL ,
                            'month'         => NULL ,
                            'tahun'         => NULL ,
            
                            'keyword'           => NULL ,
                            
                            'sifat'             => FALSE ,
                            'sifat_id'          => NULL ,
                            'jenis'             => FALSE ,
                            'jenis_id'          => NULL ,
                            'aksi_id'           => NULL ,
                            'aksi_status'       => NULL ,

                            'surat_view'        => NULL ,
                            
                            'subdis_id'         => NULL ,
                            'user_id'           => NULL ,

                            'into'          => FALSE ,
                            'subdis_from'   => NULL  ,

                            'indisposisi'   => FALSE , 
                            'subdis_to'     => NULL  ,
                            'read_status'   => FALSE ,      
                            'respon_disposisi'=> NULL ,                     

                            'order'             => 'S.surat_id DESC, S.surat_date DESC' ,
                            'user'              => NULL ,
                            'reply'             => FALSE ,
                            'reply_id'          => NULL ,
                            
                            'agenda'            => FALSE ,
							'kegiatan_tempat'	=> NULL ,

                            'group'             => NULL );
 
	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $i = 0 ;
        if ($select_self)   $select[$i++]   = $select_self ;
        else {
            $select[$i++]   = "S.*" ;
            $select[$i++]   = "DATE_FORMAT(S.surat_date,'%Y/%m/%d') AS date_diff" ;
			$select[$i++]   = "DATE_FORMAT(S.surat_date,'%H:%i') AS time_diff" ;
			$select[$i++]   = "DATE_FORMAT(S.surat_date,'%Y') as Y, DATE_FORMAT(S.surat_date,'%m') as m, DATE_FORMAT(S.surat_date,'%d') as d,DATE_FORMAT(S.surat_date,'%H') as H, DATE_FORMAT(S.surat_date,'%i') as i, DATE_FORMAT(S.surat_date,'%s') as s"  ;
        
            $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%Y/%m/%d') AS taken_date_diff" ;
            $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%H:%i') AS taken_time_diff" ;
            $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%Y') as taken_Y, DATE_FORMAT(S.surat_date_taken,'%m') as taken_m, DATE_FORMAT(S.surat_date_taken,'%d') as taken_d,DATE_FORMAT(S.surat_date_taken,'%H') as taken_H, DATE_FORMAT(S.surat_date_taken,'%i') as taken_i, DATE_FORMAT(S.surat_date_taken,'%s') as taken_s"  ;
            
            $select[$i++]   = "DATE_FORMAT(S.surat_date_taken,'%d/%m/%Y') AS date_taken" ;

            if ($sifat)     $select[$i++]   = "Sf.sifat_name" ;
            if ($jenis)     $select[$i++]   = "Jn.jenis_name" ;
            if ($reply)     $select[$i++]   = "Ry.user_id AS reply_user_id, Ry.surat_number AS reply_surat_number,Ry.surat_from AS reply_surat_from,Ry.surat_perihal AS reply_surat_perihal,Ry.surat_date AS reply_surat_date,Ry.surat_date_taken AS reply_surat_date_taken" ;
            
            if ($user)      $select[$i++]   = "U.username,U.fullname,U.position,U.user_img" ;
            
            if ($into)          $select[$i++]   = "IN.into_id,IN.subdis_id AS subdis_from,IN.into_msg" ;
            if ($indisposisi)   $select[$i++]   = "DP.indisposisi_id,DP.subdis_id AS subdis_to,DP.respon_disposisi,DP.read_status,DP.read_date" ;
            
            // if ($agenda OR $jenis_id == 21) 
                // $select[$i++] = "AG.kegiatan_id
                                // ,   AG.kegiatan_date_start
                                // ,   AG.kegiatan_date_end
                                // ,   AG.kegiatan_time_start
                                // ,   AG.kegiatan_time_end
                                // ,   AG.kegiatan_tempat
                                // ,   AG.timezone" ;
        }
            
        $this->db->select(implode(',', $select),FALSE) ;
        
        $sql_keyword    = array() ;
        
        if ($sifat)  {
            $this->db->join($this->table_sifat.' Sf','Sf.sifat_id = S.sifat_id') ;
            if ($keyword)   $sql_keyword[]    =   " Sf.sifat_name LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
        }
        
        if ($jenis)  {
            $this->db->join($this->table_jenis.' Jn','Jn.jenis_id = S.jenis_id') ;
            if ($keyword)   $sql_keyword[]    =   " Jn.jenis_name LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
        }
        
        if ($keyword) {
            $sql_keyword[]  = " S.surat_number LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            $sql_keyword[]  = " S.surat_from LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            $sql_keyword[]  = " S.surat_perihal LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            
            if (count($sql_keyword) > 0)    $this->db->where("(".implode(' OR ',$sql_keyword).")",NULL,FALSE) ;
        }
        
        if ($reply)  $this->db->join($this->table.' Ry','Ry.surat_id = S.surat_reply_id','left') ;
        
        if ($user)   $this->db->join($this->table_users.' U','U.user_id = S.user_id') ;
        
        if ($aksi_id)   $this->db->join($this->table_inaksi.' HA'     , "HA.surat_id  = S.surat_id AND HA.aksi_id = ".$this->db->escape($aksi_id)." ") ;
        
        if ($agenda OR $jenis_id == 21) $this->db->join($this->table_agenda.' AG','AG.surat_id = S.surat_id' , 'left') ;
        
        if ($subdis_id) $where_own[]  = "S.subdis_id = ".$this->db->escape($subdis_id) ;

        if ($into OR $subdis_from OR $indisposisi OR $subdis_to OR $respon_disposisi OR $read_status)  {
            $this->db->join($this->table_into.' IN','IN.surat_id = S.surat_id') ;

            if ($subdis_from) $where_own[] = "IN.subdis_id = ".$this->db->escape($subdis_from) ;

            if ($indisposisi OR $subdis_to OR $respon_disposisi OR $read_status) {
                $this->db->join($this->table_indisposisi.' DP','DP.surat_id = S.surat_id AND DP.into_id = IN.into_id','left') ;
                if ($respon_disposisi)  $this->db->where('DP.respon_disposisi', $respon_disposisi) ;
                if ($read_status)       $this->db->where('DP.read_status'     , $read_status) ;

                if ($subdis_to)   $where_own[]  = "DP.subdis_id = ".$this->db->escape($subdis_to) ;
            }
        }

        

        if (!empty($where_own) && count($where_own) > 0) {
            $this->db->where('('.implode(' OR ',$where_own).')',NULL,FALSE) ;
        }
        
        if ($surat_id)      $this->db->where('S.surat_id'   , $surat_id) ;
        if ($distinct_id)   $this->db->where('S.surat_id <>', $distinct_id) ;
        if ($sifat_id)      $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($jenis_id)      $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($reply_id)      $this->db->where('S.surat_reply_id' , $reply_id) ;
        if ($user_id)       $this->db->where('S.user_id'        , $user_id) ;
        if ($surat_view)    $this->db->where('S.surat_view' , $surat_view) ;
        
        if ($date_start)    $this->db->where('S.surat_date >=',$date_start) ;
        if ($date_end)      $this->db->where('S.surat_date <=',$date_end) ; 
        if ($month_start)   $this->db->where('MONTH(S.surat_date) >=', $month_start) ;
        if ($month_end)     $this->db->where('MONTH(S.surat_date) <=', $month_end) ;
        if ($tahun_start)   $this->db->where('YEAR(S.surat_date) >=' , $tahun_start) ;
        if ($tahun_end)     $this->db->where('YEAR(S.surat_date) <=' , $tahun_end) ;

        if ($month)         $this->db->where('MONTH(S.surat_date)'  , $month) ;
        if ($tahun)         $this->db->where('YEAR(S.surat_date)'   , $tahun) ;

        $this->db->order_by($order) ;
        
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        
        $sql = $this->db->get_compiled_select($this->table.' S') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;
        
        if ($surat_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'distinct_id'       => NULL ,
                            
                            'date_start'    => NULL ,
                            'date_end'      => NULL ,
                            'month_start'   => NULL ,
                            'month_end'     => NULL ,
                            'tahun_start'   => NULL ,
                            'tahun_end'     => NULL ,
                            'month'         => NULL ,
                            'tahun'         => NULL ,
            
                            'keyword'           => NULL ,
                            'sifat_id'          => NULL ,
                            'jenis_id'          => NULL ,
                            'aksi_id'           => NULL ,
                            'aksi_status'       => NULL ,

                            'surat_view'        => NULL ,
                            
                            'subdis_id'         => NULL ,
                            'user_id'           => NULL ,

                            'subdis_from'   => NULL  ,

                            'subdis_to'     => NULL  ,
                            'read_status'   => FALSE ,      
                            'respon_disposisi'=> NULL ,                     

                            'reply_id'          => NULL ,
                            'group'             => NULL );
 
	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }
        
        if ($aksi_id)   {
            $this->db->join($this->table_inaksi.' HA'     , "HA.surat_id  = S.surat_id AND HA.aksi_id = ".$this->db->escape($aksi_id)) ;
            
            if (empty($group))  $group = 'S.surat_id' ;
        }

        if ($subdis_id) $where_own[]  = "S.subdis_id = ".$this->db->escape($subdis_id) ;

        if ($subdis_from)  {
            $this->db->join($this->table_into.' IN','IN.surat_id = S.surat_id') ;
            $where_own[] = "IN.subdis_id = ".$this->db->escape($subdis_from) ;
            
            if (empty($group))  $group = 'S.surat_id' ;
        }

        if ($subdis_to OR $respon_disposisi OR $read_status) {
            $this->db->join($this->table_indisposisi.' DP','DP.surat_id = S.surat_id','left') ;
            if ($respon_disposisi)  $this->db->where('DP.respon_disposisi', $respon_disposisi) ;
            if ($read_status)       $this->db->where('DP.read_status'     , $read_status) ;
            if ($subdis_to)         $where_own[]  = "DP.subdis_id = ".$this->db->escape($subdis_to) ;

            if (empty($group))  $group = 'S.surat_id' ;
        }

        if (!empty($where_own) && count($where_own) > 0) {
            $this->db->where('('.implode(' OR ',$where_own).')',NULL,FALSE) ;
        }
        
        if ($distinct_id)   $this->db->where('S.surat_id <>', $distinct_id) ;
        if ($sifat_id)      $this->db->where('S.sifat_id'   , $sifat_id) ;
        if ($jenis_id)      $this->db->where('S.jenis_id'   , $jenis_id) ;
        if ($reply_id)      $this->db->where('S.surat_reply_id' , $reply_id) ;
        if ($user_id)       $this->db->where('S.user_id'        , $user_id) ;
        if ($surat_view)    $this->db->where('S.surat_view' , $surat_view) ;
        
        if ($date_start)    $this->db->where('S.surat_date >=',$date_start) ;
        if ($date_end)      $this->db->where('S.surat_date <=',$date_end) ;
        if ($month_start)   $this->db->where('MONTH(S.surat_date) >=', $month_start) ;
        if ($month_end)     $this->db->where('MONTH(S.surat_date) <=', $month_end) ;
        if ($tahun_start)   $this->db->where('YEAR(S.surat_date) >=' , $tahun_start) ;
        if ($tahun_end)     $this->db->where('YEAR(S.surat_date) <=' , $tahun_end) ;

        if ($month)         $this->db->where('MONTH(S.surat_date)'  , $month) ;
        if ($tahun)         $this->db->where('YEAR(S.surat_date)'   , $tahun) ;

        if ($keyword)           {
            $sql_keyword    = array() ;
            
            $sql_keyword[]  = " S.surat_number LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            $sql_keyword[]  = " S.surat_from LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            $sql_keyword[]  = " S.surat_perihal LIKE ".$this->db->escape('%'.$keyword.'%')." " ;
            
            if (count($sql_keyword) > 0)    $this->db->where("(".implode(' OR ',$sql_keyword).")",NULL,FALSE) ;

        }
        
        if ($group) {
            $this->db->select('S.surat_id') ;
            $this->db->group_by($group) ;
            
            $sql = $this->db->get_compiled_select($this->table.' S') ;
        
            //$query  = $this->db->query($sql) ;
            $query  = apc_get($sql) ;

            return $query->num_rows() ;
        }
        else {
            return $this->db->count_all_results($this->table.' S') ;
        }
    }
    
    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('surat_id'            , 'Surat'           , 'is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('jenis_id'            , 'Jenis Surat'     , 'required|is_numeric|trim|xss_clean');
        $this->form_validation->set_rules('sifat_id'            , 'Sifat Surat'     , 'required|trim|xss_clean');
        $this->form_validation->set_rules('surat_number'        , 'Nomor Surat'     , 'required|trim|xss_clean');
        $this->form_validation->set_rules('surat_from'          , 'Sumber Surat'    , 'trim|xss_clean');
        $this->form_validation->set_rules('surat_date'          , 'Tanggal Kirim'   , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('surat_date_taken'    , 'Tanggal Terima'  , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_perihal'       , 'Perihal'         , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('surat_kode'          , 'Kode'            , 'trim|xss_clean') ;
        $this->form_validation->set_rules('surat_indek'         , 'Indek'           , 'trim|xss_clean') ;
        $this->form_validation->set_rules('from_status'         , 'Status Surat'    , 'trim|xss_clean|is_numeric') ;
        
        if (!empty($_POST['agenda_post'])) {
            $this->form_validation->set_rules('agenda_post' , 'Tampilan'    , 'required|trim|xss_clean');
        } 
        else {
            $this->form_validation->set_rules('surat_view'  , 'Tampilan'    , 'required|trim|xss_clean');
        }
        $this->form_validation->set_rules('user_id'             , 'Pengirim'        , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('surat_reply_id'      , 'Balasan dari'    , 'trim|xss_clean|is_numeric') ;
        
        $this->form_validation->set_rules('subdis_id'           , 'Unit'    , 'required|trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('fsubdis_id'          , 'Unit'    , 'required|trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('into_msg'            , 'Pesan'   , 'trim|xss_clean') ;

        $this->form_validation->set_error_delimiters('<span class="error_label">', '</span>');

        return $this->form_validation->run() ;
    }
    
    public function save() {
        $surat_id   = set_value('surat_id') ;
        
        $data['jenis_id']           = set_value('jenis_id') ;
        $data['sifat_id']           = set_value('sifat_id') ;
        $data['surat_number']       = set_value('surat_number') ;
        $data['surat_from']         = set_value('surat_from') ;
        $data['surat_date']         = set_value('surat_date') ;
        $data['surat_perihal']      = set_value('surat_perihal') ;
        $data['user_id']            = set_value('user_id') ;
        $data['subdis_id']          = set_value('subdis_id') ;
        $data['from_status']        = set_value('from_status') ;
		$data['kegiatan_tempat']    = set_value('kegiatan_tempat') ;
		$data['kegiatan_date_start']= set_value('kegiatan_date_start') ;
		$data['kegiatan_date_end']  = set_value('kegiatan_date_end') ;
		$data['kegiatan_time_start']= set_value('kegiatan_time_start') ;
		$data['kegiatan_time_end']  = set_value('kegiatan_time_end') ;
        
        $json = array('surat_to','surat_disposisi','surat_respon','surat_tembusan') ;
        foreach ($json as $j) {
            $p = $this->input->post($j) ;
            $data[$j] = !empty($p) ? json_encode($p) : NULL ;
        }

        $surat_view = set_value('surat_view') ;
        if ( $surat_view == 'Save As Draft' )                               $surat_view = 'Hidden' ;
        elseif ( $surat_view == 'Send' OR !empty($_POST['agenda_post']) )   $surat_view = 'Visible' ;
        $data['surat_view'] = $surat_view ;

        $no_empty   = array('surat_date_taken','respon_subdis_id','surat_reply_id','surat_kode','surat_indek') ;
        foreach ($no_empty as $n) {
            $m = set_value($n) ;
            if (!empty($m)) $data[$n]   = $m ;
        }
        
        $save   = FALSE ;
        if (!empty($surat_id)) {
            $this->db->where('surat_id',$surat_id) ;
            $save   = $this->db->update($this->table,$data) ;
        }
        else {
            $save       = $this->db->insert($this->table,$data) ;
            $surat_id   = $this->db->insert_id() ;
			
			
        }
        
        if ($save) {
            $CI =& get_instance();
            $CI->load->model(array( 'surat_indocument_model'
                                ,   'surat_inaksi_model'
								,   'agenda_model'
                                ,   'surat_indisposisi_model'
                                ,   'surat_into_model')) ;

            $into_id = $CI->surat_into_model->save_id($surat_id) ;

            $this->surat_indocument_model->save($surat_id) ;
            $CI->surat_inaksi_model->save($surat_id,$data['subdis_id']) ;

            if ($data['surat_view'] == 'Visible') {
                $CI->surat_indisposisi_model->save( $surat_id
                                                ,   $this->input->post('surat_disposisi')
                                                ,   $this->input->post('surat_respon')
                                                ,   $into_id
                                                ,   $this->input->post('surat_to')
                                                ,   $this->input->post('surat_tembusan') ) ;              
            }
			
            apc_clean() ;

            return $surat_id ;
        }
        
        return FALSE ;
    }
    
    public function delete($user_id,$surat_id,$level_id = NULL) {
        if (!empty($surat_id) && is_numeric($surat_id)) {
            $this->db->where('surat_id' , $surat_id) ;
            if ($level_id != 1) $this->db->where('user_id'  , $user_id) ;
            $cek    = $this->db->get($this->table) ;
            
            if ($cek->num_rows() > 0) {
                $this->db->where('surat_id',$surat_id) ;
                $documents  = $this->db->get($this->table_document) ;

                if ($documents->num_rows() > 0) {
                    foreach ($documents->result() as $doc) {
                        if (!empty($doc->document_filename) && file_exists( _PATH_UPLOAD_FILES_ . $doc->document_filename)) {
                            $this->file_model->delete( _PATH_UPLOAD_FILES_ , $doc->document_filename ) ;
                        }
                    }
                }

                $this->db->where('surat_id',$surat_id) ;
                if ($this->db->delete($this->table)) {
                    apc_clean() ;
                    return TRUE ;
                }
            }
            
            return FALSE ;
        }
        
        return FALSE ;
    }
    
    public function order_dw() {
        $option[''] = '- urutan -' ;
        $option['S.surat_date_taken DESC']  = 'Terbaru diterima' ;
        $option['S.surat_id DESC']          = 'Terbaru' ;
        $option['S.surat_date DESC']        = 'Terbaru masuk' ;
        
        return $option ;
    }
    
    public function date_dw() {
        $sql    = "SELECT DATE_FORMAT(surat_date, '%m/%Y') AS pd,
                            DATE_FORMAT(surat_date, '%M %Y') AS d
                        FROM ".$this->table."
                            WHERE surat_date > '0000-00-00'
                        GROUP BY month(surat_date), year(surat_date)
                        ORDER BY surat_date DESC ;" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        $data['']   = '- waktu kirim -' ;
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $q) {
                $data[$q->pd] = $q->d ;
            }
        }

        return $data ;
    }
    
    public function date_taken_dw() {
        $sql    = "SELECT DATE_FORMAT(surat_date_taken, '%m/%Y') AS pd,
                            DATE_FORMAT(surat_date_taken, '%M %Y') AS d
                        FROM ".$this->table."
                            WHERE surat_date_taken > '0000-00-00'
                        GROUP BY month(surat_date_taken), year(surat_date_taken)
                        ORDER BY surat_date_taken DESC ;" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;
        
        $data['']   = '- waktu terima -' ;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $q) {
                $data[$q->pd] = $q->d ;
            }
        }

        return $data ;
    }

    public function migration_to_agenda($surat_id,$post) {
        $surat_id   = !empty($surat_id) ? $surat_id : $this->input->post('surat_id',TRUE) ;
        
        if (!empty($surat_id)) {
            $postdata['surat_id']   = $surat_id ;
            $postdata['disposisi']  = TRUE ;
            $postdata['aksi']       = TRUE ;
            $postdata['agenda']     = TRUE ;
            
            $surat  = $this->get($postdata) ;
            
            if ($surat) {
                $CI =& get_instance();
                $CI->load->model(array('disposisi_sub_model','surat_inaksi_model')) ;
                $eselon_dw = $CI->disposisi_sub_model->get(array('make_array'=>TRUE)) ;

                $aksi = $CI->surat_inaksi_model->get(array('surat_id'=>$surat_id,'aksi'=>TRUE)) ;
                if ($aksi->num_rows() > 0) {
                    foreach ($aksi->result() as $a) {
                        $aksi_text[]    = $a->aksi_name ;
                    }
                }

                $data['surat_id']               = $surat_id ;
                $data['kegiatan_name']          = $surat['surat_perihal'] ;
                $data['kegiatan_penyelenggara'] = $surat['surat_from'] ;
                $data['kegiatan_peserta']       = array_to_list($eselon_dw,json_decode($surat['surat_disposisi'],TRUE),',') ;
                $data['user_id']                = $surat['user_id'] ;
                $data['kegiatan_date']          = date('Y-m-d H:i:s') ;
                $data['kegiatan_keterangan']    = !empty($aksi_text) ? implode(', ',$aksi_text) : '-' ;
                
                $data['kegiatan_date_start']    = !empty($post['kegiatan_date_start'])  ? $post['kegiatan_date_start']  : '' ;
                $data['kegiatan_date_end']      = !empty($post['kegiatan_date_end'])    ? $post['kegiatan_date_end']    : '' ;
                $data['kegiatan_time_start']    = !empty($post['kegiatan_time_start'])  ? $post['kegiatan_time_start']  : '' ;
                $data['kegiatan_time_end']      = !empty($post['kegiatan_time_end'])    ? $post['kegiatan_time_end']    : '' ;
                $data['kegiatan_tempat']        = !empty($post['kegiatan_tempat'])      ? $post['kegiatan_tempat']      : '' ;
                $data['timezone']               = !empty($post['timezone'])             ? $post['timezone']             : '' ;

                if (!empty($surat['kegiatan_id'])) {
                    $this->db->where('kegiatan_id',$surat['kegiatan_id']) ;
                    if ( $this->db->update($this->table_agenda,$data)) {
                        return $surat['kegiatan_id'] ;
                    }
                }
                else {
                    if ($this->db->insert($this->table_agenda,$data)) {
                        return $this->db->insert_id() ;                        
                    }
                }
                    
            }
        }
        
        return FALSE ;
    }

    public function find_eselon($subdis_from) {
        $this->db->select('subdis_eselon') ;
        $this->db->where('subdis_id',$subdis_from) ;
        $query  = $this->db->get($this->table_disposisi) ;
        if ($query->num_rows() > 0) {
            return $query->row()->subdis_eselon ;
        }

        return 2 ;
    }
	
	public function ubah_status($surat_id){
		$data = array(
               'surat_view' => 'Hidden'
            );
		$this->db->where('surat_id', $surat_id);
		$this->db->update('surat__in', $data); 
		
	}
}
/* End of file Surat_in_model.php */
/* Location: ./application/models/Surat_in_model.php */