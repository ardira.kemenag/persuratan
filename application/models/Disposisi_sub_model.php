<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Disposisi_Sub_Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Disposisi_sub_model extends CI_Model {

    public $table           = 'disposisi__sublist' ;
    public $table_hub       = 'hub__surat_disposisi' ;
    public $table_hub_responbility  = 'hub__surat_responbility' ;
    public $table_surat     = 'surat__list' ;
    public $table_disposisi = 'disposisi__list' ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'subdis_id'         => NULL ,
                            'disposisi_id'      => NULL ,
                            'disposisi'         => FALSE ,
                            'subdis_parent'     => NULL ,
                            'subdis_eselon'     => NULL ,
                            'where_in'          => NULL ,
                            'make_array'        => FALSE,
                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'order'             => 'C.subdis_order' ,
                            'start_date'        => NULL ,
                            'to_date'           => NULL ,
                            'num'               => FALSE );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $i = 0 ;
        $select[$i++]   = "C.*" ;
        if ($num)       $select[$i++]   = "COUNT(CP.subdis_id) AS num" ;
        if ($disposisi) $select[$i++]   = "D.disposisi_name" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($subdis_id)     $this->db->where('C.subdis_id'    , $subdis_id) ;
        if ($where_in)      $this->db->where_in('C.subdis_id'    , $where_in) ;
        if ($disposisi_id)  $this->db->where('C.disposisi_id' , $disposisi_id) ;
        if ($subdis_parent !== NULL)    $this->db->where('C.subdis_parent',$subdis_parent) ;
        if ($subdis_eselon) $this->db->where('C.subdis_eselon', $subdis_eselon) ;
        
        if ($num) {
            $this->db->join($this->table_hub.' CP','C.subdis_id = CP.subdis_id','left') ;
            $this->db->group_by('C.subdis_id') ;

            if ($start_date OR $to_date) {
                $this->db->join($this->table_surat.' A','A.surat_id = CP.surat_id','left') ;
                if ($start_date)    $this->db->where("DATE_FORMAT(A.surat_date,'%Y/%m/%d') >= '".$start_date."'" , NULL,FALSE) ;
                if ($to_date)       $this->db->where("DATE_FORMAT(A.surat_date,'%Y/%m/%d') <= '".$to_date."'"    , NULL,FALSE) ;
            }
        }
        
        if ($disposisi)     $this->db->join($this->table_disposisi.' D','D.disposisi_id = C.disposisi_id') ;

        if ($make_array)    $this->db->order_by('subdis_order ASC') ;
        elseif ($order)     $this->db->order_by($order) ;
        else                $this->db->order_by('subdis_order ASC, subdis_name ASC') ;

        if ($limit)         $this->db->limit($limit,$page) ;

        $sql = $this->db->get_compiled_select($this->table.' C') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($subdis_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            return FALSE ;
        }
        elseif ($make_array) {
            //$rs[''] = '- disposisi -' ;
            if ($query->num_rows() >0) {
                foreach($query->result() as $r) {
                    $rs[$r->subdis_id] = $r->subdis_man ;
                }
            }
            return $rs ;
        }

        return $query ;
    }

    public function get_from_surat($surat_id = NULL, $make_array = FALSE , $disposisi_id = NULL) {
        if ($surat_id) {
            $sql    = "SELECT C.*
                            FROM ". $this->table ." C ,
                                 ". $this->table_hub ." CP
                      WHERE 1
                            AND C.subdis_id   = CP.subdis_id
                            AND CP.surat_id   = '".$surat_id."'
                      ORDER BY C.subdis_name DESC " ;

            //$query  = $this->db->query($sql) ;
            $query  = apc_get($sql) ;

            if ($make_array) {
                $d  = array() ;

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $c) {
                        $d[]    = $c->subdis_id ;
                    }
                }

                return $d ;
            }

            return $query ;
        }

        return NULL ;
    }

    public function get_from_respon($surat_id = NULL, $make_array = FALSE , $disposisi_id = NULL) {
        if ($surat_id) {
            $sql    = "SELECT C.*
                            FROM ". $this->table ." C ,
                                 ". $this->table_hub_responbility ." CP
                      WHERE 1
                            AND C.subdis_id   = CP.subdis_id
                            AND CP.surat_id   = '".$surat_id."'
                      ORDER BY C.subdis_name DESC " ;

            //$query  = $this->db->query($sql) ;
            $query  = apc_get($sql) ;

            if ($make_array) {
                $d  = array() ;

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $c) {
                        $d[]    = $c->subdis_id ;
                    }
                }

                return $d ;
            }

            return $query ;
        }

        return NULL ;
    }
    
    public function save() {
        $disposisi_id   = $this->input->post('disposisi_id') ;
        $subdis_id      = $this->input->post('subdis_id') ;
        $subdis_name    = $this->input->post('subdis_name') ;
        $subdis_order   = $this->input->post('subdis_order') ;
        $subdis_parent  = $this->input->post('subdis_parent') ;
        $subdis_kode    = $this->input->post('subdis_kode') ;
        $subdis_eselon  = $this->input->post('subdis_eselon') ;
        $subdis_man     = $this->input->post('subdis_man') ;

        if (count($subdis_name) > 0) {
            $i = 0 ;
            foreach ($subdis_name as $tm) {
                if (!empty($tm)) {
                    $data['subdis_name']    = $tm ;
                    $data['disposisi_id']   = $disposisi_id[$i] ;
                    $data['subdis_order']   = !empty($subdis_order[$i]) ? $subdis_order[$i]  : 99 ;
                    $data['subdis_parent']  = !empty($subdis_parent[$i])? $subdis_parent[$i] : 0 ;
                    $data['subdis_kode']    = !empty($subdis_kode[$i])  ? $subdis_kode[$i]   : '' ;
                    $data['subdis_eselon']  = !empty($subdis_eselon[$i])? $subdis_eselon[$i] : '' ;
                    $data['subdis_man']     = !empty($subdis_man[$i])   ? $subdis_man[$i]    : '' ;

                    if (!empty($subdis_id[$i])) {
                        $this->db->where('subdis_id',$subdis_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ; 
        }

        return TRUE ;
    }

    // DELETE
    public function delete($subdis_id) {
        if (!empty($subdis_id)) {
            $this->db->where('subdis_id',$subdis_id) ;
            if ($this->db->delete($this->table)) {
                $this->db->where('subdis_id',$subdis_id) ;
                if ($this->db->delete($this->table_hub)) {
                    
                    apc_clean() ;

                    return TRUE ;
                }
            }
        }

        return FALSE ;
    }

    public function num($subdis_id = NULL) {
        if ($subdis_id) {
            $this->db->where('subdis_id',$subdis_id) ;

            return $this->db->count_all_results($this->table_hub) ;
        }
        else {
            return $this->db->count_all_results($this->table) ;
        }
    }
    
    public function show_tree($config = array()) {
        $defaults = array(  'make_array'    => FALSE ,
                            'array_single'  => FALSE , 
                            'array_id'      => 'subdis_id' ,
                            'array_name'    => 'subdis_name' ,
                            'subdis_id'     => NULL ,
                            'where_in'      => NULL ,
                            'distinct_id'   => NULL ,
                            'subdis_parent' => NULL ,
                            'disposisi_id'  => NULL ,
                            'n1'    => TRUE ,
                            'n2'    => TRUE ,
                            'n3'    => TRUE ,
                            'space'         => 0 ,
                            'data'          => array() );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $where  = "" ;
        if ($subdis_id)                 $where .= " AND subdis_id = ".$this->db->escape($subdis_id)." " ;
        if ($where_in)                  $where .= " AND subdis_id IN (".implode(',',$where_in).")" ;
        if ($distinct_id)               $where .= " AND subdis_id <> ".$this->db->escape($distinct_id)." " ;
        if ($subdis_parent !== NULL )   $where .= " AND subdis_parent = ".$this->db->escape($subdis_parent)." " ;
        if ($disposisi_id)              $where .= " AND disposisi_id = ".$this->db->escape($disposisi_id)." " ;

        $sql    = "SELECT  subdis_id, level , subdis_name AS sdname
                           , CONCAT( REPEAT('.. ', RIGHT(level,1)), subdis_name) AS subdis_name 
                           , CONCAT( REPEAT('.. ', RIGHT(level,1)), subdis_man) AS subdis_man   
                    FROM (
                            SELECT  E.subdis_id      , E.subdis_name  ,
                                    E.subdis_parent  , E.subdis_order , 
                                    E.subdis_man     ,
                               (
                                SELECT CONCAT(LPAD(E1.subdis_order, 5, '0'),'~',0)
                                    FROM ".$this->table." E1
                                        WHERE 1
                                              AND E1.subdis_id = E.subdis_id
                                              AND E1.subdis_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.subdis_order, 5, '0'), '.',
                                              LPAD(E2.subdis_order, 5, '0'), '~',1)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.subdis_id = E2.subdis_parent)
                                        WHERE 1
                                              AND E2.subdis_id = E.subdis_id
                                              AND E1.subdis_parent = 0 " ;

        if ($n1)
        $sql    .=  "           UNION

                                SELECT CONCAT(LPAD(E1.subdis_order, 5, '0'), '.',
                                              LPAD(E2.subdis_order, 5, '0'), '.',
                                              LPAD(E3.subdis_order, 5, '0'), '~',2)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.subdis_id = E2.subdis_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.subdis_id = E3.subdis_parent)
                                        WHERE 1
                                              AND E3.subdis_id = E.subdis_id
                                              AND E1.subdis_parent = 0 " ;

        if ($n2)
        $sql    .=  "           UNION              

                                SELECT CONCAT(LPAD(E1.subdis_order, 5, '0'), '.',
                                              LPAD(E2.subdis_order, 5, '0'), '.',
                                              LPAD(E3.subdis_order, 5, '0'), '~',
                                              LPAD(E4.subdis_order, 5, '0'), '~',3)
                                    FROM disposisi__sublist E1
                                        INNER JOIN disposisi__sublist E2
                                                ON (E1.subdis_id = E2.subdis_parent)
                                        INNER JOIN disposisi__sublist E3
                                                ON (E2.subdis_id = E3.subdis_parent)
                                        INNER JOIN disposisi__sublist E4
                                                ON (E3.subdis_id = E4.subdis_parent)
                                        WHERE 1
                                              AND E4.subdis_id = E.subdis_id
                                              AND E1.subdis_parent = 0   " ;  

        
        if ($n3)
        $sql    .=  "           UNION              
                                              
                                SELECT CONCAT(LPAD(E1.subdis_order, 5, '0'), '.',
                                              LPAD(E2.subdis_order, 5, '0'), '.',
                                              LPAD(E3.subdis_order, 5, '0'), '~',
                                              LPAD(E4.subdis_order, 5, '0'), '~',
                                              LPAD(E5.subdis_order, 5, '0'), '~',4)
                                    FROM disposisi__sublist E1
                                        INNER JOIN disposisi__sublist E2
                                                ON (E1.subdis_id = E2.subdis_parent)
                                        INNER JOIN disposisi__sublist E3
                                                ON (E2.subdis_id = E3.subdis_parent)
                                        INNER JOIN disposisi__sublist E4
                                                ON (E3.subdis_id = E4.subdis_parent)
                                        INNER JOIN disposisi__sublist E5
                                                ON (E4.subdis_id = E5.subdis_parent)        
                                        WHERE 1
                                              AND E5.subdis_id = E.subdis_id
                                              AND E1.subdis_parent = 0 " ;                            

        $sql    .= " ) AS level
                        FROM ".$this->table." E
                            WHERE 1 ".$where."
                            ORDER BY SUBSTRING_INDEX(level,'~',1),subdis_order
                    ) T WHERE 1
                              AND level <> '' ;" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($array_single)    $data['']   = '' ;

        foreach ($query->result() as $q) {
            $data[$q->$array_id]  = $q->$array_name ;
        }

        return $data ;
    }

    public function runutan($surat_number) {
        $sql    = "SELECT HSD.subdis_id, DS.subdis_name 
                        FROM ".$this->table_hub." HSD 
                            INNER JOIN ".$this->table." DS 
                                ON DS.subdis_id = HSD.subdis_id
                        WHERE surat_id IN (SELECT surat_id FROM ".$this->table_surat." WHERE surat_number = ".$this->db->escape($surat_number).")
                    GROUP BY HSD.subdis_id
                    ORDER BY DS.subdis_order" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $a) {
                $results[]  = $a->subdis_name ;
            }

            return implode(' &raquo; ', $results) ;
        }            
        else {
            return NULL ;
        }
    }
	
	public function ambil_subdis_pangkalan(){
        $this->db->select('*');
        $this->db->from('disposisi__sublist');
        $this->db->like('subdis_name', 'UPT Pangkalan');
        return $query = $this->db->get();
    }  

    public function ambil_subdis_stasiun(){
        $this->db->select('*');
        $this->db->from('disposisi__sublist');
        $this->db->like('subdis_name', 'UPT Stasiun');
        return $query = $this->db->get();
    } 
}

/* End of file disposisi_sub_model.php */
/* Location: ./application/models/disposisi_sub_model.php */