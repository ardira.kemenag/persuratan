<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_aksi_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_aksi_model extends CI_Model {
    
    public $table           = 'entitas__aksi' ;
    public $table_surat     = 'surat__list' ;
    public $table_hub       = 'surat__inaksi' ;
    public $table_hub_user  = 'hub__surat_to_user' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'aksi_id'  => NULL ,
                            'surat'     => FALSE ,
                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL ,
                            'user_id'   => NULL ,
                            'to_user_id'=> NULL ,
                            'order'     => 'T.aksi_order ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($aksi_id)      $this->db->where('T.aksi_id' , $aksi_id) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($aksi_id OR $array OR $json) {
            $result = ($aksi_id OR $json) ? FALSE : array(''=>'- jenis aksi -') ;
            if ($query->num_rows() > 0) {
                if ($aksi_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->aksi_id]   = $p->aksi_name ;
                        else        $result[]              = $p->aksi_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function get_from_surat($surat_id = NULL, $make_array = FALSE) {
        if ($surat_id) {
            $sql    = "SELECT C.*
                            FROM ". $this->table ." C ,
                                 ". $this->table_hub ." CP
                      WHERE 1
                            AND C.aksi_id   = CP.aksi_id
                            AND CP.surat_id = '".$surat_id."'
                      ORDER BY C.aksi_name DESC " ;


            //$query  = $this->db->query($sql) ;
            $query  = apc_get($sql) ;

            if ($make_array) {
                $d  = array() ;

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $c) {
                        $d[]    = $c->aksi_id ;
                    }
                }

                return $d ;
            }

            return $query ;
        }

        return NULL ;
    }

    public function num($config = array()) {
        $defaults = array(  'aksi_id'   => NULL
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($aksi_id)   $this->db->where('T.aksi_id' , $aksi_id) ;

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($aksi_id) {
        if (is_numeric($aksi_id)) {
            $this->db->where('aksi_id',$aksi_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function save() {
        $aksi_id        = $this->input->post('aksi_id') ;
        $aksi_name      = $this->input->post('aksi_name') ;
        $aksi_akronim   = $this->input->post('aksi_akronim') ;
        $aksi_order     = $this->input->post('aksi_order') ;

        if (count($aksi_name) > 0) {
            $i = 0 ;
            foreach ($aksi_name as $tm) {
                if (!empty($tm)) {
                    $data   = array() ;
                    $data['aksi_name']      = $tm ;
                    $data['aksi_akronim']   = $aksi_akronim[$i] ;
                    $data['aksi_order']     = !empty($aksi_order[$i]) ? $aksi_order[$i] : 99 ;
                    if (!empty($aksi_id[$i])) {
                        $this->db->where('aksi_id',$aksi_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;
        }

        return TRUE ;
    }
}
/* End of file Entitas_aksi_model.php */
/* Location: ./application/models/Entitas_aksi_model.php */