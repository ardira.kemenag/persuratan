<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_klasifikasi_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_klasifikasi_model extends CI_Model {
    
    public $table       = 'entitas__klasifikasi' ;
    public $table_surat = 'surat__out' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'klasifikasi_id'   => NULL ,
                            'klasifikasi_kode' => NULL ,
                            'klasifikasi_parent' => NULL ,
                            'is_parent' => FALSE ,
                            'array_id'  => 'klasifikasi_id' ,
                            'array_name'=> 'klasifikasi_name' ,
                            'kode_in'   => NULL ,
                            'parent'    => NULL ,
                            'surat_num' => FALSE ,
                            'array'     => FALSE ,
                            'json'      => FALSE ,
                            'page'      => 0 ,
                            'limit'     => NULL ,
                            'group'     => NULL         ,
                            'order'     => 'klasifikasi_order ASC'
                         );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        if ($parent)    $select[$i++]   = "S.klasifikasi_name AS parent_name, S.klasifikasi_kode AS parent_kode" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($parent)    $this->db->join($this->table.' S','S.klasifikasi_parent = T.klasifikasi_id') ;

        if ($klasifikasi_id)        $this->db->where('T.klasifikasi_id'      , $klasifikasi_id) ;
        if ($klasifikasi_kode)      $this->db->where('T.klasifikasi_kode'    , $klasifikasi_kode) ;
        if ($kode_in)               $this->db->where_in('T.klasifikasi_kode' , $kode_in) ;
        if ($klasifikasi_parent)    $this->db->where('T.klasifikasi_parent'  , $klasifikasi_parent) ;
        if ($is_parent)             $this->db->where('T.klasifikasi_parent'  , 0) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($klasifikasi_id OR $array OR $json) {
            $result = ($klasifikasi_id OR $json) ? FALSE : array(''=>'- klasifikasi -') ;
            if ($query->num_rows() > 0) {
                if ($klasifikasi_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->$array_id] = $p->$array_name ;
                        else        $result[]                   = $p->$array_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'klasifikasi_kode'  => NULL ,
                            'kode_in'           => NULL ,
                            'klasifikasi_parent'=> NULL ,
                            'group'             => NULL 
                         );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }

        if ($klasifikasi_kode)      $this->db->where('T.klasifikasi_kode'   , $klasifikasi_kode) ;
        if ($klasifikasi_parent)    $this->db->where('T.klasifikasi_parent'  , $klasifikasi_parent) ;
        if ($kode_in)               $this->db->where_in('T.klasifikasi_kode' , $kode_in) ;

        if ($group) {
            $this->db->select('T.klasifikasi_id') ;
            $this->db->group_by($group) ;

            $query = $this->db->get($this->table.' T') ;

            return $query->num_rows() ;
        }

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($klasifikasi_id) {
        if (!empty($klasifikasi_id) && is_numeric($klasifikasi_id)) {
            $this->db->where('klasifikasi_id',$klasifikasi_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }
    
    public function save() {
        $klasifikasi_id     = $this->input->post('klasifikasi_id') ;
        $klasifikasi_name   = $this->input->post('klasifikasi_name') ;
        $klasifikasi_kode   = $this->input->post('klasifikasi_kode') ;
        $klasifikasi_parent = $this->input->post('klasifikasi_parent') ;
        $klasifikasi_order  = $this->input->post('klasifikasi_order') ;

        if (count($klasifikasi_name) > 0) {
            $i = 0 ;
            foreach ($klasifikasi_name as $tm) {
                if (!empty($tm)) {
                    $data['klasifikasi_name']   = $tm ;
                    $data['klasifikasi_kode']   = !empty($klasifikasi_kode[$i])  ? $klasifikasi_kode[$i]  : '' ;
                    $data['klasifikasi_parent']  = !empty($klasifikasi_parent[$i]) ? $klasifikasi_parent[$i] : '' ;
                    $data['klasifikasi_order']  = !empty($klasifikasi_order[$i]) ? $klasifikasi_order[$i] : '' ;
                    
                    if (!empty($klasifikasi_id[$i])) {
                        $this->db->where('klasifikasi_id',$klasifikasi_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;
            return TRUE ;
        }

        return FALSE ;
    }

    public function show_tree($config = array()) {
        $defaults = array(  'make_array'    => FALSE ,
                            'klasifikasi_id'     => NULL ,
                            'where_in'      => NULL ,
                            'distinct_id'   => NULL ,
                            'klasifikasi_parent' => NULL ,
                            'kode_in'       => NULL ,
                            'is_not_parent' => FALSE ,
                            'keycode'       => NULL ,
                            'space'         => 0 ,
                            'normal'        => FALSE ,
                            'data'          => array() );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }

        $where  = "" ;
        if ($klasifikasi_id)                $where .= " AND klasifikasi_id = ".$this->db->escape($klasifikasi_id)." " ;
        if ($where_in)                      $where .= " AND klasifikasi_id IN (".implode(',',$where_in).")" ;
        if ($distinct_id)                   $where .= " AND klasifikasi_id <> ".$this->db->escape($distinct_id)." " ;
        if ($klasifikasi_parent !== NULL )  $where .= " AND klasifikasi_parent = ".$this->db->escape($klasifikasi_parent)." " ;
        if ($is_not_parent)                 $where .= " AND klasifikasi_parent <> 0 " ;
        if ($keycode)                       $where .= " AND klasifikasi_kode LIKE ".$this->db->escape('%'.$keycode.'%')." " ;
        if ($kode_in) {
            if (!is_array($kode_in)) {
                $where .= " AND klasifikasi_kode = ".$this->db->escape($kode_in)." " ;
            }
            else {
                foreach ($kode_in as $k) {
                    $kin[]  = $this->db->escape($k) ;
                }
                $where .= " AND klasifikasi_kode IN (".implode(',',$kin).") " ;
            }
        }

        $sql    = "SELECT  klasifikasi_id, level , klasifikasi_kode , klasifikasi_parent ,
                           CONCAT( REPEAT('.. ', RIGHT(level,1)),'[',klasifikasi_kode,'] ', klasifikasi_name) AS klasifikasi_name 
                    FROM (
                            SELECT  E.klasifikasi_id      , E.klasifikasi_name  ,
                                    E.klasifikasi_parent  , E.klasifikasi_order ,
                                    E.klasifikasi_kode ,
                               (
                                SELECT CONCAT(LPAD(E1.klasifikasi_order, 5, '0'),'~',0)
                                    FROM ".$this->table." E1
                                        WHERE 1
                                              AND E1.klasifikasi_id = E.klasifikasi_id
                                              AND E1.klasifikasi_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E2.klasifikasi_order, 5, '0'), '~',1)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.klasifikasi_id = E2.klasifikasi_parent)
                                        WHERE 1
                                              AND E2.klasifikasi_id = E.klasifikasi_id
                                              AND E1.klasifikasi_parent = 0

                                UNION

                                SELECT CONCAT(LPAD(E1.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E2.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E3.klasifikasi_order, 5, '0'), '~',2)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.klasifikasi_id = E2.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.klasifikasi_id = E3.klasifikasi_parent)
                                        WHERE 1
                                              AND E3.klasifikasi_id = E.klasifikasi_id
                                              AND E1.klasifikasi_parent = 0

                                UNION              

                                SELECT CONCAT(LPAD(E1.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E2.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E3.klasifikasi_order, 5, '0'), '~',
                                              LPAD(E4.klasifikasi_order, 5, '0'), '~',3)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.klasifikasi_id = E2.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.klasifikasi_id = E3.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E4
                                                ON (E3.klasifikasi_id = E4.klasifikasi_parent)
                                        WHERE 1
                                              AND E4.klasifikasi_id = E.klasifikasi_id
                                              AND E1.klasifikasi_parent = 0     

                                UNION              
                                              
                                SELECT CONCAT(LPAD(E1.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E2.klasifikasi_order, 5, '0'), '.',
                                              LPAD(E3.klasifikasi_order, 5, '0'), '~',
                                              LPAD(E4.klasifikasi_order, 5, '0'), '~',
                                              LPAD(E5.klasifikasi_order, 5, '0'), '~',4)
                                    FROM ".$this->table." E1
                                        INNER JOIN ".$this->table." E2
                                                ON (E1.klasifikasi_id = E2.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E3
                                                ON (E2.klasifikasi_id = E3.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E4
                                                ON (E3.klasifikasi_id = E4.klasifikasi_parent)
                                        INNER JOIN ".$this->table." E5
                                                ON (E4.klasifikasi_id = E5.klasifikasi_parent)        
                                        WHERE 1
                                              AND E5.klasifikasi_id = E.klasifikasi_id
                                              AND E1.klasifikasi_parent = 0                            

                           ) AS level
                        FROM ".$this->table." E
                            WHERE 1 ".$where."
                            ORDER BY SUBSTRING_INDEX(level,'~',1), klasifikasi_order
                    ) T WHERE 1
                              AND level <> '' ;" ;

        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;                      

        if ($make_array)    $data['']   = '- - -' ;

        foreach ($query->result() as $q) {
            $data[ $normal ? $q->klasifikasi_id : ( !empty($q->klasifikasi_parent) ? $q->klasifikasi_id : $q->klasifikasi_kode ) ]  = $q->klasifikasi_name ;
        }

        return $data ;
    }
}
/* End of file Entitas_klasifikasi_model.php */
/* Location: ./application/models/Entitas_klasifikasi_model.php */