<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas_model extends CI_Model {
    
    public $table   = 'entitas__list' ;

    public function __construct() {
        parent::__construct();
    }
    
    public function get($config=array()) {
        $defaults = array(  'entitas_id'    => NULL ,
                            'entitas_name'  => NULL ,
                            'having'        => NULL ,
                            'page'          => 0    ,
                            'limit'         => NULL ,
                            'group'         => NULL ,
                            'order'         => 'B.entitas_name ASC'
                         );

    	foreach ($defaults as $key => $val) {
                $$key = ( ! isset($config[$key])) ? $val : $config[$key];
    	}
        
        $i = 0 ;
        $select[$i++]   = "B.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($entitas_id)    $this->db->where('B.entitas_id',$entitas_id) ;
        if ($entitas_name)  $this->db->where('B.entitas_name',trim($entitas_name)) ;

        if ($having)    $this->db->having($having) ;
        
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;
        
        $sql = $this->db->get_compiled_select($this->table.' B') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($entitas_id OR $entitas_name) {
            $result = FALSE ;
            if ($query->num_rows() > 0) {
                return $query->row_array() ;
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config=array()) {
        $defaults = array(  'group'         => NULL ,
                         );

    	foreach ($defaults as $key => $val) {
                $$key = ( ! isset($config[$key])) ? $val : $config[$key];
    	}
        
        if ($group) {
            $this->db->select('B.entitas_id') ;
            
            $this->db->group_by($group) ;
            
            $query  = $this->db->get($this->table.' B') ;
            
            return $query->num_rows() ;
        }
        
        return $this->db->count_all_results($this->table.' B') ;
    }
    
    public function validation() {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('entitas_id'       , 'ID'       , 'trim|xss_clean|is_numeric') ;
        $this->form_validation->set_rules('entitas_name'     , 'Nama'     , 'required|trim|xss_clean') ;
        $this->form_validation->set_rules('entitas_text'     , 'Variable' , 'required|trim|xss_clean') ;
        
        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }
    
    public function save() {
        $entitas_id    = set_value('entitas_id') ;
        
        $data['entitas_name']   = set_value('entitas_name') ;
        $data['entitas_text']   = set_value('entitas_text') ;
        
        if (!empty($entitas_id)) {
            $this->db->where('entitas_id',$entitas_id) ;
            $save = $this->db->update($this->table,$data) ;
        }
        else {
            $save = $this->db->insert($this->table,$data) ;
        }

        if ($save) {
            apc_clean() ;
            return TRUE ;
        }

        return FALSE ;
    }
    
    public function delete($entitas_id) {
        if (!empty($entitas_id) && is_numeric($entitas_id)) {
            $this->db->where('entitas_id',$entitas_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }
        
        return FALSE ;
    }

    public function  get_var($entitas_name) {
        if (!empty($entitas_name)) {
            $entitas    = $this->get(array('entitas_name'=>$entitas_name)) ;
            if (!empty($entitas)) {
                $entitas    = explode(';',$entitas['entitas_text']) ;

                return $entitas ;
            }
        }

        return array() ;
    }
    
}
/* End of file Entitas_model.php */
/* Location: ./application/models/Entitas_model.php */