<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Agenda_status_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Agenda_status_model extends CI_Model {
    
    public $table           = 'agenda__status' ;
    public $table_kegiatan  = 'agenda__kegiatan' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'status_id'     => NULL ,
                            'kegiatan_num'  => FALSE ,
                            'array'         => FALSE ,
                            'json'          => FALSE ,
                            'page'          => 0 ,
                            'limit'         => NULL ,
                            'group'         => NULL ,
                            'order'     => 'status_name ASC'
                         );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        if ($kegiatan_num)  {
            $sqlkegiatan = "(SELECT COUNT(*) FROM ".$this->table_kegiatan." L " ;
            
            
            $sqlkegiatan   .= " WHERE L.status_id = T.status_id " ;

            $sqlkegiatan   .= " ) AS kegiatan_num" ;
            
            $select[$i++] = $sqlkegiatan ;
        }
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($status_id)      $this->db->where('T.status_id' , $status_id) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        /*$this->db->from($this->table.' P') ;
        $sql = $this->db->_compile_select() ;
        $this->db->_reset_select();

        echo $sql ; exit (0) ; */

        $query  = $this->db->get($this->table.' T') ;

        if ($status_id OR $array OR $json) {
            $result = ($status_id OR $json) ? FALSE : array(''=>'- status kegiatan -') ;
            if ($query->num_rows() > 0) {
                if ($status_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->status_id]  = $p->status_name ;
                        else        $result[]               = $p->status_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'status_id'   => NULL
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($status_id)   $this->db->where('T.status_id' , $status_id) ;

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($status_id) {
        if (is_numeric($status_id)) {
            $this->db->where('status_id',$status_id) ;
            return $this->db->delete($this->table)  ;
        }

        return FALSE ;
    }

    public function save() {
        $status_id        = $this->input->post('status_id') ;
        $status_name      = $this->input->post('status_name') ;

        if (count($status_name) > 0) {
            $i = 0 ;
            foreach ($status_name as $tm) {
                if (!empty($tm)) {
                    $data['status_name']      = $tm ;
                    if (!empty($status_id[$i])) {
                        $this->db->where('status_id',$status_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }
        }

        return TRUE ;
    }
}
/* End of file agenda_status_model.php */
/* Location: ./application/models/agenda_status_model.php */