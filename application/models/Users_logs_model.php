<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Users Logs Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Users_logs_model extends CI_Model {

    public $table       = 'users__logs'   ;
    public $tableUser   = 'users__list'     ;
    public $tableLevel  = 'users__level'    ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config=array()) {
        $defaults = array(  'log_id'    => NULL ,
                            'user_id'   => NULL ,
                            'distinct_id' => NULL ,
                            'long_time' => FALSE ,
                            'user'      => FALSE ,
                            'level_id'  => NULL ,
                            'keyword'   => NULL ,
                            'date'      => NULL ,
                            'page'      => 0    ,
                            'limit'     => NULL ,
                            'level'     => FALSE,
                            'mobile'    => NULL ,
                            'browser'   => NULL ,
                            'platform'  => NULL ,
                            'ip'        => NULL ,
                            'order'     => NULL ,
                            'group_by'  => NULL ,
                            'select_group'  => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "H.*" ;
        $select[$i++]   = "DATE_FORMAT(H.logout_date,'%Y/%m/%d') AS logout_date_diff" ;
        $select[$i++]   = "DATE_FORMAT(H.logout_date,'%H:%i') AS logout_time_diff" ;
        $select[$i++]   = "DATE_FORMAT(H.login_date,'%Y/%m/%d') AS login_date_diff" ;
        $select[$i++]   = "DATE_FORMAT(H.login_date,'%H:%i') AS login_time_diff" ;
        
        if ($level)         $select[$i++]   = "L.level_name" ;
        if ($user)          $select[$i++]   = "U.*" ;
        if ($select_group)  $select[$i++]   = $select_group ;
        if ($long_time)     $select[$i++]   = "TIMEDIFF(logout_date,login_date) AS long_time" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($user)              $this->db->join($this->tableUser.' U','U.user_id = H.user_id','left') ;
        if ($level && $user)    $this->db->join($this->tableLevel.' L','L.level_id = U.level_id','left') ;
        
        if ($level  && $level_id)   $this->db->where('U.level_id'   , $level_id) ;
        if ($user   && $keyword)    $this->db->like('U.fullname'    , $keyword) ;

        if ($distinct_id)   $this->db->where('H.log_id <>',$distinct_id) ;
        if ($user_id)       $this->db->where('H.user_id'    , $user_id) ;
        if ($mobile)        $this->db->where('H.mobile',$mobile) ;
        if ($browser)       $this->db->where('H.browser',$browser) ;
        if ($ip)            $this->db->where('H.ip',$ip) ;
        if ($date)          $this->db->where("DATE_FORMAT(H.login_date,'%d/%m/%Y') = '".$date."'",NULL,FALSE) ;

        if ($group_by)  $this->db->group_by($group_by) ;

        if ($log_id)  $this->db->where('H.log_id',$log_id) ;

        if ($order) {
            $this->db->order_by($order) ;
        }
        else {
            $this->db->order_by('H.login_date','DESC') ;
        }

        if ($limit) $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table.' H') ;

        if ($log_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }

    public function num($config=array()) {
        $defaults = array(  'user_id'   => NULL ,
                            'user'      => FALSE ,
                            'distinct_id' => NULL ,
                            'level_id'  => NULL ,
                            'keyword'   => NULL ,
                            'date'      => NULL ,
                            'level'     => FALSE,
                            'mobile'    => NULL ,
                            'browser'   => NULL ,
                            'ip'        => NULL ,
                            'group_by'  => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($user)              $this->db->join($this->tableUser.' U','U.user_id = H.user_id','left') ;
        if ($level && $user)    $this->db->join($this->tableLevel.' L','L.level_id = U.level_id','left') ;

        if ($level  && $level_id)   $this->db->where('U.level_id'   , $level_id) ;
        if ($user   && $keyword)    $this->db->like('U.fullname'    , $keyword) ;

        if ($distinct_id)   $this->db->where('H.log_id <>',$distinct_id) ;
        if ($user_id)   $this->db->where('H.user_id'    , $user_id) ;
        if ($mobile)    $this->db->where('H.mobile',$mobile) ;
        if ($browser)   $this->db->where('H.browser',$browser) ;
        if ($ip)        $this->db->where('H.ip',$ip) ;
        if ($date)      $this->db->where("DATE_FORMAT(H.login_date,'%d/%m/%Y') = '".$date."'",NULL,FALSE) ;

        if ($group_by)  {
            $this->db->group_by($group_by) ;
            
            $num    = $this->db->get($this->table.' H') ;

            return $num->num_rows() ;
        }
        else {
            return $this->db->count_all_results($this->table.' H') ;
        }
    }

    public function insert_id($user_id) {
        $this->load->library('user_agent') ;
        
        $data['user_id']    = $user_id ;

        $data['login_date']         = date('Y-m-d H:i:s') ;
        $data['ip']                 = $_SERVER['REMOTE_ADDR'] ;
        $data['browser']            = $this->agent->is_browser()    ? $this->agent->browser()   : '' ;
        $data['browser_version']    = $this->agent->is_browser()    ? $this->agent->version()   : '' ;
        $data['platform']           = $this->agent->platform() ;
        $data['mobile']             = $this->agent->is_mobile()     ? $this->agent->mobile()    : '' ;
        $data['referrer']           = $this->agent->is_referral()   ? $this->agent->referrer()  : '' ;
        $data['robot']              = $this->agent->is_robot()      ? $this->agent->robot()     : '' ;
    
        $this->db->insert($this->table,$data) ;

        return $this->db->insert_id() ;
    }

    public function save($log_id,$log_pages) {
        $data['logout_date'] = date('Y-m-d H:i:s') ;
        $data['log_pages']   = $log_pages ;

        $this->db->where('log_id',$log_id) ;

        return $this->db->update($this->table,$data) ;
    }

    public function order_dw() {
        $data['']   = 'Order by login' ;
        $data['H.login_date ASC']   = 'Login date ASC' ;
        $data['H.login_date DESC']  = 'Login date DESC' ;
        $data['H.logout_date ASC']  = 'Logout date ASC' ;
        $data['H.logout_date DESC'] = 'Logout_date DESC' ;
        $data['U.fullname']         = 'User Access' ;

        return $data ;
    }

    public function agent_dw($field = 'browser',$name = NULL) {
        $data[0] = 'Show all '. ($name ? $name : $field) ;

        $this->db->select($field) ;
        $this->db->group_by($field) ;

        $query  = $this->db->get($this->table) ;

        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $q) {
                $data[$q[$field]]   = $q[$field] ;
            }
        }


        return $data ;
    }
}

/* End of file users_logs_model.php */
/* Location: ./application/models/users_logs_model.php */