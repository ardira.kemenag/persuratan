<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_inaksi_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Surat_inaksi_model extends CI_Model {

    public $table           = 'surat__inaksi' ;
    public $table_aksi      = 'entitas__aksi' ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'inaksi_id'         => NULL ,
                            'surat_id'          => NULL ,
                            'aksi_id'           => FALSE ,
                            'aksi_status'       => NULL ,
                            'aksi'              => FALSE ,
                            'subdis_id'         => NULL ,

                            'array'             => FALSE ,
                            'array_id'          => NULL ,
                            'array_name'        => NULL ,

                            'json'              => FALSE ,

                            'page'              => 0    ,
                            'limit'             => NULL ,
                            'order'             => 'A.aksi_id'  );

	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }
        
        $i = 0 ;
        $select[$i++]   = "A.*" ;
        if ($aksi)      $select[$i++]   = "AK.aksi_name,AK.aksi_akronim" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($aksi)  $this->db->join($this->table_aksi.' AK','AK.aksi_id = A.aksi_id') ;

        if ($inaksi_id)     $this->db->where('A.inaksi_id'   , $inaksi_id) ;
        if ($surat_id)      $this->db->where('A.surat_id'    , $surat_id) ;
        if ($aksi_id)       $this->db->where('A.aksi_id'     , $aksi_id) ;
        if ($aksi_status)   $this->db->where('A.aksi_status' , $aksi_status) ;
        if ($subdis_id)     $this->db->where('A.subdis_id'   , $subdis_id);
        
        if ($order)         $this->db->order_by($order) ;
        
        if ($limit)         $this->db->limit($limit,$page) ;

        $sql = $this->db->get_compiled_select($this->table.' A') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($inaksi_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            return FALSE ;
        }
        elseif ($array OR $json) {
            $rs = $array ? array(''=>'- - -') : NULl ;
            if ($query->num_rows() > 0) {
                foreach($query->result() as $r) {
                    if ($array) $rs[$r->$array_id] = $r->$array_name ;
                    else        $rs[]              = $r->$array_id ;
                }
            }
            return $rs ;
        }

        return $query ;
    }

    public function save($surat_id,$subdis_id = 0) {
        if (!empty($surat_id)) {
            // ERASE CURRENT
            $this->db->where('surat_id',$surat_id) ;
            $this->db->delete($this->table) ;
            /**=============== **/

            $aksi_id    = $this->input->post('aksi_id',TRUE) ; // ARRAY
            if (!empty($aksi_id)) {
                foreach ($aksi_id as $c) {
                    if (!empty($c)) {
                        $data['surat_id']   = $surat_id ;
                        $data['subdis_id']  = $subdis_id ;
                        $data['aksi_id']    = $c    ;
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }
            }

            apc_clean();

            return TRUE ;
        }

        return FALSE ;
    }

    // DELETE
    public function delete($surat_id,$inaksi_id = NULL) {
        if (!empty($surat_id)) {
            $this->db->where('surat_id',$surat_id) ;

            if ($inaksi_id) $this->db->where('inaksi_id',$inaksi_id) ;

            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function num($config = array()) {
        $defaults = array(  'surat_id'          => NULL ,
                            'aksi_id'           => FALSE ,
                            'subdis_id'         => NULL ,
                            'aksi_status'       => NULL );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
        }
        
        if ($surat_id)      $this->db->where('A.surat_id'    , $surat_id) ;
        if ($aksi_id)       $this->db->where('A.aksi_id'     , $aksi_id) ;
        if ($aksi_status)   $this->db->where('A.aksi_status' , $aksi_status) ;
        if ($subdis_id)     $this->db->where('A.subdis_id'   , $subdis_id);
        
        return $this->db->count_all_results($this->table.' A') ;
    }

    public function add() {
        $surat_id   = $this->input->post('surat_id',TRUE) ;
        $subdis_id  = $this->input->post('subdis_id',TRUE) ;
        $aksi_id    = $this->input->post('aksi_id') ;

        if (!empty($surat_id) && !empty($subdis_id) && !empty($aksi_id)) {
            $data['surat_id']   = $surat_id ;
            $data['subdis_id']  = $subdis_id ;
            $data['aksi_id']    = $aksi_id ;

            $cek    = $this->get(array('surat_id'=>$surat_id,'subdis_id'=>$subdis_id,'aksi_id'=>$aksi_id)) ;
            if ($cek->num_rows() > 0) {
                $this->db->where('inaksi_id',$cek->row()->inaksi_id) ;
                $save = $this->db->update($this->table,$data) ;
            }
            else {
                $save = $this->db->insert($this->table,$data) ;
            }

            if ($save) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function set_done() {
        $surat_id   = $this->input->post('surat_id',TRUE) ;
        $inaksi_id    = $this->input->post('inaksi_id') ;
        


        if (!empty($surat_id) && !empty($inaksi_id)) {
            $data['aksi_status']    = 2 ;
            
            $this->db->where('surat_id' , $surat_id) ;
            $this->db->where('inaksi_id', $inaksi_id) ;

            $this->db->update($this->table,$data) ;
            
            // cari di agenda
            $CI =& get_instance();
            $CI->load->model('agenda_model') ;

            $this->db->where('surat_id',$surat_id) ;
            $check  = $this->db->get($CI->agenda_model->table) ;
            if ($check->num_rows() > 0) {
                $agenda = $check->row() ;

                if ($agenda->status_id == 1) {
                    $this->db->where('kegiatan_id',$agenda->kegiatan_id)
                             ->update($CI->agenda_model->table,array('status_id'=>2)) ;                        
                }

                apc_clean() ;
            }
            
            return TRUE ;
        }
        
        return FALSE ;
    }
}

/* End of file Surat_inaksi_model.php */
/* Location: ./application/models/Surat_inaksi_model.php */