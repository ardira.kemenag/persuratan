<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CLASS USERS
 *
 * @package	BASE IGNITER
 * @subpackage	Model
 * @category	Users
 * @author	Freddy Yuswanto
 *		email	: freddy.august@gmail.com
 *		web	: http://www.kohaci.com
*/
class Users_model extends CI_Model {

    public $dir         ;
    public $dir_medium  ;
    public $dir_thumb   ;

    public $table       ;
    public $table_level ;
    public $table_question ;
    
    public $table_esatu     = 'eselon__satu' ;
    public $table_disposisi = 'disposisi__sublist' ;

    public function  __construct() {
        parent::__construct() ;
        $this->load->model('image_model') ;
        
        $this->dir        = _PATH_UPLOAD_IMG_ ; 
        $this->dir_thumb  = _PATH_UPLOAD_IMG_THUMBS_ ;

        $this->table            = 'users__list'         ;
        $this->table_level      = 'users__level'   ;
    }

    public function get($config = array()) {
        $defaults = array(  'user_id'   => NULL ,
                            'esatu_id'  => NULL ,
                            'esatu'     => FALSE ,
                            'subdis_id' => NULL ,
                            'subdis'    => FALSE ,
                            'username'  => NULL ,
                            'level_id'  => NULL ,
                            'keyword'   => NULL ,
                            'page'      => 0    ,
                            'limit'     => NULL ,
                            'level'     => FALSE,
                            'status'    => NULL ,
                            'distinct_id'=>NULL ,
                            'order'     => NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "U.*" ;
        $select[$i++]   = "DATE_FORMAT(U.time_register,'%Y/%m/%d') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(U.time_register,'%H:%s') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(U.time_register,'%Y') as Y, DATE_FORMAT(U.time_register,'%m') as m, DATE_FORMAT(U.time_register,'%d') as d,DATE_FORMAT(U.time_register,'%H') as H, DATE_FORMAT(U.time_register,'%i') as i, DATE_FORMAT(U.time_register,'%s') as s"  ;

        if ($level)     $select[$i++]   = "L.level_name" ;
        if ($esatu)     $select[$i++]   = "E.esatu_name" ;
        if ($subdis)    $select[$i++]   = "S.subdis_name,S.subdis_eselon" ;
 
        $this->db->select(implode(',', $select),FALSE) ;

        if ($level)     $this->db->join($this->table_level.' L','L.level_id = U.level_id','left') ;
        if ($esatu)     $this->db->join($this->table_esatu.' E','E.esatu_id = U.esatu_id','left') ;
        if ($subdis)    $this->db->join($this->table_disposisi.' S','S.subdis_id = U.subdis_id','left') ;
 
        if ($status)        $this->db->where('status'       , $status) ;
        if ($level_id)      $this->db->where('U.level_id'   , $level_id) ;
        if ($user_id)       $this->db->where('U.user_id'    , $user_id) ;
        if ($esatu_id)      $this->db->where('U.esatu_id'   , $esatu_id) ;
        if ($subdis_id)     $this->db->where('U.subdis_id'  , $subdis_id) ;
        if ($username)      $this->db->where('U.username'   , $username) ;
        if ($keyword)       $this->db->like('U.fullname'    , $keyword) ;

        if ($distinct_id)   $this->db->where('U.user_id <>',$distinct_id) ;

        if ($order) {
            $this->db->order_by($order) ;
        }
        else { 
            $this->db->order_by('U.fullname','ASC') ;
        }

        if ($limit) $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table.' U') ;

        if ($user_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('level_id'            , 'Level'           , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('esatu_id'            , 'Eselon I'        , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('subdis_id'           , 'Kabag'           , 'trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('username'            , 'Username'        , 'required|trim|xss_clean|matches_username');
        $this->form_validation->set_rules('password'            , 'Password'        , 'trim|xss_clean|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password'    , 'Confirm Password', 'trim|xss_clean');
        $this->form_validation->set_rules('fullname'            , 'Fullname'        , 'required|trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('email'               , 'Email'           , 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('status'              , 'Status'          , 'trim|xss_clean') ;
        $this->form_validation->set_rules('location'            , 'Alamat'          , 'trim|xss_clean') ;
        $this->form_validation->set_rules('position'            , 'Posisi'          , 'trim|xss_clean') ;

        $this->form_validation->set_error_delimiters('<div class="error_label">', '</span>');

        return $this->form_validation->run() ;
    }

    public function is_username($username = NULL) {
        $username   = $username ? $username : $this->input->post('username',TRUE) ;

        $this->db->where('username',$username) ;
        $this->db->limit(1) ;

        $cek    = $this->db->count_all_results($this->table) ;

        return $cek > 0 ? TRUE : FALSE ;
    }

    public function is_email($email = NULL) {
        $email  = $email ? $email : $this->input->post('email',TRUE) ;

        $this->db->where('email',$email) ;
        $this->db->limit(1) ;

        $cek    = $this->db->count_all_results($this->table) ;

        return $cek > 0 ? TRUE : FALSE ;
    }

    public function is_self($user_id) {
        return $user_id == $this->input->post('user_id',TRUE) ? TRUE : FALSE ;
    }

    public function save() {
        $user_id    = $this->input->post('user_id',TRUE) ;
        
        $old_email      = $this->input->post('old_email',TRUE) ;
        $old_username   = $this->input->post('old_username',TRUE) ;

        $data['username']   = set_value('username') ;
        $data['fullname']   = set_value('fullname') ;
        $data['email']      = set_value('email') ;
        $data['location']   = set_value('location') ;
        $data['position']   = set_value('position') ;

        $status     = set_value('status') ;
        $level_id   = set_value('level_id') ;
        $password   = set_value('password') ;

        if (!empty($status))    $data['status']     = $status ;
        if (!empty($password))  $data['password']   = base64_encode(md5($password,true)) ;
        if (!empty($level_id))  {
            $data['level_id']   = $level_id ;
            if ($level_id == 4)     $data['esatu_id']   = set_value('esatu_id') ; 
            elseif ($level_id == 9) $data['subdis_id']  = set_value('subdis_id') ;
            elseif ($level_id == 3) $data['subdis_id']  = 1 ; 
        }

        $register   = $this->input->post('register_status',TRUE) ;
        if (!empty($register))  $data['validation']   = sha1($data['username'] . '+pwd123') ;
        
        // Upload Profil Picture
        $temp   = $this->input->post('temp_user_img',TRUE) ;
        $photo  = $this->image_model->upload('user_img',$this->dir) ;
        if ($photo) {
            $data['user_img']   = $photo['file_name']    ;

            // ======================  Resizing ===================== //
            // Thumbnail
            $this->image_model->resize_crop(_THUMB_WIDTH_,_THUMB_HEIGHT_,$photo,$this->dir . $photo['file_name'],$this->dir_thumb) ;
            // ====================================================== //

            if (!empty($temp)) {
                $this->image_model->delete($this->dir         , $temp) ;
                $this->image_model->delete($this->dir_thumb   , $temp) ;
            }
        }
        else {
            if (!empty($temp)) $data['user_img']    = $temp ;
        }

        $result['rs']             = FALSE ;
        $result['username_error'] = '' ;
        $result['email_error']    = '' ;
        $result['password_error'] = '' ;

        if ($user_id) {
            // CHECK AVAIBILTY USERNAME
            $check_username = TRUE ;
            if ($old_username != $data['username']) {
                if ($this->is_username($data['username']) == TRUE) {
                    $result['username_error']   = '<div class="error_label">this username is not available.</div>' ;
                    $check_username = FALSE ;
                }
            }

            // CHECK AVAIBILITY EMAIL
            $check_email = TRUE ;
            if ($old_email != $data['email']) {
                if ($this->is_email($data['email']) == TRUE) {
                    $result['email_error']    = '<div class="error_label">this email has registered before.</div>' ;
                    $check_email    = FALSE ;
                }
            }

            if ($check_email && $check_username) {
                $this->db->where('user_id',$user_id) ;

                $result['rs'] = $this->db->update($this->table,$data) ;
            }
        }
        else {
            $check_username = TRUE ;
            $check_email    = TRUE ;
            $check_password = TRUE ;

            // CHECK AVAIBILITY USERNAME
            if ($this->is_username($data['username']) == TRUE) {
                $result['username_error']   = '<div class="error_label">this username is not available.</div>' ;
                $check_username = FALSE ;
            }
            // CHECK AVAIBILITY EMAIL
            if (!empty($data['email']) && $this->is_email($data['email']) == TRUE) {
                $result['email_error']    = '<div class="error_label">this email has registered before.</div>' ;
                $check_email    = FALSE ;
            }
            // CHECK PASSWORD
            if (empty($password)) {
                $result['password_error'] = '<div class="error_label">The Password field is required.</div>' ;
                $check_password    = FALSE ;
            }

            if ($check_username && $check_email && $check_password) {
                $data['time_register']  = date('Y-m-d H:i:s') ;
                if ($this->db->insert($this->table,$data)) {
                    if (!empty($register)) {
                        $this->_send_email($data) ;
                    }

                    $result['rs'] = TRUE ;
                }
            }
        }

        return $result ;
    }

    public function num($config=array()) {
        $defaults = array(  'level_id'  => NULL ,
                            'subdis_id' => NULL ,
                            'esatu_id'  => NULL ,
                            'keyword'   => NULL ,
                            'status'    => NULL ,
                            'distinct_id'=>NULL );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($status)        $this->db->where('status'     , $status) ;
        if ($level_id)      $this->db->where('level_id'   , $level_id) ;
        if ($subdis_id)     $this->db->where('subdis_id'  , $subdis_id) ;
        if ($esatu_id)      $this->db->where('esatu_id'   , $esatu_id) ;
        if ($keyword)       $this->db->like('fullname'    , $keyword) ;
        if ($distinct_id)   $this->db->where('user_id <>',$distinct_id) ;

        return $this->db->count_all_results($this->table);
    }

    /**
    *
    * @param integer $id
    * @return boolean
    */
    public function delete($user_id) {
        $this->db->where('user_id',$user_id) ;
        if ($this->db->delete($this->table)) {

            return TRUE ;
        }
    }

    /**
    *
    * @return boolean
    */
    public function process() {
        $data['username']   = $username = $this->input->post('username',TRUE) ;
        $data['level']      = TRUE ;
        $data['subdis']     = TRUE ;
        
        $password	= $this->input->post('password',TRUE) ;

        if (!empty($username) && !empty($password)) {
            $qR = $this->get($data) ;

            if ($qR->num_rows() > 0) {
                $row 	= $qR->row_array() ;
                if ($row['password'] != base64_encode(md5($password,true))) {
                    return FALSE ;
                }
                else {
                    return $row ;
                }
            }
        }
        else {
            return FALSE ;
        }
    }

    public function get_level($config=array()) {
        $defaults = array(  'level_in'  => NULL ,
                            'order'     => 'level_id' );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($level_in)  $this->db->where_in('level_id',$level_in) ;
        $this->db->order_by($order) ;
        
        return $this->db->get($this->table_level) ;
    }
    
    public function level_dw($show_all_text = TRUE) {
        $op[0] = $show_all_text ? 'Show all level' : '- - -' ;

        $this->db->order_by('level_id') ;

        $get    = $this->db->get($this->table_level) ;

        foreach($get->result() as $l) {
            $op[$l->level_id]   = $l->level_name ;
        }

        return $op ;
    }

    public function status_dw($show_all_text = TRUE) {
        $data['']           = $show_all_text ? 'Show all status' : '- - -' ;
        $data['Active']     = 'Active' ;
        $data['Deactive']   = 'Deactive' ;

        return $data ;
    }

    public function order_dw() {
        $data['']                       = 'Order by Alphabet' ;
        $data['U.fullname ASC']         = 'Name Alphabet ASC' ;
        $data['U.fullname DESC']        = 'Name Alphabet DESC' ;
        $data['U.time_register ASC']    = 'First Registered' ;
        $data['U.time_register DESC']   = 'Last Registered' ;

        return $data ;
    }

}

/* End of file users_model.php */
/* Location: ./application/models/users_model.php */