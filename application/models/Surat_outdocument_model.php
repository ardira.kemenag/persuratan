<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat_outdocument_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Surat_outdocument_model extends CI_Model {
    
    public $table       = 'surat__outdocument' ;
    public $table_list  = 'surat__out' ;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('file_model') ;
    }
    
    public function get($config=array()) {
        $defaults = array(  'surat_id'      => NULL ,
                            'document_id'   => NULL ,
                            'distinct_id'   => NULL ,
                            'page'          => 0 ,
                            'limit'         => NULL ,
                            'surat'         => FALSE ,
                            'order'         => 'document_lastupdate DESC'
                        );
 
	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}
        
        $i = 0 ;
        $select[$i++]   = "F.*" ;
        $select[$i++]   = "DATE_FORMAT(F.document_lastupdate,'%Y/%m/%d') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(F.document_lastupdate,'%H:%i') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(F.document_lastupdate,'%Y') as Y, DATE_FORMAT(F.document_lastupdate,'%m') as m, DATE_FORMAT(F.document_lastupdate,'%d') as d,DATE_FORMAT(F.document_lastupdate,'%H') as H, DATE_FORMAT(F.document_lastupdate,'%i') as i, DATE_FORMAT(F.document_lastupdate,'%s') as s"  ;
        
        if ($surat)   $select[$i++]   = "L.*" ;
        
        $this->db->select(implode(',', $select),FALSE) ;
        
        if ($surat)   $this->db->join($this->table_list.' L','L.surat_id = F.surat_id') ;
        
        if ($distinct_id)   $this->db->where('F.surat_id <>',$distinct_id) ;
        if ($surat_id)      $this->db->where('F.surat_id' , $surat_id) ;
        if ($document_id)   $this->db->where('F.document_id'    , $document_id) ;
        
        if ($order) $this->db->order_by($order) ;
        if ($limit) $this->db->limit($limit,$page) ;

        $sql = $this->db->get_compiled_select($this->table.' F') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($document_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }
    
    public function save($surat_id) {
        $data['surat_id']    = $surat_id  ;

        foreach (range(1,30) as $i) {
            $file   = $this->file_model->upload('document_name'.$i,_PATH_UPLOAD_FILES_) ;
            if ($file) {
                $data['document_filename']  = $file['file_name']    ;
                $data['document_filesize']  = $file['file_size'] ;
                $data['document_filetype']  = $file['file_type'] ;
                $data['document_fileext']   = $file['file_ext'] ;

                if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
            }
        }
        
        apc_clean() ;

        return TRUE ;
    }
    
    public function num($config) {
        $defaults = array(  'surat_id'    => NULL ,
                            'distinct_id' => NULL );
 
	    foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	    }
        
        if ($surat_id)      $this->db->where('surat_id',$surat_id) ;
        if ($distinct_id)   $this->db->where('surat_id <>',$distinct_id) ;
        
        return $this->db->count_all_results($this->table) ;
    }
    
    public function delete($document_id,$surat_id) {
        if (!empty($document_id)) {
            $this->db->where('document_id'  , $document_id) ;
            $this->db->where('surat_id'     , $surat_id) ;
            $result = $this->db->get($this->table) ;

            if ($result->num_rows() > 0) {
                $row    = $result->row() ;

                $this->file_model->delete(_PATH_UPLOAD_FILES_, $row->document_filename) ;
                
                $this->db->where('document_id',$document_id) ;
                if ($this->db->delete($this->table)) {
                    apc_clean() ;
                    return TRUE ;
                }
            }
            
            return FALSE ;

        }

        return FALSE ;
    }
}

/* End of file Surat_outdocument_model.php */
/* Location: ./application/models/Surat_outdocument_model.php */