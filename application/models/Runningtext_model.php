<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Runningtext_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */

class Runningtext_model extends CI_Model {

    public $table       = 'runningtext__content' ;
    public $tableUser   = 'users__list' ;

    public function  __construct() {
        parent::__construct() ;
    }

    public function get($config = array()) {
        $defaults = array(  'rtext_id'        => NULL ,
                            'page'           => 0     ,
                            'limit'          => NULL  ,
                            'distinct_id'    => NULL  ,
                            'rtext_view'      => NULL ,
                            'order'          => NULL  ,
                            'user'           => FALSE );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "R.*" ;
        $select[$i++]   = "DATE_FORMAT(R.rtext_date,'%Y/%m/%d') AS date_diff" ;
        $select[$i++]   = "DATE_FORMAT(R.rtext_date,'%H:%i') AS time_diff" ;
        $select[$i++]   = "DATE_FORMAT(R.rtext_date,'%Y') as Y, DATE_FORMAT(R.rtext_date,'%m') as m, DATE_FORMAT(R.rtext_date,'%d') as d,DATE_FORMAT(R.rtext_date,'%H') as H, DATE_FORMAT(R.rtext_date,'%i') as i, DATE_FORMAT(R.rtext_date,'%s') as s"  ;

        if ($user)      $select[$i++]  = "Us.*" ;

        $this->db->select(implode(',', $select),FALSE) ;

        if ($user)  $this->db->join($this->tableUser.' Us','Us.user_id = R.user_id','left') ;
        
        if ($rtext_id)      $this->db->where('R.rtext_id',$rtext_id) ;
        if ($distinct_id)   $this->db->where('R.rtext_id <>',$distinct_id) ;
        if ($rtext_view)    $this->db->where('R.rtext_view',$rtext_view)  ;
        
        if ($order) {
            $this->db->order_by($order) ;
        }
        else {
            $this->db->order_by('R.rtext_date','DESC') ;
        }

        if ($limit) $this->db->limit($limit,$page) ;

        $query  = $this->db->get($this->table.' R') ;

        if ($rtext_id) {
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }

            return FALSE ;
        }

        return $query ;
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('rtext_name'      , $this->lang->line('rtext_model_val_title')         , 'required|trim|xss_clean');
        $this->form_validation->set_rules('rtext_date'      , $this->lang->line('rtext_model_val_date')          , 'required|trim|xss_clean');
        $this->form_validation->set_rules('rtext_hours'     , $this->lang->line('rtext_model_val_hours')         , 'required|trim|xss_clean');
        $this->form_validation->set_rules('rtext_minute'    , $this->lang->line('rtext_model_val_minute')        , 'required|trim|xss_clean');
        $this->form_validation->set_rules('rtext_view'      , $this->lang->line('rtext_model_val_status')        , 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_id'         , $this->lang->line('rtext_model_val_creator')       , 'required|trim|xss_clean|is_numeric');

        $this->form_validation->set_error_delimiters('<span class="valid-error">', '</span>');

        return $this->form_validation->run() ;
    }

    public function save() {
        $rtext_id    = $this->input->post('rtext_id') ;

        $data['rtext_name'] = set_value('rtext_name')  ;
        $data['rtext_view'] = set_value('rtext_view') ;
        $data['user_id']    = set_value('user_id') ;
        $data['rtext_date'] = str_replace('/','-',set_value('rtext_date')).' '.set_value('rtext_hours').':'.set_value('rtext_minute').':'.date('s')  ;

        if (!empty($rtext_id)) {
            $this->db->where('rtext_id',$rtext_id) ;
            return $this->db->update($this->table,$data) ;
        }
        else {
            return $this->db->insert($this->table,$data) ;
        }

    }

    public function delete($rtext_id) {
        $this->db->where('rtext_id',$rtext_id) ;
        
        return $this->db->delete($this->table) ;
    }

    public function num($config = array()) {
        $defaults = array(  'rtext_view' => NULL );

        foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($rtext_view)    $this->db->where('rtext_view',$rtext_view) ;

        return $this->db->count_all_results($this->table) ;
    }

    public function status_dw() {
        $data['']           = $this->lang->line('rtext_model_status_dw') ;
        $data['Visible']    = $this->lang->line('Visible') ;
        $data['Hidden']     = $this->lang->line('Hidden') ;

        return $data ;
    }

}

/* End of file runningtext_model.php */
/* Location: ./application/models/runningtext_model.php */