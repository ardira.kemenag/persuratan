<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of file_model
 *
 * @author Freddy Yuswanto
 */
class File_model extends CI_Model {

    public function  __construct() {
        parent::__construct() ;
    }

    public function upload($filename,$destination, $encrypt = FALSE ) {
        if (!isset($_FILES[$filename]['name'])) return NULL ;

        $dir		= $destination ;

        $post   = $filename ;
        // Nama file yang diupload
        $filename	= url_title(strtolower($_FILES[$filename]['name'])) ;

        // Spesifikasi lokasi file yang akan diupload
        $target_dir = $dir . $filename ;

        /*
        * Gunakan fungsi file_exists() untuk memeriksa apakah
        * file X telah ada sebelumnya di folder Y
        */
        if(file_exists($target_dir)) {
            /*
            * Iterasi sampai 100 dg asusmi file duplikasi (bernama sama) itu maksimal 100,
            * lebih dari itu ... TERLALU (mode on Bang Rhoma) :-p
            */
            for($i = 1; $i < 100; $i++) {
                $filename   = $i . '-' . $filename ;
                $target_dir = $dir . $filename ;
                /*
                * Misal file berkas.odt ada di folder sebelumnya,
                * maka file berkas.odt yang baru akan direname menjadi 1-berkas.odt
                */
                if(!file_exists($target_dir)) break;
            }
        }

        // upload

        $data['file_name']  = $filename ;
        $data['file_ext']   = substr(strrchr($filename, '.'), 1) ;
        $data['orig_name']  = $_FILES[$post]['name'] ;
        $data['file_size']  = $_FILES[$post]['size'] ;
        $data['file_type']  = $_FILES[$post]['type'] ;

        if(move_uploaded_file($_FILES[$post]['tmp_name'], $target_dir)) {
            chmod($target_dir , 0777) ;
            return  $data ;
        }        
        else
            return FALSE ;
    }

    public function delete($dir, $filename) {
        //chmod($dir . $filename, 0777) ;
        if (!empty($dir) && !empty($filename)) {
            if (file_exists($dir . $filename)) {
                return unlink($dir . $filename) ;
            }
        }

        return TRUE ;
    }
}

/* End of file file_model.php */
/* Location: ./application/models/file_model.php */