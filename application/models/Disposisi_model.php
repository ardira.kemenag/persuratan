<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Disposisi_model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Disposisi_model extends CI_Model {
    
    public $table       = 'disposisi__list' ;
    public $table_sub   = 'disposisi__sublist' ;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get($config = array()) {
        $defaults = array(  'disposisi_id'  => NULL ,
                            'num'           => FALSE ,
                            'array'         => FALSE ,
                            'json'          => FALSE ,
                            'page'          => 0 ,
                            'limit'         => NULL ,
                            'group'         => NULL ,
                            'order'         => 'disposisi_name ASC'
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        $i = 0 ;
        $select[$i++]   = "T.*" ;
        if ($num)       $select[$i++]   = "(SELECT COUNT(*) FROM ".$this->table_sub." D WHERE T.disposisi_id = D.disposisi_id) AS num_data" ;
        
        $this->db->select(implode(',', $select),FALSE) ;

        if ($disposisi_id)      $this->db->where('T.disposisi_id' , $disposisi_id) ;
 
        if ($group) $this->db->group_by($group) ;
        if ($limit) $this->db->limit($limit,$page) ;
        if ($order) $this->db->order_by($order) ;

        $sql = $this->db->get_compiled_select($this->table.' T') ;
        
        //$query  = $this->db->query($sql) ;
        $query  = apc_get($sql) ;

        if ($disposisi_id OR $array OR $json) {
            $result = ($disposisi_id OR $json) ? FALSE : array(''=>'- disposisi -') ;
            if ($query->num_rows() > 0) {
                if ($disposisi_id) {
                    return $query->row_array() ;
                }
                else{
                    foreach ($query->result() as $p) {
                        if ($array) $result[$p->disposisi_id] = $p->disposisi_name ;
                        else        $result[]                 = $p->disposisi_name ;
                    }

                    return $result ;
                }
            }

            return $result ;
        }

        return $query ;
    }
    
    public function num($config = array()) {
        $defaults = array(  'disposisi_id'   => NULL
                         );

	foreach ($defaults as $key => $val) {
            $$key = ( ! isset($config[$key])) ? $val : $config[$key];
	}

        if ($disposisi_id)   $this->db->where('T.disposisi_id' , $disposisi_id) ;

        return $this->db->count_all_results($this->table.' T') ;
    }

    public function delete($disposisi_id) {
        if (is_numeric($disposisi_id)) {
            $this->db->where('disposisi_id',$disposisi_id) ;
            if ($this->db->delete($this->table)) {
                apc_clean() ;
                return TRUE ;
            }
        }

        return FALSE ;
    }

    public function save() {
        $disposisi_id        = $this->input->post('disposisi_id') ;
        $disposisi_name      = $this->input->post('disposisi_name') ;

        if (count($disposisi_name) > 0) {
            $i = 0 ;
            foreach ($disposisi_name as $tm) {
                if (!empty($tm)) {
                    $data['disposisi_name']      = $tm ;
                    if (!empty($disposisi_id[$i])) {
                        $this->db->where('disposisi_id',$disposisi_id[$i]) ;
                        if ( ! $this->db->update($this->table,$data) ) return FALSE ;
                    }
                    else {
                        if ( ! $this->db->insert($this->table,$data) ) return FALSE ;
                    }
                }

                $i++ ;
            }

            apc_clean() ;
            return TRUE ;
        }

        return FALSE ;
    }
	
	public function tambah_into($data){
		$this->db->insert('surat__into',$data);
	}
	
	public function tambah_pendispos($data){
		$this->db->insert('surat__indisposisi',$data);
	}
	
	public function tambah_dispos($data){
		$this->db->insert('surat__indisposisi',$data);
	}
	
	public function ubah_dispos($data,$subdis_id,$into_id){
		$this->db->where('subdis_id', $subdis_id);
		$this->db->where('into_id', $into_id);
		$this->db->update('surat__indisposisi',$data);
	}
	
	public function tambah_aksi($data){
		$this->db->insert('surat__inaksi',$data);
	}
}
/* End of file disposisi_model.php */
/* Location: ./application/models/disposisi_model.php */