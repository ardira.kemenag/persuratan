<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Agenda_Google_Model
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Agenda_google_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function save($kegiatan_id) {
        if (!empty($kegiatan_id) && is_numeric($kegiatan_id)) {
            $CI =& get_instance() ;
            $CI->load->model('agenda_model') ;
            
            $postdata['kegiatan_id']    = $kegiatan_id ;
            $agenda = $this->agenda_model->get($postdata) ;
            
            if ($agenda) {
                $this->load->library('Gcal');
                $username = 'sespriroren@gmail.com';
                $password = 'sesroren';

                $this->load->library('Gcal');
                $client = $this->gcal->clientLogin($username, $password);
                
                if (!empty($agenda['event_id'])) {
                    $this->gcal->delete ( $client , $agenda['event_id'] );
                }
                
                $eventArray = array('title'     =>  $agenda['kegiatan_penyelenggara'].'; '.$agenda['kegiatan_name'] ,
                                    'desc'      =>  $agenda['kegiatan_peserta'].'; '.$agenda['kegiatan_keterangan'] ,
                                    'where'     =>  $agenda['kegiatan_tempat'] ,
                                    'startDate' =>  $agenda['kegiatan_date_start'] ,
                                    'startTime' =>  $agenda['kegiatan_time_start'] ,
                                    'endDate'   =>  ( (!empty($agenda['kegiatan_date_end']) && $agenda['kegiatan_date_end'] != '0000-00-00') ? $agenda['kegiatan_date_end'] : $agenda['kegiatan_date_start'] ) ,
                                    'endTime'   =>  $agenda['kegiatan_time_end'] ,
                                    'tzOffset'  =>  '+07'
                                );
                
                $event_id   = $this->gcal->createEvent($client, $eventArray) ;
                
                if (!empty($event_id)) {
                    $update['event_id'] = $event_id ;
                    
                    $this->db->where( 'kegiatan_id' , $kegiatan_id ) ;
                    return $this->db->update($CI->agenda_model->table,$update) ;
                }
            }
        }
        
        return FALSE ;
    }
    
    public function delete($kegiatan_id) {
        if (!empty($kegiatan_id) && is_numeric($kegiatan_id)) {
            $CI =& get_instance() ;
            $CI->load->model('agenda_model') ;
            
            $postdata['kegiatan_id']    = $kegiatan_id ;
            $agenda = $this->agenda_model->get($postdata) ;
            
            if ($agenda) {
                $this->load->library('Gcal');
                $username = 'sespriroren@gmail.com';
                $password = 'sesroren';

                $this->load->library('Gcal');
                $client = $this->gcal->clientLogin($username, $password);
                
                if (!empty($agenda['event_id'])) {
                    return $this->gcal->delete ( $client , $agenda['event_id'] );
                }
            }
        }
        
        return FALSE ;
    }
}
/* End of file agenda_google_model.php */
/* Location: ./application/models/agenda_google_model.php */
