<?php $this->load->view( _TEMPLATE_EMAIL_ . 'general/header') ;?>

<h3>Anda mendapat Disposisi Surat (<?php echo $jenis_name ;?>)</h3>

<p>Silakan klik link berikut untuk selengkapnya :</p>

<p><?php echo anchor(site_url('admin/suratin/c/'.$surat_id).'?view=1');?></p>

<?php $this->load->view( _TEMPLATE_EMAIL_ . 'general/footer') ;?>
