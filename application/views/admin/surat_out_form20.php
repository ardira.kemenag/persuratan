<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>FORM SURAT KELUAR : <?php echo strtoupper($jenis['jenis_name']);?> </h2>
    
    <?php $alt = 0 ;?>
    
    <?php echo form_open_multipart("admin/suratout/c/".$jenis['jenis_id']) ;?>
        <div class="table-responsive">

        <table class="table widefat">
            <tbody>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td width="23%;"><b>Sifat Surat</b></th>
                    <td width="2%;"><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('sifat_id', $sifat_dw, set_value('sifat_id', ( !empty($surat['sifat_id']) ? $surat['sifat_id'] : 1 ) ),'style="width:90%;"') ;?>
                        <?php echo form_error('sifat_id') ;?>
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input id="datepicker1" name="surat_date" value="<?php echo set_value('surat_date', (!empty($surat['surat_date']) ? $surat['surat_date'] : date('Y-m-d') ) );?>" type="text" maxlength="10" size="10" class="form-control"  />
                        <?php echo form_error('surat_date') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanda Tangan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('ttd_id',$ttd_dw,set_value('ttd_id',$surat['ttd_id']),'id="ttd_id" style="width:90%;"') ;?>
                        <?php echo form_error('ttd_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td>&nbsp;</th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_radio('surat_ttd',1,$surat['surat_ttd'] == 1,'class="surat_ttd"') ;?> Langsung
                        &nbsp;&nbsp;&nbsp;
                        <?php echo form_radio('surat_ttd',2,$surat['surat_ttd'] == 2,'class="surat_ttd"') ;?> PLH
                        &nbsp;&nbsp;&nbsp;
                        <?php echo form_radio('surat_ttd',3,$surat['surat_ttd'] == 3,'class="surat_ttd"') ;?> PLT
                        <?php echo form_error('surat_ttd') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?> id="surat_plh">
                    <td align="right"><b>Atas Nama</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_plh','class'=>'form-control'),set_value('surat_plh',$surat['surat_plh'])) ;?>
                        <?php echo form_error('surat_plh') ;?>
                    </td>
                </tr>
                
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Pengolah / Asal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('olah_id',$olah_dw,set_value('olah_id',$surat['olah_id']),'id="olah_id" style="width:90%;"') ;?>
                        <?php echo form_error('olah_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Klasifikasi Surat</b></td>
                    <td><b>:</b></td>
                    <td>
                        <?php echo form_dropdown('klasifikasi_pr',$klasifikasi_pr,set_value('klasifikasi_pr',$surat['klasifikasi_pr']),'id="klasifikasi_pr" style="width:90%;"') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td colspan="2">&nbsp;</td>
                    <td>
                        <div id="klasifikasi_div">
                            <?php echo form_dropdown('klasifikasi_id',$klasifikasi_dw,set_value('klasifikasi_id',$surat['klasifikasi_id']),'id="klasifikasi_id" style="width:90%;"') ;?>
                            <?php echo form_error('klasifikasi_id') ;?>
                        </div>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Dasar</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_textarea(array('name'=>'surat_dasar','rows'=>3,'class'=>'form-control'),set_value('surat_dasar',$surat['surat_dasar'])) ;?>
                        <?php echo form_error('surat_dasar') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tujuan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_multiselect('tujuan_id[]',$tujuan_dw,!empty($surat['tujuan_id']) ? json_decode($surat['tujuan_id'],TRUE) : NULL ,'id="tujuan_id" style="width:90%;"') ;?>
                        <?php echo form_error('tujuan_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Kegiatan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_kegiatan','class'=>'form-control'),set_value('surat_kegiatan',$surat['surat_kegiatan'])) ;?>
                        <?php echo form_error('surat_kegiatan') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Lokasi</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_lokasi','class'=>'form-control'),set_value('surat_lokasi',$surat['surat_lokasi'])) ;?>
                        <?php echo form_error('surat_lokasi') ;?>
                    </td>
                </tr>


                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Pelaksana</b></th>
                    <td><b>:</b></th>
                    <td>
                        <table class="table widefat" style="border:none;">
                            <?php 
                                $key        = 0 ; 
                                $pelaksana  = !empty($surat['surat_pelaksana']) ? json_decode($surat['surat_pelaksana'],TRUE) : NULL ;
                            ?>
                            <?php if (!empty($pelaksana) && count($pelaksana) > 0) :?>
                                <?php foreach ($pelaksana['name'] as $key => $val) :?>
                                    <?php if (!empty($pelaksana['name'][$key])) :?>
                                        <tr>
                                            <td style="width:25%;">
                                                <?php echo form_input(array('name'=>'surat_pelaksana[name]['.$key.']','placeholder'=>'Nama...','style'=>'width:90%;','class'=>'addrow form-control'),$pelaksana['name'][$key]) ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo form_input(array('name'=>'surat_pelaksana[nip]['.$key.']','placeholder'=>'NIP...','class'=>'form-control'),(!empty($pelaksana['nip'][$key]) ? $pelaksana['nip'][$key] : NULL )) ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo form_input(array('name'=>'surat_pelaksana[pangkat]['.$key.']','placeholder'=>'Pangkat/Gol/Ruang...','class'=>'form-control'),(!empty($pelaksana['pangkat'][$key]) ? $pelaksana['pangkat'][$key] : NULL )) ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo form_input(array('name'=>'surat_pelaksana[jabatan]['.$key.']','placeholder'=>'Jabatan...','class'=>'form-control'),(!empty($pelaksana['jabatan'][$key]) ? $pelaksana['jabatan'][$key] : NULL )) ;?>
                                            </td>
                                        </tr>
                                    <?php endif ;?>
                                <?php endforeach ;?>    
                            <?php endif ;?>    

                            <?php 
                                $j = 1 ;
                                $k = $key+2 ;
                                $n = $key + 20 ;
                            ?>
                            <?php foreach (range($k,$n) as $i) :?>
                                <tr id="row<?php echo $j++ ;?>">
                                    <td style="width:25%;">
                                        <?php echo form_input(array('name'=>'surat_pelaksana[name]['.$i.']','placeholder'=>'Nama...','style'=>'width:90%;','class'=>'addrow form-control'),NULL) ;?>
                                    </td>
                                    <td style="width:25%;">
                                        <?php echo form_input(array('name'=>'surat_pelaksana[nip]['.$i.']','placeholder'=>'NIP...','class'=>'form-control'),NULL) ;?>
                                    </td>
                                    <td style="width:25%;">
                                                <?php echo form_input(array('name'=>'surat_pelaksana[pangkat]['.$i.']','placeholder'=>'Pangkat/Gol/Ruang...','class'=>'form-control'),NULL) ;?>
                                            </td>
                                    <td style="width:25%;">
                                        <?php echo form_input(array('name'=>'surat_pelaksana[jabatan]['.$i.']','placeholder'=>'Jabatan...','class'=>'form-control'),NULL) ;?>
                                    </td>
                                </tr>
                            <?php endforeach ;?>    
                        </table>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>PLH Sebagai</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_plh_as','class'=>'form-control'),set_value('surat_plh_as',$surat['surat_plh_as'])) ;?>
                        <?php echo form_error('surat_plh_as') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal PLH</b></th>
                    <td><b>:</b></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="datepicker2" name="surat_start" value="<?php echo set_value('surat_start',$surat['surat_start']);?>" type="text" maxlength="10" size="10" class="form-control"  />
                            </div>
                            <div class="col-sm-1">
                                <input type="text" value="S.D." class="form-control"  disabled/>
                            </div>
                            <div class="col-sm-3">
                                <input id="datepicker3" name="surat_end" value="<?php echo set_value('surat_end',$surat['surat_end']);?>" type="text" maxlength="10" size="10" class="form-control"  />
                            </div>
                        </div>
                        <?php echo form_error('surat_start') ;?>
                        <?php echo form_error('surat_end') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Nomor Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_no','class'=>'form-control'),set_value('surat_no',$surat['surat_no']),'id="surat_no"') ;?>
                        <?php echo form_error('surat_no') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Upload Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                            <table class="table">
                            <?php foreach ($document->result() as $doc) :?>
                                <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                    <tr>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]">
                                                <i class="glyphicon glyphicon-list"></i>
                                                <?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('admin/suratout/c/'.$surat['jenis_id'].'/'.$surat['surat_id']).'?delete_doc='.$doc->document_id;?>" class="btn btn-sm btn-danger">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endif ;?>
                            <?php endforeach ;?>
                            </table>
                        <?php endif ;?>

                        <div class="row">
                            <div class="col-sm-10">
                                <input type="file" name="document_name1" class="form-control" />
                            </div>
                            <div class="col-sm-2">
                                <a href="javascript:void(0)" id="button-add-softcopy" class="btn btn-sm btn-success">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </a>
                            </a>    
                            </div>
                        </div>

                        <div id="add-softcopy"></div>
                        <input type="hidden" id="doc_id" value="2" />
                    </td>
                </tr>

            </tbody>
        </table>

        </div>

        <p class="text-center">
            <input type="hidden" id="surat_id" value="<?php echo $surat['surat_id'] ;?>" />
            <input type="hidden" id="rowid" value="3" />
            <?php echo form_hidden(array(
                                        'sess_security' => $sess_security ,
                                        'user_id'       => !empty($surat['user_id']) ? $surat['user_id'] : $user_id ,
                                        'surat_id'      => $surat['surat_id'] ,
                                        'jenis_id'      => $jenis['jenis_id']
                                    )) ;?>

            <input name="savepost" class="btn btn-lg btn-primary" value="&radic; SIMPAN" type="submit" />
        </p>
    <?php echo form_close() ;?>  

</div>

<script type="text/javascript">
    $(document).ready(function(){
       $("#surat_plh").<?php if ($surat['surat_ttd'] == 2 OR $surat['surat_ttd'] == 3):?>show<?php else :?>hide<?php endif ;?>() ;
       
       $(".surat_ttd").click(function() {     
            var surat_ttd = $("input[name=surat_ttd]:checked").val() ;
            if (surat_ttd == 2 || surat_ttd == 3) {
                $("#surat_plh").show() ;
            }
            else {
                $("#surat_plh").hide() ;
            }
        }); 

        $("#klasifikasi_pr").change(function() {
            $("#klasifikasi_div").hide() ;

            $.get( "<?php echo site_url('ajax/entitas/klasifikasi') ;?>", {   
                keycode  : $(this).val() 
            },
                function( data ) {
                    var output = "";
                    $("#klasifikasi_id").select2("val","") ;
                    $("#klasifikasi_id").html(data) ;
                    console.log(data) ;
                }).done(
                function( data ) {
                    $("#klasifikasi_div").show() ;
                }
            )
        }) ; 

        $("#klasifikasi_id").change(function() {
            $.get( "<?php echo site_url('admin/suratout/no');?>", {   
                olah_id         : $("#olah_id").val() ,
                klasifikasi_id  : $(this).val() ,
                ttd_id          : $("#ttd_id").val() ,
                surat_id        : $("#surat_id").val() ,
                date            : $("#datepicker1").val(),
                jenis_id        : "<?php echo $jenis['jenis_id'] ;?>"
            })
            .done(function( data ) {
                $("#surat_no").val(data) ;    
            });
        }) ;

        for (var i = 3; i < 22; i++) { 
            $('#row' + i).hide() ;
        }

        $( ".addrow" ).blur(function() {
            var id = $("#rowid").val() ;

            $("#row" + id).show() ;

            id = (id*1 + 2) - 1 ;

            $("#rowid").val(id) ;
        });
    });
    
    
    
</script>