<div class="wrap">
    <h2>
        REKAPITULASI PERSURATAN DITJEN PSDKP - SURAT KELUAR - <?php echo strtoupper($jenis['jenis_name']) ;?>
        <?php if (!empty($surat) && $surat->num_rows() > 0) :?>
            <div id="pdf"><a href="<?php echo current_url().$uri_get.'&amp;export=pdf';?>" title="PDF">&nbsp;</a></div>
            <div id="print"><a onclick="popUpWindow('<?php echo current_url().$uri_get.'&amp;export=print';?>')" href="javascript:;" title="Print">&nbsp;</a></div>
            <div id="xls"><a href="<?php echo current_url().$uri_get.'&amp;export=xls';?>" title="Ms.Excel">&nbsp;</a></div>
        <?php endif ;?>
    </h2>

    <?php if (!empty($note)) : ?>
        <div id='message' class='<?php echo $symbol;?>' style="width: 94%;margin-bottom: 10px;"><p><strong><?php echo $note;?></strong></p></div>
    <?php endif ;?>
        
    <form action="<?php echo site_url('admin/report/rekapout/'.$jenis['jenis_id']);?>" method="get">    
        <table class="widefat" style="border: none;">
            <tr>
                <td width="12%;">Tanggal</td>
                <td width="1%">:</td>
                <td width="34%">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo form_input(array('name'=>'date_start','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker1'),$this->input->get('date_start',TRUE)) ;?>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" value="S.D." class="text-center form-control" disabled/>
                        </div>
                        <div class="col-sm-4">
                            <?php echo form_input(array('name'=>'date_end','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker2'),$this->input->get('date_end',TRUE)) ;?>
                        </div>
                    </div>
                </td>
                <td width="10%;" rowspan="3">Tampilan Kolom</td>
                <td width="1%" rowspan="3">:</td>
                <td rowspan="3"><?php echo form_multiselect('columns[]' , $columns, $cols ,'style="width:100%;"') ;?></td>
            </tr>
            <tr>
                <td>Sifat Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('sifat_id',$sifat_dw,$this->input->get('sifat_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Tanda Tangan</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('ttd_id',$ttd_dw,$this->input->get('ttd_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Pengolah / Asal Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('olah_id',$olah_dw,$this->input->get('olah_id',TRUE),'style="width:98%;"') ;?>
                </td>
                <td>Klasifikasi Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('klasifikasi_id',$klasifikasi_dw,$this->input->get('klasifikasi_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <?php if (in_array('tujuan_name',$column_keys) OR in_array('dispo_name',$column_keys) OR  in_array('type_warning',$column_keys) ) :?>
            <tr>
                <?php if (in_array('tujuan_name',$column_keys)) :?>
                    <td>Tujuan Surat</td>
                    <td>:</td>
                    <td>
                        <?php echo form_dropdown('tujuan_id',$tujuan_dw,$this->input->get('tujuan_id',TRUE),'style="width:98%;"') ;?>
                    </td>
                <?php elseif (in_array('tujuan_name',$column_keys)) :?>
                    <td>Tujuan Surat</td>
                    <td>:</td>
                    <td>
                        <?php echo form_dropdown('tujuan_disposisi',$tujuan_disposisi,$this->input->get('tujuan_disposisi',TRUE),'style="width:98%;"') ;?>
                    </td>
                <?php else :?>
                    <td colspan="3">&nbsp;</td>
                <?php endif ;?>

                <?php if (in_array('type_warning',$column_keys)) :?>
                    <td>Jenis Peringatan</td>
                    <td>:</td>
                    <td>
                        <?php echo form_dropdown('type_warning',$type_warning,$this->input->get('type_warning',TRUE),'style="width:98%;"') ;?>
                    </td>
                <?php else :?>
                    <td colspan="3">&nbsp;</td>
                <?php endif ;?>
            </tr>
        <?php endif ;?>
            <tr>
                <td>Kata Kunci</td>
                <td>:</td>
                <td>
                    <?php echo form_input(array('name'=>'keyword','placeholder'=>'Kata Kunci...','class'=>'form-control'), $this->input->get('keyword',TRUE) ) ;?>
                </td>
                <td colspan="3"><?php echo form_submit('filter', 'Cari','class="btn btn-primary btn-sm"') ;?></td>
            </tr>

            <tr>
                <td align="left" colspan="6">
                    
                </td>
            </tr>
        </table>
    </form>

    <?php if ($surat->num_rows() > 0) :?>

        <div class="table-responsive">

        <?php $alt = 1 ;?>

            <table class="table widefat table-bordered table-striped">
                <thead class="border">
                    <tr>
                        <th style="text-align: center;vertical-align: middle;">NO</th>
                        <?php foreach ($cols as $key => $val) :?>
                            <th style="text-align: center;vertical-align: middle;"><?php echo strtoupper($columns[$val]) ;?></th>
                        <?php endforeach ;?>
                    </tr>
                </thead>
                <tbody>

                    <?php $page += 1 ;?>
                    <?php foreach ($surat->result() as $a) :?>

                        <tr>
                            <td align="right" valign="top"><?php echo $page++ ;?>.</td>
                            
                            <?php foreach ($cols as $key => $val) :?>
                                <td valign="top">
                                    <?php if (in_array($val,array('tujuan_name'))) :?>
                                        <?php
                                            $tujuan = NULL ;
                                            if (!empty($a->tujuan_id))          $tujuan[] = strtr(array_to_list($tujuan_dw,json_decode($a->tujuan_id,TRUE),', '), array('..'=>'','- - -, '=>'','- - -'=>'','- tujuan -'=>'','- tujuan -, '=>'') ) ;
                                            if (!empty($a->tujuan_disposisi))   $tujuan[] = strtr(array_to_list($tujuan_disposisi,json_decode($a->tujuan_disposisi,TRUE),', '), array('..'=>'','- - -, '=>'','- - -'=>'','- tujuan -'=>'','- tujuan -, '=>'') ) ;
                                            if (!empty($a->surat_tujuan))       $tujuan[] = $a->surat_tujuan ;

                                            echo !empty($tujuan) ? implode('<br />',$tujuan) : '-' ;
                                        ?>
                                    <?php elseif (in_array($val,array('dispo_name'))) :?>
                                        <?php
                                            echo !empty($a->tujuan_disposisi) ? strtr(array_to_list($tujuan_disposisi,json_decode($a->tujuan_disposisi,TRUE),', '), array('..'=>'','- - -, '=>'','- tujuan -'=>'','- tujuan -, '=>'') ) : '-' ;
                                        ?>    
                                    <?php elseif (in_array($val,array('type_warning'))) :?>
                                        <?php
                                            echo !empty($a->type_warning) ? $type_warning[$a->type_warning] : '-' ;
                                        ?>  
                                   <?php elseif (in_array($val,array('berita_acara'))) :?>
                                        <?php
                                            echo !empty($a->berita_acara) ? $berita_acara[$a->berita_acara] : '-' ;
                                        ?>          
                                    <?php elseif (in_array($val,array('surat_plh_date','surat_spt_date'))) :?>
                                        <?php
                                            $date = NULL ;
                                            if (!empty($a->s_start))    $date[] = $a->s_start ;
                                            if (!empty($a->s_end))      $date[] = $a->s_end ;

                                            echo !empty($date) ? implode(' s.d ',$date) : '-' ;
                                        ?> 
                                    <?php elseif (in_array($val,array('surat_inv_date'))) :?>
                                        <?php
                                            $date = NULL ;
                                            if (!empty($a->s_inv_date))     $date[] = $a->s_inv_date ;
                                            if (!empty($a->surat_inv_time)) $date[] = $a->surat_inv_time ;

                                            echo !empty($date) ? implode(' ',$date) : '-' ;
                                        ?> 
                                    <?php elseif (in_array($val,array('surat_pelaksana'))) :?>
                                        <?php 
                                            $key        = 0 ; 
                                            $pelaksana  = !empty($a->surat_pelaksana) ? json_decode($a->surat_pelaksana,TRUE) : NULL ;
                                        ?>
                                        <?php if (!empty($pelaksana) && count($pelaksana) > 0) :?>
                                            <table class="widefat" style="border:none;">
                                                <tbody>
                                                <?php foreach ($pelaksana['name'] as $key => $val) :?>
                                                    <?php if (!empty($pelaksana['name'][$key])) :?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $pelaksana['name'][$key] ;?>
                                                            </td>
                                                            <td>
                                                                <?php echo !empty($pelaksana['nip'][$key]) ? $pelaksana['nip'][$key] : '-' ;?>
                                                            </td>
                                                            <?php if (in_array($jenis['jenis_id'],array(19,20)) ) :?>
                                                                <td style="width:25%;">
                                                                    <?php echo !empty($pelaksana['pangkat'][$key]) ? $pelaksana['pangkat'][$key] : '-'  ;?>
                                                                </td>
                                                            <?php endif ;?>
                                                            <td>
                                                                <?php echo !empty($pelaksana['jabatan'][$key]) ? $pelaksana['jabatan'][$key] : '-'  ;?>
                                                            </td>
                                                        </tr>
                                                    <?php endif ;?>
                                                <?php endforeach ;?>    
                                                </tbody>
                                            </table>
                                        <?php endif ;?>          
                                    <?php elseif ($val == 'file') :?>    
                                        <?php $document = $this->surat_outdocument_model->get(array('surat_id'=>$a->surat_id)) ;?>
                                        <?php if ($document->num_rows() > 0) : $no = 1 ; ?>
                                            <ul class="list-group">
                                                <?php foreach ($document->result() as $doc) :?>
                                                    <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                                        <li class="list-group-item">
                                                            <a class="btn btn-success btn-xs" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]">
                                                                <i class="glyphicon glyphicon-th-list"></i> <?php echo ellipsize($doc->document_filename, 15, .5) . '  ('.byte_format($doc->document_filesize).')';?>
                                                            </a>
                                                        </li>
                                                    <?php endif ;?>
                                                <?php endforeach ;?>
                                            </ul>
                                        <?php else :?>
                                                
                                        <?php endif ;?>
                                    <?php else :?>
                                        <?php echo $a->$val ;?>
                                    <?php endif ;?>
                                </td>
                            <?php endforeach ;?>
                        </tr>

                    <?php endforeach ;?> 

                </tbody>

                <?php if (!empty($paging)) :?>
                    <tfoot>
                        <tr>
                            <th colspan="<?php echo count($cols)+1 ;?>" class="text-left">
                                <div class="paging" style="text-align:left;">
                                    <?php echo $paging ;?>
                                </div>
                            </th>
                        </tr>
                    </tfoot>
                <?php endif ;?>
            </table>   

        </div>    

    <?php endif ;?>

</div>