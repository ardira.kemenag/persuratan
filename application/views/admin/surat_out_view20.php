<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>SURAT KELUAR : <?php echo strtoupper($jenis['jenis_name']);?> </h2>
    
    <?php $alt = 0 ;?>
    
        <table class="table table-striped">
            <tbody>
                <tr>
                    <td width="23%;"><b>Sifat Surat</b></th>
                    <td width="2%;"><b>:</b></th>
                    <td>
                        <?php echo $sifat_dw[$surat['sifat_id']] ;?>
                    </td>
                </tr>
                <tr>
                    <td><b>Tanggal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo time_to_words($surat['surat_date']);?>
                    </td>
                </tr>

                <tr>
                    <td><b>Tanda Tangan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo str_replace('.. ','',$ttd_dw[$surat['ttd_id']]) ;?>
                    </td>
                </tr>

                <?php if ($surat['surat_ttd'] != 1 && !empty($surat['surat_plh'])) :?>
                    <tr>
                        <td align="right"><b>Atas Nama</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php echo $surat['surat_plh'] ;?>
                        </td>
                    </tr>
                <?php endif ;?>
                
                <tr>
                    <td><b>Pengolah / Asal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo str_replace('.. ','',$olah_dw[$surat['olah_id']]) ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Klasifikasi Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo str_replace('.. ','',$klasifikasi_dw[$surat['klasifikasi_id']]) ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Dasar</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo nl2br($surat['surat_dasar']) ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tujuan</b></th>
                    <td><b>:</b></th>
                    <td><?php echo !empty($surat['tujuan_id']) ? str_replace('.. ','',array_to_list($tujuan_dw,json_decode($surat['tujuan_id'],TRUE),', ')) : '-' ;?></td>
                </tr>

                <tr>
                    <td><b>Kegiatan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_kegiatan'] ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Lokasi</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_lokasi'] ;?>
                    </td>
                </tr>


                <tr>
                    <td><b>Pelaksana</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php 
                            $key        = 0 ; 
                            $pelaksana  = !empty($surat['surat_pelaksana']) ? json_decode($surat['surat_pelaksana'],TRUE) : NULL ;
                        ?>
                        <?php if (!empty($pelaksana) && count($pelaksana) > 0) :?>
                            <table class="table widefat">
                                <thead>
                                    <tr>
                                        <th>NAMA</th>
                                        <th>NIP</th>
                                        <th>PANGKAT/GOL/RUANG</th>
                                        <th>JABATAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($pelaksana['name'] as $key => $val) :?>
                                    <?php if (!empty($pelaksana['name'][$key])) :?>
                                        <tr>
                                            <td style="width:25%;">
                                                <?php echo $pelaksana['name'][$key] ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo !empty($pelaksana['nip'][$key]) ? $pelaksana['nip'][$key] : '-' ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo !empty($pelaksana['pangkat'][$key]) ? $pelaksana['pangkat'][$key] : '-'  ;?>
                                            </td>
                                            <td style="width:25%;">
                                                <?php echo !empty($pelaksana['jabatan'][$key]) ? $pelaksana['jabatan'][$key] : '-'  ;?>
                                            </td>
                                        </tr>
                                    <?php endif ;?>
                                <?php endforeach ;?>    
                                </tbody>
                            </table>
                        <?php endif ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>PLH Sebagai</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_plh_as'] ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Tanggal PLH</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo time_to_words($surat['surat_start']);?>
                        &nbsp;s/d&nbsp;
                        <?php echo time_to_words($surat['surat_end']);?>
                    </td>
                </tr>

                <tr>
                    <td><b>Nomor Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_no'] ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Lampiran Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                            <ul class="list-group">
                                <?php foreach ($document->result() as $doc) :?>
                                    <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                        <li class="list-group-item">
                                            <a class="btn btn-info btn-sm" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]">
                                                <i class="glyphicon glyphicon-list"></i>
                                                <?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?>
                                            </a>
                                        </li>
                                    <?php endif ;?>
                                <?php endforeach ;?>
                            </ul>
                        <?php else :?>
                            -    
                        <?php endif ;?>
                    </td>
                </tr>

            </tbody>
        </table>

</div>