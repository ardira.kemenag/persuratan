<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<div class="wrap">
    <h2>
        FOLDER DOKUMEN
    </h2>
    
    <?php echo form_open(current_url(),array('method'=>'get','name'=>'theForm')) ;?>
    <p style="font-weight: bold;">
        ESELON /FOLDER : <?php echo form_dropdown('folder_parent',$folder_dw,$this->input->get('folder_parent'),'onchange="autoSubmit();"');?>
    </p>
    <?php echo form_close() ;?>

    <form method="post" action="<?php echo current_url() . $uri_get ;?>">

    <table class="widefat">
        <thead>
            <tr>
                <th scope="col" style="width: 2%;">NO</th>
                <th scope="col" style="width: 12%;">ESELON</th>
                <th scope="col" style="width: 15%;">FOLDER</th>
                <th scope="col" style="width: 10%;">DESCRIPTION</th>
                <th scope="col" style="width: 30%;">PARENT</th>
                <th scope="col">ORDER</th>
                <th scope="col" style="width: 5%;">FILES NUM</th>
                <th scope="col" style="width: 5%;">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php $no = ($page+1) ;?>
        <?php if ($folder->num_rows() > 0) :?>
            <?php foreach ($folder->result() as $m) :?>
                <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                    <td align="right"><?php echo $no++ ;?>.</td>
                    <td align="center"><?php echo form_dropdown('esatu_id_edit'.$m->folder_id,  $esatu_dw,$m->esatu_id,'style="width:98%;"') ;?></td>
                    <td align="center"><?php echo form_input(array('name'=>'folder_name_edit'.$m->folder_id,'style'=>'width:90%;'),$m->folder_name);?></td>
                    <td align="center"><?php echo form_input(array('name'=>'folder_description_edit'.$m->folder_id,'style'=>'width:90%;'),$m->folder_description);?></td>
                    <td align="center"><?php echo form_dropdown('folder_parent_edit'.$m->folder_id,  array_exeptions($folder_dw,$m->folder_id),$m->folder_parent,'style="width:98%;"') ;?></td>
                    <td align="center"><?php echo form_input(array('name'=>'folder_order_edit'.$m->folder_id,'size'=>2,'maxlength'=>2),$m->folder_order);?></td>
                    <td align="center">
                        <a href="<?php echo site_url('admin/dokumen/files');?>?folder_id=<?php echo $m->folder_id  ;?>" title="Ada <?php echo $m->file_num ;?> dokumen.">
                            <?php echo separator($m->file_num) ;?>
                        </a>
                    </td>
                    <td align="center"><a href="<?php echo site_url('admin/dokumen/folder/'.$m->folder_id.'/del') . $uri_get;?>" class="delete" onclick="return confirm('Apakah Anda yakin menghapus folder <?php echo $m->folder_name ;?> ?');">&nbsp;</a></td>
                </tr>
            <?php endforeach ;?>
        <?php endif ;?>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="8">
                <div class="paging">
                    <?php echo $paging ;?>
                </div>
            </th>
        </tr>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="8" style="text-align: left;">(+) TAMBAH FOLDER/SUB/KEGIATAN BARU</th>
        </tr>

        <?php foreach (range(1,5) as $i) :?>
            <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                <td align="center">&nbsp;</td>
                <td align="center">
                    <?php if (!empty($this->data['esatu_id']) && $level_id == 4) :?>
                        <?php echo $esatu_dw[$this->data['esatu_id']] ;?>
                        <?php echo form_hidden('esatu_id_new'.$i,$this->data['esatu_id']) ;?>
                    <?php else :?>
                        <?php echo form_dropdown('esatu_id_new'.$i,  $esatu_dw,$this->data['esatu_id'],'style="width:98%;"') ;?>
                    <?php endif ;?>
                </td>
                <td align="center"><?php echo form_input(array('name'=>'folder_name_new'.$i,'size'=>30));?></td>
                <td align="center"><?php echo form_input(array('name'=>'folder_description_new'.$i,'size'=>45));?></td>
                <td align="center"><?php echo form_dropdown('folder_parent_new'.$i,$folder_dw,$this->input->get('folder_parent'),'style="width:98%;"') ;?></td>
                <td align="center"><?php echo form_input(array('name'=>'folder_order_new'.$i,'size'=>2,'maxlength'=>2));?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
        <?php endforeach ;?>

        </tbody>

        <thead>
            <tr>
                <th scope="col" colspan="9" style="text-align: center;">
                    <?php echo form_hidden('limit', $limit) ;?>
                    <?php echo form_hidden('page' , $page)  ;?>
                    <?php echo form_hidden('sess_security',$sess_security) ;?>
                    <input class="button" name="savepost" value="SIMPAN &raquo;" type="submit" />
                </th>
            </tr>
        </thead>
    </table>

    <?php echo form_close() ;?>
</div>
