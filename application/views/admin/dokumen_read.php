<div class="wrap">
    <h2>
        DOKUMEN BIRO PERENCANAAN
    </h2>
    
    <p style="font-weight: bold;">
        TAHUN : <?php echo $file['tahun'] ;?>
        &nbsp;-&nbsp;
        <?php if (!empty($parent)) :?>
        <a href="<?php echo site_url('admin/dokumen/files').'?folder_id='.$parent['folder_id'].'&amp;tahun='.$file['tahun'] ;?>" class="a-ppt">
            <?php echo $parent['folder_name'] ;?> &gt;
        </a>
        <?php endif ;?>
        <a href="<?php echo site_url('admin/dokumen/files').'?folder_id='.$file['folder_id'].'&amp;tahun='.$file['tahun'] ;?>">
            <?php echo $file['folder_name'] ;?>
        </a>
    </p>
    
    <div class="vbook">
        <h4><?php echo $file['file_title'].'.'.$file['file_ext'] ;?></h4>
        <div>
            <?php echo $google_read ;?>
        </div>
    </div>
</div>