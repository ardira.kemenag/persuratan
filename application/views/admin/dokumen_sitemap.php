<!-- JQUERY TREEVIEW -->
<link rel="stylesheet" href="<?php echo _JAVASCRIPT_ ;?>jquery.treeview/jquery.treeview.css" />
<script src="<?php echo _JAVASCRIPT_ ;?>jquery.treeview/jquery.cookie.js" type="text/javascript"></script>
<script src="<?php echo _JAVASCRIPT_ ;?>jquery.treeview/jquery.treeview.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(document).ready(function(){
        $("#tree").treeview({
            persist: "location",
            collapsed: true,
            unique: true
        });
    }) ;
</script>

<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<div class="wrap">
    <h2>
        SITEMAP
    </h2>
    
    <?php echo $folders ;?>
    
</div>