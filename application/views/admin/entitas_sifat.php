<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>SIFAT SURAT</h2>
    
    <?php if ($sifat->num_rows() > 0) :?>
            
        <?php echo form_open(current_url()) ;?>  

            <div class="table-responsive">
                
            <table class="table widefat">
                <thead>
                    <tr>
                        <th scope="col" style="width: 3%;">NO</th>
                        <th scope="col" style="width: 87%;">SIFAT SURAT</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php $no = $page+1 ; foreach ($sifat->result() as $m) :?>
                    <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                        <td style="text-align: center;"><?php echo $no++ ;?>.</td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'sifat_name[]','class'=>'form-control'),$m->sifat_name);?>
                            <?php echo form_hidden('sifat_id[]',$m->sifat_id) ;?>
                        </td>
                        <td style="text-align: center;">
                            <a href="<?php echo site_url('admin/entitas/sifat/'.$m->sifat_id.'/del');?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus sifat surat ini?');">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ;?>
                
                <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                    <th scope="col" colspan="3">
                        <div class="paging">
                            <?php echo $paging ;?>
                        </div>
                    </th>
                </tr>    
                
                <?php foreach (range(1,5) as $i) :?>
                    <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                        <td style="text-align: center;">&nbsp;</td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'sifat_name[]','class'=>'form-control'),NULL);?>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                <?php endforeach ;?>
                
                </tbody>
                
            </table>

            <p class="text-center">
                <?php echo form_hidden('sess_security',$sess_security) ;?>
                <input class="btn btn-lg btn-primary" name="savepost" value="&radic; UPDATE &raquo;" type="submit" />
            </p>

            </div>
                
            <?php echo form_close() ;?>    
            
        <?php else :?>
            <div class="alert alert-warning">Data belum tersedia.</div>
        <?php endif ;?> 
    
</div>