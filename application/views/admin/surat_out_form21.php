<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>FORM SURAT KELUAR : <?php echo strtoupper($jenis['jenis_name']);?> </h2>
    
    <?php $alt = 0 ;?>
    
    <?php echo form_open_multipart("admin/suratout/c/".$jenis['jenis_id']) ;?>
        <div class="table-responsive">

        <table class="table widefat">
            <tbody>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td width="23%;"><b>Sifat Surat</b></th>
                    <td width="2%;"><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('sifat_id', $sifat_dw, set_value('sifat_id', ( !empty($surat['sifat_id']) ? $surat['sifat_id'] : 1 ) ),'style="width:90%;"') ;?>
                        <?php echo form_error('sifat_id') ;?>
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input id="datepicker1" name="surat_date" value="<?php echo set_value('surat_date', (!empty($surat['surat_date']) ? $surat['surat_date'] : date('Y-m-d') ) );?>" type="text" maxlength="10" size="10" class="form-control" />
                        <?php echo form_error('surat_date') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanda Tangan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('ttd_id',$ttd_dw,set_value('ttd_id',$surat['ttd_id']),'id="ttd_id" style="width:90%;"') ;?>
                        <?php echo form_error('ttd_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td>&nbsp;</th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_radio('surat_ttd',1,$surat['surat_ttd'] == 1,'class="surat_ttd"') ;?> Langsung
                        &nbsp;&nbsp;&nbsp;
                        <?php echo form_radio('surat_ttd',2,$surat['surat_ttd'] == 2,'class="surat_ttd"') ;?> PLH
                        &nbsp;&nbsp;&nbsp;
                        <?php echo form_radio('surat_ttd',3,$surat['surat_ttd'] == 3,'class="surat_ttd"') ;?> PLT
                        <?php echo form_error('surat_ttd') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?> id="surat_plh">
                    <td align="right"><b>Atas Nama</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_plh','class'=>'form-control'),set_value('surat_plh',$surat['surat_plh'])) ;?>
                        <?php echo form_error('surat_plh') ;?>
                    </td>
                </tr>
                
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Pengolah / Asal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('olah_id',$olah_dw,set_value('olah_id',$surat['olah_id']),'id="olah_id" style="width:90%;"') ;?>
                        <?php echo form_error('olah_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Klasifikasi Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('klasifikasi_id',$klasifikasi_dw,set_value('klasifikasi_id',( !empty($surat['klasifikasi_id']) ? $surat['klasifikasi_id'] : 318 )) ,'id="klasifikasi_id" style="width:90%;"') ;?>
                        <?php echo form_error('klasifikasi_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tujuan</b></th>
                    <td><b>:</b></th>
                    <td>&nbsp;</td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td align="right"><b>Internal PSDKP</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_multiselect('tujuan_disposisi[]',$olah_dw,!empty($surat['tujuan_disposisi']) ? json_decode($surat['tujuan_disposisi'],TRUE) : NULL ,'id="tujuan_disposisi" style="width:90%;"') ;?>
                        <?php echo form_error('tujuan_disposisi') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt%2==0):?> class="alternate"<?php endif ;?>>
                    <td align="right"><b>Internal KKP</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_multiselect('tujuan_id[]',$tujuan_dw,!empty($surat['tujuan_id']) ? json_decode($surat['tujuan_id'],TRUE) : NULL ,'id="tujuan_id" style="width:90%;"') ;?>
                        <?php echo form_error('tujuan_id') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td align="right"><b>Eksternal KKP</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_tujuan','class'=>'form-control'),set_value('surat_tujuan',$surat['surat_tujuan'])) ;?>
                        <?php echo form_error('surat_tujuan') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal Pelaksanaan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input id="datepicker2" name="surat_inv_date" value="<?php echo set_value('surat_inv_date',$surat['surat_inv_date']);?>" type="text" maxlength="10" size="10" class="form-control"  />
                        <?php echo form_error('surat_inv_date') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Jam Pelaksanaan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_inv_hour','size'=>'10','placeholder'=>'00:00','class'=>'form-control'),set_value('surat_inv_hour',$surat['surat_inv_hour'])) ;?>
                        <?php echo form_error('surat_inv_hour') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tempat Pelaksanaan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_lokasi','class'=>'form-control'),set_value('surat_lokasi',$surat['surat_lokasi'])) ;?>
                        <?php echo form_error('surat_lokasi') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Perihal</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_hal','class'=>'form-control','id'=>'surat_hal'),set_value('surat_hal',$surat['surat_hal'])) ;?>
                        <?php echo form_error('surat_hal') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Keterangan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_textarea(array('name'=>'surat_ket','class'=>'form-control','rows'=>3),set_value('surat_ket',$surat['surat_ket'])) ;?>
                        <?php echo form_error('surat_ket') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Nomor Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_input(array('name'=>'surat_no','class'=>'form-control'),set_value('surat_no',$surat['surat_no']),'id="surat_no"') ;?>
                        <?php echo form_error('surat_no') ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Upload Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                            <table class="table">
                            <?php foreach ($document->result() as $doc) :?>
                                <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                    <tr>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]">
                                                <i class="glyphicon glyphicon-list"></i>
                                                <?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('admin/suratout/c/'.$surat['jenis_id'].'/'.$surat['surat_id']).'?delete_doc='.$doc->document_id;?>" class="btn btn-sm btn-danger">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endif ;?>
                            <?php endforeach ;?>
                            </table>
                        <?php endif ;?>

                        <div class="row">
                            <div class="col-sm-10">
                                <input type="file" name="document_name1" class="form-control" />
                            </div>
                            <div class="col-sm-2">
                                <a href="javascript:void(0)" id="button-add-softcopy" class="btn btn-sm btn-success">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </a>
                            </a>    
                            </div>
                        </div>

                        <div id="add-softcopy"></div>
                        <input type="hidden" id="doc_id" value="2" /> 
                    </td>
                </tr>

            </tbody>
        </table>

        </div>

        <p class="text-center">
            <input type="hidden" id="surat_id" value="<?php echo $surat['surat_id'] ;?>" />
            <?php echo form_hidden(array(
                                        'sess_security' => $sess_security ,
                                        'user_id'       => !empty($surat['user_id']) ? $surat['user_id'] : $user_id ,
                                        'surat_id'      => $surat['surat_id'] ,
                                        'jenis_id'      => $jenis['jenis_id']
                                    )) ;?>

            <input name="savepost" class="btn btn-lg btn-primary" value="&radic; SIMPAN" type="submit" />
        </p>
    <?php echo form_close() ;?>  

</div>

<script type="text/javascript">
    $(document).ready(function(){
       $("#surat_plh").<?php if ($surat['surat_ttd'] == 2 OR $surat['surat_ttd'] == 3):?>show<?php else :?>hide<?php endif ;?>() ;
       
       $(".surat_ttd").click(function() {     
            var surat_ttd = $("input[name=surat_ttd]:checked").val() ;
            if (surat_ttd == 2 || surat_ttd == 3) {
                $("#surat_plh").show() ;
            }
            else {
                $("#surat_plh").hide() ;
            }
        }); 

       $("#surat_hal").blur(function() {
            $.get( "<?php echo site_url('admin/suratout/no');?>", {   
                olah_id         : $("#olah_id").val() ,
                klasifikasi_id  : $("#klasifikasi_id").val() ,
                ttd_id          : $("#ttd_id").val() ,
                surat_id        : $("#surat_id").val() ,
                date            : $("#datepicker1").val(),
                jenis_id        : "<?php echo $jenis['jenis_id'] ;?>"
            })
            .done(function( data ) {
                $("#surat_no").val(data) ;    
            });

            

            
       }) ;
    });
    
    
    
</script>