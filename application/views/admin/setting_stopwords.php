<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<div class="wrap">
    <h2>
        <?php echo $this->lang->line('set_stopwords_title') ;?>
    </h2>

    <?php echo form_open("admin/setting/stopwords") ;?>
    <p>
        <?php echo $this->lang->line('set_stopwords_info') ;?>
    </p>
    <p align="center">
        <textarea name="config_style" style="width: 98%;font-size: 90%;line-height: 200%;" rows="20">
            <?php echo $bi_stopwords ;?>
        </textarea>
    </p>
    <p class="submit">
        <?php echo form_hidden(
                                    array(  'sess_security'     => $sess_security,
                                            'config_name'       => 'stopwords' ,
                                            'config_name_lang'  => $this->lang->line('set_stopwords_val') ,
                                            'config_id_lang'    => $this->lang->line('set_stopwords_val') ,
                                            'config_style_lang' => $this->lang->line('set_stopwords_val'))) ;?>
        <?php echo form_submit('savepost', $this->lang->line('set_stopwords_save')) ;?>
    </p>
    <?php echo form_close() ;?>
</div>