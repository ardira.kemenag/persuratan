<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>KLASIFIKASI SURAT</h2>
    
    <?php echo form_open(current_url()) ;?>

        <div class="table-responsive">  
                
        <table class="table widefat">
            <thead>
                <tr>
                    <th scope="col" style="width: 3%;">NO</th>
                    <th scope="col" style="width: 40%;">KLASIFIKASI SURAT</th>
                    <th scope="col" style="width: 15%;">KODE</th>
                    <th scope="col" style="width: 30%;">PARENT</th>
                    <th scope="col" style="width: 8%;">ORDER</th>
                    <th scope="col">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                
            <?php if ($klasifikasi->num_rows() > 0) :?>
                <?php $no = $page+1 ; foreach ($klasifikasi->result() as $m) :?>
                    <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                        <td style="text-align: center;"><?php echo $no++ ;?>.</td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'klasifikasi_name[]','class'=>'form-control'),$m->klasifikasi_name);?>
                            <?php echo form_hidden('klasifikasi_id[]',$m->klasifikasi_id) ;?>
                        </td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'klasifikasi_kode[]','class'=>'form-control'),$m->klasifikasi_kode);?>
                        </td>
                        <td>
                            <?php echo form_dropdown('klasifikasi_parent[]',$klasifikasi_dw,$m->klasifikasi_parent,'style="width:100%;"') ;?>
                        </td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'klasifikasi_order[]','class'=>'form-control','type'=>'number'),$m->klasifikasi_order);?>
                        </td>
                        <td style="text-align: center;">
                            <a href="<?php echo site_url('admin/entitas/klasifikasi/'.$m->klasifikasi_id.'/del');?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ;?>
                
                <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                    <th scope="col" colspan="6">
                        <div class="paging">
                            <?php echo $paging ;?>
                        </div>
                    </th>
                </tr>    

            <?php endif ;?>
                
            <?php foreach (range(1,5) as $i) :?>
                <?php $no = !empty($no) ? $no : 1 ;?>
                <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'klasifikasi_name[]','class'=>'form-control'),NULL);?>
                        <?php echo form_hidden('klasifikasi_id[]',NULL) ;?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'klasifikasi_kode[]','class'=>'form-control'),NULL);?>
                    </td>
                    <td>
                        <?php echo form_dropdown('klasifikasi_parent[]',$klasifikasi_dw,NULL,'style="width:100%;"') ;?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'klasifikasi_order[]','class'=>'form-control','type'=>'number'),NULL);?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            <?php endforeach ;?>
                
            </tbody>
                
        </table>

        </div>

        <p class="text-center">
            <?php echo form_hidden('sess_security',$sess_security) ;?>
            <input class="btn btn-lg btn-primary" name="savepost" value="&radic; UPDATE &raquo;" type="submit" />
        </p>

    <?php echo form_close() ;?>    
            
        
    
</div>