<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        SURAT MASUK
        <div id="pdf"><a href="<?php echo current_url().'?view=1&amp;export=pdf';?>" title="PDF">&nbsp;</a></div>
        <div id="print"><a onclick="popUpWindow('<?php echo  current_url().'?view=1&amp;export=print';?>')" href="javascript:;" title="Print">&nbsp;</a></div>
        <div id="xls"><a href="<?php echo current_url().'?view=1&amp;export=xls';?>" title="Ms.Excel">&nbsp;</a></div>
    </h2>

    <p style="font-weight:bold;text-align:center;">KEMENTERIAN KELAUTAN DAN PERIKANAN<br />PEJABAT ESELON I</p>

    <p style="font-weight:bold;text-align:center;">LEMBAR DISPOSISI</p>
    
    <?php if (!empty($surat['reply_surat_number'])) :?>
    <p>
        <b>BALASAN DARI : 
            <a href="<?php echo site_url('admin/suratin/c/'.$surat['reply_format_id'].'/'.$surat['surat_reply_id']);?>" class="vtip" title="<small><?php echo time_to_words($surat['reply_surat_date'],TRUE);?></small><br /><b>No. Surat : <?php echo html_entity_decode($surat['reply_surat_number']) ;?></b><br /><b>Perihal &nbsp;&nbsp;&nbsp;&nbsp;: </b><?php echo html_entity_decode($surat['reply_surat_perihal']) ;?>">
                <?php echo character_limiter($surat['reply_surat_perihal'],80) ;?>
            </a>
        </b>
    </p>
    <?php endif ;?>

    <table class="table">
        <tbody>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Sifat Surat</td>
                <td colspan="3" style="border-bottom:1px solid #ccc;">: <?php echo $sifat_dw[$surat['sifat_id']] ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Nomor Agenda</td>
                <td style="border-bottom:1px solid #ccc;">: </td>
                <td style="border-bottom:1px solid #ccc;">Tgl. Terima Surat</td>
                <td style="border-bottom:1px solid #ccc;">: <?php echo time_to_words($surat['surat_date_taken']) ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Nomor Surat</td>
                <td style="border-bottom:1px solid #ccc;">: <?php echo $surat['surat_number'] ;?></td>
                <td style="border-bottom:1px solid #ccc;">Tgl. Surat</td>
                <td style="border-bottom:1px solid #ccc;">: <?php echo time_to_words($surat['surat_date']) ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Asal Surat</td>
                <td colspan="3" style="border-bottom:1px solid #ccc;">: <?php echo $surat['surat_from'] ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Perihal</td>
                <td colspan="3" style="border-bottom:1px solid #ccc;">: <?php echo $surat['surat_perihal'] ;?></td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid #ccc;border-right:1px solid #ccc;">
                    <?php if ($aksi->num_rows() > 0) :?>
                        <ul style="margin:0;padding-left:15px;list-style:none;">
                            <?php foreach ($aksi->result() as $a) :?>
                                <li><?php echo (in_array($a->aksi_id, $aksi_post) ? '&check;' : '&bull;'  ).' '.$a->aksi_name ;?></li>
                            <?php endforeach ;?>
                        </ul>
                    <?php endif ;?>
                </td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">
                    <?php if ($disposisi->num_rows() > 0) :?>
                        <ul style="margin:0;padding-left:15px;list-style:none;">
                        <?php foreach ($disposisi->result() as $d) :?>
                            <?php
                                $subdis = $this->disposisi_sub_model->show_tree(array('disposisi_id'=>$d->disposisi_id)) ;
                                if (!empty($subdis) && count($subdis) > 0) : ?>
                                    <?php foreach ($subdis as $subdis_id => $subdis_name) :?>
                                        <?php if (in_array($subdis_id, $disposisi_post)) :?>
                                            <li>&check; <?php echo str_replace('.. ','',$subdis_name) ;?></li>
                                        <?php endif ;?>    
                                    <?php endforeach ;?>
                                <?php endif ;?>
                        <?php endforeach ;?>
                        </ul>
                    <?php endif ;?>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <p>Catatan : </p>
                    <?php if (!empty($surat['surat_disposisi_message'])) :?>
                        <p><?php echo nl2br($surat['surat_disposisi_message']) ;?></p>
                    <?php else :?>
                        <p>-</p>
                    <?php endif ;?>  
                </td>
            </tr>
        </tbody>

    </table>
</div>