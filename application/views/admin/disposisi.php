<div class="wrap">
	 <?php echo form_open_multipart("admin/suratin/isi_dispos");?>
	<table class="table table-striped">
		<tr>	
			<td colspan="3"><b><?php 
				foreach($cek_subdis->result() as $b){
					echo $b->subdis_name;					
				}
			?></b></td>
		</tr>
		<tr>	
			<td width="20%"><b>Disposisi</b></td>
			<td>:</td>
			<td>
				<?php foreach ($disposisi->result() as $d) :?>
					
					<?php
						$subdis = $this->disposisi_sub_model->show_tree(array('disposisi_id'=>$d->disposisi_id,'subdis_parent'=> !in_array($level_id,array(1)) ? $subdis_id : NULL )) ;
						if (!empty($subdis) && count($subdis) > 0) : ?>
							<?php foreach ($subdis as $subdis_id => $subdis_name) :?>
							<?php
								$checkbox = form_checkbox('surat_disposisi[]', $subdis_id, in_array($subdis_id, $disposisi_post),'id="dis_id'.$subdis_id.'"') .' ' ;
								$form_category = $checkbox.' '.$subdis_name ;
							?>
							<?php echo $form_category  ;?><br />
							<?php endforeach ;?>
						<?php endif ;?>
					<br />
				<?php endforeach ;?>
			</td>
		<tr>
		<tr>	
			<td><b>Pesan Disposisi</b></td>
			<td>:</td>
			<td>
				<?php echo form_textarea(array('name'=>'into_msg','rows'=>6),set_value('into_msg'),'class="form-control"');?>
				<input type="hidden" name="surat_id" value="<?php echo $this->uri->segment(4);?>"/>
				<input type="hidden" name="into_id" value="<?php 
				$query=$this->db->get_where('surat__into', array('surat_id' => $this->uri->segment(4)));
				foreach($query->result() as $c){
					echo $c->into_id;
				}
				?>"/>
			</td>
		</tr>
		<tr>	
			<td><b>Penanggung Jawab</b></td>
			<td>:</td>
			<td>
				<?php foreach ($disposisi->result() as $d) :?>
				<?php
					$subdis = $this->disposisi_sub_model->get(array('disposisi_id'=>$d->disposisi_id));
						foreach ($subdis->result() as $sb) : ?>
						<div id="respon_subdis_id<?php echo $sb->subdis_id;?>" <?php if (!in_array($sb->subdis_id,$disposisi_post)) :?>style="display: none;"<?php endif ;?>>
							<?php echo form_checkbox('surat_respon[]', $sb->subdis_id, in_array($sb->subdis_id,$respon_post)).'&nbsp;'.$sb->subdis_name.'&nbsp;&nbsp;' ;?>
						</div>
				<?php endforeach ;?>
				<?php endforeach ;?>
			</td>
		<tr>
		<tr>	
			<td><b>Aksi yang Diperlukan|</b></td>
			<td>:</td>
			<td>
				<?php foreach ($aksi->result() as $a) :?>
					<?php echo form_checkbox('aksi_id[]', $a->aksi_id, in_array($a->aksi_id, $aksi_post)).'&nbsp;'.$a->aksi_name.' ('.$a->aksi_akronim.')' ;?>
					<br />
				<?php endforeach ;?>
			</td>
		<tr>
		<tr>
			<td align="center" colspan="3"> 
				<input name="surat_view" class="biasa btn btn-lg btn-primary" value="Send" type="submit" onclick="return confirm('Apakah Anda yakin mengirim disposisi ini, pelaksanaan instruksi yang telah terjadi sebelumnya tidak berlaku jika telah direvisi?');" />
			</td>
		</tr>
	</table>
	<?php echo form_close() ;?>
	
</div>
<script type="text/javascript">
    $(document).ready(function(){
        <?php if ($subdis->num_rows() > 0) :
            foreach ($subdis->result() as $sb) : ?>
                $("#dis_id<?php echo $sb->subdis_id;?>").click(function(){
                  if ($("#dis_id<?php echo $sb->subdis_id;?>").is(":checked"))
                     $("#respon_subdis_id<?php echo $sb->subdis_id;?>").show();
                  else
                    $("#respon_subdis_id<?php echo $sb->subdis_id;?>").hide();
               });
            <?php endforeach ;?>
        <?php endif ;?>
    }) ;
</script>