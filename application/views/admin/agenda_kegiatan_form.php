<?php echo form_open_multipart("admin/agenda/kegiatan") ;?>
    <div class="wrap">
        <?php if (!empty($note)) : ?>
            <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
        <?php endif ;?>

        <h2>FORM AGENDA UNDANGAN</h2>

        <br />
        <table class="table widefat" cellpadding="5" cellspacing="2" width="100%" style="border : 1px solid #E1E1F2;">
            <tbody>

                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_name">Agenda Rapat</label>
                        <?php echo form_error('kegiatan_name');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" name="kegiatan_name" value="<?php echo set_value('kegiatan_name', $kegiatan ? $kegiatan['kegiatan_name'] : NULL);?>" maxlength="255" style="width : 98% ;" />
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_tempat">Tempat Kegiatan</label>
                        <?php echo form_error('kegiatan_tempat');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" name="kegiatan_tempat" value="<?php echo set_value('kegiatan_tempat', $kegiatan ? $kegiatan['kegiatan_tempat'] : NULL);?>" maxlength="255" style="width : 98% ;" />
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_date_start">Tanggal Kegiatan</label>
                        <?php echo form_error('kegiatan_date_start');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" id="datepicker1" name="kegiatan_date_start" value="<?php echo set_value('kegiatan_date_start', $kegiatan ? $kegiatan['start_date_diff'] : NULL);?>" maxlength="10" size="10" />
                         s.d 
                        <input type="text" id="datepicker2" name="kegiatan_date_end" value="<?php echo set_value('kegiatan_date_end', $kegiatan ? $kegiatan['end_date_diff'] : NULL);?>" maxlength="10" size="10" /> 
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_time_start">Waktu Kegiatan</label>
                        <?php echo form_error('kegiatan_time_start');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" name="kegiatan_time_start" value="<?php echo set_value('kegiatan_time_start', $kegiatan ? $kegiatan['kegiatan_time_start'] : '00:00');?>" maxlength="20" size="20" />
                         s.d 
                        <input type="text" name="kegiatan_time_end" value="<?php echo set_value('kegiatan_time_end', $kegiatan ? $kegiatan['kegiatan_time_end'] : '00:00');?>" maxlength="20" size="20" /> 
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_name">Asal Undangan</label>
                        <?php echo form_error('kegiatan_penyelenggara');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" name="kegiatan_penyelenggara" value="<?php echo set_value('kegiatan_penyelenggara', $kegiatan ? $kegiatan['kegiatan_penyelenggara'] : NULL);?>" maxlength="255" style="width : 98% ;" />
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_name">Disposisi</label>
                        <?php echo form_error('kegiatan_peserta');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <input type="text" name="kegiatan_peserta" value="<?php echo set_value('kegiatan_peserta', $kegiatan ? $kegiatan['kegiatan_peserta'] : NULL);?>" maxlength="255" style="width : 98% ;" />
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="kegiatan_keterangan">Keterangan</label>
                        <?php echo form_error('kegiatan_keterangan');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <?php echo form_textarea(array('id'=>'textarea','rows'=>3,'style'=>'width:100%;','name'=>'kegiatan_keterangan'),set_value('kegiatan_keterangan',$kegiatan['kegiatan_keterangan'])) ;?>
                        <?php echo form_error('kegiatan_keterangan') ;?>
                    </th>
                </tr>
                
                <tr class="alternate">
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <label for="status_id">Status Kegiatan</label>
                        <?php echo form_error('status_id');?>
                    </th>
                </tr>
                <tr>
                    <th scope="row" valign="top" style="text-align : left;" colspan="2">
                        <?php echo form_dropdown ('status_id',$status_dw,$kegiatan['status_id']) ;?>
                    </th>
                </tr>
                
                <?php if ($kegiatan && !empty($kegiatan['fullname'])) :?>
                <tr class="alternate">
                    <th colspan="2" scope="row" valign="top" width="100%" style="text-align : right;font-style: italic;">
                        Penyuntingan terakhir dilakukan oleh <?php echo $kegiatan['fullname'] ;?>
                    </th>
                </tr>
                <?php endif ;?>
                
            </tbody>

        </table>

        <p class="text-center">
            <?php echo form_hidden( array(  'kegiatan_id'       => $kegiatan['kegiatan_id'] ,
                                            'surat_id'          => $kegiatan['surat_id'] ,
                                            'user_id'           => $user_id ,
                                            'sess_security'     => $sess_security)
                                ) ;?>
            <input name="savepost" value="&radic; SIMPAN &raquo;" type="submit" class="btn btn-lg btn-primary" />
        </p>
    </div>

<?php echo form_close() ;?>