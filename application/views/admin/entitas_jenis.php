<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>JENIS SURAT</h2>
    
    <?php if ($jenis->num_rows() > 0) :?>
            
        <?php echo form_open(current_url()) ;?>  
                
            <div class="table-responsive">

            <table class="table widefat">
                <thead>
                    <tr>
                        <th scope="col" style="width: 3%;">NO</th>
                        <th scope="col" style="width: 87%;">JENIS SURAT</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php $no = $page+1 ; foreach ($jenis->result() as $m) :?>
                    <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                        <td style="text-align: center;"><?php echo $no++ ;?>.</td>
                        <td style="text-align: center;">
                            <?php echo form_input(array('name'=>'jenis_name[]','class'=>'form-control'),$m->jenis_name);?>
                            <?php echo form_hidden('jenis_id[]',$m->jenis_id) ;?>
                        </td>
                        <td style="text-align: center;">
                            <a href="<?php echo site_url('admin/entitas/jenis/'.$m->jenis_id.'/del');?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus jenis surat ini?');">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ;?>
                
                <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                    <th scope="col" colspan="3">
                        <div class="paging">
                            <?php echo $paging ;?>
                        </div>
                    </th>
                </tr>    
                
                <?php foreach (range(1,5) as $i) :?>
                    <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                        <td style="text-align: center;">&nbsp;</td>
                        <td style="text-align: center;"><?php echo form_input(array('name'=>'jenis_name[]','class'=>'form-control'),NULL);?></td>
                        <td>&nbsp;</td>
                    </tr>
                <?php endforeach ;?>
                
                </tbody>
                
            </table>

            </div>

            <p class="text-center">
                <?php echo form_hidden('sess_security',$sess_security) ;?>
                <input class="btnn btn-lg btn-primary" name="savepost" value="&radic; UPDATE &raquo;" type="submit" />
            </p>
                
            <?php echo form_close() ;?>    
            
        <?php else :?>
            <div class="alert alert-warning"><p>Data belum tersedia.</p></div>
        <?php endif ;?> 
    
</div>