<div class="wrap">
    <h2>
        STATISTIK SURAT KELUAR PER BULAN
    </h2>

    <?php if (!empty($note)) : ?>
        <div id='message' class='<?php echo $symbol;?>' style="width: 94%;margin-bottom: 10px;"><p><strong><?php echo $note;?></strong></p></div>
    <?php endif ;?>
        
    <form action="<?php echo current_url();?>" method="get">    
        <table class="widefat" style="border: none;">
            <tr>
                <td width="12%;">Bulan - Tahun</td>
                <td width="1%">:</td>
                <td width="34%">
                    <?php echo form_dropdown('month_start',$month_dw,$postdata['month_start']) ;?>
                    &nbsp;s.d.&nbsp;
                    <?php echo form_dropdown('month_end',$month_dw,$postdata['month_end']) ;?>
                    &nbsp;&nbsp;
                    <?php echo form_dropdown('tahun',$tahun_dw,$postdata['tahun']) ;?>
                </td>
                <td width="12%;">Jenis Surat</td>
                <td width="1%">:</td>
                <td>
                    <?php echo form_dropdown('jenis_id',$jenis_dw,$this->input->get('jenis_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Sifat Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('sifat_id',$sifat_dw,$this->input->get('sifat_id',TRUE),'style="width:98%;"') ;?>
                </td>
                <td>Tanda Tangan</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('ttd_id',$ttd_dw,$this->input->get('ttd_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Pengolah / Asal Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('olah_id',$olah_dw,$this->input->get('olah_id',TRUE),'style="width:98%;"') ;?>
                </td>
                <td>Klasifikasi Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('klasifikasi_id',$klasifikasi_dw,$this->input->get('klasifikasi_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Tujuan Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('tujuan_id',$tujuan_dw,$this->input->get('tujuan_id',TRUE),'style="width:98%;"') ;?>
                </td>

                <td>Jenis Peringatan</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('type_warning',$type_warning,$this->input->get('type_warning',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Kata Kunci</td>
                <td>:</td>
                <td>
                    <?php echo form_input(array('name'=>'keyword','placeholder'=>'Kata Kunci...','class'=>'form-control'), $this->input->get('keyword',TRUE) ) ;?>
                </td>
                <td colspan="3"><?php echo form_submit('filter', 'Cari','class="btn btn-sm btn-primary"') ;?></td>
            </tr>

            <tr>
                <td align="left" colspan="6">
                    
                </td>
            </tr>
        </table>
    </form>

    <p>&nbsp;</p> 

    <?php foreach ($columns as $col) :?>
        <?php if (!empty($tables[$col['id']]) && $tables[$col['id']]->num_rows() > 0) :?>

            <div class="row">

                <div class="col-md-6">

                    <p><b>TABEL JUMLAH SURAT KELUAR BERDASARKAN <?php echo strtoupper($col['name']) ;?> DARI <?php echo strtoupper($month_dw[$postdata['month_start']]).' - '.strtoupper($month_dw[$postdata['month_end']]) ;?> <?php echo $postdata['tahun'] ;?></b></p>

                    <div class="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th><?php echo strtoupper($col['name']) ;?></th>
                                <th>JUMLAH SURAT KELUAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no     = 1 ;
                                $total  = 0 ;
                            ?>
                            <?php foreach ($tables[$col['id']]->result_array() as $a) :?>
                                <?php $total += $a[$col['field']] ;?>
                                <tr>
                                    <td align="center"><?php echo $no++ ;?>.</td>
                                    <td><?php echo !empty($col['dw'][$a[$col['id']]]) ? str_replace('.. ','',$col['dw'][$a[$col['id']]]) : ( $col['id'] == 'user_id' ? 'Administrator' : 'Eksternal dan Umum') ;?></td>
                                    <td align="center"><?php echo separator($a[$col['field']]) ;?></td>
                                </tr>
                            <?php endforeach ;?>    
                            <tr class="alternate" style="font-weight:bold;">
                                <td colspan="2" align="right">JUMLAH</td>
                                <td align="center"><?php echo separator($total) ;?></td>
                            </tr>
                        </tbody>
                    </table>

                    </div>

                </div>

                <div class="col-md-6">
                    <?php echo !empty($graph[$col['id']]) ? $graph[$col['id']] : '' ;?>
                </div>

            </div>
        <?php endif ;?>
    <?php endforeach ;?>    

    <?php echo $charts ;?>       
    
</div>