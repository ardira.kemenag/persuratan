<div class="wrap">
    <h2>Selamat Datang di Dokumentasi Lembaga &amp; Persuratan Internal</h2>

    <div class="row">

    <div class="col-md-8">
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sollicitudin erat id urna convallis rhoncus. In venenatis vitae magna ut aliquam. Etiam congue, eros rutrum porta aliquam, dolor enim mollis sem, et ullamcorper eros nibh eget leo. Nam mattis varius commodo. Phasellus sagittis dolor nulla, sed faucibus massa aliquet eu. Etiam maximus dapibus dapibus. Mauris pharetra auctor dapibus. Quisque eu porttitor dolor. Integer et mollis justo. Morbi at ante eu nisl porttitor convallis id eu justo. Sed quis feugiat lectus. Nullam in est massa. Ut rutrum commodo odio, sit amet aliquet nulla commodo at. Suspendisse potenti. Fusce non tortor nec elit pretium vehicula molestie nec est. Pellentesque tristique bibendum risus ut cursus.
        </p>
        <p>
            Etiam at varius massa, et sodales dui. Nulla ut lectus id ante accumsan rutrum dapibus ac nisi. Fusce ultrices orci enim, sed aliquam elit tempor sit amet. Aliquam convallis, nisl eget auctor finibus, neque mi commodo mauris, in posuere est sapien nec mi. Donec laoreet quam et pharetra lobortis. Pellentesque leo elit, interdum accumsan quam quis, suscipit auctor enim. Fusce interdum ante ac neque suscipit sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et pharetra metus. Praesent tempus ipsum urna, eget mollis mauris vulputate et.
        </p>
        <p>
            Suspendisse quis vulputate purus. Sed sed nisl placerat, ultrices dui id, lacinia lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus malesuada finibus mauris, vitae ullamcorper nulla dictum vitae. Morbi a gravida arcu. Aliquam vehicula ante ex, interdum consectetur nibh mollis non. Suspendisse sapien justo, cursus vestibulum mi vitae, sagittis commodo neque. Maecenas eget nisl mollis, blandit nisl sed, semper sem. Sed nec pharetra tortor. Integer at ornare mi, vel sollicitudin nibh. Mauris in ipsum felis. In a ipsum elementum, malesuada diam in, vulputate nisi. Etiam eget nunc vitae ex vestibulum bibendum.
        </p>
    </div>

    <div class="col-md-4 hidden-sm hidden-xs">

        <div id="zeitgeist">
            <div>
                <h3>
                    <a style="color: #000; border:none;" href="<?php echo site_url('admin/users/profile');?>">
                        Profil
                    </a>
                </h3>
                    <?php $photo = !empty($user['user_img']) && file_exists(_PATH_UPLOAD_IMG_THUMBS_ . $user['user_img'])
                                        ? _URL_UPLOAD_IMG_THUMBS_ . $user['user_img']
                                            : _IMAGES_ . 'nophoto.jpg' ;
                    ?>
                    <img width="100" style="float: left; border: 3px solid #dedede; margin: 10px 10px 3px 3px;" alt="<?php echo $user['fullname'];?>" src="<?php echo $photo ;?>">
                    <br>
                <p><b><?php echo $user['fullname'];?></b> (<?php echo $user['username'];?>)</p>
                <p>Status : <?php echo $user['level_name'] ;?></p>
                <p>Terdaftar sejak : <br /><?php echo time_to_words($user['time_register'],TRUE) .' '.$user['time_diff'];?> </p>
           </div>
        </div>

    </div>

    </div>

</div>