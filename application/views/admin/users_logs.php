<div class="wrap">
    <h2>USER LOGS</h2>

    <table class="widefat" style="border: none;">
        <tr>
            <td align="right">
                <form action="<?php echo site_url('admin/users/logs');?>" method="get">
                    <?php echo form_input('keyword', (!empty($_GET['keyword']) ? $_GET['keyword'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('user_log_list_search'),'class="button"') ;?>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form action="<?php echo site_url('admin/users/logs');?>" method="get">
                    <?php echo $this->lang->line('user_log_list_login') ;?> : <?php echo form_input(array('name'=>'date','size'=>9,'maxlength'=>10) , (!empty($_GET['date']) ? $_GET['date'] : NULL),'id="datepicker"') ;?>
                        &nbsp;
                    <?php echo form_dropdown('order'    , $order_dw     , (!empty($_GET['order'])   ? $_GET['order']    : NULL) ) ;?>
                        &nbsp; <br /><br />
                    <?php echo form_dropdown('level_id' , $level_dw     , (!empty($_GET['level_id'])? $_GET['level_id'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('browser'  , $browser_dw   , (!empty($_GET['browser']) ? $_GET['browser']  : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('platform' , $platform_dw  , (!empty($_GET['platform'])? $_GET['platform'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('ip'       , $ip_dw        , (!empty($_GET['ip'])      ? $_GET['ip']       : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('mobile'   , $mobile_dw    , (!empty($_GET['mobile'])  ? $_GET['mobile']   : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('user_log_list_filter'),'class="button"') ;?>
                </form>
            </td>
        </tr>

    </table>

    <?php if ($logs->num_rows() > 0) :?>
    <table class="table widefat">
        <thead>
            <tr>
                <th scope="col" style="width: 2%;"><?php echo $this->lang->line('user_log_list_table_no') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_login') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_logout') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_time') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_fullname') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_level') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_ip') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_browser') ;?></th>
                <th scope="col"><?php echo $this->lang->line('user_log_list_table_os') ;?></th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>
        <tbody id="the-list">
        <?php $page += 1 ; foreach($logs->result() as $a) :?>
            <tr<?php if ($page%2==0) :?> class="alternate"<?php endif;?>>
                <td valign="top" align="right"><?php echo $page++ ;?>.</td>

                <td valign="top" align="center"><?php echo $a->login_date_diff . ' ' . $a->login_time_diff ;?></td>
                <td valign="top" align="center"><?php echo $a->logout_date_diff . ' ' . $a->logout_time_diff ;?></td>
                <td valign="top" align="center"><?php echo $a->long_time ;?></td>

                <td valign="top"><?php echo $a->fullname ;?></td>
                <td valign="top" align="center"><?php echo $a->level_name ;?></td>

                <td valign="top"><?php echo $a->ip ;?></td>
                <td valign="top"><?php echo $a->browser ;?></td>
                <td valign="top"><?php echo $a->platform ;?></td>
                <td valign="top"><a href="<?php echo site_url('admin/users/logs/'.$a->log_id);?>?iframe=true&amp;width=800&amp;height=300" rel="prettyPhoto[iframe]"><?php echo $this->lang->line('user_log_list_table_more') ;?></a></td>
            </tr>
        <?php endforeach ;?>

            <tr<?php if ($page%2==0) :?> class="alternate"<?php endif;?>>
                <th scope="row" colspan="10" style="vertical-align: top;">
                    <div class="paging">
                        <?php echo $paging ;?>
                    </div>
                </th>
            </tr>
        </tbody>
    </table>

    <?php else :?>
        <div class="alert alert-warning"><?php echo $this->lang->line('user_log_list_nothing') ;?></div>
    <?php endif ;?>
</div>