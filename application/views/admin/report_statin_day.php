<div class="wrap">
    <h2>
        STATISTIK SURAT MASUK PER HARI / PERIODE
    </h2>

    <?php if (!empty($note)) : ?>
        <div id='message' class='<?php echo $symbol;?>' style="width: 94%;margin-bottom: 10px;"><p><strong><?php echo $note;?></strong></p></div>
    <?php endif ;?>
        
    <form action="<?php echo current_url();?>" method="get">    
        <table class="widefat" style="border: none;" width="50%;">
            <tr>
                <td width="24%;">Tahun</td>
                <td width="1%">:</td>
                <td colspan="2">
                    <div class="row">
                        <div class="col-sm-2">
                            <?php echo form_input(array('name'=>'date_start','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker1'), $postdata['date_start'] ) ;?>
                        </div>
                        <div class="col-sm-1">
                            <input type="text" value="S.D." class="text-center form-control" disabled/>
                        </div>
                        <div class="col-sm-2">
                            <?php echo form_input(array('name'=>'date_end','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker2'), $postdata['date_end'] ) ;?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Sifat Surat</td>
                <td>:</td>
                <td colspan="2">
                    <?php echo form_dropdown('sifat_id',$sifat_dw,$this->input->get('sifat_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Jenis Surat</td>
                <td>:</td>
                <td colspan="2">
                    <?php echo form_dropdown('jenis_id',$jenis_dw,$this->input->get('jenis_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Kata Kunci</td>
                <td>:</td>
                <td>
                    <?php echo form_input(array('name'=>'keyword','placeholder'=>'Kata Kunci...','class'=>'form-control'), $this->input->get('keyword',TRUE) ) ;?>
                </td>
                <td><?php echo form_submit('filter', 'Cari','class="btn btn-sm btn-primary"') ;?></td>
            </tr>

        </table>
    </form>

    <p>&nbsp;</p>

    <?php foreach ($columns as $col) :?>
        <?php if (!empty($tables[$col['id']]) && $tables[$col['id']]->num_rows() > 0) :?>
            <div class="row">

                <div class="col-md-6">

                    <p><b>TABEL JUMLAH SURAT MASUK BERDASARKAN <?php echo strtoupper($col['name']) ;?> DARI <?php echo strtoupper(time_to_words($postdata['date_start'])).' - '.strtoupper(time_to_words($postdata['date_end'])) ;?></b></p>

                    <div class="table-responsive">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th><?php echo strtoupper($col['name']) ;?></th>
                                    <th>JUMLAH SURAT MASUK</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no     = 1 ;
                                    $total  = 0 ;
                                ?>
                                <?php foreach ($tables[$col['id']]->result_array() as $a) :?>
                                    <?php $total += $a[$col['field']] ;?>
                                    <tr>
                                        <td align="center"><?php echo $no++ ;?>.</td>
                                        <td><?php echo !empty($col['dw'][$a[$col['id']]]) ? str_replace('.. ','',$col['dw'][$a[$col['id']]]) : ( $col['id'] == 'user_id' ? 'Administrator' : 'Eksternal dan Umum') ;?></td>
                                        <td align="center"><?php echo separator($a[$col['field']]) ;?></td>
                                    </tr>
                                <?php endforeach ;?>    
                                <tr class="alternate" style="font-weight:bold;">
                                    <td colspan="2" align="right">JUMLAH</td>
                                    <td align="center"><?php echo separator($total) ;?></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>

                <div class="col-md-6">
                    <?php echo !empty($graph[$col['chart']]) ? $graph[$col['chart']] : '' ;?>
                </div>

            </div>
        <?php endif ;?>
    <?php endforeach ;?>

    <?php echo $charts ;?>    
    
</div>