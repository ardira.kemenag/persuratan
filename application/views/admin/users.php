<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        MANAJEMEN USER
        <div id="add">
            <a href="<?php echo site_url('admin/users/c/new');?>">TAMBAH USER</a>
        </div>
    </h2>

    <table class="widefat" style="border: none;">
        <tr>
            <td align="right">
                <form action="<?php echo site_url('admin/users/c');?>" method="get">
                    <?php echo form_input('keyword', (!empty($_GET['keyword']) ? $_GET['keyword'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('user_list_search'),'class="button"') ;?>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form action="<?php echo site_url('admin/users/c');?>" method="get">
                    <?php echo form_dropdown('status'   , $status_dw, (!empty($_GET['status'])  ? $_GET['status']   : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('level_id' , $level_dw , (!empty($_GET['level_id'])? $_GET['level_id'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('order'    , $order_dw , (!empty($_GET['order'])? $_GET['order'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('user_list_filter'),'class="button"') ;?>
                </form>
            </td>
        </tr>

    </table>
    
    <?php if ($user->num_rows() > 0) :?>

    <div class="table-responsive">    

    <table class="table widefat">
        <thead>
            <tr>
                <th scope="col" style="width: 2%;"><?php echo $this->lang->line('user_list_table_no');?></th>
                <th scope="col"><?php echo $this->lang->line('user_list_table_date');?></th>
                <th scope="col" style="width: 20%;"><?php echo $this->lang->line('user_list_table_fullname');?></th>
                <th scope="col" style="width: 15%;"><?php echo $this->lang->line('user_list_table_username');?></th>
                <th scope="col"><?php echo $this->lang->line('user_list_table_level');?></th>
                <th scope="col"><?php echo $this->lang->line('user_list_table_status');?></th>
                <th scope="col" colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody id="the-list">
        <?php $page += 1 ; foreach($user->result() as $a) :?>
            <tr<?php if ($page%2==0) :?> class="alternate"<?php endif;?>>
                <td valign="top" align="right"><?php echo $page++ ;?>.</td>
                <td valign="top" align="center"><?php echo $a->date_diff.' '.$a->time_diff ;?></td>
                <td valign="top"><?php echo $a->fullname ;?></td>
                <td valign="top"><?php echo $a->username ;?></td>
                <td valign="top" align="center"><?php echo $a->level_name ;?></td>
                <td valign="top" align="center"><?php echo $a->status ;?></td>
                <td valign="top" align="center">
                    <a href="<?php echo site_url('admin/users/c/' . $a->user_id) ;?>" class="btn btn-xs btn-success">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
                <td valign="top" align="center">
                    <a href="<?php echo site_url('admin/users/c/'. $a->user_id . '/del');?>" class="btn btn-xs btn-danger" onclick="return confirm('<?php echo $this->lang->line('user_list_table_act_del_ask');?>');">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach ;?>

            <tr<?php if ($page%2==0) :?> class="alternate"<?php endif;?>>
                <th scope="row" colspan="8" style="vertical-align: top;">
                    <div class="paging">
                        <?php echo $paging ;?>
                    </div>
                </th>
            </tr>
        </tbody>
    </table>

    </div>

    <?php else :?>
        <div class="alert alert-warning">Data tidak tersedia.</div>
    <?php endif ;?>
</div>