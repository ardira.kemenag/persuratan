<link rel="stylesheet" href="<?php echo _JAVASCRIPT_;?>select2/dist/css/select2.min.css" type="text/css" />

<div class="wrap">
    <h2>
        REKAPITULASI PERSURATAN DITJEN PSDKP - SURAT MASUK
        <?php if (!empty($surat) && $surat->num_rows() > 0) :?>
            <div id="pdf"><a href="<?php echo current_url().$uri_get.'&amp;export=pdf';?>" title="PDF">&nbsp;</a></div>
            <div id="print"><a onclick="popUpWindow('<?php echo current_url().$uri_get.'&amp;export=print';?>')" href="javascript:;" title="Print">&nbsp;</a></div>
            <div id="xls"><a href="<?php echo current_url().$uri_get.'&amp;export=xls';?>" title="Ms.Excel">&nbsp;</a></div>
        <?php endif ;?>
    </h2>

    <?php if (!empty($note)) : ?>
        <div id='message' class='<?php echo $symbol;?>' style="width: 94%;margin-bottom: 10px;"><p><strong><?php echo $note;?></strong></p></div>
    <?php endif ;?>
        
    <form action="<?php echo site_url('admin/report/rekapin');?>" method="get">    
        <table class="widefat" style="border: none;">
            <tr>
                <td width="12%;">Tanggal</td>
                <td width="1%">:</td>
                <td width="34%">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo form_input(array('name'=>'date_start','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker1'),$this->input->get('date_start',TRUE)) ;?>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" value="S.D." class="text-center form-control" disabled/>
                        </div>
                        <div class="col-sm-4">
                            <?php echo form_input(array('name'=>'date_end','class'=>'text-center form-control','maxlength'=>10,'id'=>'datepicker2'),$this->input->get('date_end',TRUE)) ;?>
                        </div>
                    </div>
                </td>
                <td width="10%;" rowspan="2">Tampilan Kolom</td>
                <td width="1%" rowspan="2">:</td>
                <td><?php echo form_multiselect('columns[]' , $columns, $cols ,'style="width:100%;"') ;?></td>
            </tr>
            <tr>
                <td>Tujuan Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('subdis_to',$tujuan_dw,$this->input->get('subdis_to',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Sifat Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('sifat_id',$sifat_dw,$this->input->get('sifat_id',TRUE),'style="width:98%;"') ;?>
                </td>

                <td>Jenis Surat</td>
                <td>:</td>
                <td>
                    <?php echo form_dropdown('jenis_id',$jenis_dw,$this->input->get('jenis_id',TRUE),'style="width:98%;"') ;?>
                </td>
            </tr>
            <tr>
                <td>Kata Kunci</td>
                <td>:</td>
                <td>
                    <?php echo form_input(array('name'=>'keyword','placeholder'=>'Kata Kunci...','class'=>'form-control'), $this->input->get('keyword',TRUE) ) ;?>
                </td>
                <td colspan="3"><?php echo form_submit('filter', 'Cari','class="btn btn-primary btn-sm"') ;?></td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
        </table>
    </form>

    <?php if ($surat->num_rows() > 0) :?>

        <?php $alt = 1 ;?>

            <table class="table widefat table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;vertical-align: middle;">NO</th>
                        <?php foreach ($cols as $key => $val) :?>
                            <th style="text-align: center;vertical-align: middle;"><?php echo strtoupper($columns[$val]) ;?></th>
                        <?php endforeach ;?>
                    </tr>
                </thead>
                <tbody>

                    <?php $page += 1 ;?>
                    <?php foreach ($surat->result() as $a) :?>

                        <tr>
                            <td align="right"><?php echo $page++ ;?>.</td>
                            
                            <?php foreach ($cols as $key => $val) :?>
                                <td>
                                    <?php if (in_array($val,array('aksi','surat_disposisi','surat_to','surat_tembusan'))) :?>
                                        <?php if ($val == 'surat_disposisi') :?>
                                            <?php echo !empty($a->surat_disposisi)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_disposisi,TRUE)),array('- disposisi -'=>'')) : '-' ;?>
                                        <?php elseif ($val == 'surat_to') :?>
                                            <?php echo !empty($a->surat_to)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_to,TRUE)),array('- disposisi -'=>'')) : '-' ;?>    
                                        <?php elseif ($val == 'surat_tembusan') :?>
                                            <?php echo !empty($a->surat_tembusan)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_tembusan,TRUE)),array('- disposisi -'=>'')) : '-' ;?>        
                                        <?php else :?>
                                            <?php 
                                                $aksi = $this->surat_inaksi_model->get(array('surat_id'=>$a->surat_id ,'aksi'     => TRUE )) ;
                                                if ($aksi->num_rows() > 0) {
                                                    $r_aksi = NULL ; 
                                                    foreach ($aksi->result() as $ak) {
                                                        $r_aksi[] = $ak->aksi_akronim ;
                                                    }

                                                    echo !empty($r_aksi) ? implode(', ',$r_aksi) : '-' ;
                                                }
                                            ?>
                                        <?php endif ;?>    
                                    <?php elseif ($val == 'file') :?>    
                                        <?php $document = $this->surat_indocument_model->get(array('surat_id'=>$a->surat_id)) ;?>
                                        <?php if ($document->num_rows() > 0) : $no = 1 ; ?>
                                            <?php foreach ($document->result() as $doc) :?>
                                                <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                                    <?php if ($document->num_rows() > 1) :?>
                                                        <?php echo $no++ ;?>. 
                                                    <?php endif ;?>
                                                    
                                                    <br /><a class="a-<?php echo $doc->document_fileext;?>" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]"><?php echo ellipsize($doc->document_filename, 15, .5) . '  ('.byte_format($doc->document_filesize).')';?></a>
                                                <?php endif ;?>
                                            <?php endforeach ;?>
                                        <?php else :?>
                                                
                                        <?php endif ;?>
                                    <?php else :?>
                                        <?php echo $a->$val ;?>
                                    <?php endif ;?>
                                </td>
                            <?php endforeach ;?>
                        </tr>

                    <?php endforeach ;?> 

                </tbody>
                <?php if (!empty($paging)) :?>
                    <tfoot>
                        <tr>
                            <th colspan="<?php echo count($cols)+1 ;?>" class="text-left">
                                <div class="paging">
                                    <?php echo $paging ;?>
                                </div>
                            </th>
                        </tr>
                    </tfoot>
                <?php endif ;?>
            </table>   

    <?php endif ;?>

</div>