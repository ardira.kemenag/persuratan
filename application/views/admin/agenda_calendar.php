<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<div class="wrap">
    <h2 style="border-bottom: 1px solid #BBB">
        KALENDER KEGIATAN
    </h2>
    
    <?php if ($kegiatan->num_rows() > 0) :?>
        
    <link rel='stylesheet' href='<?php echo _JAVASCRIPT_ ;?>fullcalendar/cupertino/theme.css' />
    <link href='<?php echo _JAVASCRIPT_ ;?>fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='<?php echo _JAVASCRIPT_ ;?>fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='<?php echo _JAVASCRIPT_ ;?>fullcalendar/jquery-1.9.1.min.js'></script>
    <script src='<?php echo _JAVASCRIPT_ ;?>fullcalendar/jquery-ui-1.10.2.custom.min.js'></script>
    <script src='<?php echo _JAVASCRIPT_ ;?>fullcalendar/fullcalendar.js'></script>

    <script>

            $(document).ready(function() {

                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth();
                    var y = date.getFullYear();

                    $('#calendar').fullCalendar({
                            theme: true,
                            header: {
                                    left: '',
                                    right: 'prev,next today',
                                    center: 'title'
                            },
                            editable: true,
                            events: <?php echo $event_json ;?>
                    });

            });

    </script>
    
    <style>

	#calendar {
            width: 90%;
            margin: 0 auto;
        }
        
        h2 { border-bottom: none ;}

    </style>
    
    <div id='calendar'></div>
    
    <?php else :?>
        <div id="message" class="error"><p>Agenda Kegiatan belum tersedia.</p></div>
    <?php endif ;?>
    
</div>