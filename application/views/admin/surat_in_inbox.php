<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        INBOX
		<?php $eselon=array(1,2); $level=array(1,3,4);
		if (in_array($this->data['subdis_eselon'],$eselon)|| in_array($this->data['level_id'],$level)){
		?>
        <div id="add">
            <a href="<?php echo site_url('admin/suratin/c/new');?>">COMPOSE SURAT BARU</a>
        </div>
		<?php }?>
    </h2>

    <div class="table-responsive">        
        <table class="widefat" style="border: none;">
            <tr>
                <td align="right">
                    <form action="<?php echo site_url('admin/suratin/inbox');?>" method="get">
                        <?php echo form_input(array('name'=>'start_date','size'=>10,'maxlength'=>10,'id'=>'datepicker1'),$this->input->get('start_date',TRUE)) ;?>
                        &nbsp;s.d.&nbsp;
                        <?php echo form_input(array('name'=>'to_date','size'=>10,'maxlength'=>10,'id'=>'datepicker2'),$this->input->get('to_date',TRUE)) ;?>
                        <?php echo form_dropdown('jenis_id' , $jenis_dw, (!empty($_GET['jenis_id'])  ? $_GET['jenis_id']   : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_dropdown('sifat_id' , $sifat_dw , (!empty($_GET['sifat_id'])? $_GET['sifat_id'] : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_input('keyword', (!empty($_GET['keyword']) ? $_GET['keyword'] : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_submit('filter', 'Cari','class="button"') ;?>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    
        <?php if ($surat->num_rows() > 0) : $alt = 1 ;?>

            <div class="table-responsive">

            <table class="table widefat table-striped">
                <thead class="border">
                    <tr>
                        <th style="text-align: center;vertical-align: middle;" colspan="2"><?php echo strtoupper($admin_submenu);?></th>
                        <th style="text-align: center;vertical-align: middle;">NO. SURAT</th>
                        <th style="text-align: center;vertical-align: middle;">JENIS SURAT</th>
                        <th style="text-align: center;vertical-align: middle;">TGL MASUK</th>
                        <th style="text-align: center;vertical-align: middle;">TGL TERIMA</th>
                        
                        <th>TUJUAN</th>
                        <th>TEMBUSAN</th>
                        <th>DISPOSISI</th>

                        <th>INSTRUKSI</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php foreach ($surat->result() as $s) :?>
                
                <?php 
                    $style = "" ;
                    if ($this->data['subdis_id'] == $s->subdis_id && $s->surat_view == 'Hidden') {
                        $style = ' style="font-weight:bold;"' ;
                    }
                    elseif ($this->data['subdis_id'] == $s->subdis_to && $s->read_status == 1) {
                        $style = ' style="font-weight:bold;"' ;
                    }

                    $surat_to       = !empty($s->surat_to)       ? json_decode($s->surat_to,TRUE)       : array() ;
                    $surat_tembusan = !empty($s->surat_tembusan) ? json_decode($s->surat_tembusan,TRUE) : array() ;
                    $surat_disposisi= $this->surat_indisposisi_model->get(array('surat_id'=>$s->surat_id,'subdis'=>TRUE,'order'=>'subdis_parent,subdis_order')) ;

                    $s_to       = NULL ;
                    $s_tembusan = NULL ;
                    $s_disposisi= NULL ;

                    if ($surat_disposisi->num_rows() > 0) {
                        foreach ($surat_disposisi->result() as $sd) {
                            $icon = $sd->read_status == 1 ? '<i class="glyphicon glyphicon-time" style="color:#C9302C;"></i> ' : '<i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> ' ;
                            if ($sd->indisposisi_status == 2) {
                                $s_to[] = $icon.$sd->subdis_name ;
                            }
                            elseif ($sd->indisposisi_status == 3) {
                                $s_tembusan[] = $icon.$sd->subdis_name ;
                            }
                            else {
                                $s_disposisi[] = $icon.$sd->subdis_name ;
                            }


                        }
                    }
                ?>



                <tr<?php echo $style;?>>
                    <td>
                        <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id).'?view=1';?>" class="vtip">
                            <?php echo $eselon_dw[$s->subdis_from] ;?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id).'?view=1';?>" class="vtip">
                            <?php echo $s->surat_perihal ;?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id).'?view=1';?>" class="vtip">
                            <?php echo $s->surat_number ;?>
                        </a>
                    </td>
                    <td><?php echo $s->jenis_name ;?></td>
                    <td align="center"><?php echo $s->date_diff ;?></center></td>
                    <td align="center"><?php echo $s->taken_date_diff ;?></td>
                    
                    <td><?php echo !empty($s_to)        ? implode('<br />',$s_to)           : ( !empty($surat_to)        ? array_to_list($eselon_dw,$surat_to) : '-' ) ;?></td>
                    <td><?php echo !empty($s_tembusan)  ? implode('<br />',$s_tembusan)     : ( !empty($surat_tembusan)  ? array_to_list($eselon_dw,$surat_tembusan) : '-' ) ;?></td>
                    <td><?php echo !empty($s_disposisi) ? implode('<br />',$s_disposisi)    : ( !empty($s->surat_disposisi) ? array_to_list($eselon_dw,json_decode($s->surat_disposisi,TRUE)) : '-' ) ;?></td>
                    
                    <td>
                        <?php 
                            $aksi = $this->surat_inaksi_model->get(array('surat_id'=>$s->surat_id ,'aksi'     => TRUE )) ;
                            if ($aksi->num_rows() > 0) {
                                $r_aksi = NULL ; 
                                foreach ($aksi->result() as $ak) {
                                    $icon = $ak->aksi_status == 1 ? '<i class="glyphicon glyphicon-time" style="color:#C9302C;"></i> ' : '<i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> ' ;
                                    $r_aksi[] = $icon.$ak->aksi_akronim ;
                                }

                                echo !empty($r_aksi) ? implode('<br /> ',$r_aksi) : '-' ;
                            }
                        ?>
                    </td>
                    
                 </tr>
                
                <?php endforeach ;?>
                 
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <th scope="row" colspan="10" style="vertical-align: top;">
                        <div class="paging">
                            <?php echo $paging ;?>
                        </div>
                    </th>
                </tr> 
                 
                </tbody>
                
            </table>

            </div>
            
        <?php else :?>
            <div class="alert alert-warning"><p>Data belum tersedia.</p></div>
        <?php endif ;?>     
    
</div>