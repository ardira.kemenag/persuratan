<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>User Logs | <?php echo $this->lang->line('general_title') ;?></title>

    <link rel="shortcut icon" type="image/ico" href="<?php echo _STYLES_ICON_;?>favicon.ico" />
    <link rel="stylesheet" href="<?php echo _STYLES_ADMIN_;?>styles.css" type="text/css" />
    <style type="text/css">
        body { background: none ; }
    </style>
</head>

<body>
    <?php if ($logs) :?>
    
        <div class="wrap">
            <h2><?php echo sprintf($this->lang->line('user_log_view_title'),$logs['login_date_diff'] . ' ' . $logs['login_time_diff']  , $logs['logout_date_diff'] . ' ' . $logs['logout_time_diff']);?></h2>

            <table class="widefat">
                <thead>
                    <tr>
                        <th scope="col" style="width: 50%;text-align: left;" colspan="2"><?php echo $this->lang->line('user_log_view_profile_title');?></th>
                        <th scope="col" style="width: 50%;text-align: left;" colspan="2"><?php echo $this->lang->line('user_log_view_platform_title');?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 20%;"><?php echo $this->lang->line('user_log_view_profile_login');?></td>
                        <td>: <?php echo $logs['login_date_diff'] . ' ' . $logs['login_time_diff'] ;?></td>
                        <td style="width: 20%;"><?php echo $this->lang->line('user_log_view_platform_ip');?></td>
                        <td>: <?php echo $logs['ip'];?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('user_log_view_profile_logout');?></td>
                        <td>: <?php echo $logs['logout_date_diff'] . ' ' . $logs['logout_time_diff'] ;?></td>
                        <td><?php echo $this->lang->line('user_log_view_platform_os');?></td>
                        <td>: <?php echo $logs['platform'];?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('user_log_view_profile_fullname');?></td>
                        <td>: <?php echo $logs['fullname'] ;?></td>
                        <td><?php echo $this->lang->line('user_log_view_platform_browser');?></td>
                        <td>: <?php echo $logs['browser'] . ' ' . $logs['browser_version'] ;?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('user_log_view_profile_username');?></td>
                        <td>: <?php echo $logs['username'] ;?></td>
                        <td><?php echo $this->lang->line('user_log_view_platform_mobile');?></td>
                        <td>: <?php echo coalesce($logs['mobile']);?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('user_log_view_profile_level');?></td>
                        <td>: <?php echo $logs['level_name'] ;?></td>
                        <td><?php echo $this->lang->line('user_log_view_platform_referrer');?></td>
                        <td>: <?php echo coalesce($logs['referrer']);?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;"><?php echo $this->lang->line('user_log_view_profile_email');?></td>
                        <td>: <?php echo mailto($logs['email']) ;?></td>
                        <td><?php echo $this->lang->line('user_log_view_platform_robot');?></td>
                        <td>: <?php echo coalesce($logs['robot']);?></td>
                    </tr>
                    <tr>
                        
                    </tr>
                </tbody>
            </table>

            <?php
                if (!empty($logs['log_pages'])) :
                    $no = 1 ;
                    $logs_text  = explode('|~|', $logs['log_pages']) ; ?>

            <p style="font-weight: bold;"><?php echo $this->lang->line('user_log_view_table_title');?></p>

            <table class="widefat">
                <thead>
                    <tr>
                        <th scope="col" style="width: 2%;"><?php echo $this->lang->line('user_log_view_table_no');?></th>
                        <th scope="col" style="width: 25%;"><?php echo $this->lang->line('user_log_view_table_time');?></th>
                        <th scope="col" style="text-align: left;width: 40%;"><?php echo $this->lang->line('user_log_view_table_url');?></th>
                        <th scope="col" style="text-align: left;"><?php echo $this->lang->line('user_log_view_table_post');?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($logs_text as $log) :
                        if (!empty($log)) :
                            list ($date,$page,$post_method) = explode('=>', $log) ;
                    ?>

                    <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                        <td align="right" valign="top"><?php echo $no++;?>.</td>
                        <td align="center" valign="top"><?php echo str_replace('-', '/', $date) ;?></td>
                        <td align="left" valign="top"><?php echo htmlentities($page);?></td>
                        <td align="left" valign="top"><pre><?php !empty($post_method) ? print_r(json_decode($post_method)) : '';?></pre></td>
                    </tr>

                    <?php endif ;?>
                <?php endforeach ;?>
                </tbody>
                <thead>
                    <tr>
                        <th scope="col" colspan="4">&nbsp;</th>
                    </tr>
                </thead>
            </table>
            
            <?php endif ;?>
        </div>

    <?php else :?>
        <div id="message" class="error"><p><?php echo $this->lang->line('user_log_view_nothing');?></p></div>
    <?php endif ;?>

</body>
</html>
