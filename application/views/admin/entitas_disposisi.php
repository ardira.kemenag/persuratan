<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>DISPOSISI SURAT</h2>
    
    <?php if ($subdis->num_rows() > 0) :?>

        <?php echo form_open(current_url()) ;?>  

        <div class="table-responsive">

        <table class="table widefat">
            <thead>
                <tr>
                    <th scope="col" style="width: 3%;">NO</th>
                    <th scope="col" style="width: 20%;">BAGIAN DISPOSISI</th>
                    <th scope="col" style="width: 10%;">BAGIAN</th>
                    <th scope="col" style="width: 27%;">INDUK BAGIAN</th>
                    <th scope="col" style="width: 17%;">NAMA</th>
                    <th scope="col" style="width: 6%;">KODE</th>
                    <th scope="col" style="width: 6%;">ESELON</th>
                    <th scope="col" style="width: 6%;">ORDER</th>
                    <th scope="col" style="width: 5%;">#</th>
                </tr>
            </thead>
            <tbody>
            <?php $no = $page+1 ; foreach ($subdis->result() as $m) :?>
                <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                    <td style="text-align: center;"><?php echo $no++ ;?>.</td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_name[]','class'=>'form-control'),$m->subdis_name);?>
                        <?php echo form_hidden('subdis_id[]',$m->subdis_id) ;?>
                    </td>
                    <td>
                        <?php echo form_dropdown('disposisi_id[]',$disposisi_dw,$m->disposisi_id,'style="width:90%;"') ;?>
                    </td>
                    <td>
                        <?php echo form_dropdown('subdis_parent[]',$parent_dw,$m->subdis_parent,'style="width:90%;"') ;?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_man[]','class'=>'form-control'),$m->subdis_man);?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_kode[]','class'=>'form-control','type'=>'number'),$m->subdis_kode);?>
                    </td> 
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_eselon[]','class'=>'form-control','type'=>'number'),$m->subdis_eselon);?>
                    </td>    
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_order[]','class'=>'form-control','type'=>'number'),$m->subdis_order);?>
                    </td>
                    <td style="text-align: center;">
                        <a href="<?php echo site_url('admin/entitas/disposisi/'.$m->subdis_id.'/del');?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus bagian disposisi ini?');">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach ;?>

            <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                <th scope="col" colspan="9">
                    <div class="paging">
                        <?php echo $paging ;?>
                    </div>
                </th>
            </tr>    

            <?php foreach (range(1,5) as $i) :?>
                <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                    <td style="text-align: center;">&nbsp;</td>
                    <td style="text-align: center;"><?php echo form_input(array('name'=>'subdis_name[]','class'=>'form-control'),NULL);?></td>
                    <td><?php echo form_dropdown('disposisi_id[]',$disposisi_dw,NULL,'style="width:90%;"') ;?></td>
                    <td><?php echo form_dropdown('subdis_parent[]',$parent_dw,NULL,'style="width:90%;"') ;?></td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_man[]','class'=>'form-control'),NULL);?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_kode[]','class'=>'form-control','type'=>'number'),NULL);?>
                    </td> 
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_eselon[]','class'=>'form-control','type'=>'number'),NULL);?>
                    </td>   
                    <td style="text-align: center;">
                        <?php echo form_input(array('name'=>'subdis_order[]','class'=>'form-control','type'=>'number'),NULL);?>
                        <?php echo form_hidden('subdis_id[]',NULL) ;?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            <?php endforeach ;?>

            </tbody>
        </table>

        </div>

        <p class="text-center">
            <?php echo form_hidden('sess_security',$sess_security) ;?>
            <input class="btn btn-primary btn-lg" name="savepost" value="&radic; UPDATE &raquo;" type="submit" />
        </p>

        <?php echo form_close() ;?>    

    <?php else :?>
        <div class="alert alert-warning"><p>Data belum tersedia.</p></div>
    <?php endif ;?> 
    
</div>