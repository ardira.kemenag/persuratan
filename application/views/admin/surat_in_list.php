<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        <?php echo strtoupper($admin_submenu);?>
		<?php $eselon=array(1,2); $level=array(1,3,4);
		if (in_array($this->data['subdis_eselon'],$eselon)|| in_array($this->data['level_id'],$level)){
		?>
        <div id="add">
            <a href="<?php echo site_url('admin/suratin/c/new');?>">COMPOSE SURAT BARU</a>
        </div>
		<?php }?>
    </h2>

    <div class="table-responsive">        
        <table class="widefat" style="border: none;">
            <tr>
                <td align="right">
                    <form action="<?php echo site_url('admin/suratin/c');?>" method="get">
                        <?php echo form_input(array('name'=>'start_date','size'=>10,'maxlength'=>10,'id'=>'datepicker1'),$this->input->get('start_date',TRUE)) ;?>
                        &nbsp;s.d.&nbsp;
                        <?php echo form_input(array('name'=>'to_date','size'=>10,'maxlength'=>10,'id'=>'datepicker2'),$this->input->get('to_date',TRUE)) ;?>
                        <?php echo form_dropdown('jenis_id' , $jenis_dw, (!empty($_GET['jenis_id'])  ? $_GET['jenis_id']   : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_dropdown('sifat_id' , $sifat_dw , (!empty($_GET['sifat_id'])? $_GET['sifat_id'] : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_input('keyword', (!empty($_GET['keyword']) ? $_GET['keyword'] : NULL) ) ;?>
                            &nbsp;
                        <?php echo form_submit('filter', 'Cari','class="button"') ;?>
                    </form>
                </td>
            </tr>
        </table>
    </div>
    
    <?php echo form_open(current_url(),array('id'=>"adminForm")) ;?>    
                
        <?php if ($surat->num_rows() > 0) : $alt = 1 ;?>

            <div class="table-responsive">

            <table class="table widefat table-striped">
                <thead class="border">
                    <tr>
                        <th style="text-align: center;padding-top: 20px;">
                            <input type="checkbox" onclick="checkAll(document.getElementById('adminForm'));" type="checkbox" value="all" name="cek_all" />
                        </th>
                        <th style="text-align: center;padding-top: 20px;">
                            #
                        </th>
                        <th style="text-align: center;vertical-align: middle;" colspan="2"><?php echo strtoupper($admin_submenu);?></th>
                        <th style="text-align: center;vertical-align: middle;">JENIS SURAT</th>
                        <th style="text-align: center;vertical-align: middle;">SIFAT SURAT</th>
                        <th style="text-align: center;vertical-align: middle;">TGL MASUK</th>
                        <th style="text-align: center;vertical-align: middle;">TGL TERIMA</th>
                        
                        <th>TUJUAN</th>
                        <th>TEMBUSAN</th>
                        <th>DISPOSISI</th>

                        <th>INSTRUKSI</th>
                        <th>LABEL</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php foreach ($surat->result() as $s) :?>
                
                <?php 
                    $style = "" ;
                    if ($this->data['subdis_id'] == $s->subdis_id && $s->surat_view == 'Hidden') {
                        $style = ' style="font-weight:bold;"' ;
                    }
                    elseif ($this->data['subdis_id'] == $s->subdis_to && $s->read_status == 1) {
                        $style = ' style="font-weight:bold;"' ;
                    }

                    $surat_to       = !empty($s->surat_to)       ? json_decode($s->surat_to,TRUE)       : array() ;
                    $surat_tembusan = !empty($s->surat_tembusan) ? json_decode($s->surat_tembusan,TRUE) : array() ;
                    $surat_disposisi= $this->surat_indisposisi_model->get(array('surat_id'=>$s->surat_id,'subdis'=>TRUE,'order'=>'subdis_parent,subdis_order')) ;

                    $s_to       = NULL ;
                    $s_tembusan = NULL ;
                    $s_disposisi= NULL ;

                    if ($surat_disposisi->num_rows() > 0) {
                        foreach ($surat_disposisi->result() as $sd) {
                            $icon = $sd->read_status == 1 ? '<i class="glyphicon glyphicon-time" style="color:#C9302C;"></i> ' : '<i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> ' ;
                            if ($sd->indisposisi_status == 2) {
                                $s_to[] = $icon.$sd->subdis_name ;
                            }
                            elseif ($sd->indisposisi_status == 3) {
                                $s_tembusan[] = $icon.$sd->subdis_name ;
                            }
                            else {
                                $s_disposisi[] = $icon.$sd->subdis_name ;
                            }
                        }
                    }
                    
                ?>



                <tr<?php echo $style;?>>
                    <td>
                        <?php if (($this->data['subdis_id'] == $s->subdis_id && $s->surat_view == 'Hidden') OR in_array($level_id,array(1) )) :?>
                            <center><input type="checkbox" name="delete_id[]" value="<?php echo $s->surat_id ;?>" /></center>
                        <?php endif ;?>
                    </td>
                    <td align="center">
                        <?php if ($this->data['subdis_id'] == $s->subdis_id && $s->surat_view == 'Hidden' ) :?>
                            <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id);?>" class="vtip" title="edit">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                        <?php endif ;?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id).'?view=1';?>" class="vtip">
                            <?php echo $eselon_dw[$s->subdis_from] ;?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo site_url('admin/suratin/c/'.$s->surat_id).'?view=1';?>" class="vtip">
                            <?php echo $s->surat_perihal ;?>
                        </a>
                    </td>
                    <td><?php echo $s->jenis_name ;?></td>
                    <td><span class="<?php echo $sifat_color[$s->sifat_id];?>"><?php echo $sifat_dw[$s->sifat_id] ;?></span></td>
                    <td align="center"><?php echo $s->date_diff ;?></center></td>
                    <td align="center"><?php echo $s->taken_date_diff ;?></td>


                    
                    <td><?php echo !empty($s_to)        ? implode('<br />',$s_to)           : ( !empty($surat_to)        ? array_to_list($eselon_dw,$surat_to) : '-' ) ;?></td>
                    <td><?php echo !empty($s_tembusan)  ? implode('<br />',$s_tembusan)     : ( !empty($surat_tembusan)  ? array_to_list($eselon_dw,$surat_tembusan) : '-' ) ;?></td>
                    <td><?php echo !empty($s_disposisi) ? implode('<br />',$s_disposisi)    : ( !empty($s->surat_disposisi) ? array_to_list($eselon_dw,json_decode($s->surat_disposisi,TRUE)) : '-' ) ;?></td>
                    
                    <td>
                        <?php 
                            $aksi = $this->surat_inaksi_model->get(array('surat_id'=>$s->surat_id ,'aksi'     => TRUE )) ;
                            if ($aksi->num_rows() > 0) {
                                $r_aksi = NULL ; 
                                foreach ($aksi->result() as $ak) {
                                    $icon = $ak->aksi_status == 1 ? '<i class="glyphicon glyphicon-time" style="color:#C9302C;"></i> ' : '<i class="glyphicon glyphicon-ok" style="color:#449D44;"></i> ' ;
                                    $r_aksi[] = $icon.$ak->aksi_akronim ;
                                }

                                echo !empty($r_aksi) ? implode('<br /> ',$r_aksi) : '-' ;
                            }
                        ?>
                    </td>

                    <td align="center">
                        <?php if ($s->surat_view == 'Hidden') :?>
                            <a href="<?php echo site_url('admin/suratin/draft') ;?>" class="btn btn-xs btn-warning" style="color:#fff;">
                                draft
                            </a>
                        <?php elseif($s->surat_view == 'Visible' && $s->subdis_id == $this->data['subdis_id'] ) :?>
                            <a href="<?php echo site_url('admin/suratin/sent') ;?>" class="btn btn-xs btn-success" style="color:#fff;">
                                sent
                            </a>
                        <?php else :?>    
                            <a href="<?php echo site_url('admin/suratin/inbox') ;?>" class="btn btn-xs btn-info" style="color:#fff;">
                                inbox
                            </a>
                        <?php endif ;?>    
                    </td>
            
                    
                 </tr>
                
                <?php endforeach ;?>
                 
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <th scope="row" colspan="3" style="text-align: left;">
                        <?php echo form_hidden(array('sess_security'=>$sess_security)) ;?>
                        <?php echo form_submit('delete_post','DELETE','class="btn btn-danger btn-sm"');?>
                    </th>
                    <th scope="row" colspan="9" style="vertical-align: top;">
                        <div class="paging">
                            <?php echo $paging ;?>
                        </div>
                    </th>
                </tr> 
                 
                </tbody>
                
            </table>

            </div>
               
        <?php echo form_close() ;?>    
            
        <?php else :?>
            <div class="alert alert-warning"><p>Data belum tersedia.</p></div>
        <?php endif ;?>     
    
</div>