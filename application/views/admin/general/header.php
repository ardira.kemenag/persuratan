<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?php echo (!empty($admin_submenu) ? $admin_submenu : $adminmenu) .' | '. $this->lang->line('general_title') ;?></title>
    <link rel="shortcut icon" href="http://arthawisesa.com/dolpin1/public/styles/admin/images/logo.png">

   <!--  <link rel="shortcut icon" type="image/ico" href="<?php echo _STYLES_ICON_;?>favicon.ico" /> -->

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Cache-Control" content="no-cache" />

    <meta name="description" content="<?php echo $this->lang->line('general_description') ;?>" />
    <meta http-equiv="Copyright" content="DJP2SPKP KKP RI" />
    <meta name="author" content="Kementerian Kelautan dan Perikanan RI" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="language" content="Indonesia" />
    <meta name="generator" content="Jahex" />
    <meta name="revisit-after" content="7" />
    <meta name="webcrawlers" content="all" />
    <meta name="rating" content="general" />
    <meta name="spiders" content="all" />
    <meta name="robots" content="all" />
    <!-- end: Meta -->

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->
    
    <!-- start: CSS -->
    <?php echo implode("\n",$css) ;?>
    <!-- end: CSS -->

    <!-- start: JavaScript-->
    <?php print implode("\n",$js_top) ;?>
    <!-- end: JavaScript-->

    <script>
        // <![CDATA[
            var site_url    = "<?php echo site_url() ;?>" ;
            var base_url    = "<?php echo base_url() ;?>" ;
            var js_url      = "<?php echo _JAVASCRIPT_ ;?>" ;
            var css_url     = "<?php echo _STYLES_ADMIN_ ;?>" ;
        //]]>
    </script>

</head>
    
<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<div id="wrapper">

    <div id="wphead">
    </div>

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php if (in_array($level_id,array(1,3,4,9))) :?>
                        <!--
                        <li class="<?php  ($adminmenu == $this->lang->line('general_home'))    ? print 'active' : '' ;?>"><a href="<?php echo site_url('admin/home');?>"><?php echo strtoupper($this->lang->line('general_home')) ;?></a></li>
                        -->
                        
                        <li class="dropdown <?php  ($adminmenu == $this->lang->line('general_surat'))   ? print 'active' : '' ;?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo strtoupper($this->lang->line('general_surat')) ;?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <?php if (!in_array($level_id,array(4))) :?>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo strtoupper($this->lang->line('general_suratin')) ;?>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="<?php echo site_url('admin/suratin/');?>">
                                                    <?php echo strtoupper($this->lang->line('general_suratin_list')) ;?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('admin/suratin/inbox');?>">
                                                    <?php echo strtoupper($this->lang->line('general_suratin_inbox')) ;?>
                                                    <?php if (!empty($_inbox_num)) :?>
                                                        <span class="badge">
                                                            <?php echo separator($_inbox_num) ;?>
                                                        </span>
                                                    <?php endif ;?>    
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('admin/suratin/draft');?>">
                                                    <?php echo strtoupper($this->lang->line('general_suratin_draft')) ;?>
                                                    <?php if (!empty($_draft_num)) :?>
                                                        <span class="badge">
                                                            <?php echo separator($_draft_num) ;?>
                                                        </span>
                                                    <?php endif ;?> 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('admin/suratin/sent');?>">
                                                    <?php echo strtoupper($this->lang->line('general_suratin_sent')) ;?>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php endif ;?>

                                <?php if (!in_array($subdis_eselon,array(3,4,5,6))) :?>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo strtoupper($this->lang->line('general_suratout')) ;?>
                                        </a>
                                        <?php if ($outjenis->num_rows() > 0) :?>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($outjenis->result() as $oj) :?>
                                                    <li>
                                                        <a href="<?php echo site_url('admin/suratout/c/'.$oj->jenis_id);?>">
                                                            <?php echo strtoupper($oj->jenis_name) ;?>
                                                        </a>
                                                    </li>
                                                <?php endforeach ;?>    
                                            </ul>
                                        <?php endif ;?>    
                                    </li>
                                <?php endif ;?>

                                <?php if (in_array($level_id,array(1))) :?>
                                    <li role="separator" class="divider"></li>
                                    <li class="dropdown-header"><?php echo strtoupper($this->lang->line('general_entitas')) ;?></li>
                                    <li><a href="<?php echo site_url('admin/entitas/klasifikasi');?>"><?php echo strtoupper($this->lang->line('general_entitas_klasifikasi')) ;?></a></li>
                                    <li><a href="<?php echo site_url('admin/entitas/tujuan');?>"><?php echo strtoupper($this->lang->line('general_entitas_tujuan')) ;?></a></li>
                                    <li><a href="<?php echo site_url('admin/entitas/sifat');?>"><?php echo strtoupper($this->lang->line('general_entitas_sifat')) ;?></a></li>
                                    <li><a href="<?php echo site_url('admin/entitas/jenis');?>"><?php echo strtoupper($this->lang->line('general_entitas_jenis')) ;?></a></li>
                                    <li><a href="<?php echo site_url('admin/entitas/aksi');?>"><?php echo strtoupper($this->lang->line('general_entitas_aksi')) ;?></a></li>
                                    <li><a href="<?php echo site_url('admin/entitas/disposisi');?>"><?php echo strtoupper($this->lang->line('general_entitas_disposisi')) ;?></a></li>
                                <?php endif ;?>
                            </ul>
                        </li>

                        <!--

                        <li class="dropdown <?php  ($adminmenu == $this->lang->line('general_dokumen'))   ? print 'active' : '' ;?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo strtoupper($this->lang->line('general_dokumen')) ;?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/dokumen/folder');?>"><?php echo strtoupper($this->lang->line('general_dokumen_folder')) ;?></a></li>
                                <li><a href="<?php echo site_url('admin/dokumen/files');?>"><?php echo strtoupper($this->lang->line('general_dokumen_files')) ;?></a></li>
                                <li><a href="<?php echo site_url('admin/dokumen/sitemap');?>"><?php echo strtoupper($this->lang->line('general_dokumen_sitemap')) ;?></a></li>
                            </ul>
                        </li>

                        -->
                        
                        <?php if (in_array($level_id,array(1,3)) OR in_array($subdis_eselon,array(2))) :?>
                            <li class="dropdown <?php  ($adminmenu == $this->lang->line('general_report'))   ? print 'active' : '' ;?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?php echo strtoupper($this->lang->line('general_report')) ;?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url('admin/report/rekapin');?>"><?php echo strtoupper($this->lang->line('general_report_rekapin')) ;?></a></li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo strtoupper($this->lang->line('general_report_rekapout')) ;?>
                                        </a>
                                        <?php if ($outjenis->num_rows() > 0) :?>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($outjenis->result() as $oj) :?>
                                                    <li>
                                                        <a href="<?php echo site_url('admin/report/rekapout/'.$oj->jenis_id);?>">
                                                            <?php echo strtoupper($oj->jenis_name) ;?>
                                                        </a>
                                                    </li>
                                                <?php endforeach ;?>  
                                                <li>
                                                    <a href="<?php echo site_url('admin/report/rekapoutall');?>">
                                                        SEMUA SURAT
                                                    </a>
                                                </li>  
                                            </ul>
                                        <?php endif ;?>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo strtoupper($this->lang->line('general_report_statin')) ;?>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('admin/report/statin_eselon');?>"><?php echo strtoupper($this->lang->line('general_report_statin_eselon')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statin_year');?>"><?php echo strtoupper($this->lang->line('general_report_statin_year')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statin_month');?>"><?php echo strtoupper($this->lang->line('general_report_statin_month')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statin_day');?>"><?php echo strtoupper($this->lang->line('general_report_statin_day')) ;?></a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php echo strtoupper($this->lang->line('general_report_statout')) ;?>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('admin/report/statout_tujuan');?>"><?php echo strtoupper($this->lang->line('general_report_statout_tujuan')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statout_year');?>"><?php echo strtoupper($this->lang->line('general_report_statout_year')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statout_month');?>"><?php echo strtoupper($this->lang->line('general_report_statout_bulan')) ;?></a></li>
                                            <li><a href="<?php echo site_url('admin/report/statout_day');?>"><?php echo strtoupper($this->lang->line('general_report_statout_day')) ;?></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
							
							<?php endif ;?>
                            <?php if ( in_array($level_id,array(1,3,4,9)) ) :?>
                                <li class="dropdown <?php  ($adminmenu == $this->lang->line('general_agenda'))   ? print 'active' : '' ;?>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <?php echo strtoupper($this->lang->line('general_agenda')) ;?> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo site_url('admin/agenda/kegiatan');?>"><?php echo strtoupper($this->lang->line('general_agenda_kegiatan')) ;?></a></li>
                                        <li><a href="<?php echo site_url('admin/agenda/status');?>"><?php echo strtoupper($this->lang->line('general_agenda_status')) ;?></a></li>
                                        <li><a href="<?php echo site_url('admin/agenda/calendar');?>"><?php echo strtoupper($this->lang->line('general_agenda_calendar')) ;?></a></li>
                                    </ul>
                                </li>
                         <?php endif ;?>
                    <?php endif ;?>    
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="<?php  ($adminmenu == $this->lang->line('general_profile')) ? print 'active' : '' ;?>">
                        <a href="<?php echo site_url('admin/users/profile') ;?>">
                            <i class="glyphicon glyphicon-user"></i>&nbsp;<?php echo strtoupper($this->lang->line('general_profile')) ;?>
                        </a>
                    </li>
                    <?php if (in_array($level_id,array(1))) :?>
                        <li class="dropdown <?php  ($adminmenu == $this->lang->line('general_users'))   ? print 'active' : '' ;?>">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo strtoupper($this->lang->line('general_users')) ;?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('admin/users');?>"><?php echo $this->lang->line('general_users') ;?></a></li>
                                <li><a href="<?php echo site_url('admin/users/logs');?>"><?php echo $this->lang->line('general_users_logs') ;?></a></li>
                            </ul>
                        </li>
                    <?php endif ;?>

                    <li><a href="<?php echo site_url('logout');?>">(Logout)</a></li>
                </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>