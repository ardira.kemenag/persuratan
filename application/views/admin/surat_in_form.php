<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>COMPOSE <?php echo strtoupper($admin_submenu);?></h2>
	<?php if($this->uri->segment(4) !="new" && $this->session->userdata('level_id')!=3){ ?>
	<div class="row" style="margin-bottom:10px;">
        <div class="col-lg-12 text-right">
            <a onclick="popUpWindow('<?php echo current_url() . '?view=1&amp;export=print' ;?>')" href="javascript:;" class="btn btn-sm btn-info" style="color:#fff;">PRINT</a>
            <a href="<?php echo current_url() . '?view=1&amp;export=pdf' ;?>" class="btn btn-sm btn-danger" style="color:#fff;">PDF</a>
        </div>
    </div>  
    <?php } ?>
     <?php if (!empty($surat['reply_surat_number'])) :?>
    <p>
        <b>BALASAN DARI : 
            <a href="<?php echo site_url('admin/suratin/c/'.$surat['reply_format_id'].'/'.$surat['surat_reply_id']);?>" class="vtip" title="<small><?php echo time_to_words($surat['reply_surat_date'],TRUE);?></small><br /><b>No. Surat : <?php echo html_entity_decode($surat['reply_surat_number']) ;?></b><br /><b>Perihal &nbsp;&nbsp;&nbsp;&nbsp;: </b><?php echo html_entity_decode($surat['reply_surat_perihal']) ;?>">
                <?php echo character_limiter($surat['reply_surat_perihal'],80) ;?>
            </a>
        </b>
    </p>
    <?php endif ;?>
    
    <?php $alt = 0 ;?>
    
    <?php echo form_open_multipart("admin/suratin/c/".$surat['surat_id']) ;?>
        <div class="table-responsive">

        <?php $disabled = !empty($surat['surat_id']) ? " disabled" : "" ;?>

        <table class="widefat table">
            <tbody>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td width="23%"><b>Jenis Surat</b></th>
                    <td width="2%"><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('jenis_id', $jenis_dw, set_value('jenis_id',$surat['jenis_id']),'id="jenis_id" style="width:98%;"'.$disabled) ;?>
                        <?php echo form_error('jenis_id') ;?>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('jenis_id',$surat['jenis_id']); ?>
                        <?php endif ;?>    
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Sifat Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_dropdown('sifat_id', $sifat_dw, set_value('sifat_id',!empty($surat['sifat_id']) ? $surat['sifat_id'] : 1 ),'style="width:98%;"'.$disabled) ;?>
                        <?php echo form_error('sifat_id') ;?>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('sifat_id',$surat['sifat_id']); ?>
                        <?php endif ;?>  
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Nomor Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input name="surat_number" value="<?php echo set_value('surat_number',$surat['surat_number']);?>" type="text" class="form-control" <?php echo $disabled ;?>/>
                        <?php echo form_error('surat_number') ;?>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_number',$surat['surat_number']); ?>
                        <?php endif ;?>  
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input id="datepicker1" name="surat_date" value="<?php echo set_value('surat_date',$surat['surat_date']);?>" type="text" maxlength="10" size="10" class="form-control"  <?php echo $disabled ;?>/>
                        <?php echo form_error('surat_date') ;?>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_date',$surat['surat_date']); ?>
                        <?php endif ;?>  
                    </td>
                </tr>

                <?php if (empty($reply_id)) :?>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tanggal Surat Diterima</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input id="datepicker2" name="surat_date_taken" value="<?php echo set_value('surat_date_taken',!empty($surat['surat_date_taken']) ? $surat['surat_date_taken'] : date('Y-m-d') );?>" type="text" maxlength="10" size="10" class="form-control"  <?php echo $disabled ;?>/>
                        <?php echo form_error('surat_date_taken') ;?>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_date_taken',$surat['surat_date_taken']); ?>
                        <?php endif ;?>  
                    </td>
                </tr>
                <?php endif ;?>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td>
                        <?php if (!empty($reply_id)) :?>
                            <b>Asal/Sumber Surat</b>
                        <?php else :?>
                            <b>Dari</b>
                        <?php endif ;?>
                    </th>
                    <td><b>:</b></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php echo form_dropdown('from_status',$from_status,set_value('from_status', $surat['from_status'] ),'id="from_status" style="width:98%;"'.$disabled) ;?>
                            
                                <?php if (!empty($disabled)) :?>
                                    <?php echo form_hidden('from_status',$surat['from_status']); ?>
                                <?php endif ;?>  
                            </div>

                            <?php foreach ($from_status as $key => $val) :?>
                                <?php if (!empty($key)) :?>

                                    <div class="col-sm-9" id="sf<?php echo $key;?>" <?php echo $surat['from_status'] != $key ? ' style="display:none;"' : '' ;?>>
                                        <?php if ($key == 1) :?>
                                            <?php echo form_dropdown('surat_from'.$key,$surat_fdw,set_value('surat_from'.$key,$surat['surat_from']), 'style="width:98%;" id="sftext'.$key.'"'.$disabled ) ;?>
                                        <?php elseif ($key == 2) :?>
                                            <input name="surat_from<?php echo $key ;?>" value="<?php echo set_value('surat_from'.$key,$surat['surat_from']);?>" type="text" class="form-control" id="sftext<?php echo $key;?>" <?php echo $disabled ;?>/>
                                        <?php else :?>
                                            <?php echo form_dropdown('surat_from'.$key,$suratdr_dw,set_value('surat_from'.$key,$surat['surat_from']), 'style="width:98%;" id="sftext'.$key.'"'.$disabled ) ;?>
                                        <?php endif ;?>  
                                    </div>

                                <?php endif ;?>
                            <?php endforeach ;?>

                            <input type="hidden" id="surat_from" name="surat_from" value="<?php echo $surat['surat_from'] ;?>" >

                            <?php echo form_error('surat_from') ;?>
                        </div>
                    </td>
                </tr>
                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Kepada Yth.</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_multiselect('surat_to[]',$suratto_dw, ( !empty($surat['surat_to']) ? json_decode($surat['surat_to'],TRUE) : NULL ) ,'style="width:98%;"'.$disabled) ;?>
                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_to',( !empty($surat['surat_to']) ? json_decode($surat['surat_to'],TRUE) : NULL )); ?>
                        <?php endif ;?>  
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Tembusan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_multiselect('surat_tembusan[]',$suratto_dw, ( !empty($surat['surat_tembusan']) ? json_decode($surat['surat_tembusan'],TRUE) : NULL ) ,'style="width:98%;"'.$disabled) ;?>
                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_tembusan',( !empty($surat['surat_tembusan']) ? json_decode($surat['surat_tembusan'],TRUE) : NULL )); ?>
                        <?php endif ;?>  
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Perihal</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input name="surat_perihal" value="<?php echo $rs ? '' : set_value('surat_perihal',$surat['surat_perihal']);?>" type="text" class="form-control" <?php echo $disabled ;?>/>
                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('surat_perihal',$surat['surat_perihal']); ?>
                        <?php endif ;?> 
                    </td>
                </tr>

                <tr class="agenda<?php if ($alt++%2==0):?> alternate<?php endif ;?>">
                    <td><b>Tempat Kegiatan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <input type="text" name="kegiatan_tempat" value="<?php echo set_value('kegiatan_tempat', !empty($surat['kegiatan_tempat']) ? $surat['kegiatan_tempat'] : NULL);?>" maxlength="255" class="form-control" <?php echo $disabled ;?>/>
                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('kegiatan_tempat',(!empty($surat['kegiatan_tempat']) ? $surat['kegiatan_tempat'] : NULL) ); ?>
                        <?php endif ;?> 
                    </td>
                </tr>
                
                <tr class="agenda<?php if ($alt++%2==0):?> alternate<?php endif ;?>">
                    <td><b>Tanggal Kegiatan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" id="datepicker3" name="kegiatan_date_start" value="<?php echo set_value('kegiatan_date_start', !empty($surat['kegiatan_date_start']) ? $surat['kegiatan_date_start'] : NULL);?>" maxlength="10" size="10" class="form-control" <?php echo $disabled ;?>/>
                            </div>
                            <div class="col-sm-1">
                                <input type="text" value="S.D" disabled class="form-control" <?php echo $disabled ;?>/>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="datepicker4" name="kegiatan_date_end" value="<?php echo set_value('kegiatan_date_end', !empty($surat['kegiatan_date_end']) ? $surat['kegiatan_date_end'] : NULL);?>" maxlength="10" size="10" class="form-control" <?php echo $disabled ;?>/> 
                            </div>
                        </div>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('kegiatan_date_start',(!empty($surat['kegiatan_date_start']) ? $surat['kegiatan_date_start'] : NULL) ); ?>
                            <?php echo form_hidden('kegiatan_date_end',(!empty($surat['kegiatan_date_end']) ? $surat['kegiatan_date_end'] : NULL) ); ?>
                        <?php endif ;?> 
                    </th>
                </tr>
                
                <tr class="agenda<?php if ($alt++%2==0):?> alternate<?php endif ;?>">
                    <td><b>Waktu Kegiatan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="kegiatan_time_start" value="<?php echo set_value('kegiatan_time_start', !empty($surat['kegiatan_time_start']) ? $surat['kegiatan_time_start'] : '00:00');?>" maxlength="20" size="10" class="form-control" <?php echo $disabled ;?>/>
                            </div>
                            <div class="col-sm-1">
                                <input type="text" value="S.D" disabled class="form-control" <?php echo $disabled ;?>/>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="kegiatan_time_end" value="<?php echo set_value('kegiatan_time_end', !empty($surat['kegiatan_time_end']) ? $surat['kegiatan_time_end'] : '00:00');?>" maxlength="20" size="10" class="form-control" <?php echo $disabled ;?>/> 
                            </div>
                            <div class="col-sm-2">
                                <?php echo form_dropdown('timezone',$timezone, set_value('timezone', (!empty($surat['timezone']) ? $surat['timezone'] : 1 ) ) , 'style="width:98%;"'.$disabled ); ;?> 
                            </div>
                        </div>

                        <?php if (!empty($disabled)) :?>
                            <?php echo form_hidden('kegiatan_time_start', (!empty($surat['kegiatan_time_start']) ? $surat['kegiatan_time_start'] : NULL) ); ?>
                            <?php echo form_hidden('kegiatan_time_end'  , (!empty($surat['kegiatan_time_end']) ? $surat['kegiatan_time_end'] : NULL) ); ?>
                            <?php echo form_hidden('timezone'           , (!empty($surat['timezone']) ? $surat['timezone'] : NULL) ); ?>
                        <?php endif ;?> 
                    </th>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Upload Dokumen<?php if (!empty($reply_id)) :?> (bila ada)<?php endif ;?></b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                            <?php foreach ($document->result() as $doc) :?>
                                <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                    <?php if ($document->num_rows() > 1) :?>
                                        <?php echo $no++ ;?>. 
                                    <?php endif ;?>

                                    <a class="a-<?php echo $doc->document_fileext;?>" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]"><?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?></a>
                                        &nbsp; <a href="<?php echo site_url('admin/suratin/c/'.$surat['surat_id']).'?delete_doc='.$doc->document_id;?>"><img border="0" src="<?php echo _STYLES_ADMIN_ ;?>images/delete.png" /></a>
                                    <br />
                                <?php endif ;?>
                            <?php endforeach ;?>

                            <br />
                        <?php endif ;?>

                        <div class="row">
                            <div class="col-sm-10">
                                <input type="file" name="document_name1" class="form-control" />
                            </div>
                            <div class="col-sm-2">
                                <a href="javascript:void(0)" id="button-add-softcopy" class="btn btn-sm btn-success">
                                    <i class="glyphicon glyphicon-plus"></i>
                                </a>
                            </a>    
                            </div>
                        </div>

                        <div id="add-softcopy"></div>
                        <input type="hidden" id="doc_id" value="2" /> 
                    </td>
                </tr>

                <?php if (!in_array($level_id,array(3))) :?>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Disposisi</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php $disposisi_post = !empty($surat['surat_disposisi']) ? json_decode($surat['surat_disposisi'],TRUE) : array() ;?>
                        <?php if ($disposisi->num_rows() > 0) :?>
                            <?php foreach ($disposisi->result() as $d) :?>
                                <?php echo '<b>'.$d->disposisi_name.'&nbsp;:&nbsp;</b><br />' ;?>
                                <?php
                                    $subdis = $this->disposisi_sub_model->show_tree(array('disposisi_id'=>$d->disposisi_id,'subdis_parent'=> !in_array($level_id,array(1)) ? $subdis_id : NULL )) ;
                                    if (!empty($subdis) && count($subdis) > 0) : ?>
									<?php if($this->session->userdata('subdis_id')==2){
											$cek_level = $this->db->get_where('disposisi__sublist', array('subdis_eselon' => 2)); 
											foreach($cek_level->result() as $lv){ ?>
												<?php if ($lv->subdis_id==2){
						
												}else { ?>
													<input type="checkbox" id="dis_id<?php echo $lv->subdis_id;?>" name="surat_disposisi[]"<?php if(in_array($lv->subdis_id, $disposisi_post)){ echo 'checked';}else{echo '';} ?> value="<?php echo $lv->subdis_id;?>"> -- <?php echo $lv->subdis_name;?><br>
												<?php } ?>
											<?php }
										}	?>
                                        <?php foreach ($subdis as $subdis_id => $subdis_name) :?>
																			
                                        <?php
                                            $checkbox = form_checkbox('surat_disposisi[]', $subdis_id, in_array($subdis_id,$disposisi_post),'id="dis_id'.$subdis_id.'"') .' ' ;
                                            $form_category = $checkbox.' '.$subdis_name ;
                                        ?>
                                        <?php echo $form_category  ;?><br />
                                        <?php endforeach ;?>
                                    <?php endif ;?>
                                <br />
                            <?php endforeach ;?>
                        <?php endif ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Pesan Disposisi</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo form_textarea(array('name'=>'into_msg','rows'=>6),set_value('into_msg',$surat['into_msg']),'class="form-control"');?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td><b>Penanggung Jawab</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php $respon_post = !empty($surat['surat_respon']) ? json_decode($surat['surat_respon'],TRUE) : array() ;?>
                        <?php if ($disposisi->num_rows() > 0) :?>
                            <?php foreach ($disposisi->result() as $d) :?>
                                <?php if ($d->disposisi_id == 1) :?>
                                    <?php echo '<b>'.$d->disposisi_name.'&nbsp;:&nbsp;</b><br />' ;?>
                                    <?php
                                        $subdis = $this->disposisi_sub_model->get(array('disposisi_id'=>$d->disposisi_id)) ;
                                        if ($subdis->num_rows() > 0) :
                                            foreach ($subdis->result() as $sb) : ?>
                                                <div id="respon_subdis_id<?php echo $sb->subdis_id;?>" <?php if (!in_array($sb->subdis_id,$disposisi_post)) :?>style="display: none;"<?php endif ;?>>
                                                    <?php echo form_checkbox('surat_respon[]', $sb->subdis_id, in_array($sb->subdis_id,$respon_post)).'&nbsp;'.$sb->subdis_name.'&nbsp;&nbsp;' ;?>
                                                </div>
                                            <?php endforeach ;?>
                                        <?php endif ;?>
                                <?php endif ;?>    
                                <br />
                            <?php endforeach ;?>
                        <?php endif ;?>
                    </td>
                </tr>

                <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                    <td>
                        <?php if (!empty($reply_id)) :?>
                            <b>Aksi yang Sudah Dilakukan</b>
                        <?php else :?>
                            <b>Aksi yang Diperlukan</b>
                        <?php endif ;?>
                    </td>
                    <td><b>:</b></th>
                    <td>
                        <?php if ($aksi->num_rows() > 0) :?>
                            <?php foreach ($aksi->result() as $a) :?>
                                <?php echo form_checkbox('aksi_id[]', $a->aksi_id, in_array($a->aksi_id, $aksi_post)).'&nbsp;'.$a->aksi_name.' ('.$a->aksi_akronim.')' ;?>
                                <br />
                            <?php endforeach ;?>
                        <?php endif ;?>
                    </td>
                </tr>                

                <?php endif ;?>

            </tbody>
        </table>

        </div>

        <p style="margin: 10px 20px 0 0;" class="text-center">
            <?php echo form_hidden(array(
                                        'sess_security' => $sess_security ,
                                        'subdis_id'     => !empty($surat['subdis_id']) ? $surat['subdis_id'] : $this->data['subdis_id'] ,
                                        'fsubdis_id'    => $this->data['subdis_id'] ,
                                        'user_id'       => !empty($surat['user_id']) ? $surat['user_id'] : $user_id ,
                                        'surat_id'      => $surat['surat_id'] ,
                                        'surat_reply_id'=> $surat['surat_reply_id']
                                    )) ;?>
			
            <input name="surat_view"    value="Save As Draft" type="submit" class="btn btn-lg btn-info" />
            &nbsp;&nbsp;
			
			<?php if($surat['jenis_id']!=21){?>			 
			<input name="surat_view" class="biasa btn btn-lg btn-primary" value="Send" type="submit" onclick="return confirm('Apakah Anda yakin untuk merevisi kembali surat ini, pelaksanaan instruksi yang telah terjadi sebelumnya tidak berlaku jika telah direvisi?');" />
			<?php }?> 
			<input style="display:none;" id="agenda_post" name="agenda_post"  class="agenda btn btn-lg btn-primary" value="Send and Save as Agenda" type="submit" onclick="return confirm('Apakah Anda yakin untuk menempatkan surat undangan ini ke agenda kegiatan?');" />
        </p>
			
    <?php echo form_close() ;?>  

</div>

<script type="text/javascript">
    $(document).ready(function(){
        <?php if ($subdis->num_rows() > 0) :
            foreach ($subdis->result() as $sb) : ?>
                $("#dis_id<?php echo $sb->subdis_id;?>").click(function(){
                  if ($("#dis_id<?php echo $sb->subdis_id;?>").is(":checked"))
                     $("#respon_subdis_id<?php echo $sb->subdis_id;?>").show();
                  else
                    $("#respon_subdis_id<?php echo $sb->subdis_id;?>").hide();
               });
            <?php endforeach ;?>
        <?php endif ;?>

        $("#button-add-softcopy").click(function() {
            var base_img = '<?php echo _STYLES_ADMIN_ ;?>images/' ;
            var doc_id   = document.getElementById("doc_id").value ;
            var input = '<div class="span-add-softcopy" style="display:none"><div class="row">' ;
            input   += '<div class="col-sm-10"><input type="file" name="document_name'+ doc_id + '" class="form-control" /></div><div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-sm btn-danger button-delete-softcopy"><i class="glyphicon glyphicon-trash"></i></div>';
            input   += '</div></div>' ;

            document.getElementById("doc_id").value = (doc_id - 1) + 2 ; 

            $("#add-softcopy").append(input);
            $(".span-add-softcopy").slideDown("fast");

            //bind element with event function
            $(".button-delete-softcopy").click(function() {
                $(this).parent().slideUp("fast");
                $(this).parent().parent().empty();
            });
       });

        $("#from_status").change(function() {     
            var i = $(this).val() ;
            
            var j = (i*1)+1 ;
            var k = (i*1)-1 ;
            var l = (i*1)+2 ;
            var m = (i*1)-2 ;

            $("#sf" + i).show() ;
            $("#sf" + i).val('') ;

            $("#sf" + j).hide() ;
            $("#sf" + j).val('') ;
            $("#sf" + k).hide() ;
            $("#sf" + k).val('') ;   
            $("#sf" + l).hide() ;
            $("#sf" + l).val('') ;
            $("#sf" + m).hide() ;
            $("#sf" + m).val('') ;            
        });

        var jenis_id = $("#jenis_id").val() ;
        if (jenis_id == 21) {
            $("#agenda_post").show() ;
            $(".agenda").show() ;
        }   
        else {
            $("#agenda_post").hide() ;
            $(".agenda").hide() ;
        }

        $("#jenis_id").change(function() {
            var jenis_id = $(this).val() ;  
            if (jenis_id == 21) {
                $("#agenda_post").show() ;
                $(".agenda").show() ;
				$(".biasa").hide();
            }   
            else {
                $("#agenda_post").hide() ;
                $(".agenda").hide() ;
            }
        });

        <?php foreach ($from_status as $key => $val) :?>
            <?php if (!empty($key)) :?>
                $("#sftext<?php echo $key ;?>").change(function() {
                    var txt = $(this).val() ;     
                    $("#surat_from").val(txt) ;
                });
            <?php endif ;?>
        <?php endforeach ;?>
    }) ;
</script>