<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>PROFIL</h2>

<?php echo form_open_multipart("admin/users/profile") ;?>
    <table class="table editform" cellpadding="5" cellspacing="2" width="100%">
        <tbody>
        <?php $i = 0 ;?>
        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top" width="20%">
                <label for="type"><?php echo $this->lang->line('profile_form_level');?> :</label>
            </th>
            <td>
                <?php echo form_dropdown('level_id', $level_dw,$user ? $user['level_id'] : '','disabled') ;?>
            </td>
            <td rowspan="5" style="width: 20%;">
                <?php
                    $photo  = _BASE_URL_ . 'public/images/nophoto.jpg' ;
                    if ($user && !empty($user['user_img'])) {
                        if (file_exists(_PATH_UPLOAD_IMG_THUMBS_ . $user['user_img'])) {
                            $photo  = _URL_UPLOAD_IMG_THUMBS_ . $user['user_img'] ;
                        }
                    }
                ;?>
                <center>
                <img src="<?php echo $photo ;?>" width="100" alt="<?php echo $user ? $user['fullname'] : '';?>" />
                <br />
                <?php echo form_upload('user_img') ;?>
                </center>
             </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="username"><?php echo $this->lang->line('profile_form_username');?> :</label>
            </th>
            <td>
                <input name="username" value="<?php echo set_value('username',$user ? $user['username'] : '');?>" size="25" type="text" />
                <?php echo form_error('username') ;?>
                <?php echo $username_error ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="fullname"><?php echo $this->lang->line('profile_form_fullname');?> :</label>
            </th>
            <td>
                <input name="fullname" value="<?php echo set_value('fullname',$user ? $user['fullname'] : '');?>" size="25" type="text" />
                <?php echo form_error('fullname') ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="email"><?php echo $this->lang->line('profile_form_email');?> :</label>
            </th>
            <td colspan="2">
                <input name="email" value="<?php echo set_value('email',$user ? $user['email'] : '');?>" size="30" type="text" onkeypress="return restrictCharacters(this, event, emailCharacter);" />
                <?php echo form_error('email') ;?>
                <?php echo $email_error ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="status"><?php echo $this->lang->line('profile_form_status');?> :</label>
            </th>
            <td colspan="2">
                <?php echo form_dropdown('status',$status_dw,$user ? $user['status'] : '','disabled') ;?>
                <?php echo form_error('status') ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="location">Alamat :</label>
            </th>
            <td colspan="2">
                <input name="location" value="<?php echo set_value('location',$user ? $user['location'] : '');?>" type="text" style="width: 98%;" />
                <?php echo form_error('email') ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="position">Posisi :</label>
            </th>
            <td colspan="2">
                <input name="position" value="<?php echo set_value('position',$user ? $user['position'] : '');?>" type="text" style="width: 98%;" />
                <?php echo form_error('position') ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="password"><?php echo $this->lang->line('profile_form_pwd_new');?> :</label>
            </th>
            <td colspan="2">
                <input name="password" value="" size="25" type="password" />
                <?php echo form_error('password') ;?>
                <?php echo $password_error ;?>
            </td>
        </tr>

        <tr<?php if ($i++%2==0):?> class="alternate"<?php endif ;?>>
            <th scope="row" valign="top">
                <label for="confirm_password"><?php echo $this->lang->line('profile_form_pwd_confirm');?> :</label>
            </th>
            <td colspan="2">
                <input name="confirm_password" value="" size="25" type="password" />
                <?php echo form_error('confirm_password') ;?>
            </td>
        </tr>

        </tbody>

    </table>

    <p class="text-center">
        <?php echo form_hidden('sess_security',$sess_security) ;?>
        <?php echo form_hidden('temp_user_img',$user ? $user['user_img'] : '') ;?>
        <?php echo form_hidden('user_id',$user ? $user['user_id'] : '') ;?>
        <?php echo form_hidden('old_email',$user ? $user['email'] : '') ;?>
        <?php echo form_hidden('old_username',$user ? $user['username'] : '') ;?>

        <input name="savepost" value="&radic; SIMPAN" type="submit" class="btn btn-lg btn-primary" />
    </p>
<?php echo form_close() ;?>

</div>