<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<!-- HIGHLIGHT FADE -->
<script type="text/javascript" src="<?php echo _JAVASCRIPT_ ;?>jquery.highlightFade.js"></script>    
<script type="text/javascript">
    function addFile() {
       var id = document.getElementById("file_id").value;
       
        $("#file" + id).show() ;
       
       $('#file' + id).highlightFade({
            speed:1000
        });
       
       id = (id - 1) + 2;
       document.getElementById("file_id").value = id;
       if (id > 9) {
           $("#file_add").hide() ;
       }
   }    
</script>    
    
<div class="wrap">
    <h2>
        UPLOAD DOKUMEN DI FOLDER
    </h2>
    
    <?php echo form_open(current_url(),array('method'=>'get','name'=>'theForm')) ;?>
    <p style="font-weight: bold;">
        TAHUN : <?php echo form_dropdown('tahun',$tahun_dw,$this->input->get('tahun')) ;?>
        &nbsp;
        ESELON / FOLDER : <?php echo form_dropdown('folder_id',$folder_dw,$this->input->get('folder_id'),'onchange="autoSubmit();"');?>
        <?php echo form_submit('searchpost','CARI','class="button"');?>
    </p>
    <?php echo form_close() ;?>
    
    <?php if (!empty($folder)) :?>
    
    <form enctype="multipart/form-data" method="post" action="<?php echo current_url() . $uri_get ;?>">
    
        <ol>
            <?php if ($files->num_rows() > 0) :?>
                <?php foreach ($files->result() as $f) :?>
                    <li>
                        Nama Dokumen : <?php echo form_input(array('name'=>'file_title_edit[]','size'=>40),$f->file_title) ;?> 
                        &nbsp;&nbsp;
                        NO. URUT : <?php echo form_input(array('name'=>'file_order_edit[]','size'=>2),$f->file_order) ;?> 
                                   <?php echo form_hidden('file_id[]',$f->file_id) ;?>
                        &nbsp; 
                        <?php if (!empty($f->file_name) && file_exists(_PATH_UPLOAD_FILES_ . $f->file_name)) :?>
                            &nbsp;
                            <a title="<?php echo $f->file_name ;?>" href="<?php echo site_url('admin/dokumen/read/'.$f->file_id) ;?>" class="a-<?php echo $f->file_ext ;?>">
                                <?php echo ellipsize($f->file_name, 25, .5) .' ('.byte_format($f->file_size).')' ;?>
                            </a>
                        <?php endif ;?>
                        &nbsp;&nbsp;
                        <a title="hapus &raquo;" onclick="return confirm('Apakah Anda yakin untuk menghapus dokumen ini?');" href="<?php echo site_url('admin/dokumen/files/'.$f->file_id.'/del' ) . $uri_get ;?>">(x)</a>
                    </li>
                <?php endforeach ;?>
            <?php endif ;?>
                
            <?php foreach (range(0,9) as $i) :?>
                <li id="file<?php echo $i ;?>"<?php if ($i > 0):?> style="display: none;"<?php endif ;?>>
                    Nama Dokumen : <?php echo form_input(array('name'=>'file_title'.$i,'size'=>40)) ;?> 
                    &nbsp;&nbsp;
                    NO. URUT : <?php echo form_input(array('name'=>'file_order'.$i,'size'=>2)) ;?> 
                    &nbsp;&nbsp; 
                    Upload : <?php echo form_upload(array('name'=>'file_name'.$i)) ;?>
                </li>
            <?php endforeach ;?>
                
            <input type="hidden" value="1" id="file_id" />    
        </ol>
        
        <p id="file_add" style="margin-left: 20px;"><a href="javascript:;" onClick="addFile(); return false;">(+) TAMBAH</a></p>     
        
        <p class="submit">
            <?php echo form_hidden(array(   'folder_id'     => $folder['folder_id'] ,
                                            'user_id'       => $user_id ,
                                            'tahun'         => $this->input->get('tahun') ,
                                            'sess_security' => $sess_security )) ;?>

            <input name="savepost" value="SIMPAN &raquo;" type="submit" />
        </p>
        
    </form>
    
    <?php endif ;?>
</div>