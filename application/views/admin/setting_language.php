<?php if (!empty($note)) : ?>
    <div id='message' class='<?php echo $symbol;?>'><p><strong><?php echo $note;?></strong></p></div>
<?php endif ;?>

<div class="wrap">
    <h2>
        <?php echo $this->lang->line('set_language_title') ;?>
    </h2>
    <p>
        <?php echo form_open('admin/setting/lang') ;?>
            <?php echo $this->lang->line('set_language_default') ;?>
                &nbsp;
            <?php echo form_dropdown('config_style', $idiom_dw, set_value('config_style',$this->config->item('language')) ) ;?>
                &nbsp;
            <?php echo form_hidden(
                                    array(  'sess_security'     => $sess_security,
                                            'config_name'       => 'language' ,
                                            'config_name_lang'  => $this->lang->line('set_language_default_val') ,
                                            'config_id_lang'    => $this->lang->line('set_language_default_val') ,
                                            'config_style_lang' => $this->lang->line('set_language_default_val'))) ;?>
            <?php echo form_submit('config_post', $this->lang->line('set_language_default_save'),'class="button"') ;?>
        <?php echo form_close() ;?>
    </p>
</div>

<div class="wrap">
    <h2>
        <?php echo $this->lang->line('set_language_title_trans') ;?>
    </h2>

    <table class="widefat" style="border: none;">
        <tr>
            <td align="right">
                <form action="<?php echo site_url('admin/setting/lang');?>" method="get">
                    <?php echo form_input('keyword', (!empty($_GET['keyword']) ? $_GET['keyword'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('setting_language_search'),'class="button"') ;?>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form action="<?php echo site_url('admin/setting/lang');?>" method="get">
                    <?php echo form_dropdown('idiom', $idiom_dw, (!empty($_GET['idiom']) ? $_GET['idiom'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_dropdown('langfile', $langfile_dw, (!empty($_GET['langfile']) ? $_GET['langfile'] : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', $this->lang->line('setting_language_filter'),'class="button"') ;?>
                </form>
            </td>
        </tr>

    </table>

    <?php echo form_open('admin/setting/lang') ;?>

    <table class="widefat">
        <thead>
            <tr>
                <th scope="col" style="width: 2%;"><?php echo $this->lang->line('set_language_table_no') ;?></th>
                <th scope="col" style="width: 13%;"><?php echo $this->lang->line('set_language_table_idiom') ;?></th>
                <th scope="col" style="width: 15%;"><?php echo $this->lang->line('set_language_table_langfile') ;?></th>
                <th scope="col" style="width: 25%;"><?php echo $this->lang->line('set_language_table_key') ;?></th>
                <th scope="col" style="width: 40%;"><?php echo $this->lang->line('set_language_table_line') ;?></th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php $no = ($page+1) ;?>
        <?php if ($language->num_rows() > 0) :?>
            <?php foreach ($language->result() as $m) :?>
                <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                    <td align="right"><?php echo $no++ ;?>.</td>
                    <td align="center"><?php echo form_dropdown('idiom_edit'.$m->language_id,$idiom_dw,$m->idiom);?></td>
                    <td align="center"><?php echo form_input(array('name'=>'langfile_edit'.$m->language_id),$m->langfile,'style="width:99%;"');?></td>
                    <td align="center"><?php echo form_input(array('name'=>'language_key_edit'.$m->language_id),$m->language_key,'style="width:99%;"');?></td>
                    <td align="center"><?php echo form_input(array('name'=>'language_line_edit'.$m->language_id),$m->language_line,'style="width:99%;"');?></td>
                    <td align="center"><a href="<?php echo site_url('admin/setting/language/'.$m->language_id.'/del');?>" class="delete" onclick="return confirm('<?php echo $this->lang->line('set_language_del_ask') ;?>');"><?php echo $this->lang->line('set_language_del') ;?></a></td>
                </tr>
            <?php endforeach ;?>
        <?php endif ;?>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="6">
                <div class="paging">
                    <?php echo $paging ;?>
                </div>
            </th>
        </tr>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="6" style="text-align: left;">(+) <?php echo $this->lang->line('set_language_add') ;?></th>
        </tr>

        <?php foreach (range(1,5) as $i) :?>
            <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                <td align="right">&nbsp;</td>
                <td align="center"><?php echo form_dropdown('idiom_new'.$i,$idiom_dw);?></td>
                <td align="center"><?php echo form_input(array('name'=>'langfile_new'.$i),'','style="width:99%;"');?></td>
                <td align="center"><?php echo form_input(array('name'=>'language_key_new'.$i),'','style="width:99%;"');?></td>
                <td align="center"><?php echo form_input(array('name'=>'language_line_new'.$i),'','style="width:99%;"');?></td>
                <td align="center">&nbsp;</td>
            </tr>
        <?php endforeach ;?>

        </tbody>

        <thead>
            <tr>
                <th scope="col" colspan="6" style="text-align: center;">
                    <?php if (!empty($postdata) && is_array($postdata)) :?>
                        <?php foreach ($postdata as $key => $value ) :?>
                            <?php echo form_hidden('postdata['.$key.']',$value) ;?>
                        <?php endforeach ;?>
                    <?php endif ;?>

                    <?php echo form_hidden('sess_security',$sess_security) ;?>
                    <input class="button" name="savepost" value="<?php echo $this->lang->line('set_language_save') ;?>" type="submit" />
                </th>
            </tr>
        </thead>
    </table>

    <?php echo form_close() ;?>
</div>
