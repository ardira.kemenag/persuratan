<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        SURAT KELUAR : <?php echo strtoupper($jenis['jenis_name']);?>
        <div id="add">
            <a href="<?php echo site_url('admin/suratout/c/'.$jenis['jenis_id'].'/new');?>">COMPOSE SURAT BARU</a>
        </div>
    </h2>

    <div class="table-responsive">
        <table class="widefat" style="border: none;">
            <tr>
                <td align="right">
                    <form action="<?php echo site_url('admin/suratout/c/'.$jenis['jenis_id']);?>" method="get">
                        <?php echo form_input(array('name'=>'date_start','size'=>10,'maxlength'=>10,'id'=>'datepicker1'),$this->input->get('date_start',TRUE)) ;?>
                        &nbsp;s.d.&nbsp;
                        <?php echo form_input(array('name'=>'date_end','size'=>10,'maxlength'=>10,'id'=>'datepicker2'),$this->input->get('date_end',TRUE)) ;?>
                        <?php echo form_dropdown('klasifikasi_id' , $klasifikasi_dw, $this->input->get('klasifikasi_id',TRUE) ) ;?>
                            &nbsp;
                        <?php echo form_dropdown('sifat_id' , $sifat_dw , $this->input->get('sifat_id',TRUE) ) ;?>
                            &nbsp;
                        <?php echo form_input(array('name'=>'keyword','placeholder'=>'Keyword...'), $this->input->get('keyword',TRUE) ) ;?>
                            &nbsp;
                        <?php echo form_submit('filter', 'Cari','class="button"') ;?>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <?php if ($surat->num_rows() > 0) :?>

        <?php $alt = 1 ;?>

        <?php echo form_open(current_url(),array('id'=>"adminForm")) ;?>    

            <div class="table-responsive">

            <table class="table widefat">
                <thead>
                    <tr>
                        <th style="width: 5%;text-align: center;padding-top: 20px;">
                            <input type="checkbox" onclick="checkAll(document.getElementById('adminForm'));" type="checkbox" value="all" name="cek_all" />
                        </th>
                        <th style="text-align: center;vertical-align: middle;">SIFAT</th>
                        <th style="text-align: center;vertical-align: middle;">JENIS</th>
                        <th style="text-align: center;vertical-align: middle;">NO. SURAT</th>
                        <th style="text-align: center;vertical-align: middle;">TANGGAL</th>
                        <th style="text-align: center;vertical-align: middle;">KLASIFIKASI</th>
                        <?php $tampil=$this->uri->segment(4);?>
                        <th style="text-align: center;vertical-align: middle;">
						<?php if($tampil==1){
							echo "NAMA KAPAL";
						}else {
							echo "ASAL SURAT";
						};?></th>
                        <th style="text-align: center;vertical-align: middle;">TANDA TANGAN</th>
                        <th style="text-align: center;vertical-align: middle;width:5%;">#</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($surat->result() as $a) :?>

                        <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                            <td><center><input type="checkbox" name="delete_id[]" value="<?php echo $a->surat_id ;?>" /></center></td>
                            <td><?php echo $sifat_dw[$a->sifat_id] ;?></td>
                            <td><?php echo $jenis_dw[$a->jenis_id] ;?></td>
                            <td>
                                <a href="<?php echo site_url('admin/suratout/c/'.$a->jenis_id.'/'.$a->surat_id).'?view=1';?>">
                                    <?php echo $a->surat_no ;?>
                                </a>
                            </td>
                            <td><?php echo time_to_words($a->surat_date) ;?></td>
                            <td><?php echo str_replace('.. ','',$klasifikasi_dw[$a->klasifikasi_id]) ;?></td>
                              <td><?php 
							if($tampil==1){
								echo $a->surat_kapal;
							}else{
								echo str_replace('.. ','',$olah_dw[$a->olah_id]) ;
							}
							?></td>
                            <td><?php echo str_replace('.. ','',$ttd_dw[$a->ttd_id]) ;?></td>
                            <td align="center">
                                <a class="btn btn-success btn-xs" href="<?php echo site_url('admin/suratout/c/'.$a->jenis_id.'/'.$a->surat_id);?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach ;?>    


                    <tr<?php if ($alt++%2==0):?> class="alternate"<?php endif ;?>>
                        <td colspan="5" style="text-align: left;">
                            <?php echo form_hidden(array('sess_security'=>$sess_security)) ;?>
                            <?php echo form_submit('delete_post','DELETE','class="btn btn-md btn-danger"');?>
                        </td>
                        <td colspan="4" style="vertical-align: top;">
                            <div class="paging">
                                <?php echo $paging ;?>
                            </div>
                        </td>
                    </tr> 

                </tbody>
            </table>    

            </div>

        <?php echo form_close() ;?>

    <?php endif ;?>

</div>