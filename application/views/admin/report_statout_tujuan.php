<div class="wrap">
    <h2>
        STATISTIK SURAT KELUAR PER TUJUAN
    </h2>

    <?php if (!empty($note)) : ?>
        <div id='message' class='<?php echo $symbol;?>' style="width: 94%;margin-bottom: 10px;"><p><strong><?php echo $note;?></strong></p></div>
    <?php endif ;?>
        
    <table class="widefat" style="border: none;">
        <tr>
            <td align="right">
                <form action="<?php echo current_url();?>" method="get">
                    <?php echo form_input(array('name'=>'start_date','size'=>10,'maxlength'=>10,'id'=>'datepicker1'),$this->input->get('start_date',TRUE)) ;?>
                    &nbsp;s.d.&nbsp;
                    <?php echo form_input(array('name'=>'to_date','size'=>10,'maxlength'=>10,'id'=>'datepicker2'),$this->input->get('to_date',TRUE)) ;?>
                        &nbsp;
                    <?php echo form_submit('filter', 'Cari','class="button"') ;?>
                </form>
            </td>
        </tr>
    </table>

        <div class="table-responsive">

            <table class="table widefat table-striped">
                <thead>
                    <tr>
                        <th scope="col" style="width: 3%;" rowspan="2">NO</th>
                        <th scope="col" rowspan="2">JENIS SURAT</th>
                        <th scope="col" colspan="<?php echo count($disposisi) ;?>">TUJUAN</th>
                    </tr>
                    <tr>
                        <?php foreach ($disposisi as $key => $val) :?>
                            <th scope="col"><?php echo $val ;?></th>
                        <?php endforeach ;?>    
                    </tr>
                </thead>
                <tbody>
                
                <?php $no = 1 ; foreach ($jenis->result() as $m) :?>
                    <tr>
                        <td style="text-align: center;"><?php echo $no++ ;?>.</td>
                        <td><?php echo $m->jenis_name ;?></td>

                        <?php foreach ($disposisi as $key => $val) :?>
                            <?php $field = 'tujuan_out'.$key ; ?>
                            <td style="text-align: center;">
                                <?php echo separator($m->$field)  ;?>
                            </td>

                        <?php endforeach ;?>
                    </tr>
                <?php endforeach ;?>
                
                </tbody>
                
            </table>  

        </div>      

        <?php echo $charts ;?>    
    
</div>