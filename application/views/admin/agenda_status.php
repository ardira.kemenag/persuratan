<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
<?php endif ;?>

    <h2>STATUS KEGIATAN</h2>

    <?php echo form_open('admin/agenda/status') ;?>

    <table class="table widefat">
        <thead>
            <tr>
                <th scope="col" style="width: 3%;">NO</th>
                <th scope="col" style="width: 50%;">STATUS KEGIATAN</th>
                <th scope="col" style="width: 35%;">JUMLAH KEGIATAN</th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>

        <tbody>
        <?php $no = ($page+1) ;?>
        <?php if ($status->num_rows() > 0) :?>
            <?php foreach ($status->result() as $m) :?>
                <tr<?php if ($no%2==0) :?> class="alternate"<?php endif;?>>
                    <td align="right"><?php echo $no++ ;?>.</td>
                    <td align="center">
                        <?php echo form_input(array('name'=>'status_name[]','class'=>'form-control'),$m->status_name);?>
                        <?php echo form_hidden('status_id[]',$m->status_id) ;?>
                    </td>
                    <td align="center">
                        <?php if (!empty($m->kegiatan_num)) :?>
                            <a class="btn btn-xs btn-info" href="<?php echo site_url('admin/agenda').'?status_id='.$m->status_id;?>" title="Status kegiatan ini memiliki <?php echo $m->kegiatan_num ;?> agenda kegiatan">
                                <?php echo separator($m->kegiatan_num) ;?>
                            </a>
                        <?php else :?>
                            -
                        <?php endif ;?>
                    </td>
                    <td align="center">
                        <a href="<?php echo site_url('admin/agenda/status/'.$m->status_id.'/del');?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus status kegiatan ini?');">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach ;?>
        <?php endif ;?>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="4">
                <div class="paging">
                    <?php echo $paging ;?>
                </div>
            </th>
        </tr>

        <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
            <th scope="col" colspan="4" style="text-align: left;">(+) TAMBAH BARU</th>
        </tr>

        <?php foreach (range(1,5) as $i) :?>
            <tr<?php if ($no++%2==0) :?> class="alternate"<?php endif;?>>
                <td align="center">&nbsp;</td>
                <td align="center"><?php echo form_input(array('name'=>'status_name[]','class'=>'form-control'),NULL);?></td>
                <td colspan="2">&nbsp;</td>
            </tr>
        <?php endforeach ;?>

        </tbody>

        <thead>
            <tr>
                <th scope="col" colspan="4" style="text-align: center;">
                    <?php echo form_hidden('sess_security',$sess_security) ;?>
                    <input class="btn btn-lg btn-primary" name="savepost" value="SIMPAN &raquo;" type="submit" />
                </th>
            </tr>
        </thead>
    </table>

    <?php echo form_close() ;?>
</div>