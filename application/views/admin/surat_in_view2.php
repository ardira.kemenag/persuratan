<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        SURAT MASUK
        <div id="pdf"><a href="<?php echo current_url().'?view=1&amp;export=pdf';?>" title="PDF">&nbsp;</a></div>
        <div id="print"><a onclick="popUpWindow('<?php echo  current_url().'?view=1&amp;export=print';?>')" href="javascript:;" title="Print">&nbsp;</a></div>
        <div id="xls"><a href="<?php echo current_url().'?view=1&amp;export=xls';?>" title="Ms.Excel">&nbsp;</a></div>
    </h2>

    <p style="font-weight:bold;text-align:center;">KEMENTERIAN KELAUTAN DAN PERIKANAN<br />UNIT ESELON I</p>

    <p style="font-weight:bold;text-align:center;">UNIT KERJA ESELON <?php echo $es_view ;?></p>

    <p style="font-weight:bold;text-align:center;">LEMBAR DISPOSISI </p>
    
    <?php if (!empty($surat['reply_surat_number'])) :?>
    <p>
        <b>BALASAN DARI : 
            <a href="<?php echo site_url('admin/suratin/c/'.$surat['reply_format_id'].'/'.$surat['surat_reply_id']);?>" class="vtip" title="<small><?php echo time_to_words($surat['reply_surat_date'],TRUE);?></small><br /><b>No. Surat : <?php echo html_entity_decode($surat['reply_surat_number']) ;?></b><br /><b>Perihal &nbsp;&nbsp;&nbsp;&nbsp;: </b><?php echo html_entity_decode($surat['reply_surat_perihal']) ;?>">
                <?php echo character_limiter($surat['reply_surat_perihal'],80) ;?>
            </a>
        </b>
    </p>
    <?php endif ;?>

    <table class="table">
        <tbody>
            <tr>
                <td>Dari : </td>
                <td style="border-bottom:1px solid #ccc;">Diterima Tanggal</td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">: <?php echo time_to_words($surat['surat_date_taken']) ;?></td>
            </tr>
            <tr>
                <td rowspan="3" style="border-bottom:1px solid #ccc;"><?php echo $surat['surat_from'] ;?></td>
                <td style="border-bottom:1px solid #ccc;">Surat Nomor</td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">: <?php echo $surat['surat_number'] ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Tanggal</td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">: <?php echo time_to_words($surat['surat_date']) ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">Lampiran</td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">: 
                    <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                        <?php foreach ($document->result() as $doc) :?>
                            <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                <?php if ($document->num_rows() > 1) :?>
                                    <?php echo $no++ ;?>. 
                                <?php endif ;?>

                                <a class="a-<?php echo $doc->document_fileext;?>" href="<?php echo site_url('download/c/'.$doc->document_id.'/'.url_title($doc->document_filename));?>"><?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?></a>
                                    &nbsp; <a href="<?php echo site_url('admin/suratin/c/'.$surat['format_id'].'/'.$surat['surat_id']).'?delete_doc='.$doc->document_id;?>">
                                <br />
                            <?php endif ;?>
                        <?php endforeach ;?>
                    <?php endif ;?>
                </td>
            </tr>
            <tr>
                <td>Hal : </td>
                <td style="border-bottom:1px solid #ccc;">Sifat</td>
                <td colspan="2" style="border-bottom:1px solid #ccc;">: <?php echo $sifat_dw[$surat['sifat_id']] ;?></td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;"><?php echo $surat['surat_perihal'] ;?></td>
                <td style="border-bottom:1px solid #ccc;">Untuk</td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>Indek : </td>
                <td style="border-bottom:1px solid #ccc;" colspan="3" rowspan="6">
                    <?php if ($aksi->num_rows() > 0) :?>
                        <ul style="margin:0;padding-left:15px;list-style:none;">
                            <?php foreach ($aksi->result() as $a) :?>
                                <li><?php echo (in_array($a->aksi_id, $aksi_post) ? '&check;' : '&bull;'  ).' '.$a->aksi_name ;?></li>
                            <?php endforeach ;?>
                        </ul>
                    <?php endif ;?>

                    <p>Catatan : </p>
                    <?php if (!empty($surat['surat_disposisi_message'])) :?>
                        <p><?php echo nl2br($surat['surat_disposisi_message']) ;?></p>
                    <?php else :?>
                        <p>-</p>
                    <?php endif ;?>    
                </td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;"><?php echo $surat['surat_indek'] ;?></td>
            </tr>
            <tr>
                <td>Kode : </td>
            </tr> 
            <tr>
                <td style="border-bottom:1px solid #ccc;"><?php echo $surat['surat_kode'] ;?></td>
            </tr>   
            <tr>
                <td>Diteruskan Yth. : </td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">
                    <?php if ($disposisi->num_rows() > 0) :?>
                        <ol>
                        <?php foreach ($disposisi->result() as $d) :?>
                            <?php
                                $subdis = $this->disposisi_sub_model->show_tree(array('disposisi_id'=>$d->disposisi_id)) ;
                                if (!empty($subdis) && count($subdis) > 0) : ?>
                                    <?php foreach ($subdis as $subdis_id => $subdis_name) :?>
                                        <?php if (in_array($subdis_id, $disposisi_post)) :?>
                                            <li>&check; <?php echo str_replace('.. ','',$subdis_name) ;?></li>
                                        <?php endif ;?>    
                                    <?php endforeach ;?>
                                <?php endif ;?>
                        <?php endforeach ;?>
                        </ol>
                    <?php endif ;?>
                </td>
            </tr>  
            <tr>
                <td style="border-bottom:1px solid #ccc;">1. Oleh Sekretaris Pribadi</td>
                <td style="border-bottom:1px solid #ccc;text-align:center;">Tanggal</td>
                <td style="border-bottom:1px solid #ccc;text-align:center;">Paraf</td>
                <td style="border-bottom:1px solid #ccc;text-align:center;">Catatan Penyelesaian</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">... Diterima</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">... Diteruskan</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">... Diterima Kembali</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">2. Oleh Unit Kerja Ybs.</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">... Diterima</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #ccc;">... Diselesaikan</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
                <td style="border-bottom:1px solid #ccc;">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>