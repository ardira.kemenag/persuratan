<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>

    <h2>
        AGENDA UNDANGAN
        <!--
        <div id="add">
            <a href="<?php echo site_url('admin/agenda/kegiatan/new');?>">TAMBAH AGENDA</a>
        </div>
        -->
    </h2>
    
    <table class="widefat" style="border: none;">
        <tr>
            <td align="right">
                <form action="<?php echo site_url('admin/agenda');?>" method="get">
                    <?php echo form_dropdown('status_id' , $status_dw, (!empty($_GET['status_id'])  ? $_GET['status_id']   : NULL) ) ;?>
                        &nbsp;
                    <?php echo form_input(array('name'=>'keyword','placeholder'=>'Kata kunci..') , $this->input->get('keyword',TRUE) ) ;?>
                        &nbsp;    
                    <?php echo form_input(array('size'=>10,'maxlength'=>10,'name'=>'kdate','id'=>'datepicker1'), (!empty($_GET['kdate']) ? $_GET['kdate'] : NULL) ) ;?>
                        &nbsp;        
                    <?php echo form_submit('filter', 'Cari','class="button"') ;?>
                </form>
            </td>
        </tr>
    </table>

    <?php if ($kegiatan->num_rows() > 0) :?>

    <div class="table-responsive">

    <table class="table widefat table-striped">
        <thead>
            <tr>
                <th scope="col" style="width: 2%;">NO</th>
				<th scope="col" style="width: 2%;"></th>
                <th scope="col" style="width: 20%;">AGENDA RAPAT</th>
                <th scope="col" style="width: 20%;">WAKTU</th>
                <th scope="col" style="width: 20%;">Tempat</th>
                <th scope="col" style="width: 20%;">ASAL UNDANGAN</th>
                <th scope="col" style="width: 20%;">DIPOSISI</th>
                <th scope="col" style="width: 10%;">STATUS</th>
                <th scope="col" colspan="2">&nbsp;</th>
            </tr>
        </thead>
        <tbody id="the-list">
        <?php $page += 1 ; foreach($kegiatan->result() as $a) :?>
            <tr>
                <td valign="top" align="right"><?php echo $page++ ;?>.</td>
                <td valign="top" align="left"><?php
					$cek_tujuan = $this->db->get_where('surat__in', array('surat_id' => $a->surat_id)); 
					foreach($cek_tujuan->result() as $lv){
						$level=$lv->surat_to;
					}
											
					$array = json_decode($level);
					
					$this->db->where_in('subdis_id', $array);
					
					$query  = $this->db->get('disposisi__sublist') ;
					foreach($query->result() as $k){
						echo $k->subdis_man .' - ';
					}
				
				
				?>.</td>
                <td valign="top"><?php echo $a->kegiatan_name ;?></td>
                <td valign="top">
                    <?php if ($a->kegiatan_date_start == $a->kegiatan_date_end OR (empty($a->kegiatan_date_end) OR $a->kegiatan_date_end == '0000-00-00')) :?>
                        <?php echo $a->start_date_diff.' '.$a->kegiatan_time_start ;?>
                    <?php else :?>
                        <?php echo $a->start_d.'/'.$a->start_m.' - '.$a->end_date_diff ;?>
                    <?php endif ;?>
                </td>
                <td valign="top" align="left"><?php echo nl2br2($a->kegiatan_tempat) ;?></td>
                <td valign="top" align="left"><?php echo nl2br2($a->kegiatan_penyelenggara) ;?></td>
                <td valign="top" align="left"><?php echo nl2br2($a->kegiatan_peserta) ;?></td>
                <td valign="top" align="center">
                    <a href="javascript:;" class="btn btn-xs btn-<?php echo $a->status_id == 2 ? 'primary' : 'warning' ;?>">
                        <?php echo $a->status_name ;?>
                    </a>
                </td>
                
                <td valign="top" align="center">
                    <a href="<?php echo site_url('admin/agenda/kegiatan/' . $a->kegiatan_id) ;?>" class="btn btn-success btn-xs">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                </td>
                <td valign="top" align="center">
                    <a href="<?php echo site_url('admin/agenda/kegiatan/'. $a->kegiatan_id . '/del')  . $uri_get ;?>" class="btn btn-xs btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus kegiatan ini?');">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach ;?>

            <tr>
                <th scope="row" colspan="8" style="vertical-align: top;">
                    <div class="paging">
                        <?php echo $paging ;?>
                    </div>
                </th>
            </tr>
        </tbody>
    </table>

    </div>

    <?php else :?>
        <div class="alert alert-warning">Pencarian tidak ditemukan.</div>
    <?php endif ;?>
</div>