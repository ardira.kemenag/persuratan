<?php
if ($export == 'xls') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,
            pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    header('Content-Disposition: attachment; filename='.$export_title.'.xls');
    header("Content-Transfer-Encoding: binary ");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title></title>
    <script type="text/php">
         if (isset($pdf)) {
             $font = Font_Metrics::get_font("Helvetica", "bold");
             $pdf->page_text(72, 18, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
         }
    </script>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #000;
            line-height: 150% ;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #000;
            background-color: transparent;
            font-size: 14px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        ul , ol { padding-left: 20px ;}

        table { width:100%; font-size:0.9em; text-align:left; }
        table th { text-align: center; background: #DFDFDF ; }

        img { margin: 10px ; }
        .alternate { font-weight: bold ; }
    </style>
</head>

<body>

    <?php if ($export == 'print') :?>
    <div style="float:right;">
        <a href="javascript:;" onclick="javascript:window.print(); return false;" title="Cetak &raquo;">
            <img src="<?php echo _STYLES_ADMIN_;?>images/print.gif" width="25" alt="" style="text-decoration: none;border: none;" />
        </a>
    </div>
    <?php endif ;?>

    <p style="font-weight: bold;">
        REKAPITULASI PERSURATAN DITJEN PSDKP - SURAT MASUK
    </p>

    <?php if ($surat->num_rows() > 0) :?>
    
    <table class="widefat" style="border: 1px solid #000; border-collapse: collapse;" border="1" cellpadding="4" cellspacing="0">
        <thead>
            <tr>
                <th style="text-align: center;vertical-align: middle;">NO</th>
                <?php foreach ($cols as $key => $val) :?>
                    <th style="text-align: center;vertical-align: middle;"><?php echo strtoupper($columns[$val]) ;?></th>
                <?php endforeach ;?>
            </tr>   
        </thead>
        <tbody id="the-list">
        
                    <?php $page += 1 ;?>
                    <?php foreach ($surat->result() as $a) :?>

                        <tr>
                            <td align="right"><?php echo $page++ ;?>.</td>
                            
                            <?php foreach ($cols as $key => $val) :?>
                                <td>
                                    <?php if (in_array($val,array('aksi','surat_disposisi','surat_to','surat_tembusan'))) :?>
                                        <?php if ($val == 'surat_disposisi') :?>
                                            <?php echo !empty($a->surat_disposisi)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_disposisi,TRUE)),array('- disposisi -'=>'')) : '-' ;?>
                                        <?php elseif ($val == 'surat_to') :?>
                                            <?php echo !empty($a->surat_to)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_to,TRUE)),array('- disposisi -'=>'')) : '-' ;?>    
                                        <?php elseif ($val == 'surat_tembusan') :?>
                                            <?php echo !empty($a->surat_tembusan)  ? strtr(array_to_list($eselon_dw,json_decode($a->surat_tembusan,TRUE)),array('- disposisi -'=>'')) : '-' ;?>        
                                        <?php else :?>
                                            <?php 
                                                $aksi = $this->surat_inaksi_model->get(array('surat_id'=>$a->surat_id ,'aksi'     => TRUE )) ;
                                                if ($aksi->num_rows() > 0) {
                                                    $r_aksi = NULL ; 
                                                    foreach ($aksi->result() as $ak) {
                                                        $r_aksi[] = $ak->aksi_akronim ;
                                                    }

                                                    echo !empty($r_aksi) ? implode(', ',$r_aksi) : '-' ;
                                                }
                                            ?>
                                        <?php endif ;?>    
                                    <?php elseif ($val == 'file') :?>    
                                        <?php $document = $this->surat_indocument_model->get(array('surat_id'=>$a->surat_id)) ;?>
                                        <?php if ($document->num_rows() > 0) : $no = 1 ; ?>
                                            <?php foreach ($document->result() as $doc) :?>
                                                <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                                    <?php if ($document->num_rows() > 1) :?>
                                                        <?php echo $no++.'. ' ;?> 
                                                    <?php endif ;?>
                                                    
                                                    <?php echo ellipsize($doc->document_filename, 15, .5) . '  ('.byte_format($doc->document_filesize).')';?>
                                                    <br />
                                                <?php endif ;?>
                                            <?php endforeach ;?>
                                        <?php else :?>
                                                
                                        <?php endif ;?>
                                    <?php else :?>
                                        <?php echo $a->$val ;?>
                                    <?php endif ;?>
                                </td>
                            <?php endforeach ;?>
                        </tr>

                    <?php endforeach ;?> 
        </tbody>
    </table>

    <?php endif ;?>

</body>
</html>