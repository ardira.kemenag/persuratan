<?php
if ($export == 'xls') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,
            pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    header('Content-Disposition: attachment; filename='.$export_title.'.xls');
    header("Content-Transfer-Encoding: binary ");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title></title>
    <script type="text/php">
         if (isset($pdf)) {
             $font = Font_Metrics::get_font("Helvetica", "bold");
             $pdf->page_text(72, 18, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
         }
    </script>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 15px;
            color: #000;
            line-height: 150% ;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #000;
            background-color: transparent;
            font-size: 14px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        ul , ol { padding-left: 20px ;}

        table { width:100%; font-size:1em; text-align:left; }
        table th { text-align: center; background: #DFDFDF ; }
        table td { vertical-align: top ; }

        img { margin: 10px ; }
        .alternate { font-weight: bold ; }
		
		@media print{
				.no-print{
					display:none;
				}
			}
    </style>
</head>

<body>

    <?php if ($export == 'print') :?>
    <div class="no-print" style="float:right;">
        <a href="javascript:;" onclick="javascript:window.print(); return false;" title="Cetak &raquo;">
            <img src="<?php echo _STYLES_ADMIN_;?>images/print.gif" width="25" alt="" style="text-decoration: none;border: none;" />
        </a>
    </div>
    <?php endif ;?>
	<table cellpadding="2" cellspacing="0" style="border: 1px solid #000;">
		<tr>
			<td>
				<!--<p align="center">Format Lembar Disposisi Pejabat Eselona <?php echo $es_view ;?></p>-->
				<p align="center"><b>KEMENTERIAN KELAUTAN DAN PERIKANAN<br />DIREKTORAT JENDERAL PENGAWASAN SUMBER DAYA KELAUTAN DAN PERIKANAN </b></p>

				<p align="center" style="font-size:1.3em;"><b>LEMBAR DISPOSISI</b></p>
				<?php 
								
				$tampil_nama=$this->db->get_where('users__list',array('subdis_id'=>$surat['subdis_id']));
				
				if($this->data['level_id']==3){
					foreach($tampil_nama->result() as $nm){
						$judul_nama = $nm->fullname;
					}
				}else{
					if($this->data['subdis_id']==0){
						foreach($tampil_nama->result() as $nm){
							$judul_nama = $nm->fullname;
							}
					}else{
						if ($surat['subdis_id']==$this->data['subdis_id']){
							foreach($tampil_nama->result() as $nm){
							$judul_nama = $nm->fullname;
							}
						}else{
							$judul_nama = $fullname;
						}
					}
				}
				 ?>
				<p align="center"><b><?php echo strtoupper($judul_nama);?></b></p>
			</td>
		</tr>
	</table> <?php
        if ($into->num_rows() > 0) {
            foreach ($into->result() as $it) {
                if ($it->subdis_id != $surat['subdis_id']) {
                    $to_main[]  = '[V] '.$eselon_dw[$it->subdis_id] ;
                }
				
							
                $not_id[]   = $it->subdis_id ;
                if (!empty($it->into_msg)) $to_msg[]   = nl2br($it->into_msg) ;

                $disposisi = $this->surat_indisposisi_model->get(array('into_id'=>$it->into_id,'indisposisi_status'=>1)) ;
                if ($disposisi->num_rows() > 0) {
                    foreach ($disposisi->result() as $a) {
                        if (!in_array($a->subdis_id,$not_id)) {
                            $to_branch[] = '[V] '.$eselon_dw[$a->subdis_id] ;                            
                        }
                    }
                }
            }
        }
		$eselon_id = $this->data['subdis_eselon'];
    ?> 
	<table cellpadding="0" cellspacing="0" style="border-left: 1px solid #000; border-right: 1px solid #000;"  >
		 <tr>
			<td width="30%" style="border: 0px solid #fff;">Sifat Surat</td>
			<td width="1%">: </td>
			<td> <?php echo $sifat_dw[$surat['sifat_id']] ;?></td>
		  </tr>
		  <tr>
			<td>Nomor Agenda</td>
			<td>: </td>
			<td></td>
		  </tr>
		  <tr>
			<td>Nomor Surat</td>
			<td>: </td>
			<td> <?php echo $surat['surat_number'] ;?></td>
		  </tr>
		  <tr>
			<td>Asal Surat</td>
			<td>: </td>
			<td> <?php echo $surat['surat_from'] ;?></td>
		  </tr>
		  <tr>
			<td>Tanggal Surat</td>
			<td>: </td>
			<td> <?php echo time_to_words($surat['surat_date']) ;?></td>
		  </tr>
		  <tr>
			<td>Tanggal Penyelesaian</td>
			<td>: </td>
			<td></td>
		  </tr>
		  <tr>
			<td>Hal</td>
			<td>: </td>
			<td> <?php echo $surat['surat_perihal'] ;?></td>
		  </tr>
		  <tr>
			<td colspan="3"><br/>Resume isi surat<br/><br/><br/><br/><br/></td>
		  </tr>
	</table>
	<?php if($this->session->userdata('subdis_id')==2){ ?>
	<table cellpadding="0" cellspacing="0" style="border: 1px solid #000;">
		<tr>
			<td>
				Diteruskan Kepada
				<?php 	$this->db->where('subdis_parent', 1);
						$this->db->order_by('subdis_id', "asc");
						$this->db->not_like('subdis_name' ,"UPT");						
						$q1 =$this->db->get('disposisi__sublist'); ?>
				<?php 
				
				$q2 =$this->db->get_where('disposisi__sublist',array('subdis_parent'=>'2')); ?>
				<ul style="margin:0;padding-left:15px;list-style:none;">
					<?php
					foreach ($q1->result() as $value) { ?>
							<?php if ($value->subdis_id !=2){?>
						<li><?php	echo '[&nbsp;&nbsp;] ' .$value->subdis_name; ?></li>
					<?php  }
					}
					foreach ($q2->result() as $value) { ?>
					<li><?php	echo '[&nbsp;&nbsp;] ' .$value->subdis_name; ?></li>
					<?php }?>
					<li>[&nbsp;&nbsp;] ....................................................</li>
				</ul>
			</td>
		</tr>
	</table>

	<?php }else{ ?>
	<table cellpadding="0" cellspacing="0" style="border: 1px solid #000;">
		<tr>
			<td>
				Diteruskan Kepada<br/>
				<?php $q1=$this->db->get_where('disposisi__sublist',array('subdis_parent'=>$this->session->userdata('subdis_id'))); 
				foreach($q1->result() as $n){
					$id_sub[]=$n->subdis_id;
					$name_sub[]=$n->subdis_name;
				}
				for($i=0;$i<count($id_sub);$i++){
					echo '[ ]'.$name_sub[$i].'<br/>';
					$q2=$this->db->get_where('disposisi__sublist',array('subdis_parent'=>$id_sub[$i]));
					foreach($q2->result() as $m){
						echo '&nbsp;&nbsp;&nbsp;&nbsp;[ ]'.$m->subdis_name.'</br>';
					}
				}
				?>
			</td>
		</tr>
	</table>
	<?php }?>
	<table cellpadding="0" cellspacing="0" style="border: 1px solid #000;">
		<tr>
			<td>Catatan Penyelesaian 
			<br/><br/><br/><br/><br/><br/>
			<br/><br/><br/>
			</td>
		</tr>
	</table>  
	</body>
</html>