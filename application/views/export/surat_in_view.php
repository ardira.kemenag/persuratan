<div class="wrap">
    <?php if (!empty($note)) : ?>
        <div class='<?php echo $symbol;?>'><?php echo $note;?></div>
    <?php endif ;?>
	<div><?php echo $this->session->flashdata('alert_doc');?></div>
    <h2><?php echo strtoupper($admin_submenu);?></h2>
  	
	<?php 
	$tampil_eselon=$this->db->get_where('disposisi__sublist',array('subdis_id'=>$this->session->userdata('subdis_id')));
	if ($tampil_eselon->num_rows()>0){
		foreach($tampil_eselon->result() as $te){
		$eselon_tampil= $te->subdis_eselon;
		}
	}else{
		$eselon_tampil="0";
	}
	
	if ($eselon_tampil !=4 ){?>
    <div class="row" style="margin-bottom:10px;">
        <div class="col-lg-12 text-right">
            <a onclick="popUpWindow('<?php echo current_url() . '?view=1&amp;export=print' ;?>')" href="javascript:;" class="btn btn-sm btn-info" style="color:#fff;">PRINT</a>
            <a href="<?php echo current_url() . '?view=1&amp;export=pdf' ;?>" class="btn btn-sm btn-danger" style="color:#fff;">PDF</a>
        </div>
    </div>  
	<?php } ?>
    <?php if (!empty($surat['reply_surat_number'])) :?>
        <p>
            <b>BALASAN DARI : 
                <a href="<?php echo site_url('admin/suratin/c/'.$surat['surat_reply_id']);?>" class="vtip">
                    <?php echo 'NO : '.$surat['reply_surat_number'].' - '.$surat['reply_surat_perihal'] ;?>
                </a>
            </b>
        </p>
    <?php endif ;?>
    
    <?php $alt = 0 ;?>

    <div class="table-responsive">

        <?php echo form_open(current_url().'?view=1') ;?>

        <table class="table table-striped">
            <tbody>
                <tr>
                    <td width="23%"><b>Jenis Surat</b></th>
                    <td width="2%"><b>:</b></th>
                    <td>
                        <?php echo $jenis_dw[$surat['jenis_id']] ;?>
                    </td>
                </tr>
                <tr>
                    <td><b>Sifat Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $sifat_dw[$surat['sifat_id']] ;?>
                    </td>
                </tr>
                <tr>
                    <td><b>Nomor Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_number'];?>
                    </td>
                </tr>
                <tr>
                    <td><b>Tanggal Surat</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo time_to_words($surat['surat_date']);?>
                    </td>
                </tr>

                <tr>
                    <td><b>Tanggal Surat Diterima</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo time_to_words($surat['surat_date_taken']) ;?>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <b>Dari</b>
                    </th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $from_status[$surat['from_status']].' : '.$surat['surat_from'] ;?>
                    </td>
                </tr>
                <tr>
                    <td><b>Kepada Yth.</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo array_to_list($eselon_dw,json_decode($surat['surat_to'],TRUE)) ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Tembusan</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo array_to_list($eselon_dw,json_decode($surat['surat_tembusan'],TRUE)) ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Perihal</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_perihal'] ;?>
                    </td>
                </tr>

                <?php if ($surat['jenis_id'] == 21) :?>

                    <tr>
                        <td><b>Tempat Kegiatan</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php echo !empty($surat['kegiatan_tempat']) ? $surat['kegiatan_tempat'] : '-';?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><b>Tanggal Kegiatan</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php echo !empty($surat['kegiatan_date_start']) && $surat['kegiatan_date_start'] != '0000-00-00' ? time_to_words($surat['kegiatan_date_start']) : '' ;?>
                            <?php echo !empty($surat['kegiatan_date_end']) && $surat['kegiatan_date_end'] != '0000-00-00' ? ' s.d ' .time_to_words($surat['kegiatan_date_end']) : '' ;?>
                        </th>
                    </tr>
                    
                    <tr>
                        <td><b>Waktu Kegiatan</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php echo !empty($surat['kegiatan_time_start']) && $surat['kegiatan_time_start'] != '00:00' ? $surat['kegiatan_time_start'] : '' ;?>
                            <?php echo !empty($surat['kegiatan_time_end']) && $surat['kegiatan_time_end'] != '00:00' ? ' s.d ' .$surat['kegiatan_time_end'] : '' ;?>
                            <?php echo !empty($surat['timezone']) ? ' '.$timezone[$surat['timezone']] : '' ;?>
                        </th>
                    </tr>

                <?php endif ;?>

                <tr>
                    <td><b>Lampiran</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php if (!empty($document) && $document->num_rows() > 0) : $no = 1 ; ?>
                            <ul class="list-group">
                                <?php foreach ($document->result() as $doc) :?>
                                    <?php if (!empty($doc->document_filename) && file_exists(_PATH_UPLOAD_FILES_ . $doc->document_filename)) :?>
                                        <li class="list-group-item">
                                            <a class="btn btn-info btn-sm" href="<?php echo _URL_UPLOAD_FILES_ . $doc->document_filename ;?>?iframe=true&amp;width=800&amp;height=500" rel="prettyPhoto[iframe]">
                                                <i class="glyphicon glyphicon-list"></i>
                                                <?php echo $doc->document_filename . '  ('.byte_format($doc->document_filesize).')';?>
                                            </a>
											<?php if( $this->session->userdata('user_id') == $surat['user_id']){?>
												<a href="#" data-id="<?php echo $doc->document_id;?>" class="ubah_file btn-sm btn btn-success">Ubah File</a>
											<?php }?>
                                        </li>
										
                                    <?php endif ;?>
                                <?php endforeach ;?>
                            </ul>
                        <?php else :?>
                            -    
                        <?php endif ;?>
                    </td>
                </tr>

                <?php $no_disposisi_again = FALSE ;?>
                <?php if ($into->num_rows() > 0) :?>

                    <?php foreach ($into->result() as $it) :?>

                        <?php 
                            $disposisi = $this->surat_indisposisi_model->get(array('into_id'=>$it->into_id,'indisposisi_status'=>1)) ;
                            if ($it->subdis_id == $this->data['subdis_id']) $no_disposisi_again = TRUE ;
                        ?>

                        <tr>
                            <td colspan="3"><b><?php echo strtoupper($eselon_dw[$it->subdis_id]) ;?></b></td>
                        </tr>

                        <tr>
                            <td align="right"><b>Disposisi</b></th>
                            <td><b>:</b></th>
                            <td>
                                <?php if ($disposisi->num_rows() > 0) :?>
                                    <ul class="list-group">
                                        <?php foreach ($disposisi->result() as $a) :?>
                                            <li class="list-group-item"><i class="glyphicon glyphicon-user"></i> <?php echo $eselon_dw[$a->subdis_id] ;?></li>    
                                        <?php endforeach ;?>
                                    </ul>
                                <?php endif ;?>
                            </td>
                        </tr>

                        <tr>
                            <td align="right"><b>Pesan Disposisi</b></th>
                            <td><b>:</b></th>
                            <td>
                                <?php echo !empty($it->into_msg) ? $it->into_msg : '-' ;?>
                            </td>
                        </tr>

                        <tr>
                            <td align="right"><b>Penanggung Jawab</b></th>
                            <td><b>:</b></th>
                            <td>
                                <?php if ($disposisi->num_rows() > 0) :?>
                                    <ul class="list-group">
                                        <?php foreach ($disposisi->result() as $a) :?>
                                            <?php if ($a->respon_disposisi == 2) :?>
                                                <li class="list-group-item"><i class="glyphicon glyphicon-user"></i> <?php echo $eselon_dw[$a->subdis_id] ;?></li>    
                                            <?php endif ;?>
                                        <?php endforeach ;?>
                                    </ul>
                                <?php endif ;?>
                            </td>
                        </tr>

                        <?php $aksi = $this->surat_inaksi_model->get(array('surat_id'   => $surat['surat_id']
                                                                        ,'aksi'         => TRUE
                                                                        ,'subdis_id'    => $it->subdis_id)) ; ?>

                        <?php if ($aksi->num_rows() > 0) :?>

                            <tr>
                                <td align="right">
                                    <b>Aksi yang Diminta</b><br /><small>(checklist kalau sudah melaksanakan)</small>
                                </td>
                                <td><b>:</b></th>
                                <td>
                                    <?php if ($aksi->num_rows() > 0) :?>
                                        <ul class="list-group">
                                            <?php foreach ($aksi->result() as $a) :?>
                                                <li class="list-group-item">
                                                    <?php echo form_checkbox(array('name'=>'aksi_id','class'=>'inaksi'),$a->inaksi_id, 2 == $a->aksi_status);;?> <?php echo $a->aksi_name ;?>
                                                </li>    
                                            <?php endforeach ;?>
                                        </ul>
                                    <?php endif ;?>
                                </td>
                            </tr>

                        <?php endif ;?>                                                                        

                    <?php endforeach ;?>

                <?php endif ;?>

                <?php if ($no_disposisi_again == FALSE && $subdis_child->num_rows() > 0 && !empty($this->data['subdis_id'])) :?>
                    <tr>
                        <td colspan="3"><b><?php echo strtoupper($eselon_dw[$this->data['subdis_id']]) ;?></b></td>
                    </tr>

                    <tr>
                        <td align="right"><b>Disposisi</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php foreach ($subdis_child->result() as $sc) :?>
                                <?php
                                    $checkbox = form_checkbox('surat_disposisi[]', $sc->subdis_id,FALSE,'id="dis_id'.$sc->subdis_id.'"') .' ' ;
                                    $form_category = $checkbox.' '.$sc->subdis_name ;
                                ?>
                                <?php echo $form_category  ;?><br />
                            <?php endforeach ;?>
                        </td>
                    </tr>

                    <tr>
                        <td align="right"><b>Pesan Disposisi</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php echo form_textarea(array('name'=>'into_msg','rows'=>6),NULL,'class="form-control"');?>
                        </td>
                    </tr>

                    <tr>
                        <td align="right"><b>Penanggung Jawab</b></th>
                        <td><b>:</b></th>
                        <td>
                            <?php foreach ($subdis_child->result() as $sb) : ?>
                                    <div id="respon_subdis_id<?php echo $sb->subdis_id;?>" style="display: none;">
                                        <?php echo form_checkbox('surat_respon[]', $sb->subdis_id, NULL ).'&nbsp;'.$sb->subdis_name.'&nbsp;&nbsp;' ;?>
                                    </div>
                            <?php endforeach ;?>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>Aksi yang Diperlukan</small>
                        </td>
                        <td><b>:</b></th>
                        <td>
                            <?php $aksi = $this->entitas_aksi_model->get() ;?>
                            <?php if ($aksi->num_rows() > 0) :?>
                                <ul class="list-group">
                                    <?php foreach ($aksi->result() as $a) :?>
                                        <li class="list-group-item">
                                            <?php echo form_checkbox(array('class'=>'addaksi'),$a->aksi_id);;?> <?php echo $a->aksi_name ;?>
                                        </li>    
                                    <?php endforeach ;?>
                                </ul>
                            <?php endif ;?>
                        </td>
                    </tr>

                <?php endif ;?>    

                <tr>
                    <td><b>Indek</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_indek'] ;?>
                    </td>
                </tr>

                <tr>
                    <td><b>Kode</b></th>
                    <td><b>:</b></th>
                    <td>
                        <?php echo $surat['surat_kode'] ;?>
                    </td>
                </tr>

                

            </tbody>
        </table>

        <?php if ($no_disposisi_again == FALSE && $subdis_child->num_rows() > 0 && !empty($this->data['subdis_id'])) :?>

            <p class="text-center">
                <?php echo form_hidden(array('surat_id'     => $surat['surat_id']
                                            ,'user_id'      => $user_id
                                            ,'subdis_id'    => $this->data['subdis_id']
                                            ,'surat_view'   => 'Kirim'
                                            ,'sess_security'=> $sess_security)) ;?>

                <input type="submit" name="saveinto" value="&radic; KIRIM" class="btn btn-lg btn-primary" />                                        
            </p>

        <?php endif ;?>

        <?php echo form_close() ;?>

    </div>

</div>

<!--Moddal -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Ubah File</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- End -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.inaksi').change(function(){
            $("body").css("cursor", "wait");

            $.post( "<?php echo site_url('admin/suratin/aksi');?>", {   
                inaksi_id   : $(this).val() ,
                surat_id    : "<?php echo $surat['surat_id'] ;?>" 
            })
            .done(function( data ) {
                setTimeout(function(){ $("body").css("cursor", "default") },3000) ;
            });
        }) ;

        $('.addaksi').change(function(){
            $("body").css("cursor", "wait");

            $.post( "<?php echo site_url('admin/suratin/addaksi');?>", {   
                aksi_id     : $(this).val() ,
                surat_id    : "<?php echo $surat['surat_id'] ;?>" ,
                subdis_id   : "<?php echo $this->data['subdis_id'] ;?>" 
            })
            .done(function( data ) {
                setTimeout(function(){ $("body").css("cursor", "default") },3000) ;
            });
        }) ;

        <?php if ($subdis_child->num_rows() > 0) :
            foreach ($subdis_child->result() as $sb) : ?>
                $("#dis_id<?php echo $sb->subdis_id;?>").click(function(){
                  if ($("#dis_id<?php echo $sb->subdis_id;?>").is(":checked"))
                     $("#respon_subdis_id<?php echo $sb->subdis_id;?>").show();
                  else
                    $("#respon_subdis_id<?php echo $sb->subdis_id;?>").hide();
               });
            <?php endforeach ;?>
        <?php endif ;?>
		
		$(function(){
            $(document).on('click','.ubah_file',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
				$.post('<?php echo base_url('admin/suratin/form_ubah')?>',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
            });
        });
    })
</script>