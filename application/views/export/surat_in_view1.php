<?php
if ($export == 'xls') {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,
            pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    header('Content-Disposition: attachment; filename='.$export_title.'.xls');
    header("Content-Transfer-Encoding: binary ");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title></title>
    <script type="text/php">
         if (isset($pdf)) {
             $font = Font_Metrics::get_font("Helvetica", "bold");
             $pdf->page_text(72, 18, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
         }
    </script>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #000;
            line-height: 150% ;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #000;
            background-color: transparent;
            font-size: 14px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        ul , ol { padding-left: 20px ;}

        table { width:100%; font-size:1em; text-align:left; }
        table th { text-align: center; background: #DFDFDF ; }
        table td { vertical-align: top ; }

        img { margin: 10px ; }
        .alternate { font-weight: bold ; }
		
		@media print{
			.no-print{
				display:none;
			}
		}
    </style>
</head>
	<body>
		<?php if ($export == 'print') :?>
		<div class ="no-print" style="float:right;">
			<a href="javascript:;" onclick="javascript:window.print(); return false;" title="Cetak &raquo;">
				<img src="<?php echo _STYLES_ADMIN_;?>images/print.gif" width="25" alt="" style="text-decoration: none;border: none;" />
			</a>
		</div>
		<?php endif ;?>
		<table cellpadding="5" cellspacing="0" >
			<tr>
				<td>
					<!--<p align="center">Format Lembar Disposisi Pejabat Eselon <?php echo $es_view ;?></p>-->
					<p style="font-size:1em;" align="center"><b>KEMENTERIAN KELAUTAN DAN PERIKANAN<br />DIREKTORAT JENDERAL PENGAWASAN SUMBER DAYA KELAUTAN DAN PERIKANAN</b></p>
					
					<!--<p align="center" ><b>UNIT KERJA ESELON <?php echo $es_view ;?></b></p>-->
				</td>
				
			</tr>
			<tr>
				<td colspan="2"><p align="center" style="font-size:1.3em;"><u>LEMBAR DISPOSISI DIREKTUR JENDRAL</u></p></td>
			</tr>
		</table>
		<?php
			if ($into->num_rows() > 0) {
				foreach ($into->result() as $it) {
					if ($it->subdis_id != $surat['subdis_id']) {
						$to_main[]  = '[V] '.$eselon_dw[$it->subdis_id] ;
					}

					if ($it->subdis_id == $surat['subdis_id'] OR in_array($a->subdis_id, $subdis_eselon1)) {
						$not_id[]   = $it->subdis_id ;
						if (!empty($it->into_msg)) $to_msg[]   = nl2br($it->into_msg) ;

						$disposisi = $this->surat_indisposisi_model->get(array('into_id'=>$it->into_id)) ;
						if ($disposisi->num_rows() > 0) {
							foreach ($disposisi->result() as $a) {
								if (!in_array($a->subdis_id,$not_id) && in_array($a->subdis_id, $subdis_eselon1)) {
									$to_branch[] = '[V] '.$eselon_dw[$a->subdis_id] ;                            
								}
							}
						}                    
					}
				}
			}
		?>
		
		<table cellpadding="5" cellspacing="0" style="border: 1px solid #000; border-collapse: collapse;" border="1">
		  <tr>
			<td width="50%">Dari : <?php echo $surat['surat_from'] ;?></td>
			<td> Diterima Tanggal : <?php echo time_to_words($surat['surat_date_taken']) ;?><br/>
				 Surat Nomor :  <?php echo $surat['surat_number'] ;?><br/>
				 Tanggal Surat :<?php echo time_to_words($surat['surat_date']);?><br/>
				 Lampiran : <br/> <br/><br/>
			</td>
		  </tr>
		  <tr>
			<td>Index :</td>
			<td rowspan="2">Hal :</br><?php echo $surat['surat_perihal'] ;?></td>
		  </tr>
		  <tr>
			<td>Kode&nbsp;&nbsp;: </td>
		  </tr>
		</table>
		<table cellpadding="5" cellspacing="0" style="border: 1px solid #000; border-collapse: collapse;" border="1">
			<tr>
				<td width="35%">
				Diteruskan kepada Yth :
					<ul style="margin:0;padding-left:15px;list-style:none;">
						<li></li></ul>
						<?php $q2 =$this->db->get_where('disposisi__sublist',array('subdis_parent'=>'2')); ?>
						
						<?php 
						$this->db->where('subdis_parent', 1);
						$this->db->order_by('subdis_kode', "asc"); 
						$this->db->limit(6);
						$q1 =$this->db->get('disposisi__sublist'); ?>
						<ul style="margin:0;padding-left:15px;list-style:none;">
							<?php
						
							foreach ($q1->result() as $value) { ?>
								<li><?php	echo '[&nbsp;&nbsp;] ' .$value->subdis_name; ?></li>
							<?php }?>
							<?php foreach ($q2->result() as $value) { ?>
								<li><?php	echo '[&nbsp;&nbsp;] Kepala ' .$value->subdis_name; ?></li>
							<?php } ?>
							<li>[&nbsp;&nbsp;] ................................................</li>
						</ul>
						</br>
				</td>
				<td>
					Aksi :
					<ul style="margin:0;padding-left:15px;list-style:none;">
					<?php foreach ($aksi->result() as $a) :?>
						<li><?php echo '[&nbsp;&nbsp;]'  .' '.$a->aksi_name ;?></li>
					<?php endforeach ;?>
					</ul>
				</td>
			</tr>
		</table>
		<table cellpadding="5" cellspacing="0" style="border: 1px solid #000; border-collapse: collapse;" border="1">
			<tr>
				<td width="35%">Oleh Sekertaris Pribadi</td>
				<td width="10%" align="center">Tanggal</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;Paraf &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Catatan Penyelesaian</td>
			</tr>
			<tr>
				<td width="35%">Diterima</td>
				<td width="10%"></td>
				<td></td>
			</tr>
			<tr>
				<td width="35%">Diteruskan</td>
				<td width="10%"></td>
				<td></td>
			</tr>
			<tr>
				<td width="35%">Diterima Kembali</td>
				<td width="10%"></td>
				<td></td>
			</tr>
			<tr>
				<td width="35%">Oleh Unit Kerja Ybs</td>
				<td width="10%" align="center">Tanggal</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;Paraf</td>
			</tr>
			<tr>
				<td width="35%">Diterima</td>
				<td width="10%"></td>
				<td></td>
			</tr>
			<tr>
				<td width="35%">Diselesaikan</td>
				<td width="10%"></td>
				<td></td>
			</tr>
		</table>
	</body>
</html>