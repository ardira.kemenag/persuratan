<html>
<head>
    <title><?php echo $post['post_name'] ;?></title>
    <script type="text/php">
         if (isset($pdf)) {
             $font = Font_Metrics::get_font("Helvetica", "bold");
             $pdf->page_text(72, 18, "{PAGE_NUM} of {PAGE_COUNT}", $font, 8, array(0,0,0));
         }
    </script>

    <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 12px;
            color: #000;
            line-height: 150% ;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #000;
            background-color: transparent;
            font-size: 14px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        ul , ol { padding-left: 20px ;}

        table { width:100%; font-size:0.9em; text-align:left; }
        table th { text-align: center; }

        img { margin: 10px ; }
    </style>
</head>

<body>

    <?php if ($type == 'print') :?>
    <div style="float:right;">
        <a href="javascript:;" onclick="javascript:window.print(); return false;" title="<?php echo $this->lang->line('post_page_print') ;?>">
            <img src="<?php echo _STYLES_ADMIN_;?>images/print.gif" width="25" alt="" style="text-decoration: none;border: none;" />
        </a>
    </div>
    <?php endif ;?>

    <h1>
        <sub><?php echo sprintf($this->lang->line('post_page_date'),$post['date_diff'],$post['time_diff']) ;?> <?php if (!empty($post['category_name'])) :?>- <?php echo sprintf($this->lang->line('post_page_category'),url_category ($post['category_name'])) ;?> <?php endif;?></sub>
        <br />
        <?php echo $post['post_name'] ;?>
    </h1>

    <?php if (!empty($post['post_img']) && file_exists(_PATH_UPLOAD_IMG_MEDIUM_ . $post['post_img'])) :?>
    <img src="<?php echo ($type == 'print' ? _URL_UPLOAD_IMG_MEDIUM_ : _PATH_UPLOAD_IMG_MEDIUM_) . $post['post_img'] ;?>" style="margin: 5px 10px 3px 0; float: left;" align="left" alt="" />
    <?php endif ;?>

    <?php echo text_clean($post['post_content'], ($type == 'print' ? FALSE : TRUE)) ;?>
    
</body>
</html>