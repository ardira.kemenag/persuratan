<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
 
  <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->lang->line('login_title') ;?> :: Surat DPRD</title>

        <!-- start: CSS -->
        <?php echo implode("\n",$css) ;?>
  <!-- plugins:css -->
  
</head>
 <!-- <link href="<?php echo _STYLES_ICON_;?>favicon.ico" rel="shortcut icon" /> -->
 <link rel="shortcut icon" href="http://arthawisesa.com/surat-dprd/public/styles/admin/images/logo.png">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    html {
 /* background-color: #56baed;*/
}

body {
  font-family: "Poppins", sans-serif;
  height: 100vh;
  background:url(<?php echo base_url();?>public/styles/login/page-banner.jpg) center fixed no-repeat;
  background-size: 100%;

}

a {
  color: #92badd;
  display:inline-block;
  text-decoration: none;
  font-weight: 400;
}

h2 {
  text-align: center;
  font-size: 16px;
  font-weight: 600;
  text-transform: uppercase;
  display:inline-block;
  margin: 40px 8px 10px 8px; 
  color: #cccccc;
}



/* STRUCTURE */

.wrapper {
  display: flex;
  align-items: center;
  flex-direction: column; 
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
}

#formContent {
  -webkit-border-radius: 10px 10px 10px 10px;
  border-radius: 10px 10px 10px 10px;
  background: #ac9c329e;
  padding: 30px;
  width: 90%;
  max-width: 450px;
  position: relative;
  padding: 0px;
  -webkit-box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
  text-align: center;
}

#formFooter {
  /*background-color: #f6f6f6;*/
  border-top: 1px solid #aac8de;
  padding: 15px;
  text-align: center;
  -webkit-border-radius: 0 0 10px 10px;
  border-radius: 0 0 10px 10px;

}



/* TABS */

h2.inactive {
  color: #cccccc;
}

h2.active {
  color: #0d0d0d;
  border-bottom: 2px solid #5fbae9;
}



/* FORM TYPOGRAPHY*/

input [type=button], input[type=submit], input[type=reset]  {
  background-color: #56baed;
  border: none;
  color: white;
  padding: 15px 80px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  text-transform: uppercase;
  font-size: 13px;
  -webkit-box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
  margin: 5px 20px 40px 20px;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out;
}

input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover  {
  background-color: #39ace7;
}

input[type=button]:active, input[type=submit]:active, input[type=reset]:active  {
  -moz-transform: scale(0.95);
  -webkit-transform: scale(0.95);
  -o-transform: scale(0.95);
  -ms-transform: scale(0.95);
  transform: scale(0.95);
}

input[type=text] {
  background-color: #f6f6f6;
  border: none;
  color: #0d0d0d;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 5px;
  width: 85%;
  border: 2px solid #f6f6f6;
  -webkit-transition: all 0.5s ease-in-out;
  -moz-transition: all 0.5s ease-in-out;
  -ms-transition: all 0.5s ease-in-out;
  -o-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
}
input[type=password] {
  background-color: #f6f6f6;
  border: none;
  color: #0d0d0d;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 5px;
  width: 85%;
  border: 2px solid #f6f6f6;
  -webkit-transition: all 0.5s ease-in-out;
  -moz-transition: all 0.5s ease-in-out;
  -ms-transition: all 0.5s ease-in-out;
  -o-transition: all 0.5s ease-in-out;
  transition: all 0.5s ease-in-out;
  -webkit-border-radius: 5px 5px 5px 5px;
  border-radius: 5px 5px 5px 5px;
}



input[type=text]:focus {
  background-color: #fff;
  border-bottom: 2px solid #5fbae9;
}

input[type=text]:placeholder {
  color: #cccccc;
}



/* ANIMATIONS */

/* Simple CSS3 Fade-in-down Animation */
.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

@keyframes fadeInDown {
  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
  100% {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

/* Simple CSS3 Fade-in Animation */
@-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
@keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

.fadeIn {
  opacity:0;
  -webkit-animation:fadeIn ease-in 1;
  -moz-animation:fadeIn ease-in 1;
  animation:fadeIn ease-in 1;

  -webkit-animation-fill-mode:forwards;
  -moz-animation-fill-mode:forwards;
  animation-fill-mode:forwards;

  -webkit-animation-duration:1s;
  -moz-animation-duration:1s;
  animation-duration:1s;
}

.fadeIn.first {
  -webkit-animation-delay: 0.4s;
  -moz-animation-delay: 0.4s;
  animation-delay: 0.4s;
}

.fadeIn.second {
  -webkit-animation-delay: 0.6s;
  -moz-animation-delay: 0.6s;
  animation-delay: 0.6s;
}

.fadeIn.third {
  -webkit-animation-delay: 0.8s;
  -moz-animation-delay: 0.8s;
  animation-delay: 0.8s;
}

.fadeIn.fourth {
  -webkit-animation-delay: 1s;
  -moz-animation-delay: 1s;
  animation-delay: 1s;
}

/* Simple CSS3 Fade-in Animation */
.underlineHover:after {
  display: block;
  left: 0;
  bottom: -10px;
  width: 0;
  height: 2px;
  background-color: #56baed;
  content: "";
  transition: width 0.2s;
}

.underlineHover:hover {
  color: #0d0d0d;
}

.underlineHover:hover:after{
  width: 100%;
}

h1{
    color:#60a0ff;
}

/* OTHERS */

*:focus {
    outline: none;
} 

#icon {
  margin-bottom: 15px;
    /* width: 30%; */
    height: 100px;
    margin-top: 30px;
    margin-left: 5px;
    font-size: : 25pt;
}
.overlay{
	z-index: 0;
    position: fixed;
    bottom: 0;
    top: 0;
    left: 0;
    right: 0;
    background: rgba(233, 234, 239, 0.9);
}
@media screen and (max-width: 600px) {
  #icon {
  margin-bottom: 15px;
    /* width: 30%; */
    height: 100px;
    margin-top: 30px;
    margin-left: 5px;
    font-size: : 25pt;
}
}

</style>
<link rel="shortcut icon" href="http://arthawisesa.com/dolpin1/public/styles/admin/images/logo.png">
        
        <script>
            // <![CDATA[
                var site_url    = "<?php echo site_url() ;?>" ;
                var base_url    = "<?php echo base_url() ;?>" ;
                var js_url      = "<?php echo _JAVASCRIPT_ ;?>" ;
                var css_url     = "<?php echo _STYLES_LOGIN_ ;?>" ;
            //]]>
        </script>
<div class=" overlay">
<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
        <img src="<?php echo base_url();?>public/styles/login/logo_esurat_luar.png" id="icon" alt="User Icon" />
      <h1 style="color: #013398">Login</h1>
    </div>

                <p id="warning" class="<?php echo !empty($symbol) ? $symbol : '';?>" style="padding: 10px 0;">
                    <?php if (!empty($note)) :?>
                        <?php echo $note;?>
                    <?php endif ;?>
                </p>
    <!-- Login Form -->
    <form method="post" action="<?php echo site_url('login');?>">
                  <div class="form-group">
                    <div class="input">
                        <input type="text" name="username"  size="12" class="fadeIn second" placeholder="Username ..." required/>
                    </div>

                    <div class="input">
                        <input type="password" class="fadeIn third" name="password" size="12" class="form-control" placeholder="Password ..." required/>
                    </div>
                    <div class="submit">
                        <?php echo form_hidden('sess_security', $sess_security) ;?>
                        <input type="submit" class="fadeIn fourth" name="login_post" value="LOGIN" class="btn btn-lg btn-warning" />
                    </div>
                  
                  
               
              </form>
    <!--<form>-->
    <!--  <input type="text" id="login" class="fadeIn second" name="login" placeholder="username">-->
    <!--  <input type="text" id="password" class="fadeIn third" name="login" placeholder="password">-->
    <!--  <input type="submit" class="fadeIn fourth" value="Log In">-->
    <!--</form>-->

    <!-- Remind Passowrd -->
   

  </div>
</div>


 <?php print implode("\n",$js_bottom) ;?>
</html>
