<script type="text/javascript">
    $(function () {
        $('#<?php echo $id ;?>').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: '<?php echo $show_title ? $title : '' ;?>'
            },
            <?php if (!empty($subtitle)) :?>
            subtitle: {
                text: '<?php echo $subtitle ;?>',
                x: -20
            },
            <?php endif ;?>
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                name: '<?php echo $series_name ;?>',
                data: [<?php echo $series_data ;?>]
            }]
        });
    }) ;
</script>

<div id="<?php echo $id ;?>" style="width: <?php echo $width ;?> ;height: <?php echo $height ;?>;<?php echo $style;?>"></div>