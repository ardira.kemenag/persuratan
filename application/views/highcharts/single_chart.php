<script type="text/javascript">
    $(function () {
        $('#<?php echo $id ;?>').highcharts({
            chart: {
                type: '<?php echo $type ;?>',
                margin: [ 50, 50, 100, 80] ,
                backgroundColor:'#ffffff' 
            },
            plotOptions: {
                column: {
                    colorByPoint: true
                }
                <?php if (!empty($link)) :?>
                ,
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                location.href = '<?php echo $link_uri ;?>' + this.category ;  ;
                            }
                        }
                    }
                }
                <?php endif ;?>
            },        
            colors : ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'] ,        
            title: {
                text: '<?php echo $show_title ? $title : '' ;?>'
            },
            <?php if (!empty($subtitle)) :?>
            subtitle: {
                text: '<?php echo $subtitle ;?>',
                x: 0,
                y:33
            },
            <?php endif ;?>
            
            xAxis: {
                categories: [
                   <?php echo $categories ;?>
                ],
                title: {
                    text: '<?php echo $xAxisName ;?>' ,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif' ,
                        color : '#fff'
                    }
                },
                labels: {
                    align: 'center',
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif' ,
                        color : '#333'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $yAxisName ;?>' ,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif' ,
                        color : '#333'
                    }
                },
                labels: {
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif' ,
                        color : '#333'
                    }
                }        
                
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                name: '<?php echo $series_name ;?>',
                data: [<?php echo $series_data ;?>]
            }]
        });
    }) ;
</script>

<div id="<?php echo $id ;?>" style="width: <?php echo $width ;?>; height: <?php echo $height ;?>;<?php echo $style;?>"></div>