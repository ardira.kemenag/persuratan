<script type="text/javascript">
    $(function () {
        $('#<?php echo $id ;?>').highcharts({
            chart: {
                type: '<?php echo $type ;?>'
            },
            title: {
                text: '<?php echo $show_title ? $title : '' ;?>'
            },
            <?php if (!empty($subtitle)) :?>
            subtitle: {
                text: '<?php echo $subtitle ;?>',
                x: -20
            },
            <?php endif ;?>
            xAxis: {
                categories: [<?php echo $categories ;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $yAxisName;?>'
                }
            },
            tooltip: {
                valueSuffix: '<?php echo $valueSuffix ;?>'
            },
            plotOptions: {
                <?php if (!empty($link_uri)) :?>
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                $.prettyPhoto.open("<?php echo $link_uri ;?>") ;
                            }
                        }
                    }
                },
                <?php endif ;?>
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            <?php if (!empty($credits)) :?>
            credits: {
                text: '<?php echo $credits ;?>',
                position: {
                    align: 'left',
                    x:20
                },
                style: {
                    fontSize: '8.5pt' // you can style it!
                }
            },
            <?php endif ;?>
            series: [<?php echo $series_data ;?>]
        });
    }) ;
</script>

<div id="<?php echo $id ;?>" style="width: <?php echo $width ;?>; height: <?php echo $height ;?>;<?php echo $style;?>"></div>