<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Dokumen
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Dokumen extends BI_Admin {
    
    public function __construct() {
        parent::__construct() ;
        
        $this->load->model(array(   'folder_file_model'     , 'folder_model'    ,
                                    'eselon_satu_model'
                                    )) ;
        
        $this->data['adminmenu']    = $this->lang->line('general_dokumen') ;
    }
    
    public function index() {
        return $this->folder() ;
    }
    
    public function folder($folder_id = NULL, $tbl = NULL) {
        $data	= $this->data ;

        $data['admin_submenu']    = $this->lang->line('general_dokumen_folder') ;

        $data['page']    = $page   =  0 ;
        $data['limit']   = $limit  = 20 ;

        if ($folder_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $folder_id    = NULL ;
        }
        elseif ($folder_id && $tbl && $folder_id != 'page') {
            $a_id       = $folder_id ;
            $d  = $tbl == $this->folder_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu folder berhasil dilakukan' ;
                $data['symbol'] = 'updated' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi' ;
                $data['symbol'] = 'error' ;
            }

            $folder_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->folder_model->save_group() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan folder berhasil dilakukan.' ;
                $data['symbol'] = 'updated' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses pembaharuan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'error' ;
            }
        }
        
        //$postdata['tahun']  = date('Y') ;
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('folder_parent') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','dokumen','folder','page')) ;
        $config['total_rows']   = $data['num'] = $this->folder_model->num() ;
        $config['per_page']     = $postdata['limit']= $limit ;
        $config['cur_page']     = $postdata['page'] = $page ;
        $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

        $this->pagination->initialize($config);

        $data['paging']     = $this->pagination->create_links();
        $data['page']       = $page ;

        $postdata['esatu_id']= $this->data['esatu_id'] ;
        
        $postdata['dokumen_num']    = TRUE ;
        $postdata['file_num']       = TRUE ;
        $data['folder']   = $this->folder_model->get($postdata) ;

        $this->load->helper('array') ;
        $data['folder_dw']  = $this->folder_model->show_tree(array('make_array'=>TRUE,'esatu_id'=>$this->data['esatu_id'])) ;
        $data['esatu_dw']   = $this->eselon_satu_model->get(array('array'=>TRUE)) ; 
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'dokumen_folder') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function files($file_id = NULL) {
        $data   = $this->data ;
        
        $data['admin_submenu']    = $this->lang->line('general_dokumen_files') ;
        
        $this->load->helper('array') ;
        $data['tahun_dw'] = data_year() ;
        $data['folder_dw']  = $this->folder_model->show_tree(array('make_array'=>TRUE,'esatu_id'=>$this->data['esatu_id'])) ;
        
        $folder_id  = $this->input->get('folder_id',TRUE) ;
        $tahun      = $this->input->get('tahun',TRUE) ;
        if (!empty($folder_id)) {
            $postdata['folder_id']  = $folder_id ;
            $postdata['tahun']      = $tahun ;
            $postdata['parent']     = TRUE ;
            
            $data['folder'] = $folder  = $this->folder_model->get($postdata) ;
            
            if ($folder) {
                $data['uri_get']    = '?folder_id='.urlencode($folder_id) ;
                
                if (!empty($file_id)) {
                    if ($this->folder_file_model->delete($file_id)) {
                        $data['note']   = 'Penghapusan salah satu dokumen di folder berhasil dilakukan' ;
                        $data['symbol'] = 'updated' ;

                        $this->db->cache_delete_all() ;
                    }
                    else {
                        $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi' ;
                        $data['symbol'] = 'error' ;
                    }

                    $file_id    = NULL ;
                }
                
                $post   = $rs = FALSE ;
                if (isset($_POST['savepost'])) {
                    $sess_security  = $this->input->post('sess_security');
                    if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                        $rs     = $this->folder_file_model->save($folder_id) ;
                    }

                    $post   = TRUE ;
                }

                if ($post) {
                    if ($rs) {
                        $data['note']   = 'Pembaharuan dokumen di folder '.$folder['folder_name'].' berhasil dilakukan.' ;
                        $data['symbol'] = 'updated' ;

                        $this->db->cache_delete_all() ;
                    }
                    else {
                        $data['note']   = 'Proses pembaharuan gagal. Coba lakukan sekali lagi.' ;
                        $data['symbol'] = 'error' ;
                    }
                }
                
                $data['files']  = $this->folder_file_model->get($postdata) ;
            }
            
            $data['postdata']   = $postdata ;
        }
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'dokumen_files') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function sitemap() {
        $data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_dokumen_sitemap') ;
        
        $data['folders'] = $this->folder_model->sitemap() ;
        
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'dokumen_sitemap') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
    
    public function download($type,$id) {
        if (!empty($type) && !empty($id)) {
            if ($type == 'folder') {
                $file   = $this->folder_file_model->get(array('file_id'=>$id)) ;
                $this->folder_file_model->hint($id,'download') ;
            }
            elseif ($type == 'notulen') {
                $file   = $this->notulensi_file_model->get(array('file_id'=>$id)) ;
                $this->notulensi_file_model->hint($id,'download') ;
            }
            
            if ($file && !empty($file['file_name']) && file_exists(_PATH_UPLOAD_FILES_ . $file['file_name'])) {
                $this->load->helper('download');
                
                $data = file_get_contents( _PATH_UPLOAD_FILES_ . $file['file_name']); // Read the file's contents
                $name = !empty($file['file_title']) ? $file['file_title'].'.'.$file['file_ext'] : $file['file_name'] ;

                force_download($name, $data);
            }
            else {
                redirect(current_url(), 'refresh') ;
            }
        }
        else {
            redirect(current_url(), 'refresh') ;
        }
    }
    
    public function read($id) {
        $data   = $this->data ;
        
        $data['admin_submenu']    = $this->lang->line('general_dokumen_files') ;
        
        $file   = $this->folder_file_model->get(array(  'file_id'   => $id,
                                                        'folder'    => TRUE)) ;
        $this->folder_file_model->hint($id,'read') ;
        
        if ($file && !empty($file['file_name']) && file_exists(_PATH_UPLOAD_FILES_ . $file['file_name'])) {
            $data['google_read']    = '<iframe src="http://docs.google.com/gview?url='. _URL_UPLOAD_FILES_ . $file['file_name'].'&embedded=true" style="width:100%; height:600px;" frameborder="0"></iframe>' ;
            
            $data['file']   = $file ;
            if (!empty($file['folder_parent']))
                $data['parent'] = $this->folder_model->get(array('folder_id'=>$file['folder_parent'])) ;
            
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'dokumen_read') ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
        else {
            redirect(current_url(), 'refresh') ;
        }
    }
}
/* End of file dokumen.php */
/* Location: ./application/controllers/admin/dokumen.php */