<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->data['title']        = $this->lang->line('general_entitas') ;
        $this->data['adminmenu']    = $this->lang->line('general_surat') ;

        if (!in_array($this->data['level_id'], array(1))) {
            redirect('admin/home','refresh') ;
        }
    }

    public function jenis($jenis_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_jenis') ;
        
        $this->load->model('entitas_jenis_model') ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($jenis_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $jenis_id    = NULL ;
        }
        elseif ($jenis_id && $tbl && $jenis_id != 'page') {
            $a_id       = $jenis_id ;
            $d  = $tbl == $this->entitas_jenis_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu jenis surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $jenis_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->entitas_jenis_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan jenis surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','jenis','page')) ;
        $config['total_rows']   = $data['num'] = $this->entitas_jenis_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['jenis']  = $this->entitas_jenis_model->get($postdata) ;
        
        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_jenis') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
    
    public function sifat($sifat_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_sifat') ;
        
        $this->load->model('entitas_sifat_model') ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($sifat_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $sifat_id    = NULL ;
        }
        elseif ($sifat_id && $tbl && $sifat_id != 'page') {
            $a_id       = $sifat_id ;
            $d  = $tbl == $this->entitas_sifat_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu sifat surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $sifat_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->entitas_sifat_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan sifat surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','sifat','page')) ;
        $config['total_rows']   = $data['num'] = $this->entitas_sifat_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['sifat']  = $this->entitas_sifat_model->get($postdata) ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_sifat') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
    
    public function aksi($aksi_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_aksi') ;
        
        $this->load->model('entitas_aksi_model') ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($aksi_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $aksi_id    = NULL ;
        }
        elseif ($aksi_id && $tbl && $aksi_id != 'page') {
            $a_id       = $aksi_id ;
            $d  = $tbl == $this->entitas_aksi_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu aksi/instruksi surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $aksi_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->entitas_aksi_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan aksi surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','aksi','page')) ;
        $config['total_rows']   = $data['num'] = $this->entitas_aksi_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['aksi']   = $this->entitas_aksi_model->get($postdata) ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_aksi') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
    
    public function disposisi($subdis_id = NULL, $tbl = NULL) {
        $this->load->model('disposisi_sub_model') ;
        
        
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_disposisi') ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($subdis_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $subdis_id   = NULL ;
        }
        elseif ($subdis_id && $tbl && $subdis_id != 'page') {
            $a_id       = $subdis_id ;
            $d  = $tbl == $this->disposisi_sub_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu bagian disposisi berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $subdis_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->disposisi_sub_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan bagian disposisi berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','disposisi','page')) ;
        $config['total_rows']   = $data['num'] = $this->disposisi_sub_model->num() ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $this->load->model('disposisi_model') ;
        $data['disposisi_dw']   = $this->disposisi_model->get(array('array'=>TRUE)) ;
        $data['parent_dw']      = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        
        $data['subdis'] = $this->disposisi_sub_model->get($postdata) ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_disposisi') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }


    public function klasifikasi($klasifikasi_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_klasifikasi') ;
        
        $this->load->model(array('entitas_klasifikasi_model')) ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($klasifikasi_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $klasifikasi_id    = NULL ;
        }
        elseif ($klasifikasi_id && $tbl && $klasifikasi_id != 'page') {
            $a_id       = $klasifikasi_id ;
            $d  = $tbl == $this->entitas_klasifikasi_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu klasifikasi surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $klasifikasi_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->entitas_klasifikasi_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan klasifikasi surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','klasifikasi','page')) ;
        $config['total_rows']   = $data['num'] = $this->entitas_klasifikasi_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['klasifikasi']  = $this->entitas_klasifikasi_model->get($postdata) ;

        $data['klasifikasi_dw']  = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE,'normal'=>TRUE)) ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_klasifikasi') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function tujuan($tujuan_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        $data['admin_submenu']    = $this->lang->line('general_entitas_tujuan') ;
        
        $this->load->model(array('entitas_tujuan_model')) ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($tujuan_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $tujuan_id    = NULL ;
        }
        elseif ($tujuan_id && $tbl && $tujuan_id != 'page') {
            $a_id       = $tujuan_id ;
            $d  = $tbl == $this->entitas_tujuan_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu tujuan surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $tujuan_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->entitas_tujuan_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan tujuan surat berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['page']   = $page ;
        $postdata['limit']  = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','entitas','tujuan','page')) ;
        $config['total_rows']   = $data['num'] = $this->entitas_tujuan_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['tujuan']  = $this->entitas_tujuan_model->get($postdata) ;

        $data['tujuan_dw']  = $this->entitas_tujuan_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'entitas_tujuan') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
}
/* End of file Entitas.php */
/* Location: ./application/controllers/admin/Entitas.php */