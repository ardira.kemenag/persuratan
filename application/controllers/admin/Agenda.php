<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Agenda
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Agenda extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model(array('agenda_model','agenda_status_model')) ;
        
        $this->data['adminmenu']    = $this->lang->line('general_agenda') ;

        if (!in_array($this->data['level_id'], array(1,3,4,9))) {
            redirect('admin/home','refresh') ;
        }
    }
    
    public function index() {
        return $this->kegiatan() ;
    }
    
    public function kegiatan($kegiatan_id = NULL, $del = NULL) {
        $data   = $this->data ;

        $data['admin_submenu']  = $this->lang->line('general_agenda_kegiatan') ;
        
        $postdata = array() ;
        $postdata['order']  = 'K.kegiatan_id DESC' ;
        
        $new    = FALSE ;
        $page   = 0 ;
        $limit  = 10 ;

        if ($kegiatan_id == 'page') {
            $page           = is_numeric($del) ? $del : 0 ;
            $kegiatan_id    = NULL ;
        }
        elseif ($kegiatan_id && $del && $kegiatan_id != 'page') {
            $h_id       = $kegiatan_id ;
            $kegiatan_id = NULL ;

            if ($this->agenda_model->delete($h_id)) {
                $data['note']   = "Penghapusan salah satu kegiatan berhasil dilakukan." ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = "Proses penghapusan gagal. Coba lakukan sekali lagi!" ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        else if ($kegiatan_id && $kegiatan_id == 'new' && !is_numeric($kegiatan_id)) {
            $new   = TRUE ;
        }

        $post   = FALSE ;
        $rs     = FALSE ;

        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                if ($this->agenda_model->validation() == TRUE) {
                    $rs     = $this->agenda_model->save() ;
                }
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = "Pembaharuan data kegiatan berhasil dilakukan!" ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = "Proses gagal. Coba lakukan sekali lagi!" ;
                $data['symbol'] = 'alert alert-danger' ;

                if (!empty($_POST['kegiatan_id'])) {
                    $kegiatan_id   = $this->input->post('kegiatan_id',TRUE) ;
                }
                else {
                    $new       = TRUE ;
                }
            }
        }

        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('order','sdate','kdate','status_id','keyword') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        /* ==================== */

        if (!$kegiatan_id && !$new) {
            $this->load->library('pagination') ;
            $config['base_url']     = site_url(array('admin','agenda','kegiatan','page')) ;
            $config['total_rows']   = $this->agenda_model->num($postdata) ;
            $config['per_page']     = $limit ;
            $config['cur_page']     = $page ;

            $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

            $this->pagination->initialize($config);

            $data['paging']     = $this->pagination->create_links();
            $data['page']       = $page ;
        }
        
        $data['status_dw']  = $this->agenda_status_model->get(array('array'=>TRUE)) ;

        $data['kegiatan']   = NULL ;

        if (! $new) {
            if ($kegiatan_id) {
                $postdata['kegiatan_id']= $kegiatan_id ;
                $postdata['user']       = TRUE ;
            }
            else {
                $postdata['limit']  = $limit ;
                $postdata['page']   = $page ;
                $postdata['user']   = TRUE ;
                $postdata['status'] = TRUE ;
            }

            $data['kegiatan']   = $this->agenda_model->get($postdata) ;
        }

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker3.js' )) ;

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        if ($kegiatan_id || $new)   $this->load->view( _TEMPLATE_ADMIN_ . 'agenda_kegiatan_form') ;
        else                        $this->load->view( _TEMPLATE_ADMIN_ . 'agenda_kegiatan') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function status($status_id = NULL, $tbl = NULL) {
        $data   = $this->data ;
        
        $data['admin_submenu']    = $this->lang->line('general_agenda_status') ;
        
        $data['page']           = $page   =  0 ;
        $data['limit']          = $limit  = 10 ;

        if ($status_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $status_id    = NULL ;
        }
        elseif ($status_id && $tbl && $status_id != 'page') {
            $a_id       = $status_id ;
            $d  = $tbl == $this->agenda_status_model->delete($a_id) ;
            if ($d) {
                $data['note']   = 'Penghapusan salah satu status kegiatan berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses penghapusan gagal. Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }

            $status_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs     = $this->agenda_status_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = 'Pembaharuan status kegiatan berhasil dilakukan.' ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $postdata['kegiatan_num']   = TRUE ;
        $postdata['page']           = $page ;
        $postdata['limit']          = $limit ;

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','agenda','status','page')) ;
        $config['total_rows']   = $data['num'] = $this->agenda_status_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $this->pagination->initialize($config);

        $data['paging'] = $this->pagination->create_links();
        $data['page']   = $page ;

        $data['status']  = $this->agenda_status_model->get($postdata) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'agenda_status') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
    
    public function calendar() {
        $data   = $this->data ;
        
        $data['admin_submenu'] = $this->lang->line('general_agenda_calendar') ;
        
        $postdata['order']  = 'K.kegiatan_date_start' ;
        
        $data['kegiatan']   = $kegiatan = $this->agenda_model->get($postdata) ;
        
        if ($kegiatan->num_rows() > 0) {
            $i = 0 ;
            foreach ($kegiatan->result() as $k) {
				$cek_level = $this->db->get_where('surat__in', array('surat_id' => $k->surat_id)); 
			foreach($cek_level->result() as $lv){
				$level=$lv->surat_to;
								
			$array = json_decode($level);
			$this->db->where_in('subdis_id', $array);
			
			$query  = $this->db->get('disposisi__sublist') ;
			
			foreach($query->result() as $m){
				$to= $m->subdis_name .' - ';
				
                $event[$i]['title'] = $to.' '.$k->kegiatan_name ;
			}}
                $event[$i]['start'] = $k->kegiatan_date_start ;
                
                if (empty($k->kegiatan_date_end) OR $k->kegiatan_date_end == '0000-00-00' OR $k->kegiatan_date_end == $k->kegiatan_date_start) {
                    
                }
                else {
                    $event[$i]['end'] = $k->kegiatan_date_end ;
                }
                
                $event[$i]['url']   = site_url('admin/agenda/kegiatan/'.$k->kegiatan_id) ;
                
                $i++ ;
            }
        } 
        
        $json = json_encode($event) ;
        
        $json   = strtr($json, array('"title"'  => 'title'  , '"start"'=>'start',
                                     '"end"'    => 'end'    , '"url"'  => "url" ,
                                     '\/'       => '/'      , '"'      => "'")) ;
        
        $data['calendar']   = TRUE ;
        $data['event_json'] = $json ;
        
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'agenda_calendar') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
}
/* End of file agenda.php */
/* Location: ./application/controllers/admin/agenda.php */