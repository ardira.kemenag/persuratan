<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Home
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Home extends BI_Admin {

    public function __construct() {
        parent::__construct();

        $this->data['title']        = $this->lang->line('general_home') ;
        $this->data['adminmenu']    = $this->lang->line('general_home') ;

        $this->load->model('users_model') ;
    }

    public function index() {
        $data   = $this->data ;

        $data['user']    = $this->users_model->get(array('user_id'=>$data['user_id'],'level'=>TRUE)) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'home') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */