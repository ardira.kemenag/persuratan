<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Download
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Download extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model(array('surat_in_model','surat_indocument_model')) ;
    }
    
    public function c($document_id, $document_name = NULL) {
        if (!empty($document_id)) {
            $document  = $this->surat_indocument_model->get(array('document_id'=>$document_id)) ;
            if ($document) {
                if (!empty($document['document_filename']) && file_exists( _PATH_UPLOAD_FILES_ . $document['document_filename'])) {
                    $this->load->helper('download');

                    $data = file_get_contents( _PATH_UPLOAD_FILES_ . $document['document_filename']); // Read the file's contents
                    $name = $document['document_filename'];

                    force_download($name, $data);
                }
                else {
                    redirect('admin/suratin/inbox','refresh') ;
                }
            }
            else {
                redirect('admin/suratin/inbox','refresh') ;
            }
        }
        else {
            redirect('admin/suratin/inbox','refresh') ;
        }
    }

    public function out($document_id, $document_name = NULL) {
        if (!empty($document_id)) {
            $document  = $this->surat_outdocument_model->get(array('document_id'=>$document_id)) ;
            if ($document) {
                if (!empty($document['document_filename']) && file_exists( _PATH_UPLOAD_FILES_ . $document['document_filename'])) {
                    $this->load->helper('download');

                    $data = file_get_contents( _PATH_UPLOAD_FILES_ . $document['document_filename']); // Read the file's contents
                    $name = $document['document_filename'];

                    force_download($name, $data);
                }
                else {
                    redirect('admin/suratout/inbox','refresh') ;
                }
            }
            else {
                redirect('admin/suratout/inbox','refresh') ;
            }
        }
        else {
            redirect('admin/suratout/inbox','refresh') ;
        }
    }
    
    
}
/* End of file Download.php */
/* Location: ./application/controllers/admin/Download.php */