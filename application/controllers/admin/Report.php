<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Report
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Report extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->data['title']    = 'Pelaporan' ;
        $this->data['adminmenu']= 'Pelaporan' ;
        
        $this->load->model(array(   'surat_in_model'           
                                ,   'entitas_jenis_model'
                                ,   'disposisi_sub_model'   
                                ,   'surat_indocument_model'
                                ,   'surat_inaksi_model'
                                ,   'surat_outdocument_model'
                                ,   'surat_out_model'
                                ,   'entitas_model'
                                ,   'entitas_klasifikasi_model'
                                ,   'entitas_tujuan_model'
                                ,   'entitas_sifat_model' 
                                ,   'users_model' )) ;

        if (!in_array($this->data['level_id'], array(1,3)) && !in_array($this->data['subdis_eselon'],array(2))) {
            redirect('admin/home','refresh') ;
        }
    }
    
    public function index() {
        $this->rekapin() ;
    }
    
    public function statin_eselon($format_id = 2,$page = 0) {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statin') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $limit  = 20 ;

        $postdata['format_id']  = $data['format_id'] = $format_id ;
        $postdata['hubdis']     = TRUE ;
        $postdata['disposisi']  = $this->disposisi_sub_model->get(array('order'=>'subdis_order')) ;
        $postdata['order']      = 'T.jenis_name ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $data['disposisi']= array(  1 => 'Eselon I'
                                ,   2 => 'Eselon II'
                                ,   3 => 'Eselon III'
                                ,   4 => 'Eselon IV') ;

        $postdata['compiled']   = FALSE ;
        $data['jenis']    = $this->entitas_jenis_model->get($postdata) ;

        $this->load->helper('highcharts') ;
        $data['charts']  = multi_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['jenis']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK '.strtoupper($data['admin_submenu']) ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($postdata['start_date']) && !empty($postdata['to_date']) ? ' Periode '.time_to_words(str_replace('/','-',$postdata['start_date'])).' s.d '.time_to_words(str_replace('/','-',$postdata['to_date'])) : NULL ,
                                                'label_x'   => 'jenis_name' ,
                                                'legend'    => array('Eselon I','Eselon II','Eselon III','Eselon IV'),
                                                'label_y'   => array('disposisi1','disposisi2','disposisi3','disposisi4'),
                                                'xAxisName' => NULL ,
                                                'yAxisName' => NULL ,
                                                'series_name'=> NULL ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statin_eselon' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statin_year() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statin') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']    = 'YEAR(S.surat_date) AS tahun, COUNT(S.surat_id) AS surat_num' ;
        $postdata['group']  = 'YEAR(S.surat_date)' ;
        $postdata['order']  = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['tahun_start']    = date('Y')-1 ;
        $postdata['tahun_end']      = date('Y') ;
        $postdata['format_id']      = 2 ;
        $method_get = array('tahun_start','tahun_end','sifat_id','jenis_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_in_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['tahun_dw']       = data_year() ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        
        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT MASUK DARI TAHUN '.$postdata['tahun_start'].' - '.$postdata['tahun_end'] ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'tahun' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Tahun' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat','chart'=>'ch_sifat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat','chart'=>'ch_jenis')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Pengirim Surat','chart'=>'ch_user')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_in_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? $column['dw'][$q[$column['id']]] : 'undefined' ;
                    $datachart[$i]['num']   = $q[$column['field']] ;
                    $i++ ;
                }

                $data['graph'][$column['chart']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(    _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statin_year' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statin_month() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statin') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']    = "DATE_FORMAT(S.surat_date,'%M') AS bulan, COUNT(S.surat_id) AS surat_num" ;
        $postdata['group']  = 'MONTH(S.surat_date),YEAR(S.surat_date)' ;
        $postdata['order']  = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['tahun']          = date('Y') ;
        $postdata['month_start']    = date('n') == 1 ? '01' : sprintf("%02d",date('n') - 1) ;
        $postdata['month_end']      = date('m') ;
        $postdata['format_id']      = 2 ;
        $method_get = array('tahun','month_start','month_end','sifat_id','jenis_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_in_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['tahun_dw']       = data_year() ;
        $data['month_dw']       = data_month() ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        
        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT MASUK DARI '.strtoupper($data['month_dw'][$postdata['month_start']]).' - '.strtoupper($data['month_dw'][$postdata['month_end']]).' '.$postdata['tahun'] ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'bulan' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Bulan' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat','chart'=>'ch_sifat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat','chart'=>'ch_jenis')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Pengirim Surat','chart'=>'ch_user')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_in_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? $column['dw'][$q[$column['id']]] : 'undefined' ;
                    $datachart[$i]['num']   = $q[$column['field']] ;
                    $i++ ;
                }

                $data['graph'][$column['chart']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(    _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statin_month' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statin_day() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statin') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']    = "DATE_FORMAT(S.surat_date,'%d/%m') AS hari, COUNT(S.surat_id) AS surat_num" ;
        $postdata['group']  = 'S.surat_date' ;
        $postdata['order']  = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['date_start']    = date('Y-m-d', strtotime( date('Y-m-d')." -1 day" ));
        $postdata['date_end']      = date('Y-m-d') ;
        $postdata['format_id']      = 2 ;
        $method_get = array('date_start','date_end','sifat_id','jenis_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_in_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        
        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT MASUK DARI '.strtoupper(time_to_words($postdata['date_start'])).' - '.strtoupper(time_to_words($postdata['date_end'])) ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'hari' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Tanggal' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat','chart'=>'ch_sifat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat','chart'=>'ch_jenis')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Pengirim Surat','chart'=>'ch_user')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_in_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? $column['dw'][$q[$column['id']]] : 'undefined' ;
                    $datachart[$i]['num']   = $q[$column['field']] ;
                    $i++ ;
                }

                $data['graph'][$column['chart']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                ,    _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statin_day' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statout_tujuan() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statout') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $limit  = 20 ;

        $postdata['tujuan_out'] = TRUE ;
        $postdata['order']      = 'T.jenis_name ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $data['disposisi']= array(  1 => 'Internal KKP'
                                ,   2 => 'Luar KKP' ) ;

        $data['jenis']    = $this->entitas_jenis_model->get($postdata) ;

        $this->load->helper('highcharts') ;
        $data['charts']  = multi_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['jenis']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK '.strtoupper($data['admin_submenu']) ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($postdata['start_date']) && !empty($postdata['to_date']) ? ' Periode '.time_to_words(str_replace('/','-',$postdata['start_date'])).' s.d '.time_to_words(str_replace('/','-',$postdata['to_date'])) : NULL ,
                                                'label_x'   => 'jenis_name' ,
                                                'legend'    => array('Internal KKP','Luar KKP'),
                                                'label_y'   => array('tujuan_out1','tujuan_out2'),
                                                'xAxisName' => NULL ,
                                                'yAxisName' => NULL ,
                                                'series_name'=> NULL ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker2.js' )) ;

        
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statout_tujuan' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statout_year() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statout') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']    = 'YEAR(S.surat_date) AS tahun, COUNT(S.surat_id) AS surat_num' ;
        $postdata['group']  = 'YEAR(S.surat_date)' ;
        $postdata['order']  = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['tahun_start']    = date('Y')-1 ;
        $postdata['tahun_end']      = date('Y') ;
        $method_get = array('tahun_start','tahun_end','sifat_id','jenis_id','ttd_id','klasifikasi_id','olah_id','type_warning','tujuan_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_out_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['ttd_dw']         = $data['olah_dw'] ;
        $data['klasifikasi_dw'] = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['tujuan_dw']      = $this->entitas_tujuan_model->get(array('array'=>TRUE)) ;
        $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;
        $data['tahun_dw']       = data_year() ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        if (!empty($postdata['olah_id']))           $subtitle[] = 'Asal : '.str_replace('.. ','',$data['olah_dw'][$postdata['olah_id']]) ;
        if (!empty($postdata['ttd_id']))            $subtitle[] = 'TTD : '.str_replace('.. ','',$data['ttd_dw'][$postdata['ttd_id']]) ;
        if (!empty($postdata['klasifikasi_id']))    $subtitle[] = 'Klasifikasi : '.str_replace('.. ','',$data['klasifikasi_dw'][$postdata['klasifikasi_id']]) ;
        if (!empty($postdata['tujuan_id']))         $subtitle[] = 'Tujuan : '.$data['tujuan_dw'][$postdata['tujuan_id']] ;
        if (!empty($postdata['type_warning']))      $subtitle[] = 'Jenis Peringatan : '.$data['type_warning'][$postdata['type_warning']] ;

        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT KELUAR DARI TAHUN '.$postdata['tahun_start'].' - '.$postdata['tahun_end'] ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'tahun' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Tahun' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat')
                        ,   array('id'=>'olah_id'       ,'field'=>'olah_num'    ,'dw'=>$data['olah_dw'],'name'=>'Pengolah / Asal Surat')
                        ,   array('id'=>'ttd_id'        ,'field'=>'ttd_num'     ,'dw'=>$data['ttd_dw'],'name'=>'Penandatanganan Surat')
                        ,   array('id'=>'klasifikasi_id','field'=>'klasifikasi_num','dw'=>$data['klasifikasi_dw'],'name'=>'Klasifikasi Surat')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Petugas Entri Surat')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_out_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    if ($i < 10) {
                        $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? str_replace('.. ','',$column['dw'][$q[$column['id']]]) : 'undefined' ;
                        $datachart[$i]['num']   = $q[$column['field']] ;                        
                    }
                    $i++ ;
                }

                $data['graph'][$column['id']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statout_year' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statout_month() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statout') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']= "DATE_FORMAT(S.surat_date,'%M') AS bulan, COUNT(S.surat_id) AS surat_num" ;
        $postdata['group']      = 'MONTH(S.surat_date),YEAR(S.surat_date)' ;
        $postdata['order']      = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['tahun']          = date('Y') ;
        $postdata['month_start']    = date('n') == 1 ? '01' : sprintf("%02d",date('n') - 1) ;
        $postdata['month_end']      = date('m') ;
        $method_get = array('tahun','month_start','month_end','sifat_id','jenis_id','ttd_id','klasifikasi_id','olah_id','type_warning','tujuan_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_out_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['ttd_dw']         = $data['olah_dw'] ;
        $data['klasifikasi_dw'] = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['tujuan_dw']      = $this->entitas_tujuan_model->get(array('array'=>TRUE)) ;
        $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;
        $data['tahun_dw']       = data_year() ;
        $data['month_dw']       = data_month() ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        if (!empty($postdata['olah_id']))           $subtitle[] = 'Asal : '.str_replace('.. ','',$data['olah_dw'][$postdata['olah_id']]) ;
        if (!empty($postdata['ttd_id']))            $subtitle[] = 'TTD : '.str_replace('.. ','',$data['ttd_dw'][$postdata['ttd_id']]) ;
        if (!empty($postdata['klasifikasi_id']))    $subtitle[] = 'Klasifikasi : '.str_replace('.. ','',$data['klasifikasi_dw'][$postdata['klasifikasi_id']]) ;
        if (!empty($postdata['tujuan_id']))         $subtitle[] = 'Tujuan : '.$data['tujuan_dw'][$postdata['tujuan_id']] ;
        if (!empty($postdata['type_warning']))      $subtitle[] = 'Jenis Peringatan : '.$data['type_warning'][$postdata['type_warning']] ;

        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT KELUAR DARI '.strtoupper($data['month_dw'][$postdata['month_start']]).' - '.strtoupper($data['month_dw'][$postdata['month_end']]).' '.$postdata['tahun'] ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'bulan' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Bulan' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat')
                        ,   array('id'=>'olah_id'       ,'field'=>'olah_num'    ,'dw'=>$data['olah_dw'],'name'=>'Pengolah / Asal Surat')
                        ,   array('id'=>'ttd_id'        ,'field'=>'ttd_num'     ,'dw'=>$data['ttd_dw'],'name'=>'Penandatanganan Surat')
                        ,   array('id'=>'klasifikasi_id','field'=>'klasifikasi_num','dw'=>$data['klasifikasi_dw'],'name'=>'Klasifikasi Surat')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Petugas Entri Surat')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_out_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    if ($i < 10) {
                        $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? str_replace('.. ','',$column['dw'][$q[$column['id']]]) : 'undefined' ;
                        $datachart[$i]['num']   = $q[$column['field']] ;                        
                    }
                    $i++ ;
                }

                $data['graph'][$column['id']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(    _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statout_month' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function statout_day() {
        $data   = $this->data ;
        
        $data['admin_submenu']   = $this->lang->line('general_report_statout') ;
        
        $data['title']  = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $postdata['select_self']= "DATE_FORMAT(S.surat_date,'%d/%m') AS hari, COUNT(S.surat_id) AS surat_num" ;
        $postdata['group']      = 'S.surat_date' ;
        $postdata['order']      = 'surat_date ASC' ;
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $postdata['date_start']    = date('Y-m-d', strtotime( date('Y-m-d')." -1 day" ));
        $postdata['date_end']      = date('Y-m-d') ;
        $method_get = array('date_start','date_end','sifat_id','jenis_id','ttd_id','klasifikasi_id','olah_id','type_warning','tujuan_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (!empty($_GET[$mg])) {
                $postdata[$mg]  = urldecode($this->input->get($mg,TRUE)) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['surat']    = $this->surat_out_model->get($postdata) ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['ttd_dw']         = $data['olah_dw'] ;
        $data['klasifikasi_dw'] = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['tujuan_dw']      = $this->entitas_tujuan_model->get(array('array'=>TRUE)) ;
        $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;

        $users = $this->users_model->get() ;
        foreach ($users->result() as $u) {
            $data['user_dw'][$u->user_id]   = $u->fullname ;
        }

        if (!empty($postdata['sifat_id']))          $subtitle[] = 'Sifat : '.$data['sifat_dw'][$postdata['sifat_id']] ;
        if (!empty($postdata['jenis_id']))          $subtitle[] = 'Jenis : '.$data['jenis_dw'][$postdata['jenis_id']] ;
        if (!empty($postdata['olah_id']))           $subtitle[] = 'Asal : '.str_replace('.. ','',$data['olah_dw'][$postdata['olah_id']]) ;
        if (!empty($postdata['ttd_id']))            $subtitle[] = 'TTD : '.str_replace('.. ','',$data['ttd_dw'][$postdata['ttd_id']]) ;
        if (!empty($postdata['klasifikasi_id']))    $subtitle[] = 'Klasifikasi : '.str_replace('.. ','',$data['klasifikasi_dw'][$postdata['klasifikasi_id']]) ;
        if (!empty($postdata['tujuan_id']))         $subtitle[] = 'Tujuan : '.$data['tujuan_dw'][$postdata['tujuan_id']] ;
        if (!empty($postdata['type_warning']))      $subtitle[] = 'Jenis Peringatan : '.$data['type_warning'][$postdata['type_warning']] ;

        $this->load->helper('highcharts') ;
        $data['charts']  = single_chart(array(   
                                                'from_db'   => TRUE ,
                                                'data'      => $data['surat']  ,
                                                'type'      => 'column' ,
                                                'title'     => 'GRAFIK SURAT KELUAR DARI '.strtoupper(time_to_words($postdata['date_start'])).' - '.strtoupper(time_to_words($postdata['date_end'])) ,
                                                'show_title'=> TRUE ,
                                                'subtitle'  => !empty($subtitle) ? implode('. ',$subtitle) : NULL ,
                                                'label_x'   => 'hari' ,
                                                'label_y'   => 'surat_num',
                                                'xAxisName' => 'Tanggal' ,
                                                'yAxisName' => 'Jumlah' ,
                                                'series_name'=> 'Jumlah Surat' ,
                                                'width'     => '100%',
                                                'height'    => '350px' ,
                                                'style'     => 'margin-top:20px;' )) ;
    
        $data['postdata']   = $postdata ;

        $columns    = array(array('id'=>'sifat_id'      ,'field'=>'sifat_num'   ,'dw'=>$data['sifat_dw'],'name'=>'Sifat Surat')
                        ,   array('id'=>'jenis_id'      ,'field'=>'jenis_num'   ,'dw'=>$data['jenis_dw'],'name'=>'Jenis Surat')
                        ,   array('id'=>'olah_id'       ,'field'=>'olah_num'    ,'dw'=>$data['olah_dw'],'name'=>'Pengolah / Asal Surat')
                        ,   array('id'=>'ttd_id'        ,'field'=>'ttd_num'     ,'dw'=>$data['ttd_dw'],'name'=>'Penandatanganan Surat')
                        ,   array('id'=>'klasifikasi_id','field'=>'klasifikasi_num','dw'=>$data['klasifikasi_dw'],'name'=>'Klasifikasi Surat')
                        ,   array('id'=>'user_id'       ,'field'=>'user_num'    ,'dw'=>$data['user_dw'] ,'name'=>'Petugas Entri Surat')) ;
        
        foreach ($columns as $column) {
            $params                 = array() ;
            $params                 = $postdata ;
            $params['order']        = $column['field'].' DESC' ;
            $params['group']        = $column['id'] ;
            $params['select_self']  = $column['id'].', COUNT(S.surat_id) AS '.$column['field'] ;

            $data['tables'][$column['id']]  = $query = $this->surat_out_model->get($params) ;

            if ($query->num_rows() > 0) {
                $i = 0 ;
                foreach ($query->result_array() as $q) {
                    if ($i < 10) {
                        $datachart[$i]['name']  = !empty($column['dw'][$q[$column['id']]]) ? str_replace('.. ','',$column['dw'][$q[$column['id']]]) : 'undefined' ;
                        $datachart[$i]['num']   = $q[$column['field']] ;                        
                    }
                    $i++ ;
                }

                $data['graph'][$column['id']]    = single_chart(array(   
                                                                    'from_db'   => FALSE ,
                                                                    'data'      => $datachart ,
                                                                    'type'      => 'column' ,
                                                                    'show_title'=> TRUE ,
                                                                    'title'     => 'GRAFIK BERDASARKAN '.strtoupper($column['name']) ,
                                                                    'label_x'   => 'name' ,
                                                                    'label_y'   => 'num',
                                                                    'xAxisName' => $column['name'] ,
                                                                    'yAxisName' => 'Jumlah' ,
                                                                    'series_name'=> 'Jumlah Surat' ,
                                                                    'width'     => '100%',
                                                                    'height'    => '350px' ,
                                                                    'style'     => '' )) ;
            }
        }

        $data['columns']    = $columns ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                ,    _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
        $data['js_top'][]       = prep_js(array(    _JAVASCRIPT_  . 'highcharts/highcharts.js'   )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                ,   _JAVASCRIPT_  . 'helper/select2.js' )) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'report_statout_day' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function rekapin($page = 0) {
        $data   = $this->data ;

        $data['admin_submenu']  = $this->lang->line('general_report_rekapin') ;

        $limit  = 25 ;

        /* $_GET FILTERING AND SEARCH */
        $postdata   = array() ;
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array(    'date_start'           
                            ,   'date_end'     
                            ,   'subdis_to'
                            ,   'keyword'
                            ,   'sifat_id'
                            ,   'jenis_id'
                            ,   'export') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['sifat_dw']   = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']   = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
        $data['tujuan_dw']  = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;

        $data['columns']    = array('date_entry'        => 'Tanggal Input'
									,'surat_date'        => 'Tanggal Surat'
                                    ,'surat_number'     => 'Nomor'
                                    ,'surat_perihal'    => 'Perihal'
                                    ,'surat_from'       => 'Asal Surat'
                                    ,'surat_to'         => 'Tujuan'
                                    ,'surat_tembusan'   => 'Tembusan'
                                    ,'aksi'             => 'Keterangan'
                                    ,'surat_disposisi'  => 'Disposisi'
                                    ,'jenis_name'       => 'Jenis Surat'
                                    ,'sifat_name'       => 'Sifat Surat'
                                    ,'file'             => 'File Surat') ;

        $view   = TRUE ;

        $cols   = !empty($_GET['columns']) ? $this->input->get('columns',TRUE) : array_keys($data['columns']) ;

        if (!empty($cols) && is_array($cols)) {
            foreach ($cols as $col) {
                $filtering[]    = 'columns[]='.urlencode($col) ;
            }

            $data['cols']   = $cols ;

            $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '/?1=1' ;

            $postdata['surat_view'] = 'Visible' ;
            $postdata['page']       = $page ;
            $postdata['limit']      = $limit ;
            $postdata['sifat']      = TRUE ;
            $postdata['jenis']      = TRUE ;
            $postdata['order']      = 'S.surat_date DESC' ;
            $data['surat']          = $this->surat_in_model->get($postdata) ;

            $data['eselon_dw']      = $this->disposisi_sub_model->get(array('make_array'=>TRUE)) ;

            $data['page']   = $page ;

            if (!empty($postdata['export']) && $data['surat']->num_rows() > 0) {
                $data['export'] = $postdata['export'] ;
                $view           = FALSE ;

                $data['export_title']   = $export_title = url_title( date('YmdHis') . 'Rekapitulasi Persuratan Surat Masuk Ditjen PSDKP') ;
                $file_view  = 'report_rekap_in' ;

                if (in_array($postdata['export'],array('print','xls'))) {
                    $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data) ;
                }
                else {
                    $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data , true);

                    $this->load->helper('dompdf_helper');
                    pdf_create($html, $export_title, TRUE, 'landscape') ;
                }
            }
            else {
                $this->load->library('pagination') ;
                $config['base_url']     = site_url(array('admin','report','rekapin')) ;
                $config['total_rows']   = $data['total_rows'] = $this->surat_in_model->num($postdata) ;
                $config['per_page']     = $limit ;
                $config['cur_page']     = $page ;

                $config['uri_get']      = $data['uri_get'] ;

                $this->pagination->initialize($config);

                $data['paging']     = $this->pagination->create_links();
            }
        }

        if ($view) {
             $data['css'][]         = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                    ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                    ,   _JAVASCRIPT_  . 'helper/select2.js'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                    ,   _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'report_rekap_in' ) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
    }

    public function rekapout($jenis_id = 3 , $page = 0) {
        $data   = $this->data ;

        $limit = 25 ;

        $data['admin_submenu']  = $this->lang->line('general_report_rekapout') ;

        $jenis = $this->entitas_jenis_model->get(array('jenis_id'=>$jenis_id)) ;
        if (!empty($jenis)) $data['jenis']  = $jenis ;
        else {
            redirect('admin/report/rekapout') ;
        }

        /* $_GET FILTERING AND SEARCH */
        $postdata   = array() ;
        $postdata['jenis_id']   = $jenis_id ;
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array(    'date_start'           
                            ,   'date_end'     
                            ,   'keyword'
                            ,   'ttd_id'
                            ,   'surat_ttd'
                            ,   'olah_id'
                            ,   'klasifikasi_id'
                            ,   'tujuan_disposisi'
                            ,   'tujuan_id'
                            ,   'sifat_id'
                            ,   'type_warning'
                            ,   'export') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['columns']    = array(    'surat_no'          => 'No. Surat'
                                    ,   'sifat_name'        => 'Sifat Surat'
                                    ,   's_date'            => 'Tanggal'
                                    ,   'ttd_name'          => 'Tanda Tangan'
                                    ,   'olah_name'         => 'Pengolah / Asal Surat'
                                    ,   'klasifikasi_name'  => 'Klasifikasi') ;

        if ($jenis_id == 3) { // MEMORANDUM
            $data['columns']['surat_hal']   = 'Perihal' ;
            $data['columns']['dispo_name']  = 'Tujuan' ;
            $data['columns']['surat_ket']   = 'Keterangan' ;
        }
        elseif ($jenis_id == 2) { // NOTA DINAS
            $data['columns']['surat_hal']   = 'Perihal' ;
            $data['columns']['tujuan_name'] = 'Tujuan' ;
            $data['columns']['surat_ket']   = 'Keterangan' ;
        }
        elseif ($jenis_id == 20) { // PLH/PLT
            $data['columns']['surat_dasar']     = 'Perihal' ;
            $data['columns']['tujuan_name']     = 'Tujuan' ;
            $data['columns']['surat_kegiatan']  = 'Kegiatan' ;
            $data['columns']['surat_lokasi']    = 'Lokasi' ;
            $data['columns']['surat_pelaksana'] = 'Pelaksana' ; // JSON
            $data['columns']['surat_plh_as']    = 'PLH Sebagai' ;
            $data['columns']['surat_plh_date']  = 'Tanggal PLH' ; // Date Start
        }
        elseif ($jenis_id == 19) { // PLH/PLT
            $data['columns']['surat_hal']       = 'Perihal' ;
            //$data['columns']['tujuan_name']     = 'Tujuan' ;
            $data['columns']['surat_kegiatan']  = 'Kegiatan' ;
            $data['columns']['berita_acara']    = 'Jenis Berita Acara' ;
            $data['columns']['surat_pelaksana'] = 'Pihak' ; // JSON
        }
        elseif ($jenis_id == 1) { // SKAT
            $data['columns']['surat_kapal']         = 'Nama Kapal' ;
            $data['columns']['surat_kapal_owner']   = 'Perusahaan / Pemilik' ;
            $data['columns']['surat_kapal_address'] = 'Alamat' ;
            $data['columns']['s_kapal_date']        = 'Berlaku Surat' ;
            $data['columns']['surat_ket']           = 'Keterangan' ;
        }
        elseif ($jenis_id == 10) { // SURAT
            $data['columns']['surat_hal']       = 'Perihal' ;
            $data['columns']['tujuan_name']     = 'Tujuan' ;
            $data['columns']['surat_ket']       = 'Keterangan' ;
        }
        elseif ($jenis_id == 22) { // SURAT PERINGATAN
            $data['columns']['surat_kapal']         = 'Nama Kapal' ;
            $data['columns']['surat_kapal_owner']   = 'Perusahaan / Pemilik' ;
            $data['columns']['surat_kapal_address'] = 'Alamat' ;
            $data['columns']['type_warning']        = 'Jenis Surat' ;
            $data['columns']['surat_ket']           = 'Keterangan' ;
        }
        elseif ($jenis_id == 9) { // SURAT TUGAS (ST)
            $data['columns']['surat_timbang']   = 'Pertimbangan' ;
            $data['columns']['surat_dasar']     = 'Dasar' ;
            $data['columns']['surat_pelaksana'] = 'Pelaksana' ; // JSON
            $data['columns']['surat_tujuan']    = 'Tujuan SPT' ;
            $data['columns']['surat_lokasi']    = 'Lokasi SPT' ;
            $data['columns']['surat_spt_date']  = 'Tanggal SPT' ; // Dater Start 
            $data['columns']['surat_biaya']     = 'Pembiayaan' ;
        }
        elseif ($jenis_id == 21) { // UNDANGAN
            $data['columns']['tujuan_name']     = 'Tujuan' ;
            $data['columns']['surat_inv_date']  = 'Waktu Pelaksanaan' ;
            $data['columns']['surat_lokasi']    = 'Tempat Pelaksanaan' ;
            $data['columns']['surat_hal']       = 'Perihal' ; 
            $data['columns']['surat_ket']       = 'Keterangan' ;
        }

        $data['columns']['file']    = 'File Surat' ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['ttd_dw']             = $data['olah_dw'] ;
        $data['tujuan_disposisi']   = $data['olah_dw'] ;
        $data['klasifikasi_dw'] = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE)) ;
        $data['tujuan_dw']      = $this->entitas_tujuan_model->get(array('array'=>TRUE,'array_single'=>TRUE)) ;
        $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;
        $data['berita_acara']   = $this->entitas_model->get_var('berita_acara') ;

        $view   = TRUE ;

        $cols   = !empty($_GET['columns']) ? $this->input->get('columns',TRUE) : array_keys($data['columns']) ;

        $data['column_keys']    = array_keys($data['columns']) ;
        
        if (!empty($cols) && is_array($cols)) {
            foreach ($cols as $col) {
                $filtering[]    = 'columns[]='.urlencode($col) ;
            }

            $data['cols']   = $cols ;

            $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '/?1=1' ;

            $postdata['ttd']           = TRUE ;
            $postdata['olah']          = TRUE ;
            $postdata['klasifikasi']   = TRUE ;
            $postdata['tujuan']        = TRUE ;
            $postdata['jenis']         = TRUE ;
            $postdata['user']          = TRUE ;
            $postdata['sifat']         = TRUE ;
            $postdata['tujuan_dispo']  = TRUE ;
            $postdata['order']         = 'surat_date DESC' ;
            $postdata['limit']         = $limit ;
            $postdata['page']          = $page ;

            $data['page']       = $page ;
            $data['surat']      = $this->surat_out_model->get($postdata) ;

            if (!empty($postdata['export']) && $data['surat']->num_rows() > 0) {
                $data['export'] = $postdata['export'] ;
                $view           = FALSE ;

                $data['export_title']   = $export_title = url_title( date('YmdHis') . 'Rekapitulasi Persuratan Surat Keluar Ditjen PSDKP') ;
                $file_view  = 'report_rekap_out' ;

                if (in_array($postdata['export'],array('print','xls'))) {
                    $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data) ;
                }
                else {
                    $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data , true);

                    $this->load->helper('dompdf_helper');
                    pdf_create($html, $export_title, TRUE, 'landscape') ;
                }
            }
            else {
                $this->load->library('pagination') ;
                $config['base_url']     = site_url(array('admin','report','rekapout',$jenis_id)) ;
                $config['total_rows']   = $data['total_rows'] = $this->surat_out_model->num($postdata) ;
                $config['per_page']     = $limit ;
                $config['cur_page']     = $page ;

                $config['uri_get']      = $data['uri_get'] ;

                $this->pagination->initialize($config);

                $data['paging']     = $this->pagination->create_links();
            }
        }

        if ($view) {
            $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                    ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                    ,   _JAVASCRIPT_  . 'helper/select2.js'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                    ,   _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'report_rekap_out' ) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
    }

    public function rekapoutall($page = 0) {
        $data   = $this->data ;

        $limit = 25 ;

        $data['admin_submenu']  = $this->lang->line('general_report_rekapout') ;

        /* $_GET FILTERING AND SEARCH */
        $postdata   = array() ;
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array(    'date_start'           
                            ,   'date_end'     
                            ,   'keyword'
                            ,   'ttd_id'
                            ,   'surat_ttd'
                            ,   'olah_id'
                            ,   'klasifikasi_id'
                            ,   'tujuan_disposisi'
                            ,   'tujuan_id'
                            ,   'sifat_id'
                            ,   'type_warning'
                            ,   'export') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $data['columns']    = array(    'jenis_name'        => 'Jenis Surat'
                                    ,   'surat_no'          => 'No. Surat'
                                    ,   'sifat_name'        => 'Sifat Surat'
                                    ,   's_date'            => 'Tanggal'
                                    ,   'ttd_name'          => 'Tanda Tangan'
                                    ,   'olah_name'         => 'Pengolah / Asal Surat'
                                    ,   'klasifikasi_name'  => 'Klasifikasi') ;

        $data['columns']['surat_hal']       = 'Perihal' ;
        $data['columns']['dispo_name']      = 'Tujuan' ;
        $data['columns']['surat_ket']       = 'Keterangan' ;
        $data['columns']['tujuan_name']     = 'Tujuan' ;
        $data['columns']['surat_dasar']     = 'Dasar' ;
        $data['columns']['surat_kegiatan']  = 'Kegiatan' ;
        $data['columns']['surat_lokasi']    = 'Lokasi' ;
        $data['columns']['surat_plh_as']    = 'PLH Sebagai' ;
        $data['columns']['surat_plh_date']  = 'Tanggal PLH' ; // Date Start
        $data['columns']['surat_kapal']         = 'Nama Kapal' ;
        $data['columns']['surat_kapal_owner']   = 'Perusahaan / Pemilik' ;
        $data['columns']['surat_kapal_address'] = 'Alamat' ;
        $data['columns']['s_kapal_date']        = 'Berlaku Surat' ;
        $data['columns']['type_warning']        = 'Jenis Peringatan' ;
        $data['columns']['berita_acara']        = 'Jenis Berita Acara' ;
        $data['columns']['surat_timbang']   = 'Pertimbangan' ;
        $data['columns']['surat_pelaksana'] = 'Pelaksana/Pihak' ; // JSON
        $data['columns']['surat_tujuan']    = 'Tujuan SPT' ;
        $data['columns']['surat_spt_date']  = 'Tanggal SPT' ; // Dater Start 
        $data['columns']['surat_biaya']     = 'Pembiayaan' ;
        $data['columns']['surat_inv_date']  = 'Waktu Pelaksanaan' ;
        
        $data['columns']['file']    = 'File Surat' ;

        $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['ttd_dw']             = $data['olah_dw'] ;
        $data['tujuan_disposisi']   = $data['olah_dw'] ;
        $data['klasifikasi_dw'] = $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE,'array_single'=>TRUE)) ;
        $data['tujuan_dw']      = $this->entitas_tujuan_model->get(array('array'=>TRUE)) ;
        $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;
        $data['berita_acara']   = $this->entitas_model->get_var('berita_acara') ;

        $view   = TRUE ;

        $cols   = !empty($_GET['columns']) ? $this->input->get('columns',TRUE) : array_keys($data['columns']) ;

        $data['column_keys']    = array_keys($data['columns']) ;
        
        if (!empty($cols) && is_array($cols)) {
            foreach ($cols as $col) {
                $filtering[]    = 'columns[]='.urlencode($col) ;
            }

            $data['cols']   = $cols ;

            $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '/?1=1' ;

            $postdata['ttd']           = TRUE ;
            $postdata['olah']          = TRUE ;
            $postdata['klasifikasi']   = TRUE ;
            $postdata['tujuan']        = TRUE ;
            $postdata['jenis']         = TRUE ;
            $postdata['user']          = TRUE ;
            $postdata['sifat']         = TRUE ;
            $postdata['tujuan_dispo']  = TRUE ;
            $postdata['order']         = 'surat_id DESC' ;
            $postdata['limit']         = $limit ;
            $postdata['page']          = $page ;

            $data['page']       = $page ;
            $data['surat']      = $this->surat_out_model->get($postdata) ;

            if (!empty($postdata['export']) && $data['surat']->num_rows() > 0) {
                $data['export'] = $postdata['export'] ;
                $view           = FALSE ;

                $data['export_title']   = $export_title = url_title( date('YmdHis') . 'Rekapitulasi Persuratan Surat Keluar Ditjen PSDKP') ;
                $file_view  = 'report_rekap_outall' ;

                if (in_array($postdata['export'],array('print','xls'))) {
                    $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data) ;
                }
                else {
                    $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data , true);

                    $this->load->helper('dompdf_helper');
                    pdf_create($html, $export_title, TRUE, 'landscape') ;
                }
            }
            else {
                $this->load->library('pagination') ;
                $config['base_url']     = site_url(array('admin','report','rekapoutall')) ;
                $config['total_rows']   = $data['total_rows'] = $this->surat_out_model->num($postdata) ;
                $config['per_page']     = $limit ;
                $config['cur_page']     = $page ;

                $config['uri_get']      = $data['uri_get'] ;

                $this->pagination->initialize($config);

                $data['paging']     = $this->pagination->create_links();
            }
        }

        if ($view) {
            $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                    ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                    ,   _JAVASCRIPT_  . 'helper/select2.js'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                    ,   _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'report_rekap_outall' ) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
    }
}

/* End of file report.php */
/* Location: ./application/controllers/report.php */