<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Suratout
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Suratout extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->data['title']        = $this->lang->line('general_surat') ;
        $this->data['adminmenu']    = $this->lang->line('general_surat') ;
    
        $this->load->model(array('entitas_model')) ;
    }
    
    public function index() {
        $this->c() ;
    }
    
    public function c($jenis_id = 3,$surat_id = NULL, $pg = 0) {
        if (in_array($this->data['subdis_eselon'],array(4,5,6)) 
            OR (in_array($this->data['level_id'], array(4)) && !in_array($jenis_id,array(1,22))
            OR (in_array($this->data['level_id'], array(9)) && in_array($jenis_id,array(1,22))) ) ) {
            redirect('admin/home','refresh') ;
        }

        $data   = $this->data ;
        $data['admin_submenu']  = $this->lang->line('general_suratout') ;

        $this->load->model(array('entitas_jenis_model'
                                ,'surat_out_model'
                                ,'surat_outdocument_model'
                                ,'entitas_klasifikasi_model'
                                ,'entitas_tujuan_model'
                                ,'disposisi_sub_model'
                                ,'entitas_sifat_model')) ;

        $jenis = $this->entitas_jenis_model->get(array('jenis_id'=>$jenis_id)) ;
        if (!empty($jenis)) {
            $data['jenis']  = $jenis ;
            $data['admin_submenu']  = $this->lang->line('general_suratout').' - '.$jenis['jenis_name'] ;

            $postdata = array() ;
            $postdata['jenis_id']   = $jenis_id ;

            $page   = 0 ;
            $limit  = 20 ;
            $new    = FALSE ;

            if ($surat_id == 'page') {
                $page       = is_numeric($pg) ? $pg : 0 ;
                $surat_id   = NULL ;
            }
            else if ($surat_id && $surat_id == 'new' && !is_numeric($surat_id)) {
                $new        = TRUE ;
                $surat_id   = NULL ;
            }
            
            if (!empty($_POST['delete_post'])) {
                $sess_security  = $this->input->post('sess_security');
                if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                    $delete_id  = $this->input->post('delete_id') ;
                    if (is_array($delete_id) && count($delete_id) > 0) {
                        $rsdel  = FALSE ;
                        $rsdel  = $this->surat_out_model->delete($delete_id) ;
                        
                        if ($rsdel) {
                            $data['note']   = "Surat keluar terpilih berhasil dihapus." ;
                            $data['symbol'] = 'alert alert-success' ;
                        }
                        else {
                            $data['note']   = 'Proses gagal! Coba lakukan sekali lagi.' ;
                            $data['symbol'] = 'alert alert-danger' ;
                        }
                    }
                    else {
                        $data['note']   = "Surat keluar belum dipilih." ;
                        $data['symbol'] = 'alert alert-danger' ;
                    }    
                }
            }
            
            if (!empty($_GET['delete_doc']) && is_numeric($surat_id)) {
                if ($this->surat_outdocument_model->delete($this->input->get('delete_doc'),$surat_id)) {
                    $data['note']   = "Penghapusan salah satu berkas lampiran berhasil." ;
                    $data['symbol'] = 'alert alert-success' ;
                }
                else {
                    $data['note']   = 'Proses gagal! Coba lakukan sekali lagi.' ;
                    $data['symbol'] = 'alert alert-danger' ;
                }
            }
            
            $post   = FALSE ;
            $rs     = FALSE ;

            if (!empty($_POST['savepost']) ) {
                $sess_security  = $this->input->post('sess_security');
                if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                    if ($this->surat_out_model->validation() == TRUE) {
                        $rs     = $this->surat_out_model->save() ;
                    }
                }

                $post   = TRUE ;
            }

            if ($post) {
                if ($rs) {
                    $data['note']   = 'Penyimpanan surat keluar berhasil dilakukan.' ;
                    $data['symbol'] = 'alert alert-success' ;
                }
                else {
                    $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                    $data['symbol'] = 'alert alert-danger' ;
                }
            }
            
            /* $_GET FILTERING AND SEARCH */
            parse_str($_SERVER['QUERY_STRING'],$_GET);
            $method_get = array('sifat_id'      , 'keyword'         
                                ,'olah_id'      , 'klasifikasi_id'  
                                ,'tujuan_id'    , 'surat_date'   
                                ,'date_start'   , 'date_end'
                                ,'order'        , 'tujuan_disposisi') ;
            $j = 0 ; $filtering[0] = null ;
            foreach($method_get as $mg) {
                if (isset($_GET[$mg])) {
                    $postdata[$mg]  = urldecode($_GET[$mg]) ;
                    $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                    $j++ ;
                }
            }

            if (!in_array($this->data['level_id'],array(1,4)))  $postdata['user_id']    = $this->data['user_id'] ;
            
            
            if (!$surat_id && !$new) {
                $this->load->library('pagination') ;
                $config['base_url']     = site_url(array('admin','suratout','c',$jenis_id,'page')) ;
                $config['total_rows']   = $data['total_rows'] = $this->surat_out_model->num($postdata) ;
                $config['per_page']     = $limit ;
                $config['cur_page']     = $page ;

                $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

                $this->pagination->initialize($config);

                $data['paging']     = $this->pagination->create_links();
                $data['page']       = $page ;

                $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css' )) ;
                $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                        ,   _JAVASCRIPT_  . 'helper/datepicker2.js' )) ;
            }
            else {
                $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                        ,   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css'
                                                        ,   _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
                $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                        ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                        ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                        ,   _JAVASCRIPT_  . 'helper/select2.js'
                                                        ,   _JAVASCRIPT_ . 'helper/surat-document.js'
                                                        ,   _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                        ,   _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            }
            
            $data['surat']    = NULL ;
            
            $data['sifat_dw']       = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
            $data['jenis_dw']       = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;
            $data['olah_dw']        = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE)) ;
            $data['ttd_dw']         = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE,'array_name'=>'subdis_man')) ;
            $data['tujuan_disposisi']= $data['olah_dw'] ;
            $data['klasifikasi_pr'] = $this->entitas_klasifikasi_model->get(array('array'=>TRUE,'is_parent'=>TRUE,'array_id'=>'klasifikasi_kode')) ;
            $data['klasifikasi_dw'] = !in_array($jenis_id,array(1,22)) ? $this->entitas_klasifikasi_model->show_tree(array('make_array'=>TRUE)) : $this->entitas_klasifikasi_model->show_tree(array('array'=>TRUE,'kode_in'=>array('PW.351','PW.352','PW.353','PW.354','PW.355','PW.356') )) ;
            $data['tujuan_dw']      = $this->entitas_tujuan_model->show_tree(array('make_array'=>TRUE)) ;
            $data['type_warning']   = $this->entitas_model->get_var('type_warning') ;
            $data['berita_acara']   = $this->entitas_model->get_var('berita_acara') ;

            if (! $new) {
                if ($surat_id) {
                    $postdata['surat_id']   = $surat_id ;
                    $postdata['user']       = TRUE ;
                    $data['document']       = $this->surat_outdocument_model->get(array('surat_id'=>$surat_id)) ;
                }
                else {
                    $postdata['limit']  = $limit ;
                    $postdata['page']   = $page ;
                    $postdata['order']  = 'surat_no DESC' ;
                }

                $data['surat']   = $surat   = $this->surat_out_model->get($postdata) ;

                if ($data['surat'] == FALSE OR (empty($surat_id) && $data['surat']->num_rows() == 0)) {
                    $data['note']   = "Data yang Anda cari tidak tersedia." ;
                    $data['symbol'] = 'alert alert-danger' ;
                }
            }
            
            $this->session->unset_userdata('sess_security') ;
            $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
            $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

            $view = $this->input->get('view',TRUE) ;
            
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            if ($surat_id && !empty($view) && $data['surat'])   $this->load->view( _TEMPLATE_ADMIN_ . 'surat_out_view'.$jenis_id ) ;
            elseif ($surat_id OR $new)                          $this->load->view( _TEMPLATE_ADMIN_ . 'surat_out_form'.$jenis_id ) ;
            else                                                $this->load->view( _TEMPLATE_ADMIN_ . 'surat_out_list' ) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
        else {
            redirect('admin/suratout') ;
        }

    }
    
   

    public function no() {
        $olah_id    = $this->input->get('olah_id',TRUE) ;
        $klasifikasi_id = $this->input->get('klasifikasi_id',TRUE) ;
        $ttd_id     = $this->input->get('ttd_id',TRUE) ;
        $surat_id   = $this->input->get('surat_id',TRUE) ;
        $date       = $this->input->get('date',TRUE) ;

        if (!empty($olah_id) && !empty($klasifikasi_id) && !empty($ttd_id) && !empty($date)) {
            $this->load->model(array('entitas_klasifikasi_model'
                                    ,'surat_out_model'
                                    ,'disposisi_sub_model')) ;

            $tahun  = substr($date,0,4) ;
            $bulan  = romanic_number(substr($date,5,2)) ;

            $surat_no = $this->db->get('surat__outn')->row()->n + 1 ;

            $this->db->where('id',1)->update('surat__outn',array('n'=>$surat_no)) ;

            $subdis = $this->disposisi_sub_model->get(array('subdis_id'=>$ttd_id)) ;

            if (!empty($subdis['subdis_parent'])) {
                $olah = $this->disposisi_sub_model->get(array('subdis_id'=>$olah_id)) ;

                $A1 = '.'.$olah['subdis_kode'] ;
                        
                if (!empty($olah['subdis_parent']) && $olah['subdis_parent'] != 1) {
                    $olah = $this->disposisi_sub_model->get(array('subdis_id'=>$olah['subdis_parent'])) ;

                    if (!empty($olah['subdis_id'])) {
                        $A1 = '.'.$olah['subdis_kode'].$A1 ;

                        if (!empty($olah['subdis_parent']) && $olah['subdis_parent'] != 1) {
                            $olah = $this->disposisi_sub_model->get(array('subdis_id'=>$olah['subdis_parent'])) ;
                                
                            if (!empty($olah['subdis_id'])) {
                                $A1 = '.'.$olah['subdis_kode'].$A1 ;                                        
                            }
                        }                                
                    }
                }                        

                $klasifikasi = $this->entitas_klasifikasi_model->get(array('klasifikasi_id'=>$klasifikasi_id)) ;
                $klasifikasi_kode = !empty($klasifikasi['klasifikasi_kode']) ? $klasifikasi['klasifikasi_kode'] : '' ; 
				
				

                 //pangkalan
               $query1 = $this->disposisi_sub_model->ambil_subdis_pangkalan();
               foreach($query1->result() as $a){
                 $data_parent_pangkalan[]= $a->subdis_id ;
               }

               for($i=0; $i<count($data_parent_pangkalan); $i++){
                    $que = $this->db->get_where('disposisi__sublist', array('subdis_parent'=> $data_parent_pangkalan[$i]));
                    foreach($que->result() as $a){
                        $data_id_bagian_pangkalan[]= $a->subdis_id;
                    }
                   
               }

               $gabung_pangkalan = array_merge($data_parent_pangkalan,$data_id_bagian_pangkalan);
               
               //stasiun
               $query2 = $this->disposisi_sub_model->ambil_subdis_stasiun();
               foreach($query2->result() as $a){
                 $data_parent_stasiun[]= $a->subdis_id ;
               }

               for($i=0; $i<count($data_parent_stasiun); $i++){
                    $que = $this->db->get_where('disposisi__sublist', array('subdis_parent'=> $data_parent_stasiun[$i]));
                    foreach($que->result() as $a){
                        $data_id_bagian_stasiun[]= $a->subdis_id;
                    }
                   
               }
               $gabung_stasiun = array_merge($data_parent_stasiun,$data_id_bagian_stasiun);
               
               $var=$ttd_id;
               if (in_array($var,$gabung_pangkalan))  {
                $kode_surat= 'LAN';
               }
               elseif (in_array($var,$gabung_stasiun))  {
                $kode_surat= 'STA';
               }
               else{
                $kode_surat='PSDKP';
               }

                echo sprintf("%05d",$surat_no).'/'.$kode_surat.$A1.'/'.$klasifikasi_kode.'/'.$bulan.'/'.$tahun ;
            }
            else {
                echo sprintf("%05d",$surat_no).'/DJPSDKP/'.$bulan.'/'.$tahun ;
            }
        }

        exit(0) ;
    }
}
/* End of file Suratout.php */
/* Location: ./application/controllers/admin/Suratout.php */