<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Setting
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Setting extends BI_Admin {

    public function  __construct() {
        parent::__construct();
        
        $this->data['adminmenu']        = $this->lang->line('general_setting') ;
        $this->data['admin_submenu']    = $this->lang->line('general_setting_language') ;

        $this->lang->load('setting') ;

        if (!in_array($this->data['level_id'], array(1))) {
            redirect('admin/home','refresh') ;
        }
    }

    public function index() {
        return $this->lang() ;
    }

    public function lang($language_id = NULL, $tbl = NULL) {
        $this->load->model('language_model') ;

        $data   = $this->data ;

        $data['page']    = $page   = 0 ;
        $data['limit']   = $limit  = 20 ;

        if ($language_id == 'page') {
            $page        = is_numeric($tbl) ? $tbl : 0 ;
            $language_id = NULL ;
        }
        elseif ($language_id && $tbl && $language_id != 'page') {
            $a_id       = $language_id ;
            $d  = $tbl == $this->language_model->delete($a_id) ;
            if ($d) {
                $data['note']   = $this->lang->line('set_language_del_updated') ;
                $data['symbol'] = 'updated' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = $this->lang->line('set_language_del_error') ;
                $data['symbol'] = 'error' ;
            }

            $category_id    = NULL ;
        }

        $post   = $rs = FALSE ;
        if (isset($_POST['savepost']) OR isset($_POST['config_post'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                if (isset($_POST['savepost'])) {
                    $rs = $this->language_model->save_group() ;
                }
                else {
                    if ($this->config_model->validation()) {
                        $rs = $this->config_model->save() ;
                    }
                }
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = $this->lang->line('set_language_save_updated') ;
                $data['symbol'] = 'updated' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = $this->lang->line('set_language_save_error') ;
                $data['symbol'] = 'error' ;
            }
        }

        /* $_GET FILTERING AND SEARCH */
        $postdata  = array() ;
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('keyword','idiom','langfile') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','setting','lang','page')) ;
        $config['total_rows']   = $data['num'] = $this->language_model->num($postdata) ;
        $config['per_page']     = $postdata['limit']= $limit ;
        $config['cur_page']     = $postdata['page'] = $page ;
        $config['uri_get']      = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

        $this->pagination->initialize($config);

        $data['paging']     = $this->pagination->create_links();
        $data['page']       = $page ;

        $data['idiom_dw']   = $this->language_model->get(array('array'=>true,'group'=>'idiom')) ;
        $data['langfile_dw']= $this->language_model->get(array('array'=>true,'group'=>'langfile')) ;
        
        $data['language']   = $this->language_model->get($postdata) ;

        $data['postdata']   = $postdata ;

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'setting_language') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function stopwords() {
        $data   = $this->data ;

        $post   = $rs = FALSE ;
        
        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $rs = $this->config_model->save() ;
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = $this->lang->line('set_stopwords_save_updated') ;
                $data['symbol'] = 'updated' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = $this->lang->line('set_stopwords_save_error') ;
                $data['symbol'] = 'error' ;
            }
        }

        $data['admin_submenu']    = $this->lang->line('general_setting_stopwords') ;

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'setting_stopwords') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }
}

/* End of file setting.php */
/* Location: ./application/controllers/admin/setting.php */
