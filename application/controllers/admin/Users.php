<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Users
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com
 */
class Users extends BI_Admin {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('users_model','eselon_satu_model','disposisi_sub_model')) ;
        $this->lang->load('users') ;

        $this->data['adminmenu']        = $this->lang->line('general_users') ;
        $this->data['admin_submenu']    = $this->lang->line('general_users') ;
    }

    public function index($user_id = NULL, $del = NULL) {
        return $this->c($user_id, $del) ;
    }

    public function c($user_id = NULL, $del = NULL) {
        if (!in_array($this->data['level_id'], array(1))) {
            redirect('admin/home','refresh') ;
        }
        
        $data	= $this->data ;

        $postdata = array() ;

        $new    = FALSE ;
        $page   = 0 ;
        $limit  = 20 ;

        if ($user_id == 'page') {
            $page       = is_numeric($del) ? $del : 0 ;
            $user_id = NULL ;
        }
        elseif ($user_id && $del && $user_id != 'page') {
            $h_id       = $user_id ;
            $user_id = NULL ;

            if ($this->users_model->delete($h_id)) {
                $data['note']   = $this->lang->line('user_del_updated') ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = $this->lang->line('user_del_error') ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        else if ($user_id && $user_id == 'new' && !is_numeric($user_id)) {
            $new   = TRUE ;
        }

        $post   = FALSE ;
        $rs     = FALSE ;

        $data['email_error']    = '' ;
        $data['username_error'] = '' ;
        $data['password_error'] = '' ;

        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                if ($this->users_model->validation() == TRUE) {
                    $result = $this->users_model->save() ;
                    
                    $rs     = $result['rs'] ;
                    $data['email_error']    = $result['email_error'] ;
                    $data['username_error'] = $result['username_error'] ;
                    $data['password_error'] = $result['password_error'] ;
                }
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = $this->lang->line('user_save_updated') ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = $this->lang->line('user_save_error') ;
                $data['symbol'] = 'alert alert-danger' ;

                if (!empty($_POST['user_id'])) {
                    $user_id   = $this->input->post('user_id',TRUE) ;
                }
                else {
                    $new       = TRUE ;
                }
            }
        }

        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('status','keyword','level_id','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        /* ==================== */

        if (!$user_id && !$new) {
            $this->load->library('pagination') ;
            $config['base_url']     = site_url(array('admin','users','c','page')) ;
            $config['total_rows']   = $this->users_model->num($postdata) ;
            $config['per_page']     = $limit ;
            $config['cur_page']     = $page ;

            $config['uri_get']      = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

            $this->pagination->initialize($config);

            $data['paging']     = $this->pagination->create_links();
            $data['page']       = $page ;
        }
        else {
            $data['css'][]          = prep_css(array(   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    _JAVASCRIPT_ . 'restrict_keyboard.js'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                    ,   _JAVASCRIPT_ . 'helper/select2.js' )) ;
        }

        $data['user']   = NULL ;
        $data['method'] = 'new' ;

        $data['level_dw']   = $this->users_model->level_dw(FALSE) ;
        $data['status_dw']  = $this->users_model->status_dw() ;
        $data['esatu_dw']   = $this->eselon_satu_model->get(array('array'=>TRUE)) ;
        $data['subdis_dw']  = $this->disposisi_sub_model->show_tree(array('make_array'=>TRUE)) ;

        if (! $new) {
            if ($user_id) {
                $postdata['user_id']= $user_id ;
            }
            else {
                $postdata['limit']      = $limit ;
                $postdata['page']       = $page ;
                $postdata['level']      = TRUE ;
                
                $data['order_dw']       = $this->users_model->order_dw() ;
            }

            $data['method'] = '' ;
            $data['user']   = $this->users_model->get($postdata) ;

        }

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        if ($user_id || $new)   $this->load->view( _TEMPLATE_ADMIN_ . 'users_form') ;
        else                    $this->load->view( _TEMPLATE_ADMIN_ . 'users') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function profile() {
        $data	= $this->data ;
        $this->lang->load('profile') ;

        $data['adminmenu']  = $this->lang->line('general_profile') ;

        $postdata = array() ;
        $postdata['user_id']    = $data['user_id'] ;

        $post   = FALSE ;
        $rs     = FALSE ;

        $data['email_error']    = '' ;
        $data['username_error'] = '' ;
        $data['password_error'] = '' ;
        $userID_error           = '' ;

        if (isset($_POST['savepost'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                if ($this->users_model->is_self($data['user_id']) == TRUE) {
                    if ($this->users_model->validation() == TRUE) {

                        $result = $this->users_model->save() ;

                        $rs     = $result['rs'] ;
                        $data['email_error']    = $result['email_error'] ;
                        $data['username_error'] = $result['username_error'] ;
                        $data['password_error'] = $result['password_error'] ;
                    }                    
                }
                else {
                    $userID_error   = $this->lang->line('profile_uid_error') ;
                }
            }

            $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $data['note']   = $this->lang->line('profile_save_updated') ;
                $data['symbol'] = 'alert alert-success' ;

                $this->db->cache_delete_all() ;
            }
            else {
                $data['note']   = sprintf($this->lang->line('profile_save_error'),$userID_error) ;
                $data['symbol'] = 'alert alert-danger' ;

                if (!empty($_POST['user_id'])) {
                    $user_id   = $this->input->post('user_id',TRUE) ;
                }
            }
        }

        $data['level_dw']   = $this->users_model->level_dw() ;
        $data['status_dw']  = $this->users_model->status_dw() ;

        $data['user']   = $this->users_model->get($postdata) ;
        
        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'users_profile') ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
    }

    public function logs($log_id = NULL, $del = NULL) {
        if (!in_array($this->data['level_id'], array(1))) {
            redirect('admin/home','refresh') ;
        }
        
        $this->load->model('users_logs_model') ;
        
        $data	= $this->data ;

        $data['admin_submenu']  = $this->lang->line('general_users_logs') ;

        $postdata = array() ;

        $new    = FALSE ;
        $page   = 0 ;
        $limit  = 10 ;

        if ($log_id == 'page') {
            $page       = is_numeric($del) ? $del : 0 ;
            $log_id     = NULL ;
        }

        $postdata['log_id']   = $log_id ;

        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('date','keyword','level_id','order','browser','ip','platform','mobile') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }

        /* ==================== */
        
        if ($log_id == NULL) {
            $this->load->library('pagination') ;
            $config['base_url']     = site_url(array('admin','users','logs','page')) ;
            $config['total_rows']   = $this->users_logs_model->num($postdata) ;
            $config['per_page']     = $limit ;
            $config['cur_page']     = $page ;

            $config['uri_get']      = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

            $this->pagination->initialize($config);

            $data['paging']     = $this->pagination->create_links();
            $data['page']       = $page ;
        }

        $data['level_dw']   = $this->users_model->level_dw() ;
        $data['order_dw']   = $this->users_logs_model->order_dw() ;
        $data['mobile_dw']  = $this->users_logs_model->agent_dw('mobile') ;
        $data['ip_dw']      = $this->users_logs_model->agent_dw('ip','IP') ;
        $data['platform_dw']= $this->users_logs_model->agent_dw('platform') ;
        $data['browser_dw'] = $this->users_logs_model->agent_dw('browser') ;

        $postdata['limit']      = $limit ;
        $postdata['page']       = $page ;
        $postdata['level']      = TRUE ;
        $postdata['user']       = TRUE ;
        $postdata['long_time']  = TRUE ;
        $postdata['distinct_id']= $this->session->userdata('log_id') ;
        
        $data['logs']   = $this->users_logs_model->get($postdata) ;

        if ($log_id)    $this->load->view( _TEMPLATE_ADMIN_ . 'users_logs_view',$data) ;
        else {
            $data['css'][]          = prep_css(array( _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
            $data['js_bottom'][]    = prep_js(array(  _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                    , _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'users_logs') ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;
        }
    }
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */
