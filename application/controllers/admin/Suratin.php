<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Suratin
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Suratin extends BI_Admin {
    
    public function __construct() {
        parent::__construct();
        
        $this->data['title']        = $this->lang->line('general_surat') ;
        $this->data['adminmenu']    = $this->lang->line('general_surat') ;
    
        $this->load->model(array(   'surat_in_model'        , 'entitas_aksi_model',
                                    'disposisi_model'       , 'surat_users_model' ,
                                    'disposisi_sub_model'   , 'surat_indocument_model' ,
                                    'entitas_jenis_model'   , 'entitas_sifat_model' ,
                                    'entitas_tujuan_model'  , 'entitas_model' ,
                                    'users_model'           , 'surat_inaksi_model',
                                    'surat_indisposisi_model',
                                    'surat_into_model'   )) ;

        if (in_array($this->data['level_id'], array(4))) {
            redirect('admin/suratout/c/1') ;
        }
    }
    
    public function index() {
        $this->c() ;
    }
    
	// public function tambah_disposisi(){
		// $id = $this->uri->segment(4);
		// $this->surat_in_model->ubah_status($id);
		
		// $this->db->delete('surat__indisposisi', array('surat_id' => $id)); 
		//$this->db->delete('surat__into', array('surat_id' => $id)); 
		// apc_clean();
		// redirect(site_url().'admin/suratin/c/'.$id);
	// }
	

    public function c($surat_id = NULL , $pg = 0) {
        $data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_suratin') ;
        $data['title']          = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $page   = 0 ;
        $limit  = 20 ;
        $new    = FALSE ;

        if ($surat_id == 'page') {
            $page       = is_numeric($pg) ? $pg : 0 ;
            $surat_id   = NULL ;
        }
        else if ($surat_id && $surat_id == 'new' && !is_numeric($surat_id)) {
            $new        = TRUE ;
            $surat_id   = NULL ;
        }
        
        if (!empty($_POST['delete_post'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $delete_id  = $this->input->post('delete_id') ;
                if (is_array($delete_id) && count($delete_id) > 0) {
                    $rsdel  = FALSE ;
                    foreach ($delete_id as $did) {
                        $rsdel  = $this->surat_in_model->delete($data['user_id'],$did) ;
                    }       

                    if ($rsdel) {
                        $data['note']   = "Revisi disposisi ".$data['admin_submenu']." terpilih berhasil dihapus." ;
                        $data['symbol'] = 'alert alert-success' ;
                    }
                    else {
                        $data['note']   = 'Proses gagal! Coba lakukan sekali lagi.' ;
                        $data['symbol'] = 'alert alert-danger' ;
                    }
                }
                else {
                    $data['note']   = "Revisi disposisi ".$data['admin_submenu']." belum dipilih." ;
                    $data['symbol'] = 'alert alert-danger' ;
                }    
            }
        }
        
        if (!empty($_GET['delete_doc']) && is_numeric($surat_id)) {
            if ($this->surat_indocument_model->delete($this->input->get('delete_doc'),$surat_id)) {
                $data['note']   = "Penghapusan salah satu berkas lampiran berhasil." ;
                $data['symbol'] = 'alert alert-success' ;
            }
            else {
                $data['note']   = 'Proses gagal! Coba lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        $post   = FALSE ;
        $rs     = FALSE ;

        if (!empty($_POST['surat_view']) OR !empty($_POST['agenda_post']) OR !empty($_POST['saveinto'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                if (!empty($_POST['saveinto'])) {
                    if ($this->surat_into_model->validation() == TRUE) {
                        $rs = $this->surat_into_model->save() ;
                    }
                }
                else {
                    if ($this->surat_in_model->validation() == TRUE) {
                        $rs     = $this->surat_in_model->save() ;
                        
                        if (!empty($_POST['agenda_post']) OR $this->input->post('jenis_id',TRUE) == 21 && !empty($_POST['surat_disposisi'])) {
                            $agenda_id  = $this->surat_in_model->migration_to_agenda($rs,$_POST) ;
                            if ($agenda_id) {
                                $data['note']   = 'Proses replikasi surat undangan ke agenda kegiatan berhasil dilakukan. Silakan edit data <a href="'.site_url('admin/agenda/kegiatan/'.$agenda_id).'">agenda undangan di sini</a>.' ;
                                $data['symbol'] = 'alert alert-success' ;
                                
                                $data['reply_id']   = NULL ;

                                $surat_id   = NULL ;
                            }
                            else {
                                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                                $data['symbol'] = 'alert alert-danger' ;

                                $surat_id   = $this->input->post('surat_id',TRUE) ;
                                $surat_id   = !empty($surat_id) ? $surat_id : 'new' ;
                            }

                            $post   = FALSE ;
                        }
                        
                    }
                }
                
            }

            if (!empty($_POST['surat_view']))   $post   = TRUE ;
        }

        if ($post) {
            if ($rs) {
                $surat_view = $this->input->post('surat_view',TRUE) ;
                $data['note']   = $surat_view == 'Save As Draft' ? 'Penyimpanan '.$data['admin_submenu'].' berhasil dilakukan.' : $data['admin_submenu'].' telah terkirim.' ;
                $data['symbol'] = 'alert alert-success' ;

                $data['reply_id']   = NULL ;
                
                $surat_id   = NULL ;
            }
            else {
                $data['note']   = 'Proses gagal. Periksa field yang kosong dan lakukan sekali lagi.' ;
                $data['symbol'] = 'alert alert-danger' ;
            }
        }
        
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('sifat_id','keyword','jenis_id','start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $data['rs'] = $rs ;
        
        $postdata['aksi']       = TRUE ;

        if (in_array($this->data['level_id'],array(9)))   {
            $postdata['subdis_id']   = $this->data['subdis_id'] ;
            $postdata['subdis_to']   = $this->data['subdis_id'] ;
            $postdata['subdis_from'] = $this->data['subdis_id'] ;
        }
        
        $data['aksi_post']      = array() ;
        $data['disposisi_post'] = array() ;
        $data['level_post']     = array() ;
        $data['respon_post']    = array() ;
		
        
        if (!$surat_id && !$new) {
            $this->load->library('pagination') ;
            $config['base_url']     = site_url(array('admin','suratin','c','page')) ;
            $config['total_rows']   = $data['total_rows'] = $this->surat_in_model->num($postdata) ;
            $config['per_page']     = $limit ;
            $config['cur_page']     = $page ;

            $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

            $this->pagination->initialize($config);

            $data['paging']     = $this->pagination->create_links();
            $data['page']       = $page ;
            
            $data['aksi']       = $this->entitas_aksi_model->get() ;

            $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                    ,   _STYLES_ . 'font-awesome/css/font-awesome.min.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                    ,   _JAVASCRIPT_  . 'helper/datepicker.js' )) ;

            $data['eselon_dw']  = $this->disposisi_sub_model->get(array(  'make_array'=> TRUE )) ;     
        }
        else {
            $data['level']      = $this->users_model->get_level(array('level_in'=>array(5,6,7,8))) ;
            
            $data['disposisi']  = $this->disposisi_model->get() ;
            $data['aksi']       = $this->entitas_aksi_model->get() ;
            
            $users  = $this->users_model->get(array('distinct_id'=> $data['user_id'] ,
                                                    'status'     => 'Active' )) ;
        
            $data['subdis']     = $this->disposisi_sub_model->get($postdata) ;
            
            $users_arr  = array() ;
            if ($users->num_rows() > 0) {
                foreach ($users->result() as $u) {
                    $users_arr[]    = $u->fullname ;
                }
            }
            $data['users']  = $users_arr ;

            $data['surat_fdw']  = $this->entitas_tujuan_model->show_tree(array(  'make_array'     => TRUE
                                                                        ,   'array_id'  => 'sdname'
                                                                        ,   'array_name'=> 'tujuan_name'
                                                                        ,   'array_single'  => TRUE )) ;

            $data['suratdr_dw'] = $this->disposisi_sub_model->show_tree(array(  'make_array'=> TRUE
                                                                            ,   'array_id'  => 'sdname'
                                                                            ,   'array_name'=> 'subdis_name'
                                                                            ,   'n3'        => FALSE
                                                                            ,   'n2'        => FALSE
                                                                            ,   'array_single'=> TRUE )) ;

            $data['suratto_dw'] = $this->disposisi_sub_model->show_tree(array(  'make_array'=> TRUE
                                                                            ,   'array_name'=> 'subdis_man' )) ;            

            $data['timezone']   = $this->entitas_model->get_var('timezone') ;
            $data['from_status']= $this->entitas_model->get_var('from_status') ;

            $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/css/select2.min.css'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/css/prettyPhoto.css' )) ;
            $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                    ,   _JAVASCRIPT_  . 'helper/datepicker2.js'
                                                    ,   _JAVASCRIPT_ . 'select2/dist/js/select2.min.js'
                                                    ,   _JAVASCRIPT_  . 'helper/select2.js'
                                                    ,   _JAVASCRIPT_ . 'prettyPhoto/jquery.prettyPhoto.js'
                                                    ,   _JAVASCRIPT_ . 'helper/prettyPhoto.js' )) ;

            //$surat_disposisi = $this->surat_indisposisi_model->get(array('surat_id'=>$surat_id,'subdis'=>TRUE,'order'=>'subdis_order')) ;
        }
        
        $data['surat']    = NULL ;
        
        $data['sifat_dw']   = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
		$data['sifat_color']   = $this->entitas_sifat_model->get_color(array('array'=>TRUE)) ;
        $data['jenis_dw']   = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;

        $load_view = TRUE ; 

        $view  = $this->input->get('view',TRUE) ;
        $export= $this->input->get('export',TRUE) ;
        
        if (! $new) {
            if ($surat_id) {
                $postdata['surat_id']   = $surat_id ;
                $postdata['reply']      = TRUE ;
                $postdata['agenda']     = TRUE ;

                if (!empty($export)) {
                    $postdata['into']           = TRUE ;
                    $postdata['indisposisi']    = TRUE ;
                }
                
                $data['document']       = $this->surat_indocument_model->get(array('surat_id'=>$surat_id)) ;

                if (!empty($view)) {
                    $postdata['indisposisi']    = TRUE ;
                    $postdata['into']           = TRUE ;

                    if (!empty($export)) {
                        $data['aksi_post']      = $this->entitas_aksi_model->get_from_surat($surat_id,TRUE) ;                        
                    }
                }
                else {
                    $postdata['into']       = TRUE ;
                    $data['aksi_post']      = $this->entitas_aksi_model->get_from_surat($surat_id,TRUE) ;
                    $data['subdis']         = $this->disposisi_sub_model->get($postdata) ;
                }
                
            }
            else {
                $postdata['limit']  = $limit ;
                $postdata['page']   = $page ;
                $postdata['format'] = TRUE ;
                $postdata['jenis']  = TRUE ;
                $postdata['into']   = TRUE ;
                $postdata['indisposisi']    = TRUE ;
                $postdata['order']  = 'surat_date_taken DESC' ;
                $postdata['group']  = 'S.surat_id' ;
            }

            $data['surat']   = $surat   = $this->surat_in_model->get($postdata) ;

            if ($surat_id && !empty($surat)) {
                $data['title']  = $surat['surat_perihal'].' - '.$data['title'] ;

                if ($surat['surat_view'] == 'Visible')   $view = 1 ;

                if (!empty($view)) {
                    $this->surat_indisposisi_model->set_read($surat_id,$this->data['subdis_id']) ;

                    $data['eselon_dw']  = $this->disposisi_sub_model->get(array(  'make_array'=> TRUE )) ;  

                    $data['into']           = $this->surat_into_model->get(array('surat_id'=>$surat_id,'order'=>'into_id ASC')) ;
                    //$data['aksi']           = $this->surat_inaksi_model->get(array('surat_id'=>$surat_id,'aksi'=>TRUE)) ; 
                    $data['subdis_child']   = $this->disposisi_sub_model->get(array('subdis_parent'=>$this->data['subdis_id'])) ; 
					
					//fungsi cetak export

                    if (!empty($export)) {
                        $data['es_view']= $this->surat_in_model->find_eselon($surat['subdis_from']) ;
                        $es_view        = $data['es_view'] > 1 ? 2 : 1 ;
                        $data['es_view']= romanic_number($data['es_view']) ;

                        if ($es_view == 1) {
                            $subdis_eselon1 = $this->disposisi_sub_model->get(array('subdis_eselon'=>2)) ;
                            if ($subdis_eselon1->num_rows() > 0) {
                                foreach ($subdis_eselon1->result() as $se) {
                                    $se1[] = $se->subdis_eselon ;
                                }
                            }
                        } 

                        $data['subdis_eselon1'] = !empty($se1) ? $se1 : array() ;

                        $data['export'] = $export ;
                        $load_view      = FALSE ;

                        $data['export_title']   = $export_title = url_title( 'SURAT MASUK :'.$surat['surat_perihal'].' - '.$data['title'] ) ;
                        
						//$file_view  = 'surat_in_view'.$es_view ;
					                      
						$file_view  = 'surat_in_view'.$this->session->userdata('subdis_eselon')  ;

                        if (in_array($export,array('print','xls'))) {
                            $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data) ;
                        }
                        else {
                            $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view , $data , true);
                            $this->load->helper('dompdf_helper');
                            pdf_create($html, $export_title, TRUE, 'portrait') ;
                        }
                    }
                }
            }    
        }
        else {
            $data['title']  = 'FORM '.$data['admin_submenu'] ;
        }
        
        if ($load_view) {
            $this->session->unset_userdata('sess_security') ;
            $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
            $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

            $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
            if (!empty($view) && $surat_id) $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_view' ) ;
            elseif ($surat_id OR $new)      $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_form' ) ;
            else                            $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_list' ) ;
            $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;            
        }
    }

    public function inbox($page = 0) {
        $data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_suratin_inbox') ;
        $data['title']          = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $limit  = 20 ;
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('sifat_id','keyword','jenis_id','start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $postdata['aksi']       = TRUE ;

        $postdata['subdis_to']   = $this->data['subdis_id'] ;
        
        $data['aksi_post']      = array() ;
        $data['disposisi_post'] = array() ;
        $data['level_post']     = array() ;
        $data['respon_post']    = array() ;
        
        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','suratin','inbox')) ;
        $config['total_rows']   = $data['total_rows'] = $this->surat_in_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

        $this->pagination->initialize($config);

        $data['paging']     = $this->pagination->create_links();
        $data['page']       = $page ;
            
        $data['aksi']       = $this->entitas_aksi_model->get() ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                ,   _STYLES_ . 'font-awesome/css/font-awesome.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker.js' )) ;

        $data['eselon_dw']  = $this->disposisi_sub_model->get(array(  'make_array'=> TRUE )) ;      
        $data['sifat_dw']   = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']   = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;

        $postdata['limit']  = $limit ;
        $postdata['page']   = $page ;
        $postdata['format'] = TRUE ;
        $postdata['jenis']  = TRUE ;
        $postdata['into']   = TRUE ;
        $postdata['indisposisi']    = TRUE ;
        $postdata['order']  = 'surat_date_taken DESC' ;
        $postdata['group']  = 'S.surat_id' ;
        
        $data['surat']   = $surat   = $this->surat_in_model->get($postdata) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_inbox' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;            
    }

    public function draft($page = 0) {
        $data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_suratin_draft') ;
        $data['title']          = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $limit  = 20 ;
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('sifat_id','keyword','jenis_id','start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $postdata['aksi']       = TRUE ;

        $postdata['subdis_id']   = $this->data['subdis_id'] ;
        $postdata['surat_view']  = 'Hidden' ;
        
        $data['aksi_post']      = array() ;
        $data['disposisi_post'] = array() ;
        $data['level_post']     = array() ;
        $data['respon_post']    = array() ;
		
        
        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','suratin','draft')) ;
        $config['total_rows']   = $data['total_rows'] = $this->surat_in_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

        $this->pagination->initialize($config);

        $data['paging']     = $this->pagination->create_links();
        $data['page']       = $page ;
            
        $data['aksi']       = $this->entitas_aksi_model->get() ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                ,   _STYLES_ . 'font-awesome/css/font-awesome.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker.js' )) ;

        $data['eselon_dw']  = $this->disposisi_sub_model->get(array(  'make_array'=> TRUE )) ;      
        $data['sifat_dw']   = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
        $data['jenis_dw']   = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;

        $postdata['limit']  = $limit ;
        $postdata['page']   = $page ;
        $postdata['format'] = TRUE ;
        $postdata['jenis']  = TRUE ;
        $postdata['into']   = TRUE ;
        $postdata['indisposisi']    = TRUE ;
        $postdata['order']  = 'surat_date_taken DESC' ;
        $postdata['group']  = 'S.surat_id' ;
        
        $data['surat']   = $surat   = $this->surat_in_model->get($postdata) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_draft' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;            
    }

    public function sent($page = 0) {
        $data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_suratin_sent') ;
        $data['title']          = $data['admin_submenu'].' - '.$data['title'] ;
        
        $postdata = array() ;

        $limit  = 20 ;
        /* $_GET FILTERING AND SEARCH */
        parse_str($_SERVER['QUERY_STRING'],$_GET);
        $method_get = array('sifat_id','keyword','jenis_id','start_date','to_date','order') ;
        $j = 0 ; $filtering[0] = null ;
        foreach($method_get as $mg) {
            if (isset($_GET[$mg])) {
                $postdata[$mg]  = urldecode($_GET[$mg]) ;
                $filtering[$j]  = $mg.'=' . urlencode($postdata[$mg]) ;
                $j++ ;
            }
        }
        
        $postdata['aksi']       = TRUE ;

        $postdata['subdis_id']   = $this->data['subdis_id'] ;
        $postdata['surat_view']  = 'Visible' ;
        
        $data['aksi_post']      = array() ;
        $data['disposisi_post'] = array() ;
        $data['level_post']     = array() ;
        $data['respon_post']    = array() ;
        
        $this->load->library('pagination') ;
        $config['base_url']     = site_url(array('admin','suratin','sent')) ;
        $config['total_rows']   = $data['total_rows'] = $this->surat_in_model->num($postdata) ;
        $config['per_page']     = $limit ;
        $config['cur_page']     = $page ;

        $config['uri_get']      = $data['uri_get'] = !empty($filtering[0]) ? '/?' . implode('&amp;', $filtering) : '' ;

        $this->pagination->initialize($config);

        $data['paging']     = $this->pagination->create_links();
        $data['page']       = $page ;
            
        $data['aksi']       = $this->entitas_aksi_model->get() ;

        $data['css'][]          = prep_css(array(   '//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'
                                                ,   _STYLES_ . 'font-awesome/css/font-awesome.min.css' )) ;
        $data['js_bottom'][]    = prep_js(array(    '//code.jquery.com/ui/1.10.4/jquery-ui.js'
                                                ,   _JAVASCRIPT_  . 'helper/datepicker.js' )) ;

        $data['eselon_dw']  = $this->disposisi_sub_model->get(array(  'make_array'=> TRUE )) ;      
        $data['sifat_dw']   = $this->entitas_sifat_model->get(array('array'=>TRUE)) ;
       
        $data['jenis_dw']   = $this->entitas_jenis_model->get(array('array'=>TRUE)) ;

        $postdata['limit']  = $limit ;
        $postdata['page']   = $page ;
        $postdata['format'] = TRUE ;
        $postdata['jenis']  = TRUE ;
        $postdata['into']   = TRUE ;
        $postdata['indisposisi']    = TRUE ;
        $postdata['order']  = 'surat_date_taken DESC' ;
        $postdata['group']  = 'S.surat_id' ;
        
        $data['surat']   = $surat   = $this->surat_in_model->get($postdata) ;

        $this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'surat_in_sent' ) ;
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;            
    }

    public function aksi() {
        $this->surat_inaksi_model->set_done() ;
    }

    public function addaksi() {
        $this->surat_inaksi_model->add() ;
    }
	
	public function form_ubah(){
		$id=$_POST["id"];
		echo form_open_multipart('admin/suratin/ubah_file');
		echo '
		
			<div class="form-group row">
				<div class="col-md-9">
					<input type="hidden" name="id_doc" readonly value="'.$id.'"/>
					<input name="file_suratin" class="form-control" type="file" required/>
				</div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-primary form-control">
					<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;Upload
					</button>
				</div>
			</div>
		</form>';
		
	}
	
	public function ubah_file(){
		$id_doc=$this->input->post("id_doc");
		//cek id_surat dan nama file doc 
		$query = $this->surat_indocument_model->cek_document($id_doc);
		foreach($query->result() as $row) {
			$id_surat=$row->surat_id;
			$document_filename=$row->document_filename;
		}
				
		$this->load->helper(array('form', 'url'));
		$config['upload_path']='./upload/files/';
		$config['allowed_types'] = '*';
		$this->load->library('upload',$config);
		
		if ( $this->upload->do_upload('file_suratin'))
                {
                    $data=$this->upload->data();                  
                    $file_name = url_title(strtolower($data['file_name']));
					$ex = substr(strrchr($file_name, '.'), 1) ;
					$size=ceil($data['file_size'] *1024);
					$type=$data['file_type'];
					$tanggal = date("Y-m-j H:i:s");
					$data_doc = array(
						'document_filename' => $file_name,
						'document_filesize' => $size,
						'document_filetype' => $type,
						'document_fileext' => $ext,
						'document_lastupdate' => $tanggal
					);
					
					$this->surat_indocument_model->update_document($data_doc,$id_doc);
					unlink('./upload/files/'.$document_filename); // hapus file lama
					$this->session->set_flashdata('alert_doc', '<div class="alert alert-success ">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
					</button><i class="fa fa-check-circle"></i> Lampiran Berhasil Diubah !
					</div>');
					redirect('admin/suratin/c/'.$id_surat);
                }
				else{
					echo $this->upload->display_errors();
					$this->session->set_flashdata('alert_doc', '<div class="alert alert-danger ">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
					</button><i class="fa fa-exclamation-triangle"></i> Lampiran Gagal Diubah !
					</div>');
					redirect('admin/suratin/c/'.$id_surat);
				}			
	}
	
	
	public function clean(){
		// $id = $this->uri->segment(4);
		// $this->surat_in_model->ubah_status($id);
		apc_clean();
		// $this->c();
	}
	
	public function tambah_disposisi(){
		$data   = $this->data ;
        
        $data['admin_submenu']  = $this->lang->line('general_suratin') ;
        $data['title']          = $data['admin_submenu'].' - '.$data['title'] ;
		
		$postdata = array() ;
		
		$data['disposisi']  = $this->disposisi_model->get() ;
		$data['cek_subdis']  = $this->db->get_where('disposisi__sublist', array('subdis_id' => $this->session->userdata('subdis_id')));
        $data['aksi']       = $this->entitas_aksi_model->get() ;
		$data['subdis']         = $this->disposisi_sub_model->get($postdata) ;
		$data['aksi_post']      = array() ;
        $data['disposisi_post'] = array() ;
		  $data['respon_post']    = array() ;
		$this->load->view( _TEMPLATE_ADMIN_ . 'general/header',$data) ;
		$this->load->view( _TEMPLATE_ADMIN_ . 'disposisi') ;
        
        $this->load->view( _TEMPLATE_ADMIN_ . 'general/footer') ;         
	}
	
	public function isi_dispos(){
		$data= array(
			'surat_id' => $this->input->post('surat_id'),
			'subdis_id'=> $this->session->userdata('subdis_id'),
			'into_msg' => $this->input->post('into_msg'),
			'status_into' => 1
		);
		$this->disposisi_model->tambah_into($data);
		
		$into_id=$this->db->insert_id();
		$data1= array(
			'surat_id' => $this->input->post('surat_id'),
			'into_id'=> $into_id,
			'subdis_id'=> $this->session->userdata('subdis_id'),
			'read_status'=> 1,
			'indisposisi_status'=>2
		);
		$this->disposisi_model->tambah_pendispos($data1);
		
		$data_to=$this->input->post('surat_disposisi');
		$panjang= count($data_to);
		for($x = 0; $x < $panjang; $x++) {
			$data2= array(
				'surat_id' => $this->input->post('surat_id'),
				'into_id'=> $into_id,
				'subdis_id'=> $data_to[$x],
				'read_status'=> 1,
				'indisposisi_status'=>1
			);
			$this->disposisi_model->tambah_dispos($data2);
		}
		$aksi_to=$this->input->post('aksi_id[]');
		$panjang1=count($aksi_to);
		for($y = 0; $y < $panjang1; $y++) {
			$data3= array(
				'surat_id' => $this->input->post('surat_id'),
				'subdis_id'=> 1,
				'aksi_status'=>1,
				'aksi_id'=> $aksi_to[$y]				
			);
			$this->disposisi_model->tambah_aksi($data3);
		}
		
		$pjb=$this->input->post('surat_respon[]');
		$panjang2=count($pjb);
		for($z = 0; $z < $panjang2; $z++) {
			$data4= array(
				'respon_disposisi'=>2
			);
			$this->disposisi_model->ubah_dispos($data4,$pjb[$z],$into_id);
		}
		
		$cekin = $this->db->get_where('surat__into', array('surat_id' => $this->input->post('surat_id')), 1);
		
		foreach($cekin->result() as $a){
			$id_into = $a->into_id;
			$this->db->delete('surat__into', array('into_id' => $id_into));
			$this->db->delete('surat__indisposisi', array('into_id' => $id_into));
		}
		apc_clean();
		redirect('admin/suratin/');
	}
	
	public function tes(){
		$cek_level = $this->db->get_where('surat__in', array('surat_id' => 507)); 
			foreach($cek_level->result() as $lv){
				print_r($level=$lv->surat_to);
			}
									
			$array = json_decode($level);
			
			$this->db->where_in('subdis_id', $array);
			
			$query  = $this->db->get('disposisi__sublist') ;
			foreach($query->result() as $k){
				echo $k->subdis_name .' - ';
			}
		}
	
	
}
/* End of file Suratin.php */
/* Location: ./application/controllers/admin/Suratin.php */