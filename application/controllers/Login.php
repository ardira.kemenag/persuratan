<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of login
 *
 * @author Freddy Yuswanto
 *          email  : freddy.august@gmail.com
 *          web    : http://www.kohaci.com/
 */
class Login extends BI_Controller {

    public $data ;

    public function  __construct() {
        parent::__construct();
        $this->load->model('users_model') ;
        $this->lang->load('login') ;

        $this->data['title']        = $this->lang->line('login_title') ;

        $this->data['user_id']      = $this->session->userdata('user_id') ;
        $this->data['username']     = $this->session->userdata('username') ;
        $this->data['fullname']     = $this->session->userdata('fullname') ;
        $this->data['level_id']     = $this->session->userdata('level_id') ;
        $this->data['esatu_id']     = $this->session->userdata('esatu_id') ;
        $this->data['subdis_id']    = $this->session->userdata('subdis_id') ;
        $this->data['subdis_parent']= $this->session->userdata('subdis_parent') ;
        $this->data['subdis_eselon']= $this->session->userdata('subdis_eselon') ;

        if (!$this->data['user_id'] && !$this->data['username'] && !$this->data['level_id']) {
            // LET EMPTY
        }
        else {
            if (in_array($this->data['level_id'], array(1,3,4,9))) {
                redirect('admin/home','refresh') ;
            }
            else {
                redirect('login/form/1','refresh') ;
            }
        }

        $this->data['css'][]    = prep_css(array(   _BOOTSTRAP_     . 'css/bootstrap.min.css'
                                                ,   _STYLES_LOGIN_  . 'styles.css' )) ;

        $this->data['js_bottom'][] = prep_js(array( _JAVASCRIPT_ . 'jquery-1.11.0.min.js'
                                                ,   _BOOTSTRAP_  . 'js/bootstrap.min.js' )) ;
    }

    public function index($note="") {
        $this->form($note) ;
    }

    public function form($note="") {
        $data = $this->data ;

        if ($note==1)       { $data['note'] = $this->lang->line('login_note_text_wrong')        ; $data['symbol'] = "error" ;}
        else if ($note==2)  { $data['note'] = $this->lang->line('login_note_logout')            ; $data['symbol'] = "notice" ; }
        else if ($note==3)  { $data['note'] = $this->lang->line('login_note_registered_confirm'); $data['symbol'] = "notice" ; }
        else if ($note==4)  { $data['note'] = $this->lang->line('login_note_registered_failed') ; $data['symbol'] = "error" ; }
        else if ($note==5)  { $data['note'] = $this->lang->line('login_note_validation_confirm'); $data['symbol'] = "notice" ; }
        
        if (!empty($_POST['login_post'])) {
            $sess_security  = $this->input->post('sess_security');
            if (md5($sess_security.'+!)@') == $this->session->userdata('sess_security')) {
                $this->_process() ;
            }
            else {
                $data['note']   = $this->lang->line('login_note_cookies_wrong') ;
                $data['symbol'] = 'error' ;
            }
        }

        $this->session->unset_userdata('sess_security') ;
        $data['sess_security']   = $sess_decrypt = random_string('alnum',10) ;
        $this->session->set_userdata(array('sess_security'=>md5($sess_decrypt.'+!)@'))) ;

        $this->load->view( _TEMPLATE_LOGIN_   . 'home',$data) ;
    }

    public function _process() {
        $data = $this->users_model->process() ;
        if ($data != FALSE) {
            $this->load->model('users_logs_model') ;
            $this->session->set_userdata(array(
                                            'user_id'       => $data['user_id'] ,
                                            'username'      => $data['username'] ,
                                            'fullname'      => $data['fullname'] ,
                                            'level_id'      => $data['level_id'] ,
                                            'esatu_id'      => $data['esatu_id'] ,
                                            'level_name'    => $data['level_name'] ,
                                            'subdis_id'     => $data['subdis_id'] ,
                                            'subdis_eselon' => $data['subdis_eselon'] ,
                                            'log_id'        => $this->users_logs_model->insert_id($data['user_id']),
                                            'log_pages'     => ''
                                    )) ;

            if (in_array($data['level_id'],array(1,3,4,9))) {
                redirect('/admin','refresh') ;
            }

        }
        else {
            redirect('login/form/1','refresh') ;
        }
    }

}

/* End of file login.php */
/* Lodolpinion: ./applidolpinion/controllers/login.php */