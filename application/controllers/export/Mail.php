<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Mail
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Mail extends BI_Export {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model(array(   'surat_model'       , 'surat_aksi_model',
                                    'disposisi_sub_model' , 'surat_sifat_model' ,
                                    'disposisi_model'   , 'surat_users_model' )) ;
        
        $this->load->helper('html') ;
        
        $this->output->enable_profiler(FALSE) ;
    }
    
    public function surat($format_id = 1,$surat_id = NULL , $type = 'pdf') {
        $data   = $this->data ;
        
        $postdata = array() ;

        if (in_array($data['level_id'],array(7,3))) {  }
        elseif (!empty($data['user_id'])) $postdata['to_user_id'] = $data['user_id'] ;
        
        $postdata['format_id']  = $data['format_id'] = $format_id ;
        $postdata['have_view']  = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['respon']     = TRUE ;
        $postdata['format']     = TRUE ;
        $postdata['order']      = 'S.surat_date_taken DESC, S.surat_date DESC, S.surat_id DESC' ;
        
        $data['surat']    = NULL ;
        
        $postdata['surat_id']   = $surat_id ;
        $postdata['disposisi']  = TRUE ;
        $postdata['sifat']      = TRUE ;
        $postdata['jenis']      = TRUE ;
        $postdata['to_user']    = TRUE ;
        $postdata['to_level']   = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['reply']      = TRUE ;
        $data['document']       = $this->surat_document_model->get(array('surat_id'=>$surat_id)) ;
        $data['disposisi']      = $this->disposisi_model->get() ;
        
        $data['surat']  = $surat = $this->surat_model->get($postdata) ;
        $data['type']   = $type ;
        
        $file_view      = 'mail_surat' ;
        
        if ($surat) {
            $data['title']  = $title = '['.$surat['format_name'].'] '.$surat['surat_perihal'] ;
            $data['export_title']   = $export_title = url_title($title) ;
            
            if (in_array($type,array('print','xls'))) {
                $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data) ;
            }
            else {
                $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data,true);

                $this->load->helper('dompdf_helper');
                pdf_create($html, $export_title, TRUE) ;
            }
        }
        else {
            redirect('surat','refresh') ;            
        }
    }
    
    public function sentitems($format_id = 2, $surat_id = NULL, $type = 'pdf') {
        $data   = $this->data ;
        
        $postdata = array() ;

        if ($data['level_id'] == 3) {   }
        elseif (!empty($data['user_id'])) $postdata['user_id'] = $data['user_id'] ;
        
        $postdata['format_id']  = $data['format_id'] = $format_id ;
        $postdata['have_view']  = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['respon']     = TRUE ;
        $postdata['format']     = TRUE ;
        
        $data['surat']    = NULL ;
        
        $postdata['surat_id']   = $surat_id ;
        $postdata['disposisi']  = TRUE ;
        $postdata['sifat']      = TRUE ;
        $postdata['jenis']      = TRUE ;
        $postdata['to_user']    = TRUE ;
        $postdata['to_level']   = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['reply']      = TRUE ;
        $data['document']       = $this->surat_document_model->get(array('surat_id'=>$surat_id)) ;
        $data['disposisi']      = $this->disposisi_model->get() ;

        $data['surat']   = $surat   = $this->surat_model->get($postdata) ;
        $data['type']   = $type ;
        
        $file_view      = 'mail_sentitems' ;
        
        if ($surat) {
            $data['title']  = $title = '['.$surat['format_name'].'] '.$surat['surat_perihal'] ;
            $data['export_title']   = $export_title = url_title($title) ;
            
            if (in_array($type,array('print','xls'))) {
                $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data) ;
            }
            else {
                $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data,true);

                $this->load->helper('dompdf_helper');
                pdf_create($html, $export_title, TRUE) ;
            }
        }
        else {
            redirect('sentitems','refresh') ;            
        }
    }

    public function disposisi($format_id = 1,$surat_id = NULL , $type = 'pdf') {
        $data   = $this->data ;
        
        $postdata = array() ;

        //if ($data['level_id'] == 3) {  }
        //elseif (!empty($data['user_id'])) $postdata['to_user_id'] = $data['user_id'] ;
        
        $postdata['format_id']  = $data['format_id'] = $format_id ;
        //$postdata['have_view']  = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['respon']     = TRUE ;
        $postdata['format']     = TRUE ;
        $postdata['order']      = 'S.surat_date_taken DESC, S.surat_date DESC, S.surat_id DESC' ;
        
        $data['surat']    = NULL ;
        
        $postdata['surat_id']   = $surat_id ;
        $postdata['disposisi']  = TRUE ;
        $postdata['sifat']      = TRUE ;
        $postdata['jenis']      = TRUE ;
        $postdata['to_user']    = TRUE ;
        $postdata['to_level']   = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['reply']      = TRUE ;
        $data['document']       = $this->surat_document_model->get(array('surat_id'=>$surat_id)) ;
        $data['disposisi']      = $this->disposisi_model->get() ;
        
        $data['surat']  = $surat = $this->surat_model->get($postdata) ;
        $data['type']   = $type ;
        
        $data['subdis'] = $this->disposisi_sub_model->get(array('subdis_parent'=>0)) ;
        $data['aksi']   = $this->surat_aksi_model->get() ;  
        $data['sifat']  = $this->surat_sifat_model->get() ;
        
        $data['aksi_post']      = $this->surat_aksi_model->get_from_surat($surat_id,TRUE) ;
        $data['disposisi_post'] = $this->disposisi_sub_model->get_from_surat($surat_id,TRUE) ;
        $data['sifat_post']     = array($surat['sifat_id']) ;    
        
        $file_view      = 'mail_disposisi' ;
        
        $data['title']  = $title = 'LEMBAR DISPOSISI' ;
        $data['export_title']   = $export_title = url_title($title) ;

        $data['print'] = $print = !empty($_POST['printpost']) ? TRUE : FALSE ; 
        
        if ($print) {
            $data['aksi_post']      = !empty($_POST['aksi_id'])     && is_array($_POST['aksi_id'])   ? $this->input->post('aksi_id',TRUE)   : $data['aksi_post'] ;
            $data['disposisi_post'] = !empty($_POST['subdis_id'])   && is_array($_POST['subdis_id']) ? $this->input->post('subdis_id',TRUE) : $data['disposisi_post'] ;
            $data['sifat_post']     = !empty($_POST['sifat_id'])    && is_array($_POST['sifat_id'])  ? $this->input->post('sifat_id',TRUE)  : $data['sifat_post'] ;
        }
        
        //if ($print == FALSE) {
            $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data) ;
        /*}
        else {
            $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data,true);

            $this->load->helper('dompdf_helper');
            pdf_create($html, $export_title, TRUE) ;
        }*/
    }

    public function rekapitulasi($type = 'pdf') {
        $data   = $this->data ;
        
        $postdata = array() ;

        $data['inbox']      = 'surat/c/' ;
        $data['sentitems']  = 'sentitems/c/' ;
        if (!empty($data['user_id'])) {
            if ($data['level_id'] == 3) {
                //$postdata['user_id']    = $data['user_id'] ;
                $data['inbox']          = 'sentitems/c/' ;
                $data['sentitems']      = 'surat/c/' ;
                $postdata['format_id']  = 2 ;
            }
            else {
                if ($data['level_id'] != 5) $postdata['to_user_id'] = $data['user_id'] ;
                else {
                    $data['inbox']          = 'dashboard/mail/' ;
                    $data['sentitems']      = 'dashboard/mail/' ;
                }
            }
        }
        
        $postdata['have_view']  = TRUE ;
        $postdata['aksi']       = TRUE ;
        $postdata['respon']     = TRUE ;
        $postdata['format']     = TRUE ;
        $postdata['format_id']  = 2 ;
        $postdata['order']      = 'S.surat_date_taken DESC, S.surat_date DESC, S.surat_id DESC' ;
        
        $data['surat']  = NULL ;
        
        $data['surat']  = $surat = $this->surat_model->get($postdata) ;
        
        $data['aksi']   = $this->surat_aksi_model->get() ;
        
        $file_view      = 'mail_rekapitulasi' ;
        
        $data['type']           = $type ;
        $data['title']          = $title = 'REKAPITULASI SURAT MASUK' ;
        $data['export_title']   = $export_title = url_title($title) ;

        if (in_array($type,array('print','xls'))) {
            $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data) ;
        }
        else {
            $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data,true);

            $this->load->helper('dompdf_helper');
            pdf_create($html, $export_title, TRUE) ;
        }
    }
}
/* End of file mail.php */
/* Location: ./application/controllers/export/mail.php */