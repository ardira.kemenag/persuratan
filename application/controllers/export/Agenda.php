<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Agenda
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Agenda extends BI_Export {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model(array(   'surat_model'       , 'surat_aksi_model',
                                    'disposisi_sub_model' ,
                                    'disposisi_model'   , 'surat_users_model' )) ;
        
        $this->load->helper('html') ;
        
        $this->data['title']    = 'Agenda Kegiatan' ;
        $this->data['adminmenu']= 'Agenda Kegiatan' ;
        
        $this->load->model(array('agenda_model','agenda_status_model')) ;
    }
    
    public function detail($kegiatan_id,$type = 'pdf') {
        $data   = $this->data ;
        
        $postdata['kegiatan_id']    = $kegiatan_id ;
        $postdata['status']         = TRUE ;
        $data['kegiatan']           = $kegiatan = $this->agenda_model->get($postdata) ;
        
        $data['type']       = $type ;
        
        $file_view      = 'agenda_detail' ;
        
        if ($kegiatan) {
            $data['title']  = $title = '[AGENDA_KEGIATAN] '.$kegiatan['kegiatan_name'] ;
            $data['export_title']   = $export_title = url_title($title) ;
            
            if (in_array($type,array('print','xls'))) {
                $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data) ;
            }
            else {
                $html = $this->load->view(_TEMPLATE_EXPORT_ . $file_view,$data,true);

                $this->load->helper('dompdf_helper');
                pdf_create($html, $export_title, TRUE) ;
            }
        }
        else {
            redirect('agenda','refresh') ;            
        }
    }
}
/* End of file agenda.php */
/* Location: ./application/controllers/export/agenda.php */