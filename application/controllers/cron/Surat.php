<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Surat
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Surat extends BI_Controller {

    public function  __construct() {
        parent::__construct() ;
    }

    public function out() {
        $query  = $this->db->query("SELECT surat_id , ttd_id FROM surat__out WHERE user_id NOT IN (SELECT user_id FROM users__list)") ;

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $a) {
                $users = $this->db->where('subdis_id',$a->ttd_id)->select('user_id')->get('users__list') ;
                if ($users->num_rows() > 0) {
                    $user_id = $users->row()->user_id ;

                    $this->db->where('surat_id',$a->surat_id) ;
                    $this->db->update('surat__out',array('user_id'=>$user_id)) ;
                }
            }
        }
    }

    public function in() {
        $query  = $this->db->query("SELECT surat_id , subdis_id FROM surat__in WHERE user_id NOT IN (SELECT user_id FROM users__list)") ;

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $a) {
                $users = $this->db->where('subdis_id',$a->subdis_id)->select('user_id')->get('users__list') ;
                if ($users->num_rows() > 0) {
                    $user_id = $users->row()->user_id ;

                    $this->db->where('surat_id',$a->surat_id) ;
                    $this->db->update('surat__in',array('user_id'=>$user_id)) ;
                }
            }
        }
    }
}

/* End of file email.php */
/* Location: ./application/controllers/email.php */