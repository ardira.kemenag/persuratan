<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Email
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Email extends BI_Controller {

    public function  __construct() {
        parent::__construct() ;
        
        $this->load->model(array('email_log_model'
                                ,'entitas_model')) ;

        $this->load->library('email');
    }

    public function send() {
        $emails = $this->email_log_model->get(array('is_sent'=>0)) ;

        if ($emails->num_rows() > 0) {
            $entitas = $this->entitas_model->get() ;

            foreach ($emails->result() as $e) {

                $data   = json_decode($e->message,TRUE) ;

                $data['subject']    = $e->subject ;

                if ($entitas->num_rows() > 0) {
                    foreach ($entitas->result() as $ent) {
                        $data[$ent->entitas_name.'_dw']   = $this->entitas_model->get_var($ent->entitas_name) ;
                    }
                }

                $html = $this->load->view( _TEMPLATE_EMAIL_ . 'general/header.php' , $data , TRUE ) ;
                $html .= $this->load->view( _TEMPLATE_EMAIL_ . $e->template , NULL , TRUE) ;
                $html .= $this->load->view( _TEMPLATE_EMAIL_ . 'general/footer.php' , NULL , TRUE ) ;

                //die($html) ;

                $this->email->from( _EMAIL_ADMIN_ , 'Biro SDM Polda Metro Jaya -- no reply');
                $this->email->to($e->email);

                $this->email->subject($e->subject);
                $this->email->message($html) ;

                if ($this->email->send()) {
                    $this->email_log_model->update($e->id) ;
                }
            }
        }
    }
}

/* End of file email.php */
/* Location: ./application/controllers/email.php */