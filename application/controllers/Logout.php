<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of logout
 *
 * @author Freddy Yuswanto
 *          email  : freddy.august@gmail.com
 *          web    : http://www.kohaci.com/
 */
class Logout extends BI_Controller {

    public $data ;

    public function  __construct() {
        parent::__construct();
    }

    public function index() {
        $this->session->sess_destroy() ;
        redirect('login','refresh') ;
    }
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */