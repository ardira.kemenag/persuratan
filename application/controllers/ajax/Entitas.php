<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Entitas
 *
 * @author  Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class Entitas extends BI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function klasifikasi() {
        $postdata['keycode']    = $this->input->get('keycode',TRUE) ;
        if (!empty($postdata['keycode'])) {
            $this->load->model(array('entitas_klasifikasi_model')) ;

            $postdata['make_array']     = TRUE ;
            $postdata['is_not_parent']  = TRUE ;
            $klasifikasi = $this->entitas_klasifikasi_model->show_tree($postdata) ;

            if (count($klasifikasi) > 0) {
                foreach ($klasifikasi as $key => $val) {
                    $html[] = '<option value="'.$key.'">'.$val.'</option>' ;                    
                }

                echo implode('',$html) ;
            }
        }

        exit(0) ;
    }

}
/* End of file Entitas.php */
/* Location: ./application/controllers/ajax/Entitas.php */