<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  @author Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class BI_Export extends BI_Controller {

    public function  __construct() {
        parent::__construct();

        $this->output->enable_profiler(FALSE) ;
    }
}

/* End of file BI_Export.php */
/* Location: ./application/core/BI_Export.php */