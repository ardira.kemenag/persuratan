<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BASEIGNITER
 *
 * An open source application development framework for PHP 5.0 or newer
 *
 * @package		BASEIGNITER
 * @author		Freddy Yuswanto
 * @copyright           Copyright (c) 2010 - 2011, EllisLab, Inc.
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application BI_Portal Class
 *
 *
 * @package	BASEIGNITER
 * @subpackage	Core
 * @category	Core
 * @author	Freddy Yuswanto
 * @link	http://www.kohaci.com/
 */
class BI_Portal extends BI_Controller {

    public function __construct() {
        parent::__construct() ;
        
    }
}

// END BI_Portal class

/* End of file BI_Portal.php */
/* Location: ./application/core/BI_Portal.php */