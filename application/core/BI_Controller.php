<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  @author Freddy Yuswanto
 *          email   : freddy.august@gmail.com
 *          web     : http://www.kohaci.com/
 */
class BI_Controller extends CI_Controller {

    public $data ;

    public $table_config = 'set__config' ;

    public function  __construct() {
        parent::__construct();

        /*$this->load->library('user_agent');
        if ($this->uri->segment(1) != 'mobile' && $this->agent->is_mobile()) {
            header("Refresh:0;url=".site_url('mobile' . $this->uri->uri_string()) .  '/?' . $_SERVER['QUERY_STRING'] ) ;
            exit(0) ;
        } */
        
        $configs    = $this->config_model->get() ;
        if ($configs->num_rows() > 0) {
            foreach ($configs->result_array() as $conf) {
                $this->data['bi_'.$conf['config_name']]  = $conf['config_style'] ;
            }
        }

        $this->config->set_item('language', $this->data['bi_language']);

        $segment    = $this->uri->segment(1) ;
        $langfile   = in_array($segment,array('erim')) ? $segment : 'general' ; 

        $this->lang->load( $langfile ) ;

        // $this->output->set_profiler_sections(array('query_toggle_count'=>100));
        // $this->output->enable_profiler(ENVIRONMENT == 'development') ;
    }
}

/* CAT (DEFAULT) */
include_once 'BI_Export.php';
include_once 'BI_Admin.php' ;
include_once 'BI_Portal.php';
include_once 'BI_Mobile.php';

/* End of file BI_Controller.php */
/* Location: ./application/core/BI_Controller.php */