<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Language Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Language
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/language.html
 */
class BI_Lang extends CI_Lang {

    public $table  = 'set__language' ;

    /**
     * Constructor
     *
     * @access	public
     */
    public function __construct() {
        parent::__construct() ;
    }

    // --------------------------------------------------------------------

    /**
     * Load a language file
     *
     * @access	public
     * @param	mixed	the name of the language file to be loaded. Can be an array
     * @param	string	the language (english, etc.)
     * @return	mixed
     */
    function load($langfile = '', $idiom = 'english', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
    {
            $langfile_no_ext = $langfile ;
            $langfile = str_replace('.php', '', $langfile);

            if ($add_suffix == TRUE)
            {
                    $langfile = str_replace('_lang.', '', $langfile).'_lang';
            }

            $langfile .= '.php';

            if (in_array($langfile, $this->is_loaded, TRUE))
            {
                    return;
            }

            if ($idiom == '')
            {
                    $config =& get_config();
                    $deft_lang = ( ! isset($config['language'])) ? 'english' : $config['language'];
                    $idiom = ($deft_lang == '') ? 'english' : $deft_lang;
            }

            // Determine where the language file is and load it from Database
            $CI = & get_instance() ;
            $CI->load->database() ;
            $CI->db->where('idiom',$idiom) ;
            $CI->db->where('langfile',  $langfile_no_ext) ;

            $langfiles_query    = $CI->db->get($this->table) ;

            if ($langfiles_query->num_rows() > 0) {
                foreach ($langfiles_query->result() as $lg) {
                    $lang[$lg->language_key] = $lg->language_line ;
                }
            }

            // Determine where the language file is and load it from Text File
            elseif ($alt_path != '' && file_exists($alt_path.'language/'.$idiom.'/'.$langfile))
            {
                    include($alt_path.'language/'.$idiom.'/'.$langfile);
            }
            else
            {
                    $found = FALSE;

                    foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
                    {
                            if (file_exists($package_path.'language/'.$idiom.'/'.$langfile))
                            {
                                    include($package_path.'language/'.$idiom.'/'.$langfile);
                                    $found = TRUE;
                                    break;
                            }
                    }

                    if ($found !== TRUE)
                    {
                            show_error('Unable to load the requested language file: language/'.$idiom.'/'.$langfile);
                    }
            }


            if ( ! isset($lang))
            {
                    log_message('error', 'Language file contains no data: language/'.$idiom.'/'.$langfile);
                    return;
            }

            if ($return == TRUE)
            {
                    return $lang;
            }

            $this->is_loaded[] = $langfile;
            $this->language = array_merge($this->language, $lang);
            unset($lang);

            log_message('debug', 'Language file loaded: language/'.$idiom.'/'.$langfile);
            return TRUE;
    }

}
// END Language Class

/* End of file BI_Lang.php */
/* Location: ./application/core/BI_Lang.php */