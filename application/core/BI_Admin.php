<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BASEIGNITER
 *
 * An open source application development framework for PHP 5.0 or newer
 *
 * @package		BASEIGNITER
 * @author		Freddy Yuswanto
 * @copyright           Copyright (c) 2010 - 2011, EllisLab, Inc.
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application MY_Admin Class
 *
 *
 * @package	BASEIGNITER
 * @subpackage	Core
 * @category	Core
 * @author	Freddy Yuswanto
 * @link	http://www.kohaci.com/
 */
class BI_Admin extends BI_Controller {

    public function __construct() {
        parent::__construct() ;
        
        $this->data['note']         = '' ;
        $this->data['symbol']       = '' ;
        
        $this->data['user_id']      = $this->session->userdata('user_id') ;
        $this->data['username']     = $this->session->userdata('username') ;
        $this->data['fullname']     = $this->session->userdata('fullname') ;
        $this->data['level_id']     = $this->session->userdata('level_id') ;
        $this->data['level_name']   = $this->session->userdata('level_name') ;
        $this->data['esatu_id']     = $this->session->userdata('esatu_id') ;
        $this->data['subdis_id']    = $this->session->userdata('subdis_id') ;
        $this->data['subdis_eselon']= $this->session->userdata('subdis_eselon') ;

        if (in_array($this->data['level_id'],array(1,3,4,9))) {
            if (!$this->data['user_id'] && !$this->data['username'])
            { redirect('/login/form','refresh') ; exit(0) ; }

            $this->load->model(array('entitas_jenis_model'
                                    ,'surat_in_model'
                                    ,'surat_indisposisi_model')) ;

            $this->data['_inbox_num'] = !empty($this->data['subdis_id']) ? $this->surat_indisposisi_model->num(array('read_status'=>1,'subdis_id'=>$this->data['subdis_id'])) : 0 ;
            $this->data['_draft_num'] = !empty($this->data['subdis_id']) ? $this->surat_in_model->num(array('surat_view'=>'Hidden','subdis_id'=>$this->data['subdis_id'])) : 0 ;

            $this->data['outjenis'] = $this->entitas_jenis_model->get(array('jid'=>in_array($this->data['level_id'], array(4)) ? array(1,22) : NULL 
                                                                        ,   'jnot'=>in_array($this->data['level_id'], array(9)) ? array(1,22) : NULL  )) ;
        }
        else {
            redirect('/login/form','refresh') ; exit(0) ;
        }

        $this->data['css'][]    = prep_css(array(   _BOOTSTRAP_     . 'css/bootstrap.min.css'
                                                ,   _STYLES_ADMIN_  . 'styles.css' )) ; ;

        $this->data['js_top'][] = prep_js(array(    'jquery-1.11.0.min.js' )
                                                , _JAVASCRIPT_ ) ;

        $this->data['js_bottom'][] = prep_js(array( _BOOTSTRAP_  . 'js/bootstrap.min.js' 
                                                ,   _JAVASCRIPT_ . 'helper/default.js')) ;

        //$this->db->cache_on() ;
    }
}

// END BI_Admin class

/* End of file BI_Admin.php */
/* Location: ./application/core/BI_Admin.php */