<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BASEIGNITER
 *
 * An open source application development framework for PHP 5.0 or newer
 *
 * @package		BASEIGNITER
 * @author		Freddy Yuswanto
 * @copyright           Copyright (c) 2010 - 2011, EllisLab, Inc.
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application BI_Mobile Class
 *
 *
 * @package	BASEIGNITER
 * @subpackage	Core
 * @category	Core
 * @author	Freddy Yuswanto
 * @link	http://www.kohaci.com/
 */
class BI_Mobile extends BI_Controller {

    public function __construct() {
        parent::__construct() ;

        $this->data['title']    = 'Beranda' ;

        $this->db->cache_on() ;
    }
}

// END BI_Mobile class

/* End of file BI_Mobile.php */
/* Location: ./application/core/BI_Mobile.php */