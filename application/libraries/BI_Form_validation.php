<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright           Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * BI_Form_validation Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Form_validation
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 */
class BI_Form_validation extends CI_Form_validation {

    /**
     * Constructor
     *
     * @access	public
     */
    public function __construct() {
        parent::__construct() ;
    }

    public function matches_username($username) {
        if (preg_match('/^[a-z\d_.]{4,20}$/i', $username))  return TRUE ;

        return FALSE ;
    }

    public function matches_pattern($str, $pattern) {
        $characters = array(
            '[', ']', '\\', '^', '$',
            '.', '|', '+', '(', ')',
            '#', '?', '~'            // Our additional characters
        );

        $regex_characters = array(
            '\[', '\]', '\\\\', '\^', '\$',
            '\.', '\|', '\+', '\(', '\)',
            '[0-9]', '[a-zA-Z]', '.' // Our additional characters
        );

        $pattern = str_replace($characters, $regex_characters, $pattern);

        if (preg_match('/^' . $pattern . '$/', $str)) return TRUE;

        return FALSE;
  }

}
// END BI_Form_validation Class

/* End of file BI_Form_validation.php */
/* Location: ./application/libraries/BI_Form_validation.php */